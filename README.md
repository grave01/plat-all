##### 小程序展示
### 原创UI设计可复制抄袭不受限制
![输入图片说明](https://images.gitee.com/uploads/images/2020/0929/173602_0778c6e9_1139094.jpeg "微信图片_20200929173200.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0929/173747_5b9ceb60_1139094.png "微信截图_20200929173717.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0929/173506_2146cf34_1139094.png "1601371888(1).png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0929/173520_b356ecd1_1139094.png "微信截图_20200929173045.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0929/173530_713ef32a_1139094.png "微信截图_20200929173114.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0929/173546_134067f8_1139094.png "微信截图_20200929173151.png")
# plat-all介绍
#### V1.0基于spring boot介绍
##### 框架优势：统一接口，统一入参和响应报文，
##### plat框架，包含后台管理，api接口，小程序客户端
##### api项目以交易码形式访问，统一接口只有一个入口
##### api启动，请求地址127.0.0.1:8080/open/api(端口号自己修改，默认8080)
#### 请求报文格式
```
  {
      "head": {
        "cmd": cmd,
        "token": "token",
        "appid": "appid",
       "service": "user-center",
        "version": "1.0"
      },
     "body": param
   }
 ```
#### 参数说明：
##### head请求头放公共请求信息
 ```
cmd:IOC容器里注入的sevice名字小驼峰+类方法名字
例如：CreateCartAction下面的方法createOrder
cmd = createCartAction.createOrder
appid:可为空
service:可为空
version:可为空
param格式为json格式字符串
 ```
##### body是请求体以json形式访问
##### 默认请求方式post方式请求

#### 启动需要配置以下信息

##### 1、配置数据源---->创建数据库--->倒数数据库SQL--->配置数据库信息

#### 配置数据信息--->修改以下相关数据源以及redis信息
  ```
     application-dev.yml（开发环境）
     application-prod.yml（正式环境）
     application-test.yml（测试环境）
     common-config.yml(支付相关回调地址)
     resource下cert存放微信支付证书
     qiniu.properties七牛云相关配置
 ```
##### 在此说明：api配置与admin模块数据库配置一致
##### 回调方法在api项目下HttpController下面
