package com.plat;

import com.plat.project.app.SystemParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.MultipartConfigElement;

/**
 * 启动程序
 *
 * @author plat
 */
@Configuration
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class PlatApplication  {
    public static void main(String[] args) {
        System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(PlatApplication.class, args);
        System.out.println(" plat启动成功");
    }


    /**
     * 文件上传配置
     *
     * @return
     */
    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //  单个数据大小
        factory.setMaxFileSize("10240MB"); // KB,MB
        /// 总上传数据大小
        factory.setMaxRequestSize("102400MB");
        return factory.createMultipartConfig();
    }
}
