package com.plat.api.anonation;

import java.lang.annotation.*;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/7
 * Time: 22:16
 * Description: No Description
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Token {
}
