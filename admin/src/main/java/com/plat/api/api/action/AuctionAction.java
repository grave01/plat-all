package com.plat.api.api.action;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import cn.hutool.core.date.BetweenFormater;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONObject;
import com.plat.api.anonation.NoToken;
import com.plat.api.anonation.Token;
import com.plat.api.dao.api.*;
import com.plat.api.entity.api.*;
import com.plat.api.util.RandomStringUtil;
import com.plat.api.util.StringUtil;
import com.plat.common.constant.GoodsType;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.query.LambdaQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/6/14
 * Time: 0:38
 * Description: No Description
 */
@Service
@Token
public class AuctionAction {
    @Autowired
    private PlatAuctionDao platAuctionDao;
    @Autowired
    private PlatOrderDao orderDao;
    @Autowired
    private PlatAuctionOrderRecordDao recordDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private TokenDao tokenDao;
    @Autowired
    private PlatAuctionOrderDao platAuctionOrderDao;
    @Autowired
    private PlatAddressDao platAddressDao;
    @Autowired
    private PlatOrderDetailDao platOrderDetailDao;

    public JSONObject publish(JSONObject jsonObject) {
        String token = jsonObject.getJSONObject("head").getString("token");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", token).single();
        JSONObject json = new JSONObject();
        JSONObject body = jsonObject.getJSONObject("body");
        AuctionEntity auctionEntity = new AuctionEntity();
        auctionEntity.setName(body.getString("name"));
        auctionEntity.setDetail(body.getString("detail"));
        auctionEntity.setStartTime(body.getDate("startTime"));
        auctionEntity.setEndTime(body.getDate("endTime"));
        auctionEntity.setBasePrice(body.getDouble("basePrice"));
        auctionEntity.setSecurityPrice(0D);
        auctionEntity.setRangePrice(body.getDouble("rangePrice"));
        auctionEntity.setPublisher(0L);
        auctionEntity.setIsCheck(0);
        auctionEntity.setIsShopping(0L);
        auctionEntity.setUrl(body.getString("url"));
        auctionEntity.setNum(0);
        auctionEntity.setIsFinish(0);
        auctionEntity.setAuctionOwnerId(tokenEntity.getUserId());
        auctionEntity.setAuctionOwnerName(0);
        auctionEntity.setAuctionCode(RandomStringUtil.uniqKey());
        auctionEntity.setCreateTime(new Date());
        auctionEntity.setGoodsType(GoodsType.PAIMAI);
        platAuctionDao.insert(auctionEntity);
        json.put("object", auctionEntity);
        return json;
    }


    public JSONObject getAuctionList(JSONObject jsonObject) {
        JSONObject json = new JSONObject();
        JSONObject body = jsonObject.getJSONObject("body");
        Integer page = body.getInteger("page");
        Integer size = body.getInteger("pageSize");
        LambdaQuery<AuctionEntity> lambdaQuery = platAuctionDao.createLambdaQuery();
        List<AuctionEntity> list = lambdaQuery.orderBy("create_time desc").page(page, size).getList();
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setUrl(StringUtil.solveString(list.get(i).getUrl()));
            String[] strings = StringUtil.solveString(list.get(i).getUrl()).split(",");
            list.get(i).setImageUrl(strings);
            list.get(i).setMainImage(strings[0]);
            Integer num = platOrderDetailDao.getAuctionNum(list.get(i).getId());
            list.get(i).setNum(num);
        }
        json.put("list", list);
        return json;
    }

    public JSONObject getAuctionInfo(JSONObject jsonObject) {
        JSONObject json = new JSONObject();
        JSONObject head = jsonObject.getJSONObject("head");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", head.getString("token")).single();
        JSONObject body = jsonObject.getJSONObject("body");
        Integer id = body.getInteger("id");
        AuctionEntity auctionEntity = platAuctionDao.unique(id);
        String[] strings = StringUtil.solveString(auctionEntity.getUrl()).split(",");
        auctionEntity.setImageUrl(strings);
        auctionEntity.setMainImage(strings[0]);
        long between = DateUtil.between(auctionEntity.getStartTime(), auctionEntity.getEndTime(), DateUnit.MS);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//MMM dd hh:mm:ss Z yyyy
        auctionEntity.setSurplusTime(sdf.format(auctionEntity.getEndTime()));
        PlatAuctionOrderEntity  record = recordDao.getMAXAmount(id);
        Double sumAmount = recordDao.getPaySumMoney(id, tokenEntity.getUserId());
        if (sumAmount == null) {
            sumAmount = 0D;
        }
        auctionEntity.setSumAmount(sumAmount);
        if (record.getLastPrice() == null) {
            auctionEntity.setMaxAmount(0D);
        } else {
            auctionEntity.setMaxAmount(record.getLastPrice());
        }
        json.put("object", auctionEntity);
        return json;
    }


    public JSONObject getAuctionPayList(JSONObject jsonObject) {
        JSONObject json = new JSONObject();
        JSONObject body = jsonObject.getJSONObject("body");
        Integer id = body.getInteger("id");
        List<PlatAuctionOrderRecordEntity> list = recordDao.getPayList(id);
        for (int i = 0; i < list.size(); i++) {
            UserEntity userEntity = userDao.unique(list.get(i).getUserId());
            AuctionEntity auctionEntity = platAuctionDao.unique(id);
            list.get(i).setUserEntity(userEntity);
            list.get(i).setAuctionEntity(auctionEntity);
            list.get(i).setPTime(DateUtil.formatDateTime(list.get(i).getPayTime()));
        }
        json.put("list", list);
        return json;
    }
}
