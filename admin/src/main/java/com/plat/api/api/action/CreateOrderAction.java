package com.plat.api.api.action;

import com.alibaba.fastjson.JSONObject;
import com.plat.api.anonation.Token;
import org.springframework.stereotype.Component;

/**
 * author ss
 * createTime 2020/5/8
 * package ${com.example.demo.controller.action}
 ***/
@Component
@Token
public class CreateOrderAction {

    public JSONObject createOrder(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        String orderSn = body.getString("orderSn");
        //
        return body;
    }

}
