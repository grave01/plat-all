package com.plat.api.api.action;

import com.alibaba.fastjson.JSONObject;
import com.plat.api.anonation.Token;
import org.springframework.stereotype.Component;

/**
 * author ss
 * createTime 2020/5/8
 * package ${com.example.demo.controller.action}
 ***/
@Component
@Token
public class PhoneAction {
//    @NoToken
    public JSONObject invoke(JSONObject jsonObject) {
        JSONObject json = new JSONObject();
        json.put("name", "PhoneService.invoke");
        return json;
    }

    public JSONObject getName(JSONObject jsonObject) {
        JSONObject json = new JSONObject();
        json.put("name", "PhoneService.getName");
        return json;
    }
}
