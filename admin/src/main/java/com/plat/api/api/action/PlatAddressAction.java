package com.plat.api.api.action;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.plat.api.dao.api.PlatAddressDao;
import com.plat.api.dao.api.TokenDao;
import com.plat.api.entity.api.PlatAddressEntity;
import com.plat.api.entity.api.TokenEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/28
 * Time: 0:13
 * Description: No Description
 */
@Service
public class PlatAddressAction {
    @Autowired
    private PlatAddressDao platAddressDao;
    @Autowired
    private TokenDao tokenDao;

    public JSONObject addOrUpadte(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body").getJSONObject("json");
        String token = jsonObject.getJSONObject("head").getString("token");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", token).single();
        PlatAddressEntity platAddressEntity = platAddressDao.getSQLManager().lambdaQuery(PlatAddressEntity.class).andEq("user_id", tokenEntity.getUserId()).single();
        Boolean flag = false;
        if (platAddressEntity == null) {
            flag = true;
            platAddressEntity = new PlatAddressEntity();
        }
        String userName = body.getString("userName");
        String telNumber = body.getString("telNumber");
        String nationalCode = body.getString("nationalCode");
        String postalCode = body.getString("postalCode");
        String provinceName = body.getString("provinceName");
        String cityName = body.getString("cityName");
        String countyName = body.getString("countyName");
        String detailInfo = body.getString("detailInfo");
        platAddressEntity.setPlatAddressName(userName);
        platAddressEntity.setUserId(tokenEntity.getUserId());
        platAddressEntity.setProvince(provinceName);
        platAddressEntity.setCity(cityName);
        platAddressEntity.setCounty(countyName);
        platAddressEntity.setAddressDetail(provinceName + cityName + cityName + detailInfo);
        platAddressEntity.setAreaCode(nationalCode);
        platAddressEntity.setPostalCode(postalCode);
        platAddressEntity.setTel(telNumber);
        platAddressEntity.setIsDefault(0);
        platAddressEntity.setDeleted(0);
        platAddressEntity.setPlatAddressCode(RandomUtil.randomString(20));
        if (flag) {
            platAddressDao.insert(platAddressEntity);
        } else {
            platAddressDao.updateById(platAddressEntity);
        }
         platAddressEntity = platAddressDao.getSQLManager().lambdaQuery(PlatAddressEntity.class).andEq("user_id", tokenEntity.getUserId()).single();
        JSONObject json = new JSONObject();
        json.put("object", platAddressEntity);
        return json;
    }

    public JSONObject delete(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        String token = jsonObject.getJSONObject("head").getString("token");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", token).single();
        String userName = body.getString("userName");
        String telNumber = body.getString("telNumber");
        String nationalCode = body.getString("nationalCode");
        String postalCode = body.getString("postalCode");
        String provinceName = body.getString("provinceName");
        String cityName = body.getString("cityName");
        String countyName = body.getString("countyName");
        String detailInfo = body.getString("detailInfo");
        PlatAddressEntity entity = new PlatAddressEntity();
        entity.setPlatAddressName(userName);
        entity.setUserId(tokenEntity.getUserId());
        entity.setProvince(provinceName);
        entity.setCity(cityName);
        entity.setCounty(countyName);
        entity.setAddressDetail(provinceName + cityName + cityName + detailInfo);
        entity.setAreaCode(nationalCode);
        entity.setPostalCode(postalCode);
        entity.setTel(telNumber);
        entity.setIsDefault(0);
        entity.setDeleted(0);
        entity.setPlatAddressCode(RandomUtil.randomString(20));
        platAddressDao.deleteById(entity);
        JSONObject json = new JSONObject();
        json.put("object", entity);
        return json;
    }

}
