package com.plat.api.api.action;

import com.alibaba.fastjson.JSONObject;
import com.plat.api.anonation.NoToken;
import com.plat.api.anonation.Token;
import com.plat.api.dao.api.PlatBannerDao;
import com.plat.api.dao.api.PlatCategoryDao;
import com.plat.api.entity.api.PlatBannerEntity;
import com.plat.api.entity.api.PlatCategoryEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/15
 * Time: 22:49
 * 平台分类
 */
@Service
@Token
public class PlatBannerAction {
    @Autowired
    private PlatBannerDao platBanneryDao;
    @NoToken
    public Object getBannerList(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        List<PlatBannerEntity> list = platBanneryDao.all();
        Map<String,Object> map = new HashMap<>();
        map.put("list",list);
        return map;
    }
}
