package com.plat.api.api.action;

import com.alibaba.fastjson.JSONObject;
import com.plat.api.dao.api.PlatVideoDao;
import com.plat.api.dao.api.TokenDao;
import com.plat.api.dao.api.UserDao;
import com.plat.api.entity.api.PlatVideoEntity;
import com.plat.api.entity.api.TokenEntity;
import com.plat.api.entity.api.UserEntity;
import com.plat.api.util.StringUtil;
import org.beetl.sql.core.query.LambdaQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/11
 * Time: 13:28
 * Description: 短视频接口
 */
@Service
public class PlatShortVideoAction {
    private static final Integer SHORT_VIDEO_TYPE = 2;
    @Autowired
    private PlatVideoDao platVideoDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private TokenDao tokenDao;
    /**
     * 获取短视频列表
     */
    public JSONObject getShortVideo(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        Integer page = body.getInteger("page");
        Integer pageSize = body.getInteger("pageSize");
        String token = jsonObject.getJSONObject("head").getString("token");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", token).single();
        UserEntity userEntity = userDao.getSQLManager().lambdaQuery(UserEntity.class).andEq("id", tokenEntity.getUserId()).single();
        LambdaQuery<PlatVideoEntity> query = platVideoDao.getSQLManager().lambdaQuery(PlatVideoEntity.class);
        List<PlatVideoEntity> list = query.andEq("video_type", SHORT_VIDEO_TYPE).page(page, pageSize).getList();
        JSONObject json = new JSONObject();
         Integer count = 0;
         List<Map<String,Object>> list1 = new ArrayList<>();
        for (PlatVideoEntity platVideoEntity :list){
            platVideoEntity.setPoster(StringUtil.solveString(platVideoEntity.getPoster(),"[]"));
            platVideoEntity.setSrc(platVideoEntity.getUrl());
            count++;
            platVideoEntity.setTitle(platVideoEntity.getTitle()+count);
            Map<String,Object> map = new HashMap<>();
            map.put("id",platVideoEntity.getId());
            map.put("videoid",platVideoEntity.getUrl());
            map.put("commentNum",0);
            map.put("shareNum",0);
            map.put("dianzanNum",0);
            map.put("author",userEntity.getName());
            map.put("content",platVideoEntity.getDetail().substring(0,20)+"...");
            map.put("userId",userEntity.getId());
            map.put("dianzanColor",1);
            map.put("avator",userEntity.getWxHeadPhoto());
            map.put("poster",StringUtil.solveString(platVideoEntity.getPoster(),"[]"));
            list1.add(map);
        }
        json.put("list", list1);
        return json;
    }
}
