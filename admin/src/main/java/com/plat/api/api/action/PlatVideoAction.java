package com.plat.api.api.action;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.plat.api.anonation.NoToken;
import com.plat.api.anonation.Token;
import com.plat.api.common.OrderType;
import com.plat.api.common.VideoStatus;
import com.plat.api.dao.api.*;
import com.plat.api.entity.api.PlatOrderDetailEntity;
import com.plat.api.entity.api.PlatOrderEntity;
import com.plat.api.entity.api.PlatVideoEntity;
import com.plat.api.entity.api.TokenEntity;
import com.plat.api.util.StringUtil;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.query.LambdaQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/10
 * Time: 8:49
 * Description: No Description
 */
@Service
@Token
public class PlatVideoAction implements VideoStatus, OrderType {
    @Autowired
    private PlatVideoDao platVideoDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private TokenDao tokenDao;
    private static final Integer is_recommend = 1; //是否推荐
    @Autowired
    private PlatOrderDao platOrderDao;
    @Autowired
    private PlatOrderDetailDao platOrderDetailDao;

    @NoToken
    public JSONObject getVideoList(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        String keyword = body.getString("keyword");
        Integer page = body.getInteger("page");
        Integer is_free = body.getInteger("is_free");
        Integer is_recommend = body.getInteger("is_recommend");
        Date create_time = body.getDate("create_time");
        String category = body.getString("category");
        List<String> li = new ArrayList<>();
        if (category != null) {
            for (int i = 0; i < category.split(",").length; i++) {
                li.add(category.split(",")[i]);
            }
        }
        Integer type = body.getInteger("type");
        Integer pageSize;
        if (body.getInteger("pageSize") == null) {
            pageSize = 10;
        } else {
            pageSize = body.getInteger("pageSize");
        }
        LambdaQuery<PlatVideoEntity> query = platVideoDao.getSQLManager().lambdaQuery(PlatVideoEntity.class);
        PageQuery<PlatVideoEntity> list;
        if (category != null) {
            if (!category.equals("0000")) {
                query.andLike("category", "%" + li.get(0) + "%");
            }

        }

        if (is_recommend != null) {
            list = query.orderBy("is_top desc").andEq("is_recommend", is_recommend).page(page, pageSize);

        } else {
            list = query.orderBy("is_top desc").page(page, pageSize);
        }
        JSONObject json = new JSONObject();
        List<PlatVideoEntity> lis = new ArrayList<>();
        for (int i = 0; i < list.getList().size(); i++) {
            list.getList().get(i).setPoster(StringUtil.solveString(list.getList().get(i).getPoster()));
            list.getList().get(i).setImageId(StringUtil.solveString(list.getList().get(i).getImageId()));
            list.getList().get(i).setPublishTime(DateUtil.format(list.getList().get(i).getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
            lis.add(list.getList().get(i));
        }
        json.put("list", lis);
        return json;
    }

    @NoToken
    public JSONObject getRecommendVideoList(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        String keyword = body.getString("keyword");
        Integer page = body.getInteger("page");
        Integer is_free = body.getInteger("is_free");
        Date create_time = body.getDate("create_time");
        String category = body.getString("category");
        List<String> li = new ArrayList<>();
        for (int i = 0; i < category.split(",").length; i++) {
            li.add(category.split(",")[i]);
        }
        Integer type = body.getInteger("type");
        Integer pageSize = body.getInteger("pageSize");
        LambdaQuery<PlatVideoEntity> query = platVideoDao.getSQLManager().lambdaQuery(PlatVideoEntity.class);
        PageQuery<PlatVideoEntity> list = query.andLike("name", keyword)
                .andLike("desc", keyword)
                .andEq("is_free", is_free)
                .andEq("create_time", create_time)
                .andEq("type", type).andEq("is_recommend", is_recommend)
                .andIn("category", li).orderBy("is_recommend desc")
                .page(page, pageSize);
        JSONObject json = new JSONObject();
        json.put("list", list.getList());
        return json;
    }

    @NoToken
    public JSONObject getMyVideoList(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        JSONObject head = jsonObject.getJSONObject("head");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", head.getString("token")).single();
        String keyword = body.getString("keyword");
        Integer page = body.getInteger("page");
        Integer is_free = body.getInteger("is_free");
        Date create_time = body.getDate("create_time");
        String category = body.getString("category");
        List<String> li = new ArrayList<>();
        for (int i = 0; i < category.split(",").length; i++) {
            li.add(category.split(",")[i]);
        }
        Integer type = body.getInteger("type");
        Integer pageSize = body.getInteger("pageSize");
        LambdaQuery<PlatVideoEntity> query = platVideoDao.getSQLManager().lambdaQuery(PlatVideoEntity.class);
        PageQuery<PlatVideoEntity> list = query.andLike("name", keyword)
                .andLike("desc", keyword)
                .andEq("is_free", is_free)
                .andEq("is_free", tokenEntity.getUserId())
                .andEq("create_time", create_time)
                .andEq("type", type).andEq("is_recommend", is_recommend)
                .andIn("category", li).orderBy("is_recommend desc")
                .page(page, pageSize);
        JSONObject json = new JSONObject();
        json.put("list", list.getList());
        return json;
    }

    @NoToken
    public JSONObject getVideo(JSONObject jsonObject) {
        JSONObject param = jsonObject.getJSONObject("body");
        Integer id = param.getInteger("id");
        PlatVideoEntity platVideoEntity = platVideoDao.unique(id);
        JSONObject json = new JSONObject();
        platVideoEntity.setPublishTime(DateUtil.format(platVideoEntity.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
        LambdaQuery<PlatVideoEntity> lambdaQuery = platVideoDao.createLambdaQuery().andLike("category", "%" + StringUtil.solveString(platVideoEntity.getCategory()).split(",")[0] + "%");
        PageQuery<PlatVideoEntity> pageQuery = lambdaQuery.page(1, 10);
        platVideoEntity.setNum(platVideoEntity.getNum() + 1);
        platVideoDao.updateById(platVideoEntity);//修改播放量
        if (null != platVideoEntity.getM3u8Url()) {
            // platVideoEntity.setUrl(platVideoEntity.getM3u8Url());
            platVideoEntity.setUrl(platVideoEntity.getUrl());
        }
        JSONObject head = jsonObject.getJSONObject("head");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", head.getString("token")).single();
        LambdaQuery<PlatOrderDetailEntity> lambdaQuery1 = platOrderDetailDao.createLambdaQuery().andEq("user_id", tokenEntity.getUserId()).andEq("goods_id", platVideoEntity.getId()).andEq("order_type", OrderType.VIDE0);
        PlatOrderDetailEntity platOrderDetailEntity = lambdaQuery1.single();

        if (platVideoEntity.getIsFree() == FREE) {
            platVideoEntity.setShowFreeItem(1);
        } else {
            if (platOrderDetailEntity == null) {
                platVideoEntity.setShowFreeItem(0);
            } else {
                LambdaQuery<PlatOrderEntity> lambdaQuery2 = platOrderDao.getSQLManager().lambdaQuery(PlatOrderEntity.class).andEq("order_no", platOrderDetailEntity.getOrderNo()).andEq("order_type", OrderType.VIDE0);
                PlatOrderEntity orderEntity = lambdaQuery2.single();
                if (orderEntity == null) {
                    platVideoEntity.setShowFreeItem(0);
                } else {
                    if (orderEntity.getPayStatus().equals(OrderType.FINISH_PAY)) {
                        platVideoEntity.setIsFree(1);
                        platVideoEntity.setShowFreeItem(2);
                    } else {
                        platVideoEntity.setShowFreeItem(0);
                    }
                }
            }
        }
        json.put("object", platVideoEntity);
        List<PlatVideoEntity> list = pageQuery.getList();
        List<PlatVideoEntity> lis = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setPoster(StringUtil.solveString(list.get(i).getPoster()));
            list.get(i).setImageId(StringUtil.solveString(list.get(i).getImageId()));
            list.get(i).setPublishTime(DateUtil.format(list.get(i).getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
            if (null != list.get(i).getM3u8Url()) {
                // list.get(i).setUrl(StringUtil.solveString(list.get(i).getM3u8Url()));
            }
            lis.add(list.get(i));
        }
        json.put("list", lis);
        return json;
    }
}
