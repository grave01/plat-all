package com.plat.api.api.action;

import com.alibaba.fastjson.JSONObject;
import com.plat.api.anonation.Token;
import com.plat.api.dao.api.PlatWechatActivityDao;
import com.plat.api.entity.api.PlatWechatActivityEntity;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/2
 * Time: 23:47
 * Description: No Description
 */
@Service
@Token
public class PlatWechatActivityAction {
    @Autowired
    private PlatWechatActivityDao platWechatActivityDao;

    public Object getActivityList(JSONObject param) {
        JSONObject body = param.getJSONObject("body");
        Integer page = body.getInteger("page");
        Integer pageSize = body.getInteger("pageSize");
        PageQuery<PlatWechatActivityEntity> pageQuery = platWechatActivityDao.createLambdaQuery().andEq("is_delete", 0).page(page, pageSize);
        JSONObject json = new JSONObject();
        json.put("list", pageQuery.getList());
        return json;
    }

    public Object getActivity(JSONObject param) {
        JSONObject body = param.getJSONObject("body");
        Integer id = body.getInteger("id");
        PlatWechatActivityEntity platWechatActivityEntity = platWechatActivityDao.createLambdaQuery().andEq("is_delete", 0).andEq("id",id).single();
        JSONObject json = new JSONObject();
        json.put("object", platWechatActivityEntity);
        return json;
    }
}
