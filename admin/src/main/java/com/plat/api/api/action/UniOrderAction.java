package com.plat.api.api.action;

import com.alibaba.fastjson.JSONObject;
import com.plat.api.common.RespCode;
import com.plat.api.dao.api.PlatVideoDao;
import com.plat.api.entity.api.PlatVideoEntity;
import com.plat.api.exception.PlatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/6/14
 * Time: 0:02
 * Description: No Description
 */
@Service
public class UniOrderAction {
    private static final Integer VIDEO_TYPE = 1;
    private static final Integer AUCTION_TYPE = 2;
    @Autowired
    private PlatVideoDao platVideoDao;

    //统一下单接口
    public JSONObject createOrder(JSONObject jsonObject) {
        //目前支持两种下单，视频下单，拍卖下单
        JSONObject param = jsonObject.getJSONObject("body");
        Integer type = param.getInteger("type");
        Integer goodId = param.getInteger("GoodsId");
        //执行视频下单
        PlatVideoEntity platVideoEntity;
        JSONObject json = new JSONObject();
        String orderNo = "";
        if (type.equals(VIDEO_TYPE)) {
            platVideoEntity = platVideoDao.single(goodId);
            if (platVideoEntity != null) {
                orderNo = doCreateVideoOrder(platVideoEntity);
                json.put("orderNo", orderNo);
                json.put("GoodsInfo", "GoodsInfo");
            } else {
                throw new PlatException(RespCode.NOT_EXIST_GOOD_INFO.getCode(), RespCode.NOT_EXIST_GOOD_INFO.getError_msg());
            }
        }
        if (type.equals(AUCTION_TYPE)) {
            platVideoEntity = platVideoDao.single(goodId);
            if (platVideoEntity != null) {
                orderNo = doCreateAuctionOrder(platVideoEntity);
                json.put("orderNo", orderNo);
                json.put("GoodsInfo", "GoodsInfo");
            } else {
                throw new PlatException(RespCode.NOT_EXIST_GOOD_INFO.getCode(), RespCode.NOT_EXIST_GOOD_INFO.getError_msg());
            }
        } else {
            throw new PlatException(RespCode.NOT_SUPPORT_ORDER_TYPE.getCode(), RespCode.NOT_SUPPORT_ORDER_TYPE.getError_msg());
        }
        return json;
    }

    private String doCreateAuctionOrder(PlatVideoEntity platVideoEntity) {
        return "";
    }

    private String doCreateVideoOrder(PlatVideoEntity platVideoEntity) {
        return "";
    }
}
