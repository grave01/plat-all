package com.plat.api.api.action;

import com.alibaba.fastjson.JSONObject;
import com.plat.api.anonation.NoToken;
import com.plat.api.anonation.Token;
import com.plat.api.common.OrderType;
import com.plat.api.dao.api.*;
import com.plat.api.entity.api.AuctionEntity;
import com.plat.api.entity.api.TokenEntity;
import com.plat.api.entity.api.UserEntity;
import org.beetl.sql.core.query.LambdaQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author ss
 * createTime 2020/5/8
 * package ${com.example.demo.controller.action}
 ***/
@Component
@Token
public class UserAction {
    @Autowired
    private UserDao userDao;
    @Autowired
    private TokenDao tokenDao;
    @Autowired
    private PlatAuctionDao platAuctionDao;
    @Autowired
    private PlatOrderDao orderDao;
    @Autowired
    private PlatOrderDetailDao detailDao;

    // @NoToken
    public JSONObject getName(JSONObject jsonObject) {
        JSONObject json = new JSONObject();
        json.put("name", "UserService.getName");
        return json;
    }

    public JSONObject getUserInfo(JSONObject jsonObject) {
        String token = jsonObject.getJSONObject("head").getString("token");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", token).single();
        UserEntity userEntity = userDao.getSQLManager().lambdaQuery(UserEntity.class).andEq("id", tokenEntity.getUserId()).single();

        long count = platAuctionDao.createLambdaQuery().andEq("auction_owner_id", tokenEntity.getUserId()).count();
        JSONObject json = new JSONObject();
        userEntity.setCount(count);
        json.put("userInfo", userEntity);
        json.put("countInfo", getCount(userEntity));
        return json;
    }

    /**
     * 统计数量
     **/
    public Map getCount(UserEntity userEntity) {
        Map<String, Object> map = new HashMap<>();
        //全部
        long count1 = orderDao.createLambdaQuery().andEq("user_id", userEntity.getId()).count();
        map.put("all", count1);
        //待支付
        long count2 = orderDao.createLambdaQuery().andEq("user_id", userEntity.getId()).andEq("pay_status", 0).count();
        map.put("waitingPay", count2);
        //待发货
        long count3 = orderDao.createLambdaQuery().andEq("user_id", userEntity.getId())
                .andEq("pay_status", 1).andEq("express_status", OrderType.ORDER_EXPRESS_NO_DISTRIBUTE).count();
        map.put("waitingExpress", count3);
        //待收货
        long count4 = orderDao.createLambdaQuery().andEq("user_id", userEntity.getId())
                .andEq("pay_status", 1).andEq("express_status", OrderType.ORDER_EXPRESS_DISTRIBUTE).count();
        map.put("waitingExpressFinish", count4);
        //完成
        long count5 = orderDao.createLambdaQuery().andEq("user_id", userEntity.getId())
                .andEq("pay_status", 1).andEq("order_status", OrderType.ORDER_FINISH).count();
        map.put("finish", count5);
        Map<String, Object> map1 = new HashMap<>();
        map1.put("countInfo", map);
        return map1;
    }

    public void test() {
        List<UserEntity> list = userDao.all();
        System.out.println(list);
    }
}
