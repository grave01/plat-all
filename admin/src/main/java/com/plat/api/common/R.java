package top.lconcise;

import java.util.ArrayList;
import java.util.List;

/**
 * author ss
 * createTime 2020/7/8
 * package ${top.lconcise}
 ***/
public class R {
    private Object object;

    private List<Object> list;

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public List<Object> getList() {
        return list;
    }

    public void setList(List<Object> list) {
        this.list = list;
    }

    public static R ok(Object object) {
        R r = new R();
        if (object instanceof List) {
            r.setList((List<Object>) object);
        } else {
            r.setObject(object);
        }
        return r;
    }
}
