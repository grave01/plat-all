package com.plat.api.common;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/8
 * Time: 23:49
 * Description: No Description
 */
public enum RespCode {
    SUCCESS("00000", "SUCCESS"),
    FAIL("40001", "FAIL"),
    NOT_FOUND_METHOD("404", "NOT_FOUND_METHOD"),
    WX_AUTH_ERROR("55551", "WX_AUTH_ERROR"),
    PASSWORD_ERROR("55552", "PASSWORD_ERROR_IS_NOT_LIKE"),
    PHONE_OR_EMAIL_ERROR("55553", "PHONE_OR_EMAIL_ERROR"),
    EMAIL_ERROR("55554", "EMAIL_ERROR_IS_NOT_MATCH"),
    LOGIN_ERROR("55555", "LOGIN_ERROR_USE_PHONE_OR_PHONE_ERROR"),
    CHECK_CODE_ERROR("55556", "CHECK_CODE_ERROR"),
    SEND_SMS_LIMIT("55557","SEND_SMS_LIMIT"),
    NOT_EXIST_GOOD_INFO("60000","NOT_EXIST_GOOD_INFO"),
    REQUEST_PARAM_ERROR("77777", "LOGIN_ERROR_USE_PHONE_OR_PHONE_ERROR"),
    NOT_SUPPORT_ORDER_TYPE("55558","NOT_SUPPORT_ORDER_TYPE"),
    WEIXIN_CREATE_LIVE_ERROR("10000","WEIXIN_CREATE_LIVE_ERROR"),
    CREATE_ORDER_FAIL("10002","CREATE_ORDER_FAIL"),
    CREATE_ORDER_ADDRESS_NULL("10003","CREATE_ORDER_ADDRESS_NULL"),
    SYS_ERROR("500","ERROR_IS_NOT_DEFINED"),
    IP_REQUEST_LIMIT("7000","IP_REQUEST_LIMIT");
    ;
    private String code;
    private String error_msg;

    RespCode(String code, String error_msg) {
        this.code = code;
        this.error_msg = error_msg;
    }

    public String getCode() {
        return code;
    }

    public String getError_msg() {
        return error_msg;
    }
}
