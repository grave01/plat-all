package com.plat.api.common;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/21
 * Time: 16:15
 * Description: No Description
 */
public interface VideoStatus {
    Integer FREE  = 1; //视频原本免费的
    Integer PAY  =1;//已经付费
    Integer IS_FREE  =2;//第二个字段 免费 控制显示按钮
}
