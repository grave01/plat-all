package com.plat.api.common.base;


import cn.hutool.core.util.ReflectUtil;

import java.lang.reflect.Method;

/**
 * author ss
 * createTime 2020/5/8
 * package ${com.example.demo.controller.base}
 ***/
public class ReflectClassUtil {
    public static Method[] getMethod(Class<?> beanClass)  {
        Method[] methods = ReflectUtil.getMethods(beanClass);
        return methods;
    }
}
