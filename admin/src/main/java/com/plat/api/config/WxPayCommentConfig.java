package com.plat.api.config;

import com.github.binarywang.wxpay.config.WxPayConfig;
import com.plat.api.util.YmalPropertiesUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/21
 * Time: 16:38
 * Description: 微信支付相关配置信息
 */
public class WxPayCommentConfig {

    /**
     * 默认付款回调配置
     */
    public static WxPayConfig getCreateOrderConfig() throws FileNotFoundException {
        WxPayConfig payConfig = getWxPayConfig();
        payConfig.setNotifyUrl(YmalPropertiesUtil.getValue("wxpay.notify-url").toString());
        return payConfig;
    }

    /**
     * 默认退款回调配置
     */
    public static WxPayConfig getRefundConfig() throws FileNotFoundException {
        WxPayConfig payConfig = getWxPayConfig();
        payConfig.setNotifyUrl(YmalPropertiesUtil.getValue("wxpay.refund-notify-url").toString());
        return payConfig;
    }

    /**
     * 拍卖付款回调配置
     */
    public static WxPayConfig getAuctionConfig() throws FileNotFoundException {
        WxPayConfig payConfig = getWxPayConfig();
        payConfig.setNotifyUrl(YmalPropertiesUtil.getValue("wxpay.auction-notify-url").toString());
        return payConfig;
    }

    private static WxPayConfig getWxPayConfig() throws FileNotFoundException {
        WxPayConfig payConfig = new WxPayConfig();
        payConfig.setAppId(StringUtils.trimToNull("wxe461b76c983ac6bb"));
        payConfig.setMchId(StringUtils.trimToNull("1583531981"));
        payConfig.setMchKey(StringUtils.trimToNull("SQgxqGozx2EDtHfLCHfthnhRAQq65Spl"));
        String os = System.getProperty("os.name");
        if(!os.toLowerCase().contains("win")) {
            File file = ResourceUtils.getFile("/www/admin/cert/apiclient_cert.p12");
            payConfig.setKeyPath(file.getPath());
        } else {
            payConfig.setKeyPath("E:/plat/src/main/resources/cert/apiclient_cert.p12");
        }
        return payConfig;
    }

}
