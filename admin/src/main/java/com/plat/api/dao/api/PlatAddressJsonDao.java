package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatAddressJsonEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/6/9
 * Time: 23:44
 * Description: No Description
 */
public interface PlatAddressJsonDao extends BaseMapper<PlatAddressJsonEntity> {
}
