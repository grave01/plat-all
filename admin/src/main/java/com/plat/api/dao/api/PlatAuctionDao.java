package com.plat.api.dao.api;

import com.plat.api.entity.api.AuctionEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/4
 * Time: 13:06
 * Description: No Description
 */
public interface PlatAuctionDao extends BaseMapper<AuctionEntity> {
}
