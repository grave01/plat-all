package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatAuctionOrderEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/21
 * Time: 14:08
 * Description: No Description
 */
public interface PlatAuctionOrderDao extends BaseMapper<PlatAuctionOrderEntity> {
    Double getMAXAmount(Integer id);

    Double getSumAmount(Integer id, Integer userId);

    Integer getJionAuctionNum(Long id);
}
