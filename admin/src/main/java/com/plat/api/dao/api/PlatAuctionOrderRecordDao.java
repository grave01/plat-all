package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatAuctionOrderEntity;
import com.plat.api.entity.api.PlatAuctionOrderRecordEntity;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/21
 * Time: 14:12
 * Description: 记录
 */
public interface PlatAuctionOrderRecordDao extends BaseMapper<PlatAuctionOrderRecordEntity> {
    List<PlatAuctionOrderRecordEntity> getPayList(Integer id);

    Double getPaySumMoney(Integer id, Integer userId);

    PlatAuctionOrderEntity getMAXAmount(Integer id);
}
