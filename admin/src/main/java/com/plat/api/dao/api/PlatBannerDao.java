package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatBannerEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/15
 * Time: 23:09
 * Description: No Description
 */
public interface PlatBannerDao extends BaseMapper<PlatBannerEntity> {
}
