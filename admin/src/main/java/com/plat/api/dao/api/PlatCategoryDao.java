package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatCategoryEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/15
 * Time: 22:54
 * Description: No Description
 */
public interface PlatCategoryDao extends BaseMapper<PlatCategoryEntity> {
}
