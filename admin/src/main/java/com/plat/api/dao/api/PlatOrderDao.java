package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatOrderEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/20
 * Time: 15:54
 * Description: No Description
 */

public interface PlatOrderDao extends BaseMapper<PlatOrderEntity> {
    Double getMAXAmount(Integer id);

    Double getSumAmount(Integer id, Integer userId);

}
