package com.plat.api.dao.api;


import com.plat.api.entity.api.PlatOrderDetailEntity;
import org.beetl.sql.core.mapper.BaseMapper;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/21
 * Time: 9:58
 * Description: No Description
 */

public interface PlatOrderDetailDao extends BaseMapper<PlatOrderDetailEntity> {
    Integer getAuctionNum(Long id);
}
