package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatPeopleEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/6/4
 * Time: 13:50
 * Description: No Description
 */
public interface PlatPeopleDao extends BaseMapper<PlatPeopleEntity> {
}
