package com.plat.api.dao.api;

import com.plat.api.entity.api.TokenEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/9
 * Time: 22:51
 * Description: No Description
 */
public interface TokenDao extends BaseMapper<TokenEntity> {
}
