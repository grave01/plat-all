package com.plat.api.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/9
 * Time: 16:47
 * Description: No Description
 */
@Data
public class RepGoodsInfo {
    private String name;
    private String image;
    private BigDecimal money;
}
