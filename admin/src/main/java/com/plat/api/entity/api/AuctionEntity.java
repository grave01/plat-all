package com.plat.api.entity.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.plat.framework.aspectj.lang.annotation.Excel;
import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/4
 * Time: 13:03
 * Description: No Description
 */
@Data
@Table(name = "plat_auction")
public class AuctionEntity {

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 名称
     */
    @Excel(name = "名称")
    private String name;

    /**
     * 描述
     */
    @Excel(name = "描述")
    private String detail;

    /**
     * 开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /**
     * 结束时间
     */

    private Date endTime;

    /**
     * 拍卖底价
     */
    @Excel(name = "拍卖底价")
    private Double basePrice;

    /**
     * 拍卖押金
     */
    @Excel(name = "拍卖押金")
    private Double securityPrice;

    /**
     * 加价幅度
     */
    @Excel(name = "加价幅度")
    private double rangePrice;

    /**
     * 发布者
     */
    @Excel(name = "发布者")
    private Long publisher;

    /**
     * 是否审核
     */
    @Excel(name = "是否审核")
    private Integer isCheck;

    /**
     * 上架拍买
     */
    @Excel(name = "上架拍买")
    private Long isShopping;

    /**
     * 拍马图片
     */
    @Excel(name = "拍马图片")
    private String url;

    /**
     * 参拍人数
     */
    @Excel(name = "参拍人数")
    private Integer num;

    /**
     * 是否结束
     */
    @Excel(name = "是否结束")
    private Integer isFinish;

    /**
     * 竞拍得主
     */
    @Excel(name = "竞拍得主")
    private Integer auctionOwnerId;

    /**
     * 竞拍得主名称
     */
    @Excel(name = "竞拍得主名称")
    private Integer auctionOwnerName;
    /**
     * 商品类型 商品类型VIDEO-视频，PAIMAI-拍卖，NORMAL-普通实物
     */
    @Excel(name = "商品类型")
    private String goodsType;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间")
    private Date createTime;
    //主图
    private String mainImage;
    private String[] imageUrl;
    // 剩余时间
    private String surplusTime;

    private String auctionCode;
    //需要支付的总额的计算公式  支付总额=加价格幅度*加价次数+第一出价的价格-出价累计价格最高的价格
    private Double payMoney;
    //加价次数
    private Double addPriceNum;

    private Double maxAmount;
    //总价
    private Double sumAmount;
}
