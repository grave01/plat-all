package com.plat.api.entity.api;

import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/6/9
 * Time: 23:42
 * Description: No Description
 */
@Data
@Table(name = "plat_address_json")
public class PlatAddressJsonEntity {
    private Integer id;
    private String json;
    private Date createTime;
}
