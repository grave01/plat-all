package com.plat.api.entity.api;

import com.plat.framework.web.domain.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.AutoID;
import org.beetl.sql.core.annotatoin.Table;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/15
 * Time: 22:52
 * Description: 分类实体
 */
@Data
@Table(name = "plat_category")
public class PlatCategoryEntity extends BaseEntity {
    @AutoID
    private Integer id;
    private String name;//名称
    private String url;//图片
}
