package com.plat.api.entity.api;

import com.plat.framework.aspectj.lang.annotation.Excel;
import com.plat.framework.web.domain.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.AutoID;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/10
 * Time: 12:43
 * Description: No Description
 */
@Data
@Table(name = "plat_comment")
public class PlatCommentEntity  {
    /**
     * $column.columnComment
     */
    @AutoID
    private Long id;

    /**
     * 回复id
     */
    @Excel(name = "回复id")
    private Integer replayId;

    /**
     * 评论内容
     */
    @Excel(name = "评论内容")
    private String content;

    /**
     * 评论带图
     */
    @Excel(name = "评论带图")
    private String photo;

    /**
     * 是否删除
     */
    @Excel(name = "是否删除")
    private Integer isDelete;

    /**
     * 是否显示
     */
    @Excel(name = "是否显示")
    private Integer isShow;

    /**
     * 评论类型暂时1是圈子，在哪儿显示出来
     */
    @Excel(name = "评论类型暂时1是圈子，在哪儿显示出来")
    private Integer com_target_type;

    /**
     * 评论人昵称
     */
    @Excel(name = "评论人昵称")
    private String nickname;

    /**
     * 被评论的内容id
     */
    @Excel(name = "被评论的内容id")
    private Integer targetId;

    /**
     * 回复的用户的名称
     */
    @Excel(name = "回复的用户的名称")
    private String replyName;
    /**
     * 评论发布者
     */
    @Excel(name = "评论发布者")
    private Integer ownerId;
    /**
     * 类型
     */
    @Excel(name = "类型（1是圈子，2是短视频）" )
    private Integer type;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间")
    private Date  createTime;
    //评论时间字符串类型
    private String commentTime;
    private UserEntity userEntity;

}
