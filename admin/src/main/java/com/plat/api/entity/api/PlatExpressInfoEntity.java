package com.plat.api.entity.api;

import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/9
 * Time: 15:49
 * Description: No Description
 */
@Table(name = "plat_express_info")
@Data
public class PlatExpressInfoEntity {
    private Integer id;
    private String orderNo;
    private String info;
    private Date updateTime;
    private Date createTime;
    private String expressNo;
    private String expressPhone;
    private String expressName;
    private String expressImage;
    private String expressCompany;

}
