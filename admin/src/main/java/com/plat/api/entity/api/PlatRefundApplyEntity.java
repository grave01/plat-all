package com.plat.api.entity.api;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/6/9
 * Time: 23:53
 * Description: 退款申请
 */
@Data
public class PlatRefundApplyEntity {
    private String mainOrder;
    private String refundOrder;
    private Date createTime;
    private Double refundAmt;
    private Double refundRealAmt;
    private Date finishTime;
    private Integer refundStatus;
    private String refundMsg;
    private String refundInfo;
    private Integer userId;
    private String applyName;
}
