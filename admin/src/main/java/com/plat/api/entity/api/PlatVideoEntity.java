package com.plat.api.entity.api;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.plat.framework.aspectj.lang.annotation.Excel;
import com.plat.framework.web.domain.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.AutoID;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/10
 * Time: 8:45
 * Description: No Description
 */
@Data
@Table(name = "plat_video")
public class PlatVideoEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;


    /**
     * 主键
     */
    private Long id;

    /**
     * 视频名称
     */
    @Excel(name = "视频名称")
    private String videoName;

    /**
     * 地址
     */
    @Excel(name = "地址")
    private String url;

    /**
     * 描述
     */
    @Excel(name = "描述")
    private String detail;

    /**
     * 是否付费
     */
    @Excel(name = "是否付费")
    private Integer isFree;

    /**
     * 原价格
     */
    @Excel(name = "原价格")
    private Double price;

    /**
     * 是否删除
     */
    @Excel(name = "是否删除")
    private Integer isDelete;

    /**
     * 创建者id
     */
    @Excel(name = "创建者id")
    private Long createUser;

    /**
     * 创建者
     */
    @Excel(name = "创建者")
    private String createUsername;

    /**
     * 分类
     */
    @Excel(name = "分类")
    private String category;

    private String categoryName;
    /**
     * 类型
     */
    @Excel(name = "类型")
    private Long videoType;

    /**
     * 审核1是审核0是未审核2是拒绝3是下架
     */
    @Excel(name = "审核1是审核0是未审核2是拒绝3是下架")
    private Long isCheck;

    /**
     * 是否置顶
     */
    @Excel(name = "是否置顶")
    private Integer isTop;

    /**
     * 是否推荐
     */
    @Excel(name = "是否推荐")
    private Integer isRecommend;

    /**
     * 广告id
     */
    @Excel(name = "广告id")
    private String adId;

    /**
     * 免费时长s为单位
     */
    @Excel(name = "免费时长s为单位")
    private Long freeTime;

    /**
     * 封面图片
     */
    @Excel(name = "封面图片")
    private String poster;

    /**
     * 相片集合
     */
    @Excel(name = "相片集合")
    private String imageId;

    /**
     * 播放量
     */
    @Excel(name = "播放量")
    private Integer num;
    /**
     * 商品类型 商品类型VIDEO-视频，PAIMAI-拍卖，NORMAL-普通实物
     */
    @Excel(name = "商品类型")
    private String goodsType;

    private List<Long> listCategory;
    private Date createTime;
    private String publishTime;
    private String adUrl;
    private String m3u8Url;
    private String src;//短视频读取地址
    private String title;
    private Integer payStatus;//1支付，0未支付，2是免费不显示
    private Integer showHasPayPayBtn;//显示已支付按钮
    /**
     * 收费控制
     */
    private Integer showFreeItem;
    /**
     * 视频信息
     */
    private String videoInfo;

}