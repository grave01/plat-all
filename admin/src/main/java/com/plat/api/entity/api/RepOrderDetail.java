package com.plat.api.entity.api;

import com.plat.api.entity.RepGoodsInfo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/9
 * Time: 15:18
 * Description: No Description
 */
@Data
public class RepOrderDetail {

    private  String orderNo;
    private String orderStatus;
    private  List<RepGoodsInfo> goodsList;
    private Integer  count;
    private BigDecimal countMoney;
    private Integer payStatus;
    private Integer status;
}


