package com.plat.api.entity.api;

import com.plat.framework.web.domain.BaseEntity;
import io.swagger.models.auth.In;
import lombok.Data;
import org.beetl.sql.core.annotatoin.AutoID;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * author ss
 * createTime 2020/5/9
 * package ${com.plat.api.entity.cms}
 ***/
@Data
@Table(name = "plat_user")
public class UserEntity extends BaseEntity {
    @AutoID
    private Integer id;
    private String name;
    private String openId;
    private String email;
    private String phone;
    private String idNo;
    private String idFront;
    private String idBack;
    private Integer authStatus;
    private Date createTime;
    private Date authTime;
    private String wxHeadPhoto;
    private String nickname;
    private String password;
    private Integer type;
    private String userInfo;
    private Integer gender;//1是男 2是女 0是未知
    private String nowAddress;
    private String field;//所在领域
    private Integer hasAuth; //是否认证
    private Integer hasEdit;//信息已编辑过0未编辑1已编辑
    private Integer age;
    private String account;
    private Long count;
}
