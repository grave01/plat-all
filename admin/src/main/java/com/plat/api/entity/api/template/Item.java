package com.plat.api.entity.api.template;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/3
 * Time: 1:43
 * Description: No Description
 */
@Data
public class Item {
    private String id;
    private String[] title;
    private InItem inItem;
    private String itemClass;
}
