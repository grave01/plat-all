package com.plat.api.util;

import com.plat.api.config.QiNiuConfig;

import java.io.IOException;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/13
 * Time: 16:57
 * Description: No Description
 */
public class PropertiesUtil {
    /**
     * 七牛云获取外网访问地址
     * */
    public static String getQiniuFileUrl() throws IOException {
        Properties prop = new Properties();
        prop.load(QiNiuConfig.class.getResourceAsStream("/qiniu.properties"));
       return prop.getProperty("qiniu.http.request.file.url") ;
    }
}
