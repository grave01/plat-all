package com.plat.api.util;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.util.Properties;

/**
 * author ss
 * createTime 2020/7/16
 * package ${com.dt.modules.migrate.util}
 ***/
public class YmalPropertiesUtil {
    private static String PROPERTY = "common-config.yml";

    public static Object getValue(String key) {
        Resource resource = new ClassPathResource(PROPERTY);
        Properties properties = null;
        try {
            YamlPropertiesFactoryBean yamlfactoryBean  =new YamlPropertiesFactoryBean();
            yamlfactoryBean.setResources(resource);
            properties = yamlfactoryBean.getObject();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
        return properties.get(key);
    }

    public static void main(String[] args) {
        String path = getValue("transfer.file-path").toString();
        System.out.println(path);
    }
}
