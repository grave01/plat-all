package com.plat.common.exception.user;

import com.plat.common.exception.BaseException;

/**
 * 用户信息异常类
 * 
 * @author plat
 */
public class UserException extends BaseException
{
    private static final long serialVersionUID = 1L;

    public UserException(String code, Object[] args)
    {
        super("user", code, args, null);
    }
}
