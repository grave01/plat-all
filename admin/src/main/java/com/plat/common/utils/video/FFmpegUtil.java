package com.plat.common.utils.video;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.RuntimeUtil;
import com.plat.common.utils.file.FileUploadUtils;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/6/27
 * Time: 16:51
 * Description: 切片成m3u8
 */
public class FFmpegUtil {

    private static final Integer limit_time = 5;//限制时长5s

    /**
     * @param originFilePath 原本文件路径
     * @param targetPath     存放文件路径
     * @return 文件存放的路径
     */
    public static String toM3U8(String originFilePath, String targetPath, String m3u8Filename) throws Exception {
        String path = "/" + new Date().getYear() + "/" + new Date().getMonth() + "/" + new Date().getDay()+"/";
        String dir = targetPath + path;
        String finalPath = dir + m3u8Filename;
        FileUtil.mkdir(dir);
        StringBuilder sb = new StringBuilder();
        sb.append("ffmpeg -i ");
        sb.append(originFilePath);
        sb.append(" -c:v libx264 -hls_time ");
        sb.append(limit_time);
        sb.append(" -hls_list_size 0 -c:a aac -strict -2 -f hls ");
        sb.append(finalPath);
        Process process = Runtime.getRuntime().exec(sb.toString());
       // RuntimeUtil.exec(sb.toString());
        return path+m3u8Filename;
    }

}
