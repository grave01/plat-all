package com.plat.framework.aspectj.lang.enums;

/**
 * 数据源
 * 
 * @author plat
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
