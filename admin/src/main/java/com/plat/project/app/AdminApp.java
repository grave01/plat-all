package com.plat.project.app;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/26
 * Time: 23:51
 * Description: No Description
 */
@Component
@Slf4j
public class AdminApp implements ApplicationContextAware {
    @Autowired
    private SystemParam systemParam;

    @SneakyThrows
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
       new Thread(new SystemParamThread(systemParam)).start();
    }

}
