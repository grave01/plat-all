package com.plat.project.app;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/30
 * Time: 0:16
 * Description: No Description
 */
@Slf4j
public class SystemParamThread implements Runnable {

    private SystemParam systemParam;
    public SystemParamThread(SystemParam systemParam) {
        this.systemParam = systemParam;
    }

    @SneakyThrows
    @Override
    public void run() {
        log.info("加载系统参数");
        while (true) {
            log.info("刷新系统参数");
            systemParam.init();
            Thread.sleep(1000 * 30);
        }
    }
}
