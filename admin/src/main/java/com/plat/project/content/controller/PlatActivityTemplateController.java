package com.plat.project.content.controller;

import java.util.List;

import cn.hutool.core.util.RandomUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.plat.framework.aspectj.lang.annotation.Log;
import com.plat.framework.aspectj.lang.enums.BusinessType;
import com.plat.project.content.domain.PlatActivityTemplate;
import com.plat.project.content.service.IPlatActivityTemplateService;
import com.plat.framework.web.controller.BaseController;
import com.plat.framework.web.domain.AjaxResult;
import com.plat.common.utils.poi.ExcelUtil;
import com.plat.framework.web.page.TableDataInfo;

/**
 * 活动样式模板Controller
 * 
 * @author plat
 * @date 2020-08-03
 */
@RestController
@RequestMapping("/content/template")
public class PlatActivityTemplateController extends BaseController
{
    @Autowired
    private IPlatActivityTemplateService platActivityTemplateService;

    /**
     * 查询活动样式模板列表
     */
    @PreAuthorize("@ss.hasPermi('content:template:list')")
    @GetMapping("/list")
    public TableDataInfo list(PlatActivityTemplate platActivityTemplate)
    {
        startPage();
        List<PlatActivityTemplate> list = platActivityTemplateService.selectPlatActivityTemplateList(platActivityTemplate);
        return getDataTable(list);
    }

    /**
     * 导出活动样式模板列表
     */
    @PreAuthorize("@ss.hasPermi('content:template:export')")
    @Log(title = "活动样式模板", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PlatActivityTemplate platActivityTemplate)
    {
        List<PlatActivityTemplate> list = platActivityTemplateService.selectPlatActivityTemplateList(platActivityTemplate);
        ExcelUtil<PlatActivityTemplate> util = new ExcelUtil<PlatActivityTemplate>(PlatActivityTemplate.class);
        return util.exportExcel(list, "template");
    }

    /**
     * 获取活动样式模板详细信息
     */
    @PreAuthorize("@ss.hasPermi('content:template:query')")
    @GetMapping(value = "/{activityId}")
    public AjaxResult getInfo(@PathVariable("activityId") String activityId)
    {
        return AjaxResult.success(platActivityTemplateService.selectPlatActivityTemplateById(activityId));
    }

    /**
     * 新增活动样式模板
     */
    @PreAuthorize("@ss.hasPermi('content:template:add')")
    @Log(title = "活动样式模板", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PlatActivityTemplate platActivityTemplate)
    {
        platActivityTemplate.setActivityId(RandomUtil.randomNumbers(10));
        return toAjax(platActivityTemplateService.insertPlatActivityTemplate(platActivityTemplate));
    }

    /**
     * 修改活动样式模板
     */
    @PreAuthorize("@ss.hasPermi('content:template:edit')")
    @Log(title = "活动样式模板", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PlatActivityTemplate platActivityTemplate)
    {
        return toAjax(platActivityTemplateService.updatePlatActivityTemplate(platActivityTemplate));
    }

    /**
     * 删除活动样式模板
     */
    @PreAuthorize("@ss.hasPermi('content:template:remove')")
    @Log(title = "活动样式模板", businessType = BusinessType.DELETE)
	@DeleteMapping("/{activityIds}")
    public AjaxResult remove(@PathVariable String[] activityIds)
    {
        return toAjax(platActivityTemplateService.deletePlatActivityTemplateByIds(activityIds));
    }
}
