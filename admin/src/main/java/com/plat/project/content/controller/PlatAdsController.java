package com.plat.project.content.controller;

import java.util.List;

import cn.hutool.core.util.IdUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.plat.framework.aspectj.lang.annotation.Log;
import com.plat.framework.aspectj.lang.enums.BusinessType;
import com.plat.project.content.domain.PlatAds;
import com.plat.project.content.service.IPlatAdsService;
import com.plat.framework.web.controller.BaseController;
import com.plat.framework.web.domain.AjaxResult;
import com.plat.common.utils.poi.ExcelUtil;
import com.plat.framework.web.page.TableDataInfo;

/**
 * 广告管理Controller
 * 
 * @author plat
 * @date 2020-06-01
 */
@RestController
@RequestMapping("/content/ads")
public class PlatAdsController extends BaseController
{
    @Autowired
    private IPlatAdsService platAdsService;

    /**
     * 查询广告管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:ads:list')")
    @GetMapping("/list")
    public TableDataInfo list(PlatAds platAds)
    {
        startPage();
        List<PlatAds> list = platAdsService.selectPlatAdsList(platAds);
        return getDataTable(list);
    }

    /**
     * 导出广告管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:ads:export')")
    @Log(title = "广告管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PlatAds platAds)
    {
        List<PlatAds> list = platAdsService.selectPlatAdsList(platAds);
        ExcelUtil<PlatAds> util = new ExcelUtil<PlatAds>(PlatAds.class);
        return util.exportExcel(list, "ads");
    }

    /**
     * 获取广告管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:ads:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(platAdsService.selectPlatAdsById(id));
    }

    /**
     * 新增广告管理
     */
    @PreAuthorize("@ss.hasPermi('system:ads:add')")
    @Log(title = "广告管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PlatAds platAds)
    {
        platAds.setAdCode(IdUtil.objectId());
        return toAjax(platAdsService.insertPlatAds(platAds));
    }

    /**
     * 修改广告管理
     */
    @PreAuthorize("@ss.hasPermi('system:ads:edit')")
    @Log(title = "广告管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PlatAds platAds)
    {
        return toAjax(platAdsService.updatePlatAds(platAds));
    }

    /**
     * 删除广告管理
     */
    @PreAuthorize("@ss.hasPermi('system:ads:remove')")
    @Log(title = "广告管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(platAdsService.deletePlatAdsByIds(ids));
    }
}
