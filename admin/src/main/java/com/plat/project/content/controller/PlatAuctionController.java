package com.plat.project.content.controller;

import java.util.List;

import com.plat.common.constant.GoodsType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.plat.framework.aspectj.lang.annotation.Log;
import com.plat.framework.aspectj.lang.enums.BusinessType;
import com.plat.project.content.domain.PlatAuction;
import com.plat.project.content.service.IPlatAuctionService;
import com.plat.framework.web.controller.BaseController;
import com.plat.framework.web.domain.AjaxResult;
import com.plat.common.utils.poi.ExcelUtil;
import com.plat.framework.web.page.TableDataInfo;

import javax.servlet.http.HttpServletRequest;

/**
 * 拍卖管理Controller
 * 
 * @author plat
 * @date 2020-06-02
 */
@RestController
@RequestMapping("/content/auction")
public class PlatAuctionController extends BaseController
{
    @Autowired
    private IPlatAuctionService platAuctionService;

    /**
     * 查询拍卖管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:auction:list')")
    @GetMapping("/list")
    public TableDataInfo list(PlatAuction platAuction)
    {
        startPage();
        List<PlatAuction> list = platAuctionService.selectPlatAuctionList(platAuction);
        return getDataTable(list);
    }

    /**
     * 导出拍卖管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:auction:export')")
    @Log(title = "拍卖管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PlatAuction platAuction, HttpServletRequest request)
    {
        List<PlatAuction> list = platAuctionService.selectPlatAuctionList(platAuction);
        ExcelUtil<PlatAuction> util = new ExcelUtil<PlatAuction>(PlatAuction.class);
        return util.exportExcel(list, "auction");
    }

    /**
     * 获取拍卖管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:auction:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(platAuctionService.selectPlatAuctionById(id));
    }

    /**
     * 新增拍卖管理
     */
    @PreAuthorize("@ss.hasPermi('system:auction:add')")
    @Log(title = "拍卖管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PlatAuction platAuction)
    { platAuction.setGoodsType(GoodsType.PAIMAI);
        return toAjax(platAuctionService.insertPlatAuction(platAuction));
    }

    /**
     * 修改拍卖管理
     */
    @PreAuthorize("@ss.hasPermi('system:auction:edit')")
    @Log(title = "拍卖管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PlatAuction platAuction)
    {
        return toAjax(platAuctionService.updatePlatAuction(platAuction));
    }

    /**
     * 删除拍卖管理
     */
    @PreAuthorize("@ss.hasPermi('system:auction:remove')")
    @Log(title = "拍卖管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(platAuctionService.deletePlatAuctionByIds(ids));
    }
}
