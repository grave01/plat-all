package com.plat.project.content.controller;

import java.util.List;

import com.plat.common.utils.poi.ExcelUtil;
import com.plat.project.content.domain.PlatBanner;
import com.plat.project.content.service.IPlatBannerService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.plat.framework.aspectj.lang.annotation.Log;
import com.plat.framework.aspectj.lang.enums.BusinessType;

import com.plat.framework.web.controller.BaseController;
import com.plat.framework.web.domain.AjaxResult;
import com.plat.framework.web.page.TableDataInfo;

/**
 * 前台bannerController
 * 
 * @author plat
 * @date 2020-06-02
 */
@RestController
@RequestMapping("/content/banner")
public class PlatBannerController extends BaseController
{
    @Autowired
    private IPlatBannerService platBannerService;

    /**
     * 查询前台banner列表
     */
    @PreAuthorize("@ss.hasPermi('system:banner:list')")
    @GetMapping("/list")
    public TableDataInfo list(PlatBanner platBanner)
    {
        startPage();
        List<PlatBanner> list = platBannerService.selectPlatBannerList(platBanner);
        return getDataTable(list);
    }

    /**
     * 导出前台banner列表
     */
    @PreAuthorize("@ss.hasPermi('system:banner:export')")
    @Log(title = "前台banner", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PlatBanner platBanner)
    {
        List<PlatBanner> list = platBannerService.selectPlatBannerList(platBanner);
        ExcelUtil<PlatBanner> util = new ExcelUtil<PlatBanner>(PlatBanner.class);
        return util.exportExcel(list, "banner");
    }

    /**
     * 获取前台banner详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:banner:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(platBannerService.selectPlatBannerById(id));
    }

    /**上传地址
     * 新增前台banner
     */
    @PreAuthorize("@ss.hasPermi('system:banner:add')")
    @Log(title = "前台banner", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PlatBanner platBanner)
    {
        return toAjax(platBannerService.insertPlatBanner(platBanner));
    }

    /**
     * 修改前台banner
     */
    @PreAuthorize("@ss.hasPermi('system:banner:edit')")
    @Log(title = "前台banner", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PlatBanner platBanner)
    {
        return toAjax(platBannerService.updatePlatBanner(platBanner));
    }

    /**
     * 删除前台banner
     */
    @PreAuthorize("@ss.hasPermi('system:banner:remove')")
    @Log(title = "前台banner", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(platBannerService.deletePlatBannerByIds(ids));
    }
}
