package com.plat.project.content.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.plat.framework.aspectj.lang.annotation.Log;
import com.plat.framework.aspectj.lang.enums.BusinessType;
import com.plat.project.content.domain.PlatCategory;
import com.plat.project.content.service.IPlatCategoryService;
import com.plat.framework.web.controller.BaseController;
import com.plat.framework.web.domain.AjaxResult;
import com.plat.common.utils.poi.ExcelUtil;
import com.plat.framework.web.page.TableDataInfo;

/**
 * 分类Controller
 * 
 * @author plat
 * @date 2020-05-30
 */
@RestController
@RequestMapping("/content/category")
public class PlatCategoryController extends BaseController
{
    @Autowired
    private IPlatCategoryService platCategoryService;

    /**
     * 查询分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(PlatCategory platCategory)
    {
        startPage();
        List<PlatCategory> list = platCategoryService.selectPlatCategoryList(platCategory);
        return getDataTable(list);
    }

    /**
     * 导出分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:category:export')")
    @Log(title = "分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PlatCategory platCategory)
    {
        List<PlatCategory> list = platCategoryService.selectPlatCategoryList(platCategory);
        ExcelUtil<PlatCategory> util = new ExcelUtil<PlatCategory>(PlatCategory.class);
        return util.exportExcel(list, "category");
    }

    /**
     * 获取分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:category:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(platCategoryService.selectPlatCategoryById(id));
    }

    /**
     * 新增分类
     */
    @PreAuthorize("@ss.hasPermi('system:category:add')")
    @Log(title = "分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PlatCategory platCategory)
    {
        return toAjax(platCategoryService.insertPlatCategory(platCategory));
    }

    /**
     * 修改分类
     */
    @PreAuthorize("@ss.hasPermi('system:category:edit')")
    @Log(title = "分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PlatCategory platCategory)
    {
        return toAjax(platCategoryService.updatePlatCategory(platCategory));
    }

    /**
     * 删除分类
     */
    @PreAuthorize("@ss.hasPermi('system:category:remove')")
    @Log(title = "分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(platCategoryService.deletePlatCategoryByIds(ids));
    }
}
