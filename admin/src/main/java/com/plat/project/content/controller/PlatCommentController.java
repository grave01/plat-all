package com.plat.project.content.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.plat.framework.aspectj.lang.annotation.Log;
import com.plat.framework.aspectj.lang.enums.BusinessType;
import com.plat.project.content.domain.PlatComment;
import com.plat.project.content.service.IPlatCommentService;
import com.plat.framework.web.controller.BaseController;
import com.plat.framework.web.domain.AjaxResult;
import com.plat.common.utils.poi.ExcelUtil;
import com.plat.framework.web.page.TableDataInfo;

/**
 * 评论管理Controller
 * 
 * @author plat
 * @date 2020-06-02
 */
@RestController
@RequestMapping("/content/comment")
public class PlatCommentController extends BaseController
{
    @Autowired
    private IPlatCommentService platCommentService;

    /**
     * 查询评论管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:comment:list')")
    @GetMapping("/list")
    public TableDataInfo list(PlatComment platComment)
    {
        startPage();
        List<PlatComment> list = platCommentService.selectPlatCommentList(platComment);
        return getDataTable(list);
    }

    /**
     * 导出评论管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:comment:export')")
    @Log(title = "评论管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PlatComment platComment)
    {
        List<PlatComment> list = platCommentService.selectPlatCommentList(platComment);
        ExcelUtil<PlatComment> util = new ExcelUtil<PlatComment>(PlatComment.class);
        return util.exportExcel(list, "comment");
    }

    /**
     * 获取评论管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:comment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(platCommentService.selectPlatCommentById(id));
    }

    /**
     * 新增评论管理
     */
    @PreAuthorize("@ss.hasPermi('system:comment:add')")
    @Log(title = "评论管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PlatComment platComment)
    {
        return toAjax(platCommentService.insertPlatComment(platComment));
    }

    /**
     * 修改评论管理
     */
    @PreAuthorize("@ss.hasPermi('system:comment:edit')")
    @Log(title = "评论管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PlatComment platComment)
    {
        return toAjax(platCommentService.updatePlatComment(platComment));
    }

    /**
     * 删除评论管理
     */
    @PreAuthorize("@ss.hasPermi('system:comment:remove')")
    @Log(title = "评论管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(platCommentService.deletePlatCommentByIds(ids));
    }
}
