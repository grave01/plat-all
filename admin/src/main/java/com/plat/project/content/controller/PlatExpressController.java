package com.plat.project.content.controller;

import com.plat.api.common.OrderType;
import com.plat.framework.aspectj.lang.annotation.Log;
import com.plat.framework.aspectj.lang.enums.BusinessType;
import com.plat.framework.web.domain.AjaxResult;
import com.plat.project.content.domain.ExpressInfo;
import com.plat.project.content.domain.PlatOrder;
import com.plat.project.content.service.IOrderExpressService;
import com.plat.project.content.service.IPlatOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/2
 * Time: 16:28
 * Description: No Description
 */
@RestController
@RequestMapping("/content/express")
public class PlatExpressController {
    @Autowired
    private IOrderExpressService orderExpressService;
    @Autowired
    private IPlatOrderService platOrderService;
    /**
     * 查看订单发货信息
     */
    @PreAuthorize("@ss.hasPermi('content:order:getOrderExpress')")
    @Log(title = "查看订单发货信息", businessType = BusinessType.OTHER)
    @GetMapping("/getOrderExpress/{orderNo}")
    public AjaxResult getOrderExpress(@PathVariable("orderNo") String orderNo) {
        ExpressInfo expressInfo = orderExpressService.getOrderExpressByOrderNo(orderNo);
        if (expressInfo==null){
            expressInfo = new ExpressInfo();
            expressInfo.setOrderNo(orderNo);
            expressInfo.setIsEdit(0);
        }else {
            expressInfo.setIsEdit(1);
            //查询快递信息
            List<Map<String,Object>> list = new ArrayList<>();
            Map<String,Object> map = new HashMap<>();
            map.put("content","暂无");
            map.put("timestamp","暂无");
            list.add(map);
            expressInfo.setExpressList(list);
        }

        return AjaxResult.success(expressInfo);
    }

    /**
     * 插入发货信息
     */
    @PreAuthorize("@ss.hasPermi('content:order:getOrderExpress')")
    @Log(title = "查看订单详情", businessType = BusinessType.OTHER)
    @PostMapping("/insertOrderExpress")
    public AjaxResult insertOrderExpress(@RequestBody ExpressInfo expressInfo) {
        String orderNo = expressInfo.getOrderNo();
        ExpressInfo expressInfo_1 = orderExpressService.getOrderExpressByOrderNo(orderNo);
        if (expressInfo_1 == null) {
            orderExpressService.insert(expressInfo);
            PlatOrder order =  platOrderService.selectByOrderNo(orderNo);
            order.setExpressStatus(OrderType.ORDER_EXPRESS_DISTRIBUTE);
            platOrderService.updatePlatOrder(order);
            expressInfo.setIsEdit(1);
            return AjaxResult.success("快递发货",expressInfo);
        }else {
            return AjaxResult.error("该笔订单已经存在订单号,不能修改");
        }
    }
}
