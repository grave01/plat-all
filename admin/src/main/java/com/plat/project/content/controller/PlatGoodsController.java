package com.plat.project.content.controller;

import java.util.List;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONObject;
import com.plat.framework.redis.RedisCache;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.plat.framework.aspectj.lang.annotation.Log;
import com.plat.framework.aspectj.lang.enums.BusinessType;
import com.plat.project.content.domain.PlatGoods;
import com.plat.project.content.service.IPlatGoodsService;
import com.plat.framework.web.controller.BaseController;
import com.plat.framework.web.domain.AjaxResult;
import com.plat.common.utils.poi.ExcelUtil;
import com.plat.framework.web.page.TableDataInfo;

/**
 * 商品管理Controller
 *
 * @author plat
 * @date 2020-09-13
 */
@RestController
@RequestMapping("/content/goods")
public class PlatGoodsController extends BaseController {
    @Autowired
    private IPlatGoodsService platGoodsService;
    @Autowired
    private RedisCache redisCache;

    /**
     * 查询商品管理列表
     */
    @PreAuthorize("@ss.hasPermi('content:goods:list')")
    @GetMapping("/list")
    public TableDataInfo list(PlatGoods platGoods) {
        startPage();
        List<PlatGoods> list = platGoodsService.selectPlatGoodsList(platGoods);
        return getDataTable(list);
    }

    /**
     * 导出商品管理列表
     */
    @PreAuthorize("@ss.hasPermi('content:goods:export')")
    @Log(title = "商品管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PlatGoods platGoods) {
        List<PlatGoods> list = platGoodsService.selectPlatGoodsList(platGoods);
        ExcelUtil<PlatGoods> util = new ExcelUtil<PlatGoods>(PlatGoods.class);
        return util.exportExcel(list, "goods");
    }

    /**
     * 获取商品管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('content:goods:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(platGoodsService.selectPlatGoodsById(id));
    }

    /**
     * 新增商品管理
     */
    @PreAuthorize("@ss.hasPermi('content:goods:add')")
    @Log(title = "商品管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PlatGoods platGoods) {
        if (platGoods.getNum() < 0) {
            return AjaxResult.error("数量不能小于0");
        }
        String goodsSn = RandomUtil.randomNumbers(11);
        platGoods.setGoodsSn(goodsSn);
        redisCache.setCacheObject(goodsSn, platGoods.getNum().intValue());
        platGoods.setInitNum(platGoods.getNum());
        return toAjax(platGoodsService.insertPlatGoods(platGoods));
    }

    /**
     * 修改商品管理
     */
    @PreAuthorize("@ss.hasPermi('content:goods:edit')")
    @Log(title = "商品管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PlatGoods platGoods) {
        if (platGoods.getNum() < 0) {
            return AjaxResult.error("数量不能小于0");
        }
        PlatGoods platGoods1 = platGoodsService.selectPlatGoodsById(platGoods.getId());
        Long num = platGoods1.getInitNum() + platGoods.getNum();
        platGoods.setInitNum(num);
        platGoods.setNum(platGoods1.getNum()+platGoods.getNum());
        redisCache.setCacheObject(platGoods1.getGoodsSn(), platGoods.getNum().intValue());

        return toAjax(platGoodsService.updatePlatGoods(platGoods));
    }

    /**
     * 删除商品管理
     */
    @PreAuthorize("@ss.hasPermi('content:goods:remove')")
    @Log(title = "商品管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(platGoodsService.deletePlatGoodsByIds(ids));
    }
}
