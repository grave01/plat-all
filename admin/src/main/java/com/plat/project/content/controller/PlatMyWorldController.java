package com.plat.project.content.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.plat.api.util.StringUtil;
import com.plat.api.util.YmalPropertiesUtil;
import com.plat.project.content.domain.SolveImage;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.plat.framework.aspectj.lang.annotation.Log;
import com.plat.framework.aspectj.lang.enums.BusinessType;
import com.plat.project.content.domain.PlatMyWorld;
import com.plat.project.content.service.IPlatMyWorldService;
import com.plat.framework.web.controller.BaseController;
import com.plat.framework.web.domain.AjaxResult;
import com.plat.common.utils.poi.ExcelUtil;
import com.plat.framework.web.page.TableDataInfo;

/**
 * 圈子管理Controller
 *
 * @author plat
 * @date 2020-06-02
 */
@RestController
@RequestMapping("/content/world")
public class PlatMyWorldController extends BaseController {
    @Autowired
    private IPlatMyWorldService platMyWorldService;

    /**
     * 查询圈子管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:world:list')")
    @GetMapping("/list")
    public TableDataInfo list(PlatMyWorld platMyWorld) {
        startPage();
        List<PlatMyWorld> list = platMyWorldService.selectPlatMyWorldList(platMyWorld);
        List<PlatMyWorld> platMyWorldList = new ArrayList<>();
//        for (int i =0;i<list.size();i++){
//
//            String[] strings = StringUtil.solveString(list.get(i).getUrl()).split(",");
//
//            list.get(i).setUrls(strings);
//            platMyWorldList.add(list.get(i));
//        }
        return getDataTable(list);
    }

    /**
     * 导出圈子管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:world:export')")
    @Log(title = "圈子管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PlatMyWorld platMyWorld) {
        List<PlatMyWorld> list = platMyWorldService.selectPlatMyWorldList(platMyWorld);
        ExcelUtil<PlatMyWorld> util = new ExcelUtil<PlatMyWorld>(PlatMyWorld.class);
        return util.exportExcel(list, "world");
    }

    /**
     * 获取圈子管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:world:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        PlatMyWorld platMyWorld =    platMyWorldService.selectPlatMyWorldById(id);
        String[] strings = StringUtil.solveString(platMyWorld.getUrl()).split(",");

        platMyWorld.setUrls(strings);
        return AjaxResult.success(platMyWorld);
    }

    /**
     * 新增圈子管理
     */
    @PreAuthorize("@ss.hasPermi('system:world:add')")
    @Log(title = "圈子管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PlatMyWorld platMyWorld) {
        return toAjax(platMyWorldService.insertPlatMyWorld(platMyWorld));
    }

    /**
     * 修改圈子管理
     */
    @PreAuthorize("@ss.hasPermi('system:world:edit')")
    @Log(title = "圈子管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PlatMyWorld platMyWorld) {

        return toAjax(platMyWorldService.updatePlatMyWorld(platMyWorld));
    }

    /**
     * 删除圈子管理
     */
    @PreAuthorize("@ss.hasPermi('system:world:remove')")
    @Log(title = "圈子管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(platMyWorldService.deletePlatMyWorldByIds(ids));
    }

    /**
     * 处理违规图片
     */
    @PreAuthorize("@ss.hasPermi('system:world:solve')")
    @Log(title = "处理违规图片", businessType = BusinessType.INSERT)
    @PostMapping("/solve")
    public AjaxResult solve(@RequestBody SolveImage solveImage) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        String url =  solveImage.getUrl();
        Long id = solveImage.getId();
        //PlatMyWorld platMyWorld =    platMyWorldService.selectPlatMyWorldById(id);
        String[] strings = StringUtil.solveString(solveImage.getPlatMyWorld().getUrl()).split(",");
        for (int i= 0; i< strings.length;i++){
            if (strings[i].equals(url)){
                //替换为非法地址
                strings[i] = YmalPropertiesUtil.getValue("illegal.image-url").toString();
            }
        }
        JSONArray jsonArray=new JSONArray(Arrays.asList(strings));
        solveImage.getPlatMyWorld().setUrl(jsonArray.toString());
        solveImage.getPlatMyWorld().setUrls(strings);
       // platMyWorldService.updatePlatMyWorld(platMyWorld);
        return AjaxResult.success(solveImage.getPlatMyWorld());
    }
    /**
     * 处理违规发圈
     */
    @PreAuthorize("@ss.hasPermi('system:world:solveTxt')")
    @Log(title = "处理违规图片", businessType = BusinessType.INSERT)
    @PostMapping("/solveTxt")
    public AjaxResult solveTxt( @RequestBody PlatMyWorld platMyWorld)  {
        platMyWorld.setContent("内容违规已删除");
        return AjaxResult.success(platMyWorld);
    }
}
