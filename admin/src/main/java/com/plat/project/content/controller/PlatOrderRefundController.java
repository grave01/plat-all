package com.plat.project.content.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.plat.framework.aspectj.lang.annotation.Log;
import com.plat.framework.aspectj.lang.enums.BusinessType;
import com.plat.project.content.domain.PlatOrderRefund;
import com.plat.project.content.service.IPlatOrderRefundService;
import com.plat.framework.web.controller.BaseController;
import com.plat.framework.web.domain.AjaxResult;
import com.plat.common.utils.poi.ExcelUtil;
import com.plat.framework.web.page.TableDataInfo;

/**
 * 订单申请Controller
 * 
 * @author plat
 * @date 2020-09-06
 */
@RestController
@RequestMapping("/content/refund")
public class PlatOrderRefundController extends BaseController
{
    @Autowired
    private IPlatOrderRefundService platOrderRefundService;

    /**
     * 查询订单申请列表
     */
    @PreAuthorize("@ss.hasPermi('content:refund:list')")
    @GetMapping("/list")
    public TableDataInfo list(PlatOrderRefund platOrderRefund)
    {
        startPage();
        List<PlatOrderRefund> list = platOrderRefundService.selectPlatOrderRefundList(platOrderRefund);
        return getDataTable(list);
    }

    /**
     * 导出订单申请列表
     */
    @PreAuthorize("@ss.hasPermi('content:refund:export')")
    @Log(title = "订单申请", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PlatOrderRefund platOrderRefund)
    {
        List<PlatOrderRefund> list = platOrderRefundService.selectPlatOrderRefundList(platOrderRefund);
        ExcelUtil<PlatOrderRefund> util = new ExcelUtil<PlatOrderRefund>(PlatOrderRefund.class);
        return util.exportExcel(list, "refund");
    }

    /**
     * 获取订单申请详细信息
     */
    @PreAuthorize("@ss.hasPermi('content:refund:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(platOrderRefundService.selectPlatOrderRefundById(id));
    }

    /**
     * 新增订单申请
     */
    @PreAuthorize("@ss.hasPermi('content:refund:add')")
    @Log(title = "订单申请", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PlatOrderRefund platOrderRefund)
    {
        return toAjax(platOrderRefundService.insertPlatOrderRefund(platOrderRefund));
    }

    /**
     * 修改订单申请
     */
    @PreAuthorize("@ss.hasPermi('content:refund:edit')")
    @Log(title = "订单申请", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PlatOrderRefund platOrderRefund)
    {
        return toAjax(platOrderRefundService.updatePlatOrderRefund(platOrderRefund));
    }

    /**
     * 删除订单申请
     */
    @PreAuthorize("@ss.hasPermi('content:refund:remove')")
    @Log(title = "订单申请", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(platOrderRefundService.deletePlatOrderRefundByIds(ids));
    }
}
