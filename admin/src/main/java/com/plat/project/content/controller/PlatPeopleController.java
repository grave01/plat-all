package com.plat.project.content.controller;

import java.util.ArrayList;
import java.util.List;

import com.plat.project.content.domain.PlatCategory;
import com.plat.project.content.service.IPlatCategoryService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.plat.framework.aspectj.lang.annotation.Log;
import com.plat.framework.aspectj.lang.enums.BusinessType;
import com.plat.project.content.domain.PlatPeople;
import com.plat.project.content.service.IPlatPeopleService;
import com.plat.framework.web.controller.BaseController;
import com.plat.framework.web.domain.AjaxResult;
import com.plat.common.utils.poi.ExcelUtil;
import com.plat.framework.web.page.TableDataInfo;

/**
 * 平台相关的人物Controller
 * 
 * @author plat
 * @date 2020-05-31
 */
@RestController
@RequestMapping("/content/people")
public class PlatPeopleController extends BaseController
{
    @Autowired
    private IPlatPeopleService platPeopleService;
    @Autowired
    private IPlatCategoryService platCategoryService;
    /**
     * 查询平台相关的人物列表
     */
    @PreAuthorize("@ss.hasPermi('system:people:list')")
    @GetMapping("/list")
    public TableDataInfo list(PlatPeople platPeople)
    {
        startPage();
        List<PlatPeople> list = platPeopleService.selectPlatPeopleList(platPeople);
        List<PlatPeople> platPeopleList = new ArrayList<>();
        for (PlatPeople p:list){
            String[] category = p.getCategory().replaceAll("\\[","").replaceAll("]","").split(",");
            StringBuilder  name =new StringBuilder();
            for (String s:category){
                PlatCategory platCategory =  platCategoryService.selectPlatCategoryById(Long.valueOf(s));
                name.append(platCategory.getName())
                        .append("  ");
            }
            p.setCategoryName(name.toString());
            platPeopleList.add(p);
        }
        return getDataTable(platPeopleList);
    }

    /**
     * 导出平台相关的人物列表
     */
    @PreAuthorize("@ss.hasPermi('system:people:export')")
    @Log(title = "平台相关的人物", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PlatPeople platPeople)
    {
        List<PlatPeople> list = platPeopleService.selectPlatPeopleList(platPeople);
        ExcelUtil<PlatPeople> util = new ExcelUtil<PlatPeople>(PlatPeople.class);
        return util.exportExcel(list, "people");
    }

    /**
     * 获取平台相关的人物详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:people:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        PlatPeople platPeople = platPeopleService.selectPlatPeopleById(id);
        String[] category = platPeople.getCategory().replaceAll("\\[","").replaceAll("]","").split(",");
        List<Long> list = new ArrayList<>();
        for (String s:category){
           PlatCategory platCategory =  platCategoryService.selectPlatCategoryById(Long.valueOf(s));
           list.add(platCategory.getId());
        }
        platPeople.setPlatCategory(list);
        return AjaxResult.success(platPeople);
    }

    /**
     * 新增平台相关的人物
     */
    @PreAuthorize("@ss.hasPermi('system:people:add')")
    @Log(title = "平台相关的人物", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PlatPeople platPeople)
    {
        return toAjax(platPeopleService.insertPlatPeople(platPeople));
    }

    /**
     * 修改平台相关的人物
     */
    @PreAuthorize("@ss.hasPermi('system:people:edit')")
    @Log(title = "平台相关的人物", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PlatPeople platPeople)
    {
        return toAjax(platPeopleService.updatePlatPeople(platPeople));
    }

    /**
     * 删除平台相关的人物
     */
    @PreAuthorize("@ss.hasPermi('system:people:remove')")
    @Log(title = "平台相关的人物", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(platPeopleService.deletePlatPeopleByIds(ids));
    }
}
