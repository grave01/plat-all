package com.plat.project.content.controller;

import java.util.List;

import com.alibaba.fastjson.JSONArray;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.plat.framework.aspectj.lang.annotation.Log;
import com.plat.framework.aspectj.lang.enums.BusinessType;
import com.plat.project.content.domain.PlatUser;
import com.plat.project.content.service.IPlatUserService;
import com.plat.framework.web.controller.BaseController;
import com.plat.framework.web.domain.AjaxResult;
import com.plat.common.utils.poi.ExcelUtil;
import com.plat.framework.web.page.TableDataInfo;

/**
 * 用户认证Controller
 *
 * @author plat
 * @date 2020-09-01
 */
@RestController
@RequestMapping("/content/user")
public class PlatUserController extends BaseController {
    @Autowired
    private IPlatUserService platUserService;

    /**
     * 查询用户认证列表
     */
    @PreAuthorize("@ss.hasPermi('content:user:list')")
    @GetMapping("/list")
    public TableDataInfo list(PlatUser platUser) {
        startPage();
        List<PlatUser> list = platUserService.selectPlatUserList(platUser);
        for (PlatUser user : list) {
            JSONArray jsonArray = (JSONArray) JSONArray.parse(user.getIdFront());
            JSONArray jsonArray1 = (JSONArray) JSONArray.parse(user.getIdBack());
            JSONArray jsonArray2 = (JSONArray) JSONArray.parse(user.getBusinessLicense());
            user.setIdFront(jsonArray == null ? "" : jsonArray.get(0).toString());
            user.setIdBack(jsonArray1 == null ? "" : jsonArray1.get(0).toString());
            user.setBusinessLicense(jsonArray2 == null ? "" : jsonArray2.get(0).toString());
        }
        return getDataTable(list);
    }

    /**
     * 导出用户认证列表
     */
    @PreAuthorize("@ss.hasPermi('content:user:export')")
    @Log(title = "用户认证", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PlatUser platUser) {
        List<PlatUser> list = platUserService.selectPlatUserList(platUser);
        ExcelUtil<PlatUser> util = new ExcelUtil<PlatUser>(PlatUser.class);
        return util.exportExcel(list, "user");
    }

    /**
     * 获取用户认证详细信息
     */
    @PreAuthorize("@ss.hasPermi('content:user:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        PlatUser user = platUserService.selectPlatUserById(id);
        JSONArray jsonArray = (JSONArray) JSONArray.parse(user.getIdFront());
        JSONArray jsonArray1 = (JSONArray) JSONArray.parse(user.getIdBack());
        JSONArray jsonArray2 = (JSONArray) JSONArray.parse(user.getBusinessLicense());
        user.setIdFront(jsonArray == null ? "" : jsonArray.get(0).toString());
        user.setIdBack(jsonArray1 == null ? "" : jsonArray1.get(0).toString());
        user.setBusinessLicense(jsonArray2 == null ? "" : jsonArray2.get(0).toString());
        return AjaxResult.success(user);
    }

    /**
     * 新增用户认证
     */
    @PreAuthorize("@ss.hasPermi('content:user:add')")
    @Log(title = "用户认证", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PlatUser platUser) {
        return toAjax(platUserService.insertPlatUser(platUser));
    }

    /**
     * 修改用户认证
     */
    @PreAuthorize("@ss.hasPermi('content:user:edit')")
    @Log(title = "用户认证", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PlatUser platUser) {
        JSONArray ar1 = new JSONArray();
        ar1.add(platUser.getIdFront());
        JSONArray ar2 = new JSONArray();
        ar2.add(platUser.getIdBack());
        JSONArray ar3 = new JSONArray();
        ar3.add(platUser.getBusinessLicense());
        platUser.setIdFront(ar1.toJSONString());
        platUser.setIdBack(ar2.toJSONString());
        platUser.setBusinessLicense(ar3.toJSONString());
        return  toAjax(platUserService.updatePlatUser(platUser));
    }

    /**
     * 删除用户认证
     */
    @PreAuthorize("@ss.hasPermi('content:user:remove')")
    @Log(title = "用户认证", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(platUserService.deletePlatUserByIds(ids));
    }
}
