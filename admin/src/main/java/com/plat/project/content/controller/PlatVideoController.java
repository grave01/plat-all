package com.plat.project.content.controller;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.plat.api.util.StringUtil;
import com.plat.common.constant.GoodsType;
import com.plat.common.utils.poi.ExcelUtil;
import com.plat.project.content.domain.PlatCategory;
import com.plat.project.content.domain.PlatPeople;
import com.plat.project.content.domain.PlatVideo;
import com.plat.project.content.service.IPlatCategoryService;
import com.plat.project.content.service.IPlatPeopleService;
import com.plat.project.content.service.IPlatVideoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.plat.framework.aspectj.lang.annotation.Log;
import com.plat.framework.aspectj.lang.enums.BusinessType;
import com.plat.framework.web.controller.BaseController;
import com.plat.framework.web.domain.AjaxResult;
import com.plat.framework.web.page.TableDataInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户Controller
 *
 * @author plat
 * @date 2020-05-24
 */
@RestController
@RequestMapping("/content/content")
public class PlatVideoController extends BaseController {
    @Autowired
    private IPlatVideoService platVideoService;
    @Autowired
    private IPlatCategoryService platCategoryService;
    @Autowired
    private IPlatPeopleService platPeopleService;

    /**
     * 查询用户列表
     */
    @PreAuthorize("@ss.hasPermi('system:content:list')")
    @GetMapping("/list")
    public TableDataInfo list(PlatVideo platVideo) {
        startPage();
        List<PlatVideo> list = platVideoService.selectPlatVideoList(platVideo);
        List<PlatVideo> platPeopleList = new ArrayList<>();
        for (PlatVideo p : list) {
            String[] category = p.getCategory().replaceAll("\\[", "").replaceAll("]", "").split(",");
            StringBuilder name = new StringBuilder();
            for (String s : category) {
                PlatCategory platCategory = platCategoryService.selectPlatCategoryById(Long.valueOf(s));
                name.append(platCategory.getName())
                        .append("  ");
            }
            p.setCategoryName(name.toString());
            platPeopleList.add(p);
        }
        return getDataTable(list);
    }

    /**
     * 导出用户列表
     */
    @PreAuthorize("@ss.hasPermi('system:content:export')")
    @Log(title = "用户", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PlatVideo platVideo) {
        List<PlatVideo> list = platVideoService.selectPlatVideoList(platVideo);
        ExcelUtil<PlatVideo> util = new ExcelUtil<PlatVideo>(PlatVideo.class);
        return util.exportExcel(list, "video");
    }

    /**
     * 获取用户详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:content:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        PlatVideo platVideo = platVideoService.selectPlatVideoById(id);
        String[] category = platVideo.getCategory().replaceAll("\\[", "").replaceAll("]", "").split(",");
        List<Long> list = new ArrayList<>();
        for (String s : category) {
            PlatCategory platCategory = platCategoryService.selectPlatCategoryById(Long.valueOf(s));
            list.add(platCategory.getId());
        }
        platVideo.setListCategory(list);
        return AjaxResult.success(platVideo);
    }

    /**
     * 新增用户
     */
    @PreAuthorize("@ss.hasPermi('system:content:add')")
    @Log(title = "用户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PlatVideo platVideo) {
        PlatPeople platPeople = platPeopleService.selectPlatPeopleById(platVideo.getCreateUser());
        platVideo.setCreateUsername(platPeople.getUserName());
        platVideo.setGoodsType(GoodsType.VIDEO);
        String videoInfo = HttpUtil.get(platVideo.getUrl()+"?avinfo");
        platVideo.setVideoInfo(videoInfo);
        platVideo.setPoster(StringUtil.solveString(platVideo.getPoster(),"{}"));
        platVideo.setImageId(StringUtil.solveString(platVideo.getImageId(),"{}"));
        platVideo.setCategory(StringUtil.solveString(platVideo.getCategory(),"{}"));
        if (platVideo.getIsFree() != 1) {
            if (platVideo.getPrice() == null || platVideo.getPrice() == 0 || platVideo.getPrice() < 0) {
                return AjaxResult.error("价格不能为空或者小于0");
            }
        }
        return toAjax(platVideoService.insertPlatVideo(platVideo));
    }

    /**
     * 修改用户
     */
    @PreAuthorize("@ss.hasPermi('system:content:edit')")
    @Log(title = "用户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PlatVideo platVideo) {
        String[] category = platVideo.getCategory().replaceAll("\\[", "").replaceAll("]", "").split(",");
        if (category[0].isEmpty()) {
            return AjaxResult.error("至少选择一个分类");
        }
        PlatPeople platPeople = platPeopleService.selectPlatPeopleById(platVideo.getCreateUser());
        platVideo.setCreateUsername(platPeople.getUserName());
        if(platVideo.getUrl().contains("bangnila")||platVideo.getUrl().contains("myvideo")){
            String videoInfo = HttpUtil.get(platVideo.getUrl()+"?avinfo");
            platVideo.setVideoInfo(videoInfo);
        }
        platVideo.setPoster(StringUtil.solveString(platVideo.getPoster(),"{}"));
        platVideo.setImageId(StringUtil.solveString(platVideo.getImageId(),"{}"));
        platVideo.setCategory(StringUtil.solveString(platVideo.getCategory(),"{}"));
        if (platVideo.getIsFree() != 1) {
            if (platVideo.getPrice() == null || platVideo.getPrice() == 0 || platVideo.getPrice() < 0) {
                return AjaxResult.error("价格不能为空或者小于0");
            }
        }
        return toAjax(platVideoService.updatePlatVideo(platVideo));
    }

    /**
     * 删除用户
     */
    @PreAuthorize("@ss.hasPermi('system:content:remove')")
    @Log(title = "用户", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(platVideoService.deletePlatVideoByIds(ids));
    }

}
