package com.plat.project.content.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.plat.framework.aspectj.lang.annotation.Log;
import com.plat.framework.aspectj.lang.enums.BusinessType;
import com.plat.project.content.domain.PlatWechatActivity;
import com.plat.project.content.service.IPlatWechatActivityService;
import com.plat.framework.web.controller.BaseController;
import com.plat.framework.web.domain.AjaxResult;
import com.plat.common.utils.poi.ExcelUtil;
import com.plat.framework.web.page.TableDataInfo;

/**
 * 活动Controller
 * 
 * @author plat
 * @date 2020-08-04
 */
@RestController
@RequestMapping("/content/activity")
public class PlatWechatActivityController extends BaseController
{
    @Autowired
    private IPlatWechatActivityService platWechatActivityService;

    /**
     * 查询活动列表
     */
    @PreAuthorize("@ss.hasPermi('content:activity:list')")
    @GetMapping("/list")
    public TableDataInfo list(PlatWechatActivity platWechatActivity)
    {
        startPage();
        List<PlatWechatActivity> list = platWechatActivityService.selectPlatWechatActivityList(platWechatActivity);
        return getDataTable(list);
    }

    /**
     * 导出活动列表
     */
    @PreAuthorize("@ss.hasPermi('content:activity:export')")
    @Log(title = "活动", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PlatWechatActivity platWechatActivity)
    {
        List<PlatWechatActivity> list = platWechatActivityService.selectPlatWechatActivityList(platWechatActivity);
        ExcelUtil<PlatWechatActivity> util = new ExcelUtil<PlatWechatActivity>(PlatWechatActivity.class);
        return util.exportExcel(list, "activity");
    }

    /**
     * 获取活动详细信息
     */
    @PreAuthorize("@ss.hasPermi('content:activity:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(platWechatActivityService.selectPlatWechatActivityById(id));
    }

    /**
     * 新增活动
     */
    @PreAuthorize("@ss.hasPermi('content:activity:add')")
    @Log(title = "活动", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PlatWechatActivity platWechatActivity)
    {
        return toAjax(platWechatActivityService.insertPlatWechatActivity(platWechatActivity));
    }

    /**
     * 修改活动
     */
    @PreAuthorize("@ss.hasPermi('content:activity:edit')")
    @Log(title = "活动", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PlatWechatActivity platWechatActivity)
    {
        return toAjax(platWechatActivityService.updatePlatWechatActivity(platWechatActivity));
    }

    /**
     * 删除活动
     */
    @PreAuthorize("@ss.hasPermi('content:activity:remove')")
    @Log(title = "活动", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(platWechatActivityService.deletePlatWechatActivityByIds(ids));
    }
}
