package com.plat.project.content.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.plat.framework.aspectj.lang.annotation.Log;
import com.plat.framework.aspectj.lang.enums.BusinessType;
import com.plat.project.content.domain.PlatWechatLiveRoom;
import com.plat.project.content.service.IPlatWechatLiveRoomService;
import com.plat.framework.web.controller.BaseController;
import com.plat.framework.web.domain.AjaxResult;
import com.plat.common.utils.poi.ExcelUtil;
import com.plat.framework.web.page.TableDataInfo;

/**
 * 直播间管理Controller
 *
 * @author plat
 * @date 2020-07-11
 */
@RestController
@RequestMapping("/wechat/live")
public class PlatWechatLiveRoomController extends BaseController
{
    @Autowired
    private IPlatWechatLiveRoomService platWechatLiveRoomService;

    /**
     * 查询直播间管理列表
     */
    @PreAuthorize("@ss.hasPermi('content:live:list')")
    @GetMapping("/list")
    public TableDataInfo list(PlatWechatLiveRoom platWechatLiveRoom)
    {
        startPage();
        List<PlatWechatLiveRoom> list = platWechatLiveRoomService.selectPlatWechatLiveRoomList(platWechatLiveRoom);
        return getDataTable(list);
    }

    /**
     * 导出直播间管理列表
     */
    @PreAuthorize("@ss.hasPermi('content:live:export')")
    @Log(title = "直播间管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PlatWechatLiveRoom platWechatLiveRoom)
    {
        List<PlatWechatLiveRoom> list = platWechatLiveRoomService.selectPlatWechatLiveRoomList(platWechatLiveRoom);
        ExcelUtil<PlatWechatLiveRoom> util = new ExcelUtil<PlatWechatLiveRoom>(PlatWechatLiveRoom.class);
        return util.exportExcel(list, "live");
    }

    /**
     * 获取直播间管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('content:live:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(platWechatLiveRoomService.selectPlatWechatLiveRoomById(id));
    }

    /**
     * 新增直播间管理
     */
    @PreAuthorize("@ss.hasPermi('content:live:add')")
    @Log(title = "直播间管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PlatWechatLiveRoom platWechatLiveRoom)
    {
        return toAjax(platWechatLiveRoomService.insertPlatWechatLiveRoom(platWechatLiveRoom));
    }

    /**
     * 修改直播间管理
     */
    @PreAuthorize("@ss.hasPermi('content:live:edit')")
    @Log(title = "直播间管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PlatWechatLiveRoom platWechatLiveRoom)
    {
        return toAjax(platWechatLiveRoomService.updatePlatWechatLiveRoom(platWechatLiveRoom));
    }

    /**
     * 删除直播间管理
     */
    @PreAuthorize("@ss.hasPermi('content:live:remove')")
    @Log(title = "直播间管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(platWechatLiveRoomService.deletePlatWechatLiveRoomByIds(ids));
    }
}