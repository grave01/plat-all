package com.plat.project.content.domain;

import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/2
 * Time: 16:31
 * Description: No Description
 */
@Data
public class ExpressInfo {
    private Integer id;
    private String orderNo;
    private String info;
    private Date updateTime;
    private Date createTime;
    private String expressNo;
    private String expressPhone;
    private String expressName;
    private String expressImage;
    private String expressCompany;
    private Integer isEdit;//是否已经填写了发货单
    private List<Map<String,Object>> expressList;//快递信息

}