package com.plat.project.content.domain;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/2
 * Time: 14:51
 * Description 订单详细
 */
@Data
public class OrderDetail {

    private String orderNo;
    private Double orderPrice;
    private String userName;
    private String goodsName;
    private String createTime;
    private String payTime;
    private Integer payStatus;
    //发货信息
    private Integer expressInfo;
    private Integer expressStatus;
    //拍卖结束时间
    private String endTime;
    private String payStatusTxt;
    private String expressStatusTxt;
    private String goodsType;
    private String orderType;
    private String goodsDetail;
    private Long goodsId;
}
