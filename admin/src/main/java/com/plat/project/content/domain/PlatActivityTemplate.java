package com.plat.project.content.domain;

import com.plat.framework.aspectj.lang.annotation.Excel;
import com.plat.framework.web.domain.BaseEntity;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 活动样式模板对象 plat_activity_template
 * 
 * @author plat
 * @date 2020-08-03
 */
public class PlatActivityTemplate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 模板id */
    private String activityId;

    /** 样式 */
    @Excel(name = "样式")
    private String itemClass;

    /** 样式图片展示 */
    @Excel(name = "样式图片展示")
    private String imageShow;

    /** 是否可用 */
    @Excel(name = "是否可用")
    private Integer isUse;

    /** 逻辑删除 */
    @Excel(name = "逻辑删除")
    private Integer isDelete;

    public void setActivityId(String activityId) 
    {
        this.activityId = activityId;
    }

    public String getActivityId() 
    {
        return activityId;
    }
    public void setItemClass(String itemClass) 
    {
        this.itemClass = itemClass;
    }

    public String getItemClass() 
    {
        return itemClass;
    }
    public void setImageShow(String imageShow) 
    {
        this.imageShow = imageShow;
    }

    public String getImageShow() 
    {
        return imageShow;
    }
    public void setIsUse(Integer isUse) 
    {
        this.isUse = isUse;
    }

    public Integer getIsUse() 
    {
        return isUse;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("activityId", getActivityId())
            .append("itemClass", getItemClass())
            .append("imageShow", getImageShow())
            .append("isUse", getIsUse())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
