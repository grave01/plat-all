package com.plat.project.content.domain;

import com.plat.framework.aspectj.lang.annotation.Excel;
import com.plat.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 广告管理对象 plat_ads
 * 
 * @author plat
 * @date 2020-06-01
 */
public class PlatAds extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 广告码 */
    @Excel(name = "广告码")
    private String adCode;

    /** 1是视频2是文本 */
    @Excel(name = "1是视频2是文本")
    private Integer type;

    /** 1上架0是下架 */
    @Excel(name = "1上架0是下架")
    private Long adStatus;

    /** 内容 富文本 */
    @Excel(name = "内容 富文本")
    private String content;

    /** 视频地址 */
    @Excel(name = "视频地址")
    private String url;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAdCode(String adCode) 
    {
        this.adCode = adCode;
    }

    public String getAdCode() 
    {
        return adCode;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setAdStatus(Long adStatus) 
    {
        this.adStatus = adStatus;
    }

    public Long getAdStatus() 
    {
        return adStatus;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("adCode", getAdCode())
            .append("type", getType())
            .append("createTime", getCreateTime())
            .append("adStatus", getAdStatus())
            .append("content", getContent())
            .append("url", getUrl())
            .toString();
    }
}
