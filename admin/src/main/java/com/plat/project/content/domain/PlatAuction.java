package com.plat.project.content.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.plat.framework.aspectj.lang.annotation.Excel;
import com.plat.framework.web.domain.BaseEntity;
import lombok.Data;


/**
 * 拍卖管理对象 plat_auction
 * 
 * @author plat
 * @date 2020-06-02
 */
@Data
public class PlatAuction extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 描述 */
    @Excel(name = "描述")
    private String detail;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private String startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private String endTime;

    /** 拍卖底价 */
    @Excel(name = "拍卖底价")
    private Double basePrice;

    /** 拍卖押金 */
    @Excel(name = "拍卖押金")
    private Double securityPrice;

    /** 加价幅度 */
    @Excel(name = "加价幅度")
    private Double rangePrice;

    /** 发布者 */
    @Excel(name = "发布者")
    private Long publisher;

    /** 是否审核 */
    @Excel(name = "是否审核")
    private Integer isCheck;

    /** 上架拍买 */
    @Excel(name = "上架拍买")
    private Long isShopping;

    /** 拍马图片 */
    @Excel(name = "拍马图片")
    private String url;

    /** 参拍人数 */
    @Excel(name = "参拍人数")
    private String num;

    /** 是否结束 */
    @Excel(name = "是否结束")
    private Integer isFinish;

    /** 竞拍得主 */
    @Excel(name = "竞拍得主")
    private Integer auctionOwnerId;

    /** 竞拍得主名称 */
    @Excel(name = "竞拍得主名称")
    private Integer auctionOwnerName;

    /** 拍卖码 */
    @Excel(name = "拍卖码")
    private String auctionCode;

    /**
     * 商品类型 商品类型VIDEO-视频，PAIMAI-拍卖，NORMAL-普通实物
     */
    @Excel(name = "商品类型")
    private String goodsType;

}
