package com.plat.project.content.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.plat.framework.aspectj.lang.annotation.Excel;
import com.plat.framework.web.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 前台banner对象 plat_banner
 * 
 * @author plat
 * @date 2020-06-02
 */
@Data
public class PlatBanner extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 跳转地址 */
    @Excel(name = "跳转地址")
    private String url;

    /** 描述 */
    @Excel(name = "描述")
    private String detail;

    /** 排序 */
    private Long orderNum;

    /** 图片 */
    @Excel(name = "图片")
    private String imageUrl;

    /** 上下架 */
    @Excel(name = "上下架")
    private Long bannerStatus;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 过期时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "过期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expireTime;
    /** 是否是H5 */
    @Excel(name = "是否是H5")
     private Integer isH5;
}
