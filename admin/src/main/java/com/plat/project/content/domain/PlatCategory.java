package com.plat.project.content.domain;

import com.plat.framework.aspectj.lang.annotation.Excel;
import com.plat.framework.web.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 分类对象 plat_category
 * 
 * @author plat
 * @date 2020-05-30
 */
@Data
public class PlatCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 分类名称 */
    @Excel(name = "分类名称")
    private String name;

    /** 分类icon */
    @Excel(name = "分类icon")
    private String url;

    /** 分类ixcxUrl*/
    @Excel(name = "分类xcxUrl")
    private String xcxUrl;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long isDelete;

    /** 顺序 */
    @Excel(name = "顺序")
    private Long orderNum;

    /** 是否是H5 */
    @Excel(name = "是否是H5")
    private Integer isH5;
}
