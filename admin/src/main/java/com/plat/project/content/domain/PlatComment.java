package com.plat.project.content.domain;

import com.plat.framework.aspectj.lang.annotation.Excel;
import com.plat.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 评论管理对象 plat_comment
 * 
 * @author plat
 * @date 2020-06-02
 */
public class PlatComment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 回复id */
    @Excel(name = "回复id")
    private Long replayId;

    /** 评论内容 */
    @Excel(name = "评论内容")
    private String content;

    /** 评论带图 */
    @Excel(name = "评论带图")
    private String photo;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer isDelete;

    /** 是否显示 */
    @Excel(name = "是否显示")
    private Integer isShow;

    /** 评论类型暂时1是圈子，在哪儿显示出来 */
    @Excel(name = "评论类型暂时1是圈子，在哪儿显示出来")
    private Integer comTargetType;

    /** 评论人昵称 */
    @Excel(name = "评论人昵称")
    private String nickname;

    /** 被评论的内容id */
    @Excel(name = "被评论的内容id")
    private Long targetId;

    /** 回复的用户的名称 */
    @Excel(name = "回复的用户的名称")
    private String replyName;

    /** 评论发布者 */
    @Excel(name = "评论发布者")
    private Long ownerId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setReplayId(Long replayId) 
    {
        this.replayId = replayId;
    }

    public Long getReplayId() 
    {
        return replayId;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setPhoto(String photo) 
    {
        this.photo = photo;
    }

    public String getPhoto() 
    {
        return photo;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }
    public void setIsShow(Integer isShow) 
    {
        this.isShow = isShow;
    }

    public Integer getIsShow() 
    {
        return isShow;
    }
    public void setComTargetType(Integer comTargetType) 
    {
        this.comTargetType = comTargetType;
    }

    public Integer getComTargetType() 
    {
        return comTargetType;
    }
    public void setNickname(String nickname) 
    {
        this.nickname = nickname;
    }

    public String getNickname() 
    {
        return nickname;
    }
    public void setTargetId(Long targetId) 
    {
        this.targetId = targetId;
    }

    public Long getTargetId() 
    {
        return targetId;
    }
    public void setReplyName(String replyName) 
    {
        this.replyName = replyName;
    }

    public String getReplyName() 
    {
        return replyName;
    }
    public void setOwnerId(Long ownerId) 
    {
        this.ownerId = ownerId;
    }

    public Long getOwnerId() 
    {
        return ownerId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("replayId", getReplayId())
            .append("createTime", getCreateTime())
            .append("content", getContent())
            .append("photo", getPhoto())
            .append("isDelete", getIsDelete())
            .append("isShow", getIsShow())
            .append("comTargetType", getComTargetType())
            .append("nickname", getNickname())
            .append("targetId", getTargetId())
            .append("replyName", getReplyName())
            .append("ownerId", getOwnerId())
            .toString();
    }
}
