package com.plat.project.content.domain;

import com.plat.framework.aspectj.lang.annotation.Excel;
import com.plat.framework.web.domain.BaseEntity;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 商品管理对象 plat_goods
 * 
 * @author plat
 * @date 2020-09-13
 */
public class PlatGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 商品名 */
    @Excel(name = "商品名")
    private String goodsName;

    /** 商品类型 */
    @Excel(name = "商品类型")
    private Integer goodsType;

    /** 商品说明 */
    @Excel(name = "商品说明")
    private String goodsDesc;

    /** 商品状态 0 待上架 1上架 2 下架 */
    @Excel(name = "商品状态 0 待上架 1上架 2 下架")
    private Integer goodsStatus;

    /** 用户id */
    private Long userId;

    /** 用户名(创建者) */
    private String userName;

    /** 商品价钱 */
    @Excel(name = "商品价钱")
    private Double goodsPrice;

    /** 商品折扣价 */
    @Excel(name = "商品折扣价")
    private Double discountPrice;

    /** 商品图片 */
    @Excel(name = "商品图片")
    private String goodsImage;

    /** 商品明细 */
    @Excel(name = "商品明细")
    private String goodsDeatail;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String goodsSn;

    /** 分类 */
    @Excel(name = "分类")
    private String category;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private Long num;

    /** 初始化库存 */
    @Excel(name = "初始化库存")
    private Long initNum;
    /** 初始化库存 */
    @Excel(name = "运费")
    private Long expressFee;

    /** 详细信息Url */
    @Excel(name = "详细信息Url")
    private Long detailUrl;

    public Long getExpressFee() {
        return expressFee;
    }

    public void setExpressFee(Long expressFee) {
        this.expressFee = expressFee;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setGoodsName(String goodsName) 
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName() 
    {
        return goodsName;
    }
    public void setGoodsType(Integer goodsType) 
    {
        this.goodsType = goodsType;
    }

    public Integer getGoodsType() 
    {
        return goodsType;
    }
    public void setGoodsDesc(String goodsDesc) 
    {
        this.goodsDesc = goodsDesc;
    }

    public String getGoodsDesc() 
    {
        return goodsDesc;
    }
    public void setGoodsStatus(Integer goodsStatus) 
    {
        this.goodsStatus = goodsStatus;
    }

    public Integer getGoodsStatus() 
    {
        return goodsStatus;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setGoodsPrice(Double goodsPrice) 
    {
        this.goodsPrice = goodsPrice;
    }

    public Double getGoodsPrice() 
    {
        return goodsPrice;
    }
    public void setDiscountPrice(Double discountPrice) 
    {
        this.discountPrice = discountPrice;
    }

    public Double getDiscountPrice() 
    {
        return discountPrice;
    }
    public void setGoodsImage(String goodsImage) 
    {
        this.goodsImage = goodsImage;
    }

    public String getGoodsImage() 
    {
        return goodsImage;
    }
    public void setGoodsDeatail(String goodsDeatail) 
    {
        this.goodsDeatail = goodsDeatail;
    }

    public String getGoodsDeatail() 
    {
        return goodsDeatail;
    }
    public void setGoodsSn(String goodsSn) 
    {
        this.goodsSn = goodsSn;
    }

    public String getGoodsSn() 
    {
        return goodsSn;
    }
    public void setCategory(String category) 
    {
        this.category = category;
    }

    public String getCategory() 
    {
        return category;
    }
    public void setNum(Long num) 
    {
        this.num = num;
    }

    public Long getNum() 
    {
        return num;
    }
    public void setInitNum(Long initNum) 
    {
        this.initNum = initNum;
    }

    public Long getInitNum() 
    {
        return initNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsName", getGoodsName())
            .append("goodsType", getGoodsType())
            .append("goodsDesc", getGoodsDesc())
            .append("goodsStatus", getGoodsStatus())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("goodsPrice", getGoodsPrice())
            .append("discountPrice", getDiscountPrice())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("goodsImage", getGoodsImage())
            .append("goodsDeatail", getGoodsDeatail())
            .append("goodsSn", getGoodsSn())
            .append("category", getCategory())
            .append("num", getNum())
            .append("initNum", getInitNum())
            .toString();
    }
}
