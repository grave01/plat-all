package com.plat.project.content.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.plat.framework.aspectj.lang.annotation.Excel;
import com.plat.framework.web.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 圈子管理对象 plat_my_world
 * 
 * @author plat
 * @date 2020-06-02
 */
@Data
public class PlatMyWorld extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 发布的信息 */
    @Excel(name = "发布的信息")
    private String content;

    /** 图片地址json */
    @Excel(name = "图片地址json")
    private String url;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishTime;

    /** 0是未发布1是发布 */
    @Excel(name = "0是未发布1是发布")
    private Long isPublish;

    /** 发布者id */
    @Excel(name = "发布者id")
    private Long userId;

    /** 是否私密 */
    private Long isSelf;

    /** 只允许谁看 */
    @Excel(name = "只允许谁看")
    private String viewUserId;

    /** 不给谁看 */
    @Excel(name = "不给谁看")
    private String noViewUserId;

    /** 阅览人数 */
    @Excel(name = "阅览人数")
    private Long readNum;

    /** 喜欢人数 */
    @Excel(name = "喜欢人数")
    private Long likeNum;

    /** 不喜欢人数 */
    @Excel(name = "不喜欢人数")
    private Long noLikeNum;

    /** 点赞人数 */
    @Excel(name = "点赞人数")
    private Long tapNum;

    /** 是否检查通过 */
    @Excel(name = "是否检查通过")
    private Long isCheck;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long isDelete;

    /** 是否置顶 */
    @Excel(name = "是否置顶")
    private Long isTop;

    /** 是否是广告 */
    @Excel(name = "是否是广告")
    private Long isAdd;
    /** 备注信息 */
    @Excel(name = "备注信息")
     private String remark;
    /** 备注信息 */
    @Excel(name = "备注信息")
    private String[] urls;

   private String phone;

   private String openId;
   private String name;
}
