package com.plat.project.content.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.plat.framework.aspectj.lang.annotation.Excel;
import com.plat.framework.web.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 顶大对象 plat_order
 * 
 * @author plat
 * @date 2020-08-01
 */
@Data
public class PlatOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 订单号码 */
    @Excel(name = "订单号码")
    private String orderNo;

    /** 订单流水 */
    @Excel(name = "订单流水")
    private String orderSn;

    /** 订单金额 */
    @Excel(name = "订单金额")
    private Double orderPrice;

    /** 订单实际金额 */
    @Excel(name = "订单实际金额")
    private Double orderRealPrice;

    /** 顶大优惠金额 */
    @Excel(name = "顶大优惠金额")
    private Double orderCouponPrice;

    /** 订单优惠id */
    @Excel(name = "订单优惠id")
    private Long orderCouponId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 订单状态100完成，200发货，300收货，400退款，500待发货600未发货 */
    @Excel(name = "订单状态100完成，200发货，300收货，400退款，500待发货600未发货")
    private Integer orderStatus;

    /** 支付方式 */
    @Excel(name = "支付方式")
    private String payType;

    /** 支付时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "支付时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date payTime;

    /** 下单手机号 */
    @Excel(name = "下单手机号")
    private String phone;

    /** 配送地址id */
    @Excel(name = "配送地址id")
    private Long deliveryId;

    /** 类型vitual,real  VIRTUAL,REAL */
    @Excel(name = "类型vitual,real  VIRTUAL,REAL")
    private String orderType;

    /** 支付状态1支付0是未支付2是退款 */
    @Excel(name = "支付状态1支付0是未支付2是退款")
    private Long payStatus;

    /** 发货状态 */
    @Excel(name = "发货状态")
    private Integer expressStatus;

    /** 退货状态 */
    @Excel(name = "退货状态")
    private Integer refundStatus;

    /** 下单用户昵称 */
    @Excel(name = "下单用户昵称")
    private String userName;

    /** 最后一次加的价格 */
    @Excel(name = "最后一次加的价格")
    private Double lastAddPrice;


}
