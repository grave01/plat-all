package com.plat.project.content.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.plat.framework.aspectj.lang.annotation.Excel;
import com.plat.framework.web.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 订单申请对象 plat_order_refund
 * 
 * @author plat
 * @date 2020-09-06
 */
@Data
public class PlatOrderRefund extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 主单号 */
    @Excel(name = "主单号")
    private String orderNo;

    /** 退款单号 */
    @Excel(name = "退款单号")
    private String refundNo;

    /** 总金额 */
    @Excel(name = "总金额")
    private Double refundAmt;

    /** 实际退款 */
    @Excel(name = "实际退款")
    private Double refundRealAmt;

    /** 完成时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "完成时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date finishTime;

    /** 退款状态 */
    @Excel(name = "退款状态")
    private Integer refundStatus;

    /** 退款理由 */
    @Excel(name = "退款理由")
    private String refundReason;

    /** 退款信息（全部信息，以json保存） */
    @Excel(name = "退款信息", readConverterExp = "全=部信息，以json保存")
    private String refundInfo;

    /** 申请用户 */
    @Excel(name = "申请用户")
    private Long userId;

    /** 申请用户名 */
    @Excel(name = "申请用户名")
    private String userName;

    /** 拒接理由 */
    @Excel(name = "拒接理由")
    private String rejectReason;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phone;
}
