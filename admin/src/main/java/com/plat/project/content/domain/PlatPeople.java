package com.plat.project.content.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.plat.framework.aspectj.lang.annotation.Excel;
import com.plat.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 平台相关的人物对象 plat_people
 * 
 * @author plat
 * @date 2020-05-31
 */
public class PlatPeople extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 人物名称 */
    @Excel(name = "人物名称")
    private String userName;

    /** 昵称 */
    @Excel(name = "昵称")
    private String nickname;

    /** 分类逗号隔开 */
    @Excel(name = "分类逗号隔开")
    private String category;
    /** 分类逗号隔开 */
    private String categoryName;
    /** 人物描述 */
    @Excel(name = "人物描述")
    private String detail;

    /** 个人照片 */
    @Excel(name = "个人照片")
    private String albumId;

    /** 作品集合 */
    @Excel(name = "作品集合")
    private String workAlbumId;

    /** 绑定系统用户id */
    private Long userId;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phone;

    /** qq */
    @Excel(name = "qq")
    private String qq;

    /** 微信 */
    @Excel(name = "微信")
    private String wx;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 主图 头像url */
    @Excel(name = "主图 头像url")
    private String mainPhoto;

    /** 是否解雇 */
    @Excel(name = "是否解雇")
    private Integer isFire;

    /** 专业 */
    @Excel(name = "专业")
    private String pro;

    /** 生日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birth;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String idNo;

    /** 家庭住址 */
    @Excel(name = "家庭住址")
    private String address;

    /** 目前住址 */
    @Excel(name = "目前住址")
    private String nowAddress;

    private List<Long> platCategory;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<Long> getPlatCategory() {
        return platCategory;
    }

    public void setPlatCategory(List<Long> platCategory) {
        this.platCategory = platCategory;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setNickname(String nickname) 
    {
        this.nickname = nickname;
    }

    public String getNickname() 
    {
        return nickname;
    }
    public void setCategory(String category) 
    {
        this.category = category;
    }

    public String getCategory() 
    {
        return category;
    }
    public void setDetail(String detail) 
    {
        this.detail = detail;
    }

    public String getDetail() 
    {
        return detail;
    }
    public void setAlbumId(String albumId) 
    {
        this.albumId = albumId;
    }

    public String getAlbumId() 
    {
        return albumId;
    }
    public void setWorkAlbumId(String workAlbumId) 
    {
        this.workAlbumId = workAlbumId;
    }

    public String getWorkAlbumId() 
    {
        return workAlbumId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setQq(String qq) 
    {
        this.qq = qq;
    }

    public String getQq() 
    {
        return qq;
    }
    public void setWx(String wx) 
    {
        this.wx = wx;
    }

    public String getWx() 
    {
        return wx;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setMainPhoto(String mainPhoto) 
    {
        this.mainPhoto = mainPhoto;
    }

    public String getMainPhoto() 
    {
        return mainPhoto;
    }
    public void setIsFire(Integer isFire) 
    {
        this.isFire = isFire;
    }

    public Integer getIsFire() 
    {
        return isFire;
    }
    public void setPro(String pro) 
    {
        this.pro = pro;
    }

    public String getPro() 
    {
        return pro;
    }
    public void setBirth(Date birth) 
    {
        this.birth = birth;
    }

    public Date getBirth() 
    {
        return birth;
    }
    public void setIdNo(String idNo)
    {
        this.idNo = idNo;
    }

    public String getIdNo()
    {
        return idNo;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setNowAddress(String nowAddress) 
    {
        this.nowAddress = nowAddress;
    }

    public String getNowAddress() 
    {
        return nowAddress;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userName", getUserName())
            .append("nickname", getNickname())
            .append("category", getCategory())
            .append("detail", getDetail())
            .append("albumId", getAlbumId())
            .append("workAlbumId", getWorkAlbumId())
            .append("userId", getUserId())
            .append("phone", getPhone())
            .append("qq", getQq())
            .append("wx", getWx())
            .append("email", getEmail())
            .append("mainPhoto", getMainPhoto())
            .append("isFire", getIsFire())
            .append("pro", getPro())
            .append("birth", getBirth())
            .append("idNo", getIdNo())
            .append("address", getAddress())
            .append("nowAddress", getNowAddress())
            .toString();
    }
}
