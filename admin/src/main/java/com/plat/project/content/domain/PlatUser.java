package com.plat.project.content.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.plat.framework.aspectj.lang.annotation.Excel;
import com.plat.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 用户认证对象 plat_user
 * 
 * @author plat
 * @date 2020-09-01
 */
@Data
public class PlatUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 昵称 */
    @Excel(name = "昵称")
    private String name;

    /** 微信openid */
    @Excel(name = "微信openid")
    private String openId;

    /** 邮箱 */
    private String email;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phone;

    /** 身份证 */
    @Excel(name = "身份证")
    private String idNo;

    /** 身份证前照 */
    @Excel(name = "身份证前照")
    private String idFront;

    /** 身份证后照 */
    @Excel(name = "身份证后照")
    private String idBack;

    /** 企业执照 */
    @Excel(name = "企业执照")
    private String businessLicense;

    /** 认证状态1是认证2是拒绝0是未认证3是等待认证 */
    @Excel(name = "认证状态1是认证2是拒绝0是未认证3是等待认证")
    private Integer authStatus;

    /** 认证时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "认证时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date authTime;

    /** 微信头像地址 */
    private String wxHeadPhoto;

    /** 昵称 */
    private String nickname;

    /** 密码 */
    private String password;

    /** 账户类型1是微信2是支付宝3是其他 */
    private Integer type;

    /** 是否认证完毕选择is_import其他人物 未认真是普通人，可以人工处理认证 */
    private Integer hasAuth;

    /** 3 普通人 4 公众人物（明星4-1，网红4-2）  6 军人 7 港澳同胞 8 台湾同胞  */
    private String isImport;

    /** 所在地址（认证之后为身份证地址） */
    private String address;

    /** 现居住地 */
    private String nowAddress;

    /** 平台vip会员(购买或者赠送得到) */
    private Integer isVip;

    /** vip等级 */
    private Integer vipLevel;

    /** 拥有勋章（存储json图片地址） */
    private String medal;

    /** 账户 一年范围内改一次 */
    private String account;

    /** 账户索引 这个永不更改（平台唯一标识，非技术员不可见） */
    private String accountIndex;

    /** 微信用户信息 */
    private String userinfo;

    /** 1是男 2是女 0是未知 */
    @Excel(name = "1是男 2是女 0是未知")
    private Integer gender;

    /** 账户是否被锁 */
    private Integer isLock;

    /** 领域 */
    private String field;

    /** 领域id */
    private Long fieldId;

    /** 信息已编辑 */
    private Integer hasEdit;

    /** 是否允许直播 */
    private Integer isAllowLive;

    /** 是否允许发布直播 */
    private Integer isAllowPublishLive;

    /** 认证失败原因 */
    @Excel(name = "认证失败原因")
    private String authFail;

    /** 认证类型: 1 个人 2 企业 3 事业单位 */
    @Excel(name = "认证类型: 0 个人 1 教师 2 企业 3 机构")
    private Integer authType;
    @Excel(name = "允许开直播（1是允许0是不允许）")
    private Integer hasLive;
    @Excel(name = "允许发艺术品（1是允许0是不允许）")
    private Integer  hasYsp ;
    @Excel(name = "允许发圈子（1是允许0是不允许）")
    private Integer hasMyWorld;
}
