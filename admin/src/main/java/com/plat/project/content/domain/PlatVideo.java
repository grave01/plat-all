package com.plat.project.content.domain;

import com.plat.framework.aspectj.lang.annotation.Excel;
import com.plat.framework.web.domain.BaseEntity;
import lombok.Data;

import java.util.List;


/**
 * 用户对象 plat_video
 * 
 * @author plat
 * @date 2020-05-24
 */
@Data
public class PlatVideo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 视频名称 */
    @Excel(name = "视频名称")
    private String videoName;

    /** 地址 */
    @Excel(name = "地址")
    private String url;

    /** 描述 */
    @Excel(name = "描述")
    private String detail;

    /** 是否付费 */
    @Excel(name = "是否付费")
    private Integer isFree;

    /** 原价格 */
    @Excel(name = "原价格")
    private Double price;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer isDelete;

    /** 创建者id */
    @Excel(name = "创建者id")
    private Long createUser;

    /** 创建者 */
    @Excel(name = "创建者")
    private String createUsername;

    /** 分类 */
    @Excel(name = "分类")
    private String category;

    private String categoryName;
    /** 类型 */
    @Excel(name = "类型")
    private Long videoType;

    /** 审核1是审核0是未审核2是拒绝3是下架 */
    @Excel(name = "审核1是审核0是未审核2是拒绝3是下架")
    private Long isCheck;

    /** 是否置顶 */
    @Excel(name = "是否置顶")
    private Integer isTop;

    /** 是否推荐 */
    @Excel(name = "是否推荐")
    private Integer isRecommend;

    /** 广告id */
    @Excel(name = "广告id")
    private String adId;

    /** 免费时长s为单位 */
    @Excel(name = "免费时长s为单位")
    private Long freeTime;

    /** 封面图片 */
    @Excel(name = "封面图片")
    private String poster;

    /** 相片集合 */
    @Excel(name = "相片集合")
    private  String imageId;

    /** 播放量 */
    @Excel(name = "播放量")
    private  Integer num;

    private  List<Long> listCategory;
    /** m3u8地址 */
    @Excel(name = "m3u8地址")
    private String m3u8Url;
    /**
     * 商品类型 商品类型VIDEO-视频，PAIMAI-拍卖，NORMAL-普通实物
     */
    @Excel(name = "商品类型")
    private String goodsType;

    /**
     * 视频信息
     */
    @Excel(name = "视频信息")
    private String videoInfo;
}
