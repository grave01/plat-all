package com.plat.project.content.domain;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/2
 * Time: 2:04
 * Description: 处理违规照片
 */
@Data
public class SolveImage {
    private String url;
    private Long id;
    private PlatMyWorld platMyWorld;
}
