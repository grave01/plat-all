package com.plat.project.content.mapper;

import com.plat.project.content.domain.ExpressInfo;
import io.lettuce.core.dynamic.annotation.Param;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/2
 * Time: 16:30
 * Description: No Description
 */
public interface OrderExpressMapper {
    ExpressInfo getOrderExpressByOrderNo(@Param("orderNo") String orderNo);

    int insert(ExpressInfo expressInfo);
}
