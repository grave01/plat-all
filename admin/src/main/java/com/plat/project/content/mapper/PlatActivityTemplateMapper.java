package com.plat.project.content.mapper;

import java.util.List;
import com.plat.project.content.domain.PlatActivityTemplate;

/**
 * 活动样式模板Mapper接口
 * 
 * @author plat
 * @date 2020-08-03
 */
public interface PlatActivityTemplateMapper 
{
    /**
     * 查询活动样式模板
     * 
     * @param activityId 活动样式模板ID
     * @return 活动样式模板
     */
    public PlatActivityTemplate selectPlatActivityTemplateById(String activityId);

    /**
     * 查询活动样式模板列表
     * 
     * @param platActivityTemplate 活动样式模板
     * @return 活动样式模板集合
     */
    public List<PlatActivityTemplate> selectPlatActivityTemplateList(PlatActivityTemplate platActivityTemplate);

    /**
     * 新增活动样式模板
     * 
     * @param platActivityTemplate 活动样式模板
     * @return 结果
     */
    public int insertPlatActivityTemplate(PlatActivityTemplate platActivityTemplate);

    /**
     * 修改活动样式模板
     * 
     * @param platActivityTemplate 活动样式模板
     * @return 结果
     */
    public int updatePlatActivityTemplate(PlatActivityTemplate platActivityTemplate);

    /**
     * 删除活动样式模板
     * 
     * @param activityId 活动样式模板ID
     * @return 结果
     */
    public int deletePlatActivityTemplateById(String activityId);

    /**
     * 批量删除活动样式模板
     * 
     * @param activityIds 需要删除的数据ID
     * @return 结果
     */
    public int deletePlatActivityTemplateByIds(String[] activityIds);
}
