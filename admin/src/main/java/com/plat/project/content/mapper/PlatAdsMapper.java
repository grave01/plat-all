package com.plat.project.content.mapper;

import java.util.List;
import com.plat.project.content.domain.PlatAds;

/**
 * 广告管理Mapper接口
 * 
 * @author plat
 * @date 2020-06-01
 */
public interface PlatAdsMapper 
{
    /**
     * 查询广告管理
     * 
     * @param id 广告管理ID
     * @return 广告管理
     */
    public PlatAds selectPlatAdsById(Long id);

    /**
     * 查询广告管理列表
     * 
     * @param platAds 广告管理
     * @return 广告管理集合
     */
    public List<PlatAds> selectPlatAdsList(PlatAds platAds);

    /**
     * 新增广告管理
     * 
     * @param platAds 广告管理
     * @return 结果
     */
    public int insertPlatAds(PlatAds platAds);

    /**
     * 修改广告管理
     * 
     * @param platAds 广告管理
     * @return 结果
     */
    public int updatePlatAds(PlatAds platAds);

    /**
     * 删除广告管理
     * 
     * @param id 广告管理ID
     * @return 结果
     */
    public int deletePlatAdsById(Long id);

    /**
     * 批量删除广告管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePlatAdsByIds(Long[] ids);
}
