package com.plat.project.content.mapper;

import com.plat.project.content.domain.PlatBanner;

import java.util.List;


/**
 * 前台bannerMapper接口
 * 
 * @author plat
 * @date 2020-06-02
 */
public interface PlatBannerMapper 
{
    /**
     * 查询前台banner
     * 
     * @param id 前台bannerID
     * @return 前台banner
     */
    public PlatBanner selectPlatBannerById(Long id);

    /**
     * 查询前台banner列表
     * 
     * @param platBanner 前台banner
     * @return 前台banner集合
     */
    public List<PlatBanner> selectPlatBannerList(PlatBanner platBanner);

    /**
     * 新增前台banner
     * 
     * @param platBanner 前台banner
     * @return 结果
     */
    public int insertPlatBanner(PlatBanner platBanner);

    /**
     * 修改前台banner
     * 
     * @param platBanner 前台banner
     * @return 结果
     */
    public int updatePlatBanner(PlatBanner platBanner);

    /**
     * 删除前台banner
     * 
     * @param id 前台bannerID
     * @return 结果
     */
    public int deletePlatBannerById(Long id);

    /**
     * 批量删除前台banner
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePlatBannerByIds(Long[] ids);
}
