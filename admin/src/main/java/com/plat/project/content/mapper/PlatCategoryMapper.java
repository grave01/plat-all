package com.plat.project.content.mapper;

import java.util.List;
import com.plat.project.content.domain.PlatCategory;

/**
 * 分类Mapper接口
 * 
 * @author plat
 * @date 2020-05-30
 */
public interface PlatCategoryMapper 
{
    /**
     * 查询分类
     * 
     * @param id 分类ID
     * @return 分类
     */
    public PlatCategory selectPlatCategoryById(Long id);

    /**
     * 查询分类列表
     * 
     * @param platCategory 分类
     * @return 分类集合
     */
    public List<PlatCategory> selectPlatCategoryList(PlatCategory platCategory);

    /**
     * 新增分类
     * 
     * @param platCategory 分类
     * @return 结果
     */
    public int insertPlatCategory(PlatCategory platCategory);

    /**
     * 修改分类
     * 
     * @param platCategory 分类
     * @return 结果
     */
    public int updatePlatCategory(PlatCategory platCategory);

    /**
     * 删除分类
     * 
     * @param id 分类ID
     * @return 结果
     */
    public int deletePlatCategoryById(Long id);

    /**
     * 批量删除分类
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePlatCategoryByIds(Long[] ids);
}
