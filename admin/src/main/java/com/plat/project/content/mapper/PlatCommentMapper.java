package com.plat.project.content.mapper;

import java.util.List;
import com.plat.project.content.domain.PlatComment;

/**
 * 评论管理Mapper接口
 * 
 * @author plat
 * @date 2020-06-02
 */
public interface PlatCommentMapper 
{
    /**
     * 查询评论管理
     * 
     * @param id 评论管理ID
     * @return 评论管理
     */
    public PlatComment selectPlatCommentById(Long id);

    /**
     * 查询评论管理列表
     * 
     * @param platComment 评论管理
     * @return 评论管理集合
     */
    public List<PlatComment> selectPlatCommentList(PlatComment platComment);

    /**
     * 新增评论管理
     * 
     * @param platComment 评论管理
     * @return 结果
     */
    public int insertPlatComment(PlatComment platComment);

    /**
     * 修改评论管理
     * 
     * @param platComment 评论管理
     * @return 结果
     */
    public int updatePlatComment(PlatComment platComment);

    /**
     * 删除评论管理
     * 
     * @param id 评论管理ID
     * @return 结果
     */
    public int deletePlatCommentById(Long id);

    /**
     * 批量删除评论管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePlatCommentByIds(Long[] ids);
}
