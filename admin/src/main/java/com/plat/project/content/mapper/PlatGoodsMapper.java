package com.plat.project.content.mapper;

import java.util.List;
import com.plat.project.content.domain.PlatGoods;

/**
 * 商品管理Mapper接口
 * 
 * @author plat
 * @date 2020-09-13
 */
public interface PlatGoodsMapper 
{
    /**
     * 查询商品管理
     * 
     * @param id 商品管理ID
     * @return 商品管理
     */
    public PlatGoods selectPlatGoodsById(Long id);

    /**
     * 查询商品管理列表
     * 
     * @param platGoods 商品管理
     * @return 商品管理集合
     */
    public List<PlatGoods> selectPlatGoodsList(PlatGoods platGoods);

    /**
     * 新增商品管理
     * 
     * @param platGoods 商品管理
     * @return 结果
     */
    public int insertPlatGoods(PlatGoods platGoods);

    /**
     * 修改商品管理
     * 
     * @param platGoods 商品管理
     * @return 结果
     */
    public int updatePlatGoods(PlatGoods platGoods);

    /**
     * 删除商品管理
     * 
     * @param id 商品管理ID
     * @return 结果
     */
    public int deletePlatGoodsById(Long id);

    /**
     * 批量删除商品管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePlatGoodsByIds(Long[] ids);
}
