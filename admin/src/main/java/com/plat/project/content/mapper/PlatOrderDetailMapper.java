package com.plat.project.content.mapper;

import com.plat.project.content.domain.OrderDetail;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/2
 * Time: 14:52
 * Description: No Description
 */
public interface PlatOrderDetailMapper {
    List<OrderDetail> getOrderDetail(@Param("orderNo") String orderNo, @Param("orderType") String orderType, @Param("date") Date date);
}
