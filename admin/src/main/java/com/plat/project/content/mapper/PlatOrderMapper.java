package com.plat.project.content.mapper;


import com.plat.project.content.domain.PlatOrder;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 顶大Mapper接口
 * 
 * @author plat
 * @date 2020-08-01
 */
public interface PlatOrderMapper 
{
    /**
     * 查询顶大
     * 
     * @param id 顶大ID
     * @return 顶大
     */
    public PlatOrder selectPlatOrderById(Long id);

    /**
     * 查询顶大列表
     * 
     * @param platOrder 顶大
     * @return 顶大集合
     */
    public List<PlatOrder> selectPlatOrderList(PlatOrder platOrder);

    /**
     * 新增顶大
     * 
     * @param platOrder 顶大
     * @return 结果
     */
    public int insertPlatOrder(PlatOrder platOrder);

    /**
     * 修改顶大
     * 
     * @param platOrder 顶大
     * @return 结果
     */
    public int updatePlatOrder(PlatOrder platOrder);

    /**
     * 删除顶大
     * 
     * @param id 顶大ID
     * @return 结果
     */
    public int deletePlatOrderById(Long id);

    /**
     * 批量删除顶大
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePlatOrderByIds(Long[] ids);

    PlatOrder selectByOrderNo(@Param("orderNo") String orderNo);
}
