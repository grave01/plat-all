package com.plat.project.content.mapper;

import java.util.List;
import com.plat.project.content.domain.PlatOrderRefund;

/**
 * 订单申请Mapper接口
 * 
 * @author plat
 * @date 2020-09-06
 */
public interface PlatOrderRefundMapper 
{
    /**
     * 查询订单申请
     * 
     * @param id 订单申请ID
     * @return 订单申请
     */
    public PlatOrderRefund selectPlatOrderRefundById(Long id);

    /**
     * 查询订单申请列表
     * 
     * @param platOrderRefund 订单申请
     * @return 订单申请集合
     */
    public List<PlatOrderRefund> selectPlatOrderRefundList(PlatOrderRefund platOrderRefund);

    /**
     * 新增订单申请
     * 
     * @param platOrderRefund 订单申请
     * @return 结果
     */
    public int insertPlatOrderRefund(PlatOrderRefund platOrderRefund);

    /**
     * 修改订单申请
     * 
     * @param platOrderRefund 订单申请
     * @return 结果
     */
    public int updatePlatOrderRefund(PlatOrderRefund platOrderRefund);

    /**
     * 删除订单申请
     * 
     * @param id 订单申请ID
     * @return 结果
     */
    public int deletePlatOrderRefundById(Long id);

    /**
     * 批量删除订单申请
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePlatOrderRefundByIds(Long[] ids);

    PlatOrderRefund selectByOrderNo(String orderNo);
}
