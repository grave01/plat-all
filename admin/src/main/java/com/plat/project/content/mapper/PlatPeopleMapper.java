package com.plat.project.content.mapper;

import java.util.List;
import com.plat.project.content.domain.PlatPeople;

/**
 * 平台相关的人物Mapper接口
 * 
 * @author plat
 * @date 2020-05-31
 */
public interface PlatPeopleMapper 
{
    /**
     * 查询平台相关的人物
     * 
     * @param id 平台相关的人物ID
     * @return 平台相关的人物
     */
    public PlatPeople selectPlatPeopleById(Long id);

    /**
     * 查询平台相关的人物列表
     * 
     * @param platPeople 平台相关的人物
     * @return 平台相关的人物集合
     */
    public List<PlatPeople> selectPlatPeopleList(PlatPeople platPeople);

    /**
     * 新增平台相关的人物
     * 
     * @param platPeople 平台相关的人物
     * @return 结果
     */
    public int insertPlatPeople(PlatPeople platPeople);

    /**
     * 修改平台相关的人物
     * 
     * @param platPeople 平台相关的人物
     * @return 结果
     */
    public int updatePlatPeople(PlatPeople platPeople);

    /**
     * 删除平台相关的人物
     * 
     * @param id 平台相关的人物ID
     * @return 结果
     */
    public int deletePlatPeopleById(Long id);

    /**
     * 批量删除平台相关的人物
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePlatPeopleByIds(Long[] ids);
}
