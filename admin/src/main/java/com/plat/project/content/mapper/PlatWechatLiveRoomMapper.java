package com.plat.project.content.mapper;

import java.util.List;
import com.plat.project.content.domain.PlatWechatLiveRoom;

/**
 * 直播间管理Mapper接口
 *
 * @author plat
 * @date 2020-07-11
 */
public interface PlatWechatLiveRoomMapper
{
    /**
     * 查询直播间管理
     *
     * @param id 直播间管理ID
     * @return 直播间管理
     */
    public PlatWechatLiveRoom selectPlatWechatLiveRoomById(Long id);

    /**
     * 查询直播间管理列表
     *
     * @param platWechatLiveRoom 直播间管理
     * @return 直播间管理集合
     */
    public List<PlatWechatLiveRoom> selectPlatWechatLiveRoomList(PlatWechatLiveRoom platWechatLiveRoom);

    /**
     * 新增直播间管理
     *
     * @param platWechatLiveRoom 直播间管理
     * @return 结果
     */
    public int insertPlatWechatLiveRoom(PlatWechatLiveRoom platWechatLiveRoom);

    /**
     * 修改直播间管理
     *
     * @param platWechatLiveRoom 直播间管理
     * @return 结果
     */
    public int updatePlatWechatLiveRoom(PlatWechatLiveRoom platWechatLiveRoom);

    /**
     * 删除直播间管理
     *
     * @param id 直播间管理ID
     * @return 结果
     */
    public int deletePlatWechatLiveRoomById(Long id);

    /**
     * 批量删除直播间管理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePlatWechatLiveRoomByIds(Long[] ids);
}