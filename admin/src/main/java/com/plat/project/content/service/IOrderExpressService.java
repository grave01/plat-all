package com.plat.project.content.service;

import com.plat.project.content.domain.ExpressInfo;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/2
 * Time: 16:29
 * Description: No Description
 */
public interface IOrderExpressService {
    ExpressInfo getOrderExpressByOrderNo(String orderNo);

    int insert(ExpressInfo expressInfo);
}
