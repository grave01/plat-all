package com.plat.project.content.service;

import java.util.List;
import com.plat.project.content.domain.PlatAuction;

/**
 * 拍卖管理Service接口
 * 
 * @author plat
 * @date 2020-06-02
 */
public interface IPlatAuctionService 
{
    /**
     * 查询拍卖管理
     * 
     * @param id 拍卖管理ID
     * @return 拍卖管理
     */
    public PlatAuction selectPlatAuctionById(Long id);

    /**
     * 查询拍卖管理列表
     * 
     * @param platAuction 拍卖管理
     * @return 拍卖管理集合
     */
    public List<PlatAuction> selectPlatAuctionList(PlatAuction platAuction);

    /**
     * 新增拍卖管理
     * 
     * @param platAuction 拍卖管理
     * @return 结果
     */
    public int insertPlatAuction(PlatAuction platAuction);

    /**
     * 修改拍卖管理
     * 
     * @param platAuction 拍卖管理
     * @return 结果
     */
    public int updatePlatAuction(PlatAuction platAuction);

    /**
     * 批量删除拍卖管理
     * 
     * @param ids 需要删除的拍卖管理ID
     * @return 结果
     */
    public int deletePlatAuctionByIds(Long[] ids);

    /**
     * 删除拍卖管理信息
     * 
     * @param id 拍卖管理ID
     * @return 结果
     */
    public int deletePlatAuctionById(Long id);
}
