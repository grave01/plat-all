package com.plat.project.content.service;

import java.util.List;
import com.plat.project.content.domain.PlatMyWorld;

/**
 * 圈子管理Service接口
 * 
 * @author plat
 * @date 2020-06-02
 */
public interface IPlatMyWorldService 
{
    /**
     * 查询圈子管理
     * 
     * @param id 圈子管理ID
     * @return 圈子管理
     */
    public PlatMyWorld selectPlatMyWorldById(Long id);

    /**
     * 查询圈子管理列表
     * 
     * @param platMyWorld 圈子管理
     * @return 圈子管理集合
     */
    public List<PlatMyWorld> selectPlatMyWorldList(PlatMyWorld platMyWorld);

    /**
     * 新增圈子管理
     * 
     * @param platMyWorld 圈子管理
     * @return 结果
     */
    public int insertPlatMyWorld(PlatMyWorld platMyWorld);

    /**
     * 修改圈子管理
     * 
     * @param platMyWorld 圈子管理
     * @return 结果
     */
    public int updatePlatMyWorld(PlatMyWorld platMyWorld);

    /**
     * 批量删除圈子管理
     * 
     * @param ids 需要删除的圈子管理ID
     * @return 结果
     */
    public int deletePlatMyWorldByIds(Long[] ids);

    /**
     * 删除圈子管理信息
     * 
     * @param id 圈子管理ID
     * @return 结果
     */
    public int deletePlatMyWorldById(Long id);
}
