package com.plat.project.content.service;

import com.plat.project.content.domain.OrderDetail;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/2
 * Time: 14:49
 * Description: No Description
 */
public interface IPlatOrderDetailService {
    List<OrderDetail> getOrderDetail(String orderNo,String orderType);
}
