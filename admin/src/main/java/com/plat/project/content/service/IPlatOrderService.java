package com.plat.project.content.service;

import com.plat.project.content.domain.PlatOrder;

import java.util.List;

/**
 * 顶大Service接口
 * 
 * @author plat
 * @date 2020-08-01
 */
public interface IPlatOrderService 
{
    /**
     * 查询顶大
     * 
     * @param id 顶大ID
     * @return 顶大
     */
    public PlatOrder selectPlatOrderById(Long id);

    /**
     * 查询顶大列表
     * 
     * @param platOrder 顶大
     * @return 顶大集合
     */
    public List<PlatOrder> selectPlatOrderList(PlatOrder platOrder);

    /**
     * 新增顶大
     * 
     * @param platOrder 顶大
     * @return 结果
     */
    public int insertPlatOrder(PlatOrder platOrder);

    /**
     * 修改顶大
     * 
     * @param platOrder 顶大
     * @return 结果
     */
    public int updatePlatOrder(PlatOrder platOrder);

    /**
     * 批量删除顶大
     * 
     * @param ids 需要删除的顶大ID
     * @return 结果
     */
    public int deletePlatOrderByIds(Long[] ids);

    /**
     * 删除顶大信息
     * 
     * @param id 顶大ID
     * @return 结果
     */
    public int deletePlatOrderById(Long id);

    /**
     * 删除顶大信息
     * @return orderNo
     */
    PlatOrder selectByOrderNo(String orderNo);
}
