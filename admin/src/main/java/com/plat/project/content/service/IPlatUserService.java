package com.plat.project.content.service;

import java.util.List;
import com.plat.project.content.domain.PlatUser;

/**
 * 用户认证Service接口
 * 
 * @author plat
 * @date 2020-09-01
 */
public interface IPlatUserService 
{
    /**
     * 查询用户认证
     * 
     * @param id 用户认证ID
     * @return 用户认证
     */
    public PlatUser selectPlatUserById(Long id);

    /**
     * 查询用户认证列表
     * 
     * @param platUser 用户认证
     * @return 用户认证集合
     */
    public List<PlatUser> selectPlatUserList(PlatUser platUser);

    /**
     * 新增用户认证
     * 
     * @param platUser 用户认证
     * @return 结果
     */
    public int insertPlatUser(PlatUser platUser);

    /**
     * 修改用户认证
     * 
     * @param platUser 用户认证
     * @return 结果
     */
    public int updatePlatUser(PlatUser platUser);

    /**
     * 批量删除用户认证
     * 
     * @param ids 需要删除的用户认证ID
     * @return 结果
     */
    public int deletePlatUserByIds(Long[] ids);

    /**
     * 删除用户认证信息
     * 
     * @param id 用户认证ID
     * @return 结果
     */
    public int deletePlatUserById(Long id);
}
