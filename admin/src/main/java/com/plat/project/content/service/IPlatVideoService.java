package com.plat.project.content.service;

import com.plat.project.content.domain.PlatVideo;

import java.util.List;


/**
 * 用户Service接口
 * 
 * @author plat
 * @date 2020-05-24
 */
public interface IPlatVideoService 
{
    /**
     * 查询用户
     * 
     * @param id 用户ID
     * @return 用户
     */
    public PlatVideo selectPlatVideoById(Long id);

    /**
     * 查询用户列表
     * 
     * @param platVideo 用户
     * @return 用户集合
     */
    public List<PlatVideo> selectPlatVideoList(PlatVideo platVideo);

    /**
     * 新增用户
     * 
     * @param platVideo 用户
     * @return 结果
     */
    public int insertPlatVideo(PlatVideo platVideo);

    /**
     * 修改用户
     * 
     * @param platVideo 用户
     * @return 结果
     */
    public int updatePlatVideo(PlatVideo platVideo);

    /**
     * 批量删除用户
     * 
     * @param ids 需要删除的用户ID
     * @return 结果
     */
    public int deletePlatVideoByIds(Long[] ids);

    /**
     * 删除用户信息
     * 
     * @param id 用户ID
     * @return 结果
     */
    public int deletePlatVideoById(Long id);
}
