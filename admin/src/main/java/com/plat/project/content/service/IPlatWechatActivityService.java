package com.plat.project.content.service;

import java.util.List;
import com.plat.project.content.domain.PlatWechatActivity;

/**
 * 活动Service接口
 * 
 * @author plat
 * @date 2020-08-04
 */
public interface IPlatWechatActivityService 
{
    /**
     * 查询活动
     * 
     * @param id 活动ID
     * @return 活动
     */
    public PlatWechatActivity selectPlatWechatActivityById(Long id);

    /**
     * 查询活动列表
     * 
     * @param platWechatActivity 活动
     * @return 活动集合
     */
    public List<PlatWechatActivity> selectPlatWechatActivityList(PlatWechatActivity platWechatActivity);

    /**
     * 新增活动
     * 
     * @param platWechatActivity 活动
     * @return 结果
     */
    public int insertPlatWechatActivity(PlatWechatActivity platWechatActivity);

    /**
     * 修改活动
     * 
     * @param platWechatActivity 活动
     * @return 结果
     */
    public int updatePlatWechatActivity(PlatWechatActivity platWechatActivity);

    /**
     * 批量删除活动
     * 
     * @param ids 需要删除的活动ID
     * @return 结果
     */
    public int deletePlatWechatActivityByIds(Long[] ids);

    /**
     * 删除活动信息
     * 
     * @param id 活动ID
     * @return 结果
     */
    public int deletePlatWechatActivityById(Long id);
}
