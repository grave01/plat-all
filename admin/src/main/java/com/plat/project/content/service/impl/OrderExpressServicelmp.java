package com.plat.project.content.service.impl;

import com.plat.project.content.domain.ExpressInfo;
import com.plat.project.content.mapper.OrderExpressMapper;
import com.plat.project.content.service.IOrderExpressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.soap.Addressing;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/2
 * Time: 16:30
 * Description: No Description
 */
@Service
public class OrderExpressServicelmp implements IOrderExpressService {
    @Autowired
    private OrderExpressMapper orderExpressMapper;
    @Override
    public ExpressInfo getOrderExpressByOrderNo(String orderNo) {
        return orderExpressMapper.getOrderExpressByOrderNo(orderNo);
    }

    @Override
    public int insert(ExpressInfo expressInfo) {
        return orderExpressMapper.insert(expressInfo);
    }
}
