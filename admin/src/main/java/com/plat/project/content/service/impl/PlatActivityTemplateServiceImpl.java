package com.plat.project.content.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.plat.project.content.mapper.PlatActivityTemplateMapper;
import com.plat.project.content.domain.PlatActivityTemplate;
import com.plat.project.content.service.IPlatActivityTemplateService;

/**
 * 活动样式模板Service业务层处理
 * 
 * @author plat
 * @date 2020-08-03
 */
@Service
public class PlatActivityTemplateServiceImpl implements IPlatActivityTemplateService 
{
    @Autowired
    private PlatActivityTemplateMapper platActivityTemplateMapper;

    /**
     * 查询活动样式模板
     * 
     * @param activityId 活动样式模板ID
     * @return 活动样式模板
     */
    @Override
    public PlatActivityTemplate selectPlatActivityTemplateById(String activityId)
    {
        return platActivityTemplateMapper.selectPlatActivityTemplateById(activityId);
    }

    /**
     * 查询活动样式模板列表
     * 
     * @param platActivityTemplate 活动样式模板
     * @return 活动样式模板
     */
    @Override
    public List<PlatActivityTemplate> selectPlatActivityTemplateList(PlatActivityTemplate platActivityTemplate)
    {
        return platActivityTemplateMapper.selectPlatActivityTemplateList(platActivityTemplate);
    }

    /**
     * 新增活动样式模板
     * 
     * @param platActivityTemplate 活动样式模板
     * @return 结果
     */
    @Override
    public int insertPlatActivityTemplate(PlatActivityTemplate platActivityTemplate)
    {
        return platActivityTemplateMapper.insertPlatActivityTemplate(platActivityTemplate);
    }

    /**
     * 修改活动样式模板
     * 
     * @param platActivityTemplate 活动样式模板
     * @return 结果
     */
    @Override
    public int updatePlatActivityTemplate(PlatActivityTemplate platActivityTemplate)
    {
        return platActivityTemplateMapper.updatePlatActivityTemplate(platActivityTemplate);
    }

    /**
     * 批量删除活动样式模板
     * 
     * @param activityIds 需要删除的活动样式模板ID
     * @return 结果
     */
    @Override
    public int deletePlatActivityTemplateByIds(String[] activityIds)
    {
        return platActivityTemplateMapper.deletePlatActivityTemplateByIds(activityIds);
    }

    /**
     * 删除活动样式模板信息
     * 
     * @param activityId 活动样式模板ID
     * @return 结果
     */
    @Override
    public int deletePlatActivityTemplateById(String activityId)
    {
        return platActivityTemplateMapper.deletePlatActivityTemplateById(activityId);
    }
}
