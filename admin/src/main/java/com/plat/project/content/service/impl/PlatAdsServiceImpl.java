package com.plat.project.content.service.impl;

import java.util.List;
import com.plat.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.plat.project.content.mapper.PlatAdsMapper;
import com.plat.project.content.domain.PlatAds;
import com.plat.project.content.service.IPlatAdsService;

/**
 * 广告管理Service业务层处理
 * 
 * @author plat
 * @date 2020-06-01
 */
@Service
public class PlatAdsServiceImpl implements IPlatAdsService 
{
    @Autowired
    private PlatAdsMapper platAdsMapper;

    /**
     * 查询广告管理
     * 
     * @param id 广告管理ID
     * @return 广告管理
     */
    @Override
    public PlatAds selectPlatAdsById(Long id)
    {
        return platAdsMapper.selectPlatAdsById(id);
    }

    /**
     * 查询广告管理列表
     * 
     * @param platAds 广告管理
     * @return 广告管理
     */
    @Override
    public List<PlatAds> selectPlatAdsList(PlatAds platAds)
    {
        return platAdsMapper.selectPlatAdsList(platAds);
    }

    /**
     * 新增广告管理
     * 
     * @param platAds 广告管理
     * @return 结果
     */
    @Override
    public int insertPlatAds(PlatAds platAds)
    {
        platAds.setCreateTime(DateUtils.getNowDate());
        return platAdsMapper.insertPlatAds(platAds);
    }

    /**
     * 修改广告管理
     * 
     * @param platAds 广告管理
     * @return 结果
     */
    @Override
    public int updatePlatAds(PlatAds platAds)
    {
        return platAdsMapper.updatePlatAds(platAds);
    }

    /**
     * 批量删除广告管理
     * 
     * @param ids 需要删除的广告管理ID
     * @return 结果
     */
    @Override
    public int deletePlatAdsByIds(Long[] ids)
    {
        return platAdsMapper.deletePlatAdsByIds(ids);
    }

    /**
     * 删除广告管理信息
     * 
     * @param id 广告管理ID
     * @return 结果
     */
    @Override
    public int deletePlatAdsById(Long id)
    {
        return platAdsMapper.deletePlatAdsById(id);
    }
}
