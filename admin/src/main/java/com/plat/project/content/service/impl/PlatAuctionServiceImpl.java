package com.plat.project.content.service.impl;

import java.util.List;
import com.plat.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.plat.project.content.mapper.PlatAuctionMapper;
import com.plat.project.content.domain.PlatAuction;
import com.plat.project.content.service.IPlatAuctionService;

/**
 * 拍卖管理Service业务层处理
 * 
 * @author plat
 * @date 2020-06-02
 */
@Service
public class PlatAuctionServiceImpl implements IPlatAuctionService 
{
    @Autowired
    private PlatAuctionMapper platAuctionMapper;

    /**
     * 查询拍卖管理
     * 
     * @param id 拍卖管理ID
     * @return 拍卖管理
     */
    @Override
    public PlatAuction selectPlatAuctionById(Long id)
    {
        return platAuctionMapper.selectPlatAuctionById(id);
    }

    /**
     * 查询拍卖管理列表
     * 
     * @param platAuction 拍卖管理
     * @return 拍卖管理
     */
    @Override
    public List<PlatAuction> selectPlatAuctionList(PlatAuction platAuction)
    {
        return platAuctionMapper.selectPlatAuctionList(platAuction);
    }

    /**
     * 新增拍卖管理
     * 
     * @param platAuction 拍卖管理
     * @return 结果
     */
    @Override
    public int insertPlatAuction(PlatAuction platAuction)
    {
        platAuction.setCreateTime(DateUtils.getNowDate());
        return platAuctionMapper.insertPlatAuction(platAuction);
    }

    /**
     * 修改拍卖管理
     * 
     * @param platAuction 拍卖管理
     * @return 结果
     */
    @Override
    public int updatePlatAuction(PlatAuction platAuction)
    {
        return platAuctionMapper.updatePlatAuction(platAuction);
    }

    /**
     * 批量删除拍卖管理
     * 
     * @param ids 需要删除的拍卖管理ID
     * @return 结果
     */
    @Override
    public int deletePlatAuctionByIds(Long[] ids)
    {
        return platAuctionMapper.deletePlatAuctionByIds(ids);
    }

    /**
     * 删除拍卖管理信息
     * 
     * @param id 拍卖管理ID
     * @return 结果
     */
    @Override
    public int deletePlatAuctionById(Long id)
    {
        return platAuctionMapper.deletePlatAuctionById(id);
    }
}
