package com.plat.project.content.service.impl;

import java.util.List;

import com.plat.project.content.domain.PlatBanner;
import com.plat.project.content.mapper.PlatBannerMapper;
import com.plat.project.content.service.IPlatBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 前台bannerService业务层处理
 * 
 * @author plat
 * @date 2020-06-02
 */
@Service
public class PlatBannerServiceImpl implements IPlatBannerService
{
    @Autowired
    private PlatBannerMapper platBannerMapper;

    /**
     * 查询前台banner
     * 
     * @param id 前台bannerID
     * @return 前台banner
     */
    @Override
    public PlatBanner selectPlatBannerById(Long id)
    {
        return platBannerMapper.selectPlatBannerById(id);
    }

    /**
     * 查询前台banner列表
     * 
     * @param platBanner 前台banner
     * @return 前台banner
     */
    @Override
    public List<PlatBanner> selectPlatBannerList(PlatBanner platBanner)
    {
        return platBannerMapper.selectPlatBannerList(platBanner);
    }

    /**
     * 新增前台banner
     * 
     * @param platBanner 前台banner
     * @return 结果
     */
    @Override
    public int insertPlatBanner(PlatBanner platBanner)
    {
        return platBannerMapper.insertPlatBanner(platBanner);
    }

    /**
     * 修改前台banner
     * 
     * @param platBanner 前台banner
     * @return 结果
     */
    @Override
    public int updatePlatBanner(PlatBanner platBanner)
    {
        return platBannerMapper.updatePlatBanner(platBanner);
    }

    /**
     * 批量删除前台banner
     * 
     * @param ids 需要删除的前台bannerID
     * @return 结果
     */
    @Override
    public int deletePlatBannerByIds(Long[] ids)
    {
        return platBannerMapper.deletePlatBannerByIds(ids);
    }

    /**
     * 删除前台banner信息
     * 
     * @param id 前台bannerID
     * @return 结果
     */
    @Override
    public int deletePlatBannerById(Long id)
    {
        return platBannerMapper.deletePlatBannerById(id);
    }
}
