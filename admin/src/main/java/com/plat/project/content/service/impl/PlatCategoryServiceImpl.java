package com.plat.project.content.service.impl;

import java.util.List;
import com.plat.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.plat.project.content.mapper.PlatCategoryMapper;
import com.plat.project.content.domain.PlatCategory;
import com.plat.project.content.service.IPlatCategoryService;

/**
 * 分类Service业务层处理
 * 
 * @author plat
 * @date 2020-05-30
 */
@Service
public class PlatCategoryServiceImpl implements IPlatCategoryService 
{
    @Autowired
    private PlatCategoryMapper platCategoryMapper;

    /**
     * 查询分类
     * 
     * @param id 分类ID
     * @return 分类
     */
    @Override
    public PlatCategory selectPlatCategoryById(Long id)
    {
        return platCategoryMapper.selectPlatCategoryById(id);
    }

    /**
     * 查询分类列表
     * 
     * @param platCategory 分类
     * @return 分类
     */
    @Override
    public List<PlatCategory> selectPlatCategoryList(PlatCategory platCategory)
    {
        return platCategoryMapper.selectPlatCategoryList(platCategory);
    }

    /**
     * 新增分类
     * 
     * @param platCategory 分类
     * @return 结果
     */
    @Override
    public int insertPlatCategory(PlatCategory platCategory)
    {
        platCategory.setCreateTime(DateUtils.getNowDate());
        return platCategoryMapper.insertPlatCategory(platCategory);
    }

    /**
     * 修改分类
     * 
     * @param platCategory 分类
     * @return 结果
     */
    @Override
    public int updatePlatCategory(PlatCategory platCategory)
    {
        return platCategoryMapper.updatePlatCategory(platCategory);
    }

    /**
     * 批量删除分类
     * 
     * @param ids 需要删除的分类ID
     * @return 结果
     */
    @Override
    public int deletePlatCategoryByIds(Long[] ids)
    {
        return platCategoryMapper.deletePlatCategoryByIds(ids);
    }

    /**
     * 删除分类信息
     * 
     * @param id 分类ID
     * @return 结果
     */
    @Override
    public int deletePlatCategoryById(Long id)
    {
        return platCategoryMapper.deletePlatCategoryById(id);
    }
}
