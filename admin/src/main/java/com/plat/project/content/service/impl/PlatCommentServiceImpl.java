package com.plat.project.content.service.impl;

import java.util.List;
import com.plat.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.plat.project.content.mapper.PlatCommentMapper;
import com.plat.project.content.domain.PlatComment;
import com.plat.project.content.service.IPlatCommentService;

/**
 * 评论管理Service业务层处理
 * 
 * @author plat
 * @date 2020-06-02
 */
@Service
public class PlatCommentServiceImpl implements IPlatCommentService 
{
    @Autowired
    private PlatCommentMapper platCommentMapper;

    /**
     * 查询评论管理
     * 
     * @param id 评论管理ID
     * @return 评论管理
     */
    @Override
    public PlatComment selectPlatCommentById(Long id)
    {
        return platCommentMapper.selectPlatCommentById(id);
    }

    /**
     * 查询评论管理列表
     * 
     * @param platComment 评论管理
     * @return 评论管理
     */
    @Override
    public List<PlatComment> selectPlatCommentList(PlatComment platComment)
    {
        return platCommentMapper.selectPlatCommentList(platComment);
    }

    /**
     * 新增评论管理
     * 
     * @param platComment 评论管理
     * @return 结果
     */
    @Override
    public int insertPlatComment(PlatComment platComment)
    {
        platComment.setCreateTime(DateUtils.getNowDate());
        return platCommentMapper.insertPlatComment(platComment);
    }

    /**
     * 修改评论管理
     * 
     * @param platComment 评论管理
     * @return 结果
     */
    @Override
    public int updatePlatComment(PlatComment platComment)
    {
        return platCommentMapper.updatePlatComment(platComment);
    }

    /**
     * 批量删除评论管理
     * 
     * @param ids 需要删除的评论管理ID
     * @return 结果
     */
    @Override
    public int deletePlatCommentByIds(Long[] ids)
    {
        return platCommentMapper.deletePlatCommentByIds(ids);
    }

    /**
     * 删除评论管理信息
     * 
     * @param id 评论管理ID
     * @return 结果
     */
    @Override
    public int deletePlatCommentById(Long id)
    {
        return platCommentMapper.deletePlatCommentById(id);
    }
}
