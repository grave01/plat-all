package com.plat.project.content.service.impl;

import java.util.List;
import com.plat.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.plat.project.content.mapper.PlatGoodsMapper;
import com.plat.project.content.domain.PlatGoods;
import com.plat.project.content.service.IPlatGoodsService;

/**
 * 商品管理Service业务层处理
 * 
 * @author plat
 * @date 2020-09-13
 */
@Service
public class PlatGoodsServiceImpl implements IPlatGoodsService 
{
    @Autowired
    private PlatGoodsMapper platGoodsMapper;

    /**
     * 查询商品管理
     * 
     * @param id 商品管理ID
     * @return 商品管理
     */
    @Override
    public PlatGoods selectPlatGoodsById(Long id)
    {
        return platGoodsMapper.selectPlatGoodsById(id);
    }

    /**
     * 查询商品管理列表
     * 
     * @param platGoods 商品管理
     * @return 商品管理
     */
    @Override
    public List<PlatGoods> selectPlatGoodsList(PlatGoods platGoods)
    {
        return platGoodsMapper.selectPlatGoodsList(platGoods);
    }

    /**
     * 新增商品管理
     * 
     * @param platGoods 商品管理
     * @return 结果
     */
    @Override
    public int insertPlatGoods(PlatGoods platGoods)
    {
        platGoods.setCreateTime(DateUtils.getNowDate());
        return platGoodsMapper.insertPlatGoods(platGoods);
    }

    /**
     * 修改商品管理
     * 
     * @param platGoods 商品管理
     * @return 结果
     */
    @Override
    public int updatePlatGoods(PlatGoods platGoods)
    {
        platGoods.setUpdateTime(DateUtils.getNowDate());
        return platGoodsMapper.updatePlatGoods(platGoods);
    }

    /**
     * 批量删除商品管理
     * 
     * @param ids 需要删除的商品管理ID
     * @return 结果
     */
    @Override
    public int deletePlatGoodsByIds(Long[] ids)
    {
        return platGoodsMapper.deletePlatGoodsByIds(ids);
    }

    /**
     * 删除商品管理信息
     * 
     * @param id 商品管理ID
     * @return 结果
     */
    @Override
    public int deletePlatGoodsById(Long id)
    {
        return platGoodsMapper.deletePlatGoodsById(id);
    }
}
