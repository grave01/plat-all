package com.plat.project.content.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.plat.project.content.mapper.PlatMyWorldMapper;
import com.plat.project.content.domain.PlatMyWorld;
import com.plat.project.content.service.IPlatMyWorldService;

/**
 * 圈子管理Service业务层处理
 * 
 * @author plat
 * @date 2020-06-02
 */
@Service
public class PlatMyWorldServiceImpl implements IPlatMyWorldService 
{
    @Autowired
    private PlatMyWorldMapper platMyWorldMapper;

    /**
     * 查询圈子管理
     * 
     * @param id 圈子管理ID
     * @return 圈子管理
     */
    @Override
    public PlatMyWorld selectPlatMyWorldById(Long id)
    {
        return platMyWorldMapper.selectPlatMyWorldById(id);
    }

    /**
     * 查询圈子管理列表
     * 
     * @param platMyWorld 圈子管理
     * @return 圈子管理
     */
    @Override
    public List<PlatMyWorld> selectPlatMyWorldList(PlatMyWorld platMyWorld)
    {
        return platMyWorldMapper.selectPlatMyWorldList(platMyWorld);
    }

    /**
     * 新增圈子管理
     * 
     * @param platMyWorld 圈子管理
     * @return 结果
     */
    @Override
    public int insertPlatMyWorld(PlatMyWorld platMyWorld)
    {
        return platMyWorldMapper.insertPlatMyWorld(platMyWorld);
    }

    /**
     * 修改圈子管理
     * 
     * @param platMyWorld 圈子管理
     * @return 结果
     */
    @Override
    public int updatePlatMyWorld(PlatMyWorld platMyWorld)
    {
        return platMyWorldMapper.updatePlatMyWorld(platMyWorld);
    }

    /**
     * 批量删除圈子管理
     * 
     * @param ids 需要删除的圈子管理ID
     * @return 结果
     */
    @Override
    public int deletePlatMyWorldByIds(Long[] ids)
    {
        return platMyWorldMapper.deletePlatMyWorldByIds(ids);
    }

    /**
     * 删除圈子管理信息
     * 
     * @param id 圈子管理ID
     * @return 结果
     */
    @Override
    public int deletePlatMyWorldById(Long id)
    {
        return platMyWorldMapper.deletePlatMyWorldById(id);
    }
}
