package com.plat.project.content.service.impl;

import com.plat.project.content.domain.OrderDetail;
import com.plat.project.content.mapper.PlatOrderDetailMapper;
import com.plat.project.content.service.IPlatOrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/2
 * Time: 14:49
 * Description: No Description
 */
@Service
public class PlatOrderDetailServicelmp implements IPlatOrderDetailService {
    @Autowired
    private PlatOrderDetailMapper platOrderDetailMapper;
    @Override
    public List<OrderDetail> getOrderDetail(String orderNo,String orderType) {
        return platOrderDetailMapper.getOrderDetail(orderNo,orderType,new Date());
    }
}
