package com.plat.project.content.service.impl;

import java.util.List;
import com.plat.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.plat.project.content.mapper.PlatOrderRefundMapper;
import com.plat.project.content.domain.PlatOrderRefund;
import com.plat.project.content.service.IPlatOrderRefundService;

/**
 * 订单申请Service业务层处理
 * 
 * @author plat
 * @date 2020-09-06
 */
@Service
public class PlatOrderRefundServiceImpl implements IPlatOrderRefundService 
{
    @Autowired
    private PlatOrderRefundMapper platOrderRefundMapper;

    /**
     * 查询订单申请
     * 
     * @param id 订单申请ID
     * @return 订单申请
     */
    @Override
    public PlatOrderRefund selectPlatOrderRefundById(Long id)
    {
        return platOrderRefundMapper.selectPlatOrderRefundById(id);
    }

    /**
     * 查询订单申请列表
     * 
     * @param platOrderRefund 订单申请
     * @return 订单申请
     */
    @Override
    public List<PlatOrderRefund> selectPlatOrderRefundList(PlatOrderRefund platOrderRefund)
    {
        return platOrderRefundMapper.selectPlatOrderRefundList(platOrderRefund);
    }

    /**
     * 新增订单申请
     * 
     * @param platOrderRefund 订单申请
     * @return 结果
     */
    @Override
    public int insertPlatOrderRefund(PlatOrderRefund platOrderRefund)
    {
        platOrderRefund.setCreateTime(DateUtils.getNowDate());
        return platOrderRefundMapper.insertPlatOrderRefund(platOrderRefund);
    }

    /**
     * 修改订单申请
     * 
     * @param platOrderRefund 订单申请
     * @return 结果
     */
    @Override
    public int updatePlatOrderRefund(PlatOrderRefund platOrderRefund)
    {
        return platOrderRefundMapper.updatePlatOrderRefund(platOrderRefund);
    }

    /**
     * 批量删除订单申请
     * 
     * @param ids 需要删除的订单申请ID
     * @return 结果
     */
    @Override
    public int deletePlatOrderRefundByIds(Long[] ids)
    {
        return platOrderRefundMapper.deletePlatOrderRefundByIds(ids);
    }

    /**
     * 删除订单申请信息
     * 
     * @param id 订单申请ID
     * @return 结果
     */
    @Override
    public int deletePlatOrderRefundById(Long id)
    {
        return platOrderRefundMapper.deletePlatOrderRefundById(id);
    }

    @Override
    public PlatOrderRefund selectByOrderNo(String orderNo) {
        return platOrderRefundMapper.selectByOrderNo(orderNo);
    }
}
