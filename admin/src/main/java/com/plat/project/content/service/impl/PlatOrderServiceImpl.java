package com.plat.project.content.service.impl;

import java.util.List;
import com.plat.common.utils.DateUtils;
import com.plat.project.content.domain.PlatOrder;
import com.plat.project.content.mapper.PlatOrderMapper;
import com.plat.project.content.service.IPlatOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 顶大Service业务层处理
 * 
 * @author plat
 * @date 2020-08-01
 */
@Service
public class PlatOrderServiceImpl implements IPlatOrderService
{
    @Autowired
    private PlatOrderMapper platOrderMapper;

    /**
     * 查询顶大
     * 
     * @param id 顶大ID
     * @return 顶大
     */
    @Override
    public PlatOrder selectPlatOrderById(Long id)
    {
        return platOrderMapper.selectPlatOrderById(id);
    }

    /**
     * 查询顶大列表
     * 
     * @param platOrder 顶大
     * @return 顶大
     */
    @Override
    public List<PlatOrder> selectPlatOrderList(PlatOrder platOrder)
    {
        return platOrderMapper.selectPlatOrderList(platOrder);
    }

    /**
     * 新增顶大
     * 
     * @param platOrder 顶大
     * @return 结果
     */
    @Override
    public int insertPlatOrder(PlatOrder platOrder)
    {
        platOrder.setCreateTime(DateUtils.getNowDate());
        return platOrderMapper.insertPlatOrder(platOrder);
    }

    /**
     * 修改顶大
     * 
     * @param platOrder 顶大
     * @return 结果
     */
    @Override
    public int updatePlatOrder(PlatOrder platOrder)
    {
        return platOrderMapper.updatePlatOrder(platOrder);
    }

    /**
     * 批量删除顶大
     * 
     * @param ids 需要删除的顶大ID
     * @return 结果
     */
    @Override
    public int deletePlatOrderByIds(Long[] ids)
    {
        return platOrderMapper.deletePlatOrderByIds(ids);
    }

    /**
     * 删除顶大信息
     * 
     * @param id 顶大ID
     * @return 结果
     */
    @Override
    public int deletePlatOrderById(Long id)
    {
        return platOrderMapper.deletePlatOrderById(id);
    }

    /**
     * 查询 order
     * @return 结果
     */
    @Override
    public PlatOrder selectByOrderNo(String orderNo) {
      return  platOrderMapper.selectByOrderNo(orderNo);
    }
}
