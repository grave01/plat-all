package com.plat.project.content.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.plat.project.content.mapper.PlatPeopleMapper;
import com.plat.project.content.domain.PlatPeople;
import com.plat.project.content.service.IPlatPeopleService;

/**
 * 平台相关的人物Service业务层处理
 * 
 * @author plat
 * @date 2020-05-31
 */
@Service
public class PlatPeopleServiceImpl implements IPlatPeopleService 
{
    @Autowired
    private PlatPeopleMapper platPeopleMapper;

    /**
     * 查询平台相关的人物
     * 
     * @param id 平台相关的人物ID
     * @return 平台相关的人物
     */
    @Override
    public PlatPeople selectPlatPeopleById(Long id)
    {
        return platPeopleMapper.selectPlatPeopleById(id);
    }

    /**
     * 查询平台相关的人物列表
     * 
     * @param platPeople 平台相关的人物
     * @return 平台相关的人物
     */
    @Override
    public List<PlatPeople> selectPlatPeopleList(PlatPeople platPeople)
    {
        return platPeopleMapper.selectPlatPeopleList(platPeople);
    }

    /**
     * 新增平台相关的人物
     * 
     * @param platPeople 平台相关的人物
     * @return 结果
     */
    @Override
    public int insertPlatPeople(PlatPeople platPeople)
    {
        return platPeopleMapper.insertPlatPeople(platPeople);
    }

    /**
     * 修改平台相关的人物
     * 
     * @param platPeople 平台相关的人物
     * @return 结果
     */
    @Override
    public int updatePlatPeople(PlatPeople platPeople)
    {
        return platPeopleMapper.updatePlatPeople(platPeople);
    }

    /**
     * 批量删除平台相关的人物
     * 
     * @param ids 需要删除的平台相关的人物ID
     * @return 结果
     */
    @Override
    public int deletePlatPeopleByIds(Long[] ids)
    {
        return platPeopleMapper.deletePlatPeopleByIds(ids);
    }

    /**
     * 删除平台相关的人物信息
     * 
     * @param id 平台相关的人物ID
     * @return 结果
     */
    @Override
    public int deletePlatPeopleById(Long id)
    {
        return platPeopleMapper.deletePlatPeopleById(id);
    }
}
