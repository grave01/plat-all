package com.plat.project.content.service.impl;

import java.util.List;
import com.plat.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.plat.project.content.mapper.PlatUserMapper;
import com.plat.project.content.domain.PlatUser;
import com.plat.project.content.service.IPlatUserService;

/**
 * 用户认证Service业务层处理
 * 
 * @author plat
 * @date 2020-09-01
 */
@Service
public class PlatUserServiceImpl implements IPlatUserService 
{
    @Autowired
    private PlatUserMapper platUserMapper;

    /**
     * 查询用户认证
     * 
     * @param id 用户认证ID
     * @return 用户认证
     */
    @Override
    public PlatUser selectPlatUserById(Long id)
    {
        return platUserMapper.selectPlatUserById(id);
    }

    /**
     * 查询用户认证列表
     * 
     * @param platUser 用户认证
     * @return 用户认证
     */
    @Override
    public List<PlatUser> selectPlatUserList(PlatUser platUser)
    {
        return platUserMapper.selectPlatUserList(platUser);
    }

    /**
     * 新增用户认证
     * 
     * @param platUser 用户认证
     * @return 结果
     */
    @Override
    public int insertPlatUser(PlatUser platUser)
    {
        platUser.setCreateTime(DateUtils.getNowDate());
        return platUserMapper.insertPlatUser(platUser);
    }

    /**
     * 修改用户认证
     * 
     * @param platUser 用户认证
     * @return 结果
     */
    @Override
    public int updatePlatUser(PlatUser platUser)
    {
        return platUserMapper.updatePlatUser(platUser);
    }

    /**
     * 批量删除用户认证
     * 
     * @param ids 需要删除的用户认证ID
     * @return 结果
     */
    @Override
    public int deletePlatUserByIds(Long[] ids)
    {
        return platUserMapper.deletePlatUserByIds(ids);
    }

    /**
     * 删除用户认证信息
     * 
     * @param id 用户认证ID
     * @return 结果
     */
    @Override
    public int deletePlatUserById(Long id)
    {
        return platUserMapper.deletePlatUserById(id);
    }
}
