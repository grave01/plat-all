package com.plat.project.content.service.impl;

import com.plat.common.utils.DateUtils;
import com.plat.project.content.domain.PlatVideo;
import com.plat.project.content.mapper.PlatVideoMapper;
import com.plat.project.content.service.IPlatVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户Service业务层处理
 * 
 * @author plat
 * @date 2020-05-24
 */
@Service
public class PlatVideoServiceImpl implements IPlatVideoService
{
    @Autowired
    private PlatVideoMapper platVideoMapper;

    /**
     * 查询用户
     * 
     * @param id 用户ID
     * @return 用户
     */
    @Override
    public PlatVideo selectPlatVideoById(Long id)
    {
        return platVideoMapper.selectPlatVideoById(id);
    }

    /**
     * 查询用户列表
     * 
     * @param platVideo 用户
     * @return 用户
     */
    @Override
    public List<PlatVideo> selectPlatVideoList(PlatVideo platVideo)
    {
        return platVideoMapper.selectPlatVideoList(platVideo);
    }

    /**
     * 新增用户
     * 
     * @param platVideo 用户
     * @return 结果
     */
    @Override
    public int insertPlatVideo(PlatVideo platVideo)
    {
        platVideo.setCreateTime(DateUtils.getNowDate());
        return platVideoMapper.insertPlatVideo(platVideo);
    }

    /**
     * 修改用户
     * 
     * @param platVideo 用户
     * @return 结果
     */
    @Override
    public int updatePlatVideo(PlatVideo platVideo)
    {
        platVideo.setUpdateTime(DateUtils.getNowDate());
        return platVideoMapper.updatePlatVideo(platVideo);
    }

    /**
     * 批量删除用户
     * 
     * @param ids 需要删除的用户ID
     * @return 结果
     */
    @Override
    public int deletePlatVideoByIds(Long[] ids)
    {
        return platVideoMapper.deletePlatVideoByIds(ids);
    }

    /**
     * 删除用户信息
     * 
     * @param id 用户ID
     * @return 结果
     */
    @Override
    public int deletePlatVideoById(Long id)
    {
        return platVideoMapper.deletePlatVideoById(id);
    }
}
