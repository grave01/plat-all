package com.plat.project.content.service.impl;

import java.util.List;
import com.plat.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.plat.project.content.mapper.PlatWechatActivityMapper;
import com.plat.project.content.domain.PlatWechatActivity;
import com.plat.project.content.service.IPlatWechatActivityService;

/**
 * 活动Service业务层处理
 * 
 * @author plat
 * @date 2020-08-04
 */
@Service
public class PlatWechatActivityServiceImpl implements IPlatWechatActivityService 
{
    @Autowired
    private PlatWechatActivityMapper platWechatActivityMapper;

    /**
     * 查询活动
     * 
     * @param id 活动ID
     * @return 活动
     */
    @Override
    public PlatWechatActivity selectPlatWechatActivityById(Long id)
    {
        return platWechatActivityMapper.selectPlatWechatActivityById(id);
    }

    /**
     * 查询活动列表
     * 
     * @param platWechatActivity 活动
     * @return 活动
     */
    @Override
    public List<PlatWechatActivity> selectPlatWechatActivityList(PlatWechatActivity platWechatActivity)
    {
        return platWechatActivityMapper.selectPlatWechatActivityList(platWechatActivity);
    }

    /**
     * 新增活动
     * 
     * @param platWechatActivity 活动
     * @return 结果
     */
    @Override
    public int insertPlatWechatActivity(PlatWechatActivity platWechatActivity)
    {
        platWechatActivity.setCreateTime(DateUtils.getNowDate());
        return platWechatActivityMapper.insertPlatWechatActivity(platWechatActivity);
    }

    /**
     * 修改活动
     * 
     * @param platWechatActivity 活动
     * @return 结果
     */
    @Override
    public int updatePlatWechatActivity(PlatWechatActivity platWechatActivity)
    {
        return platWechatActivityMapper.updatePlatWechatActivity(platWechatActivity);
    }

    /**
     * 批量删除活动
     * 
     * @param ids 需要删除的活动ID
     * @return 结果
     */
    @Override
    public int deletePlatWechatActivityByIds(Long[] ids)
    {
        return platWechatActivityMapper.deletePlatWechatActivityByIds(ids);
    }

    /**
     * 删除活动信息
     * 
     * @param id 活动ID
     * @return 结果
     */
    @Override
    public int deletePlatWechatActivityById(Long id)
    {
        return platWechatActivityMapper.deletePlatWechatActivityById(id);
    }
}
