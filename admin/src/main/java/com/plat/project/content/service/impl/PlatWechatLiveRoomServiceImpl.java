package com.plat.project.content.service.impl;

import java.util.List;
import com.plat.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.plat.project.content.mapper.PlatWechatLiveRoomMapper;
import com.plat.project.content.domain.PlatWechatLiveRoom;
import com.plat.project.content.service.IPlatWechatLiveRoomService;

/**
 * 直播间管理Service业务层处理
 *
 * @author plat
 * @date 2020-07-11
 */
@Service
public class PlatWechatLiveRoomServiceImpl implements IPlatWechatLiveRoomService
{
    @Autowired
    private PlatWechatLiveRoomMapper platWechatLiveRoomMapper;

    /**
     * 查询直播间管理
     *
     * @param id 直播间管理ID
     * @return 直播间管理
     */
    @Override
    public PlatWechatLiveRoom selectPlatWechatLiveRoomById(Long id)
    {
        return platWechatLiveRoomMapper.selectPlatWechatLiveRoomById(id);
    }

    /**
     * 查询直播间管理列表
     *
     * @param platWechatLiveRoom 直播间管理
     * @return 直播间管理
     */
    @Override
    public List<PlatWechatLiveRoom> selectPlatWechatLiveRoomList(PlatWechatLiveRoom platWechatLiveRoom)
    {
        return platWechatLiveRoomMapper.selectPlatWechatLiveRoomList(platWechatLiveRoom);
    }

    /**
     * 新增直播间管理
     *
     * @param platWechatLiveRoom 直播间管理
     * @return 结果
     */
    @Override
    public int insertPlatWechatLiveRoom(PlatWechatLiveRoom platWechatLiveRoom)
    {
        platWechatLiveRoom.setCreateTime(DateUtils.getNowDate());
        return platWechatLiveRoomMapper.insertPlatWechatLiveRoom(platWechatLiveRoom);
    }

    /**
     * 修改直播间管理
     *
     * @param platWechatLiveRoom 直播间管理
     * @return 结果
     */
    @Override
    public int updatePlatWechatLiveRoom(PlatWechatLiveRoom platWechatLiveRoom)
    {
        return platWechatLiveRoomMapper.updatePlatWechatLiveRoom(platWechatLiveRoom);
    }

    /**
     * 批量删除直播间管理
     *
     * @param ids 需要删除的直播间管理ID
     * @return 结果
     */
    @Override
    public int deletePlatWechatLiveRoomByIds(Long[] ids)
    {
        return platWechatLiveRoomMapper.deletePlatWechatLiveRoomByIds(ids);
    }

    /**
     * 删除直播间管理信息
     *
     * @param id 直播间管理ID
     * @return 结果
     */
    @Override
    public int deletePlatWechatLiveRoomById(Long id)
    {
        return platWechatLiveRoomMapper.deletePlatWechatLiveRoomById(id);
    }
}