getPayList
===
SELECT
	a.*,b.*,c.* 
FROM
	plat_auction_record AS a
	LEFT JOIN plat_user AS b ON a.user_id = b.id
	LEFT JOIN plat_auction as c ON a.goods_id = c.id
	WHERE a.goods_id  = #id# AND  a.pay_sn IS NOT NULL 
    order by a.id	DESC
    
getPaySumMoney
===
SELECT SUM(this_amount)  FROM plat_auction_record as a WHERE a.this_amount IS NOT NULL AND a.goods_id = #id# and  a.user_id  = #userId#


getMAXAmount
===
SELECT
	SUM( this_amount ) as  last_price,a.user_id 
FROM
	plat_auction_record AS a 
WHERE
	a.this_amount IS NOT NULL 
	AND a.goods_id = #id# 
GROUP BY
	a.user_id 
	LIMIT 1
