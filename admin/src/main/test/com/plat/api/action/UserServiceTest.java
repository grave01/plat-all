package com.plat.api.action;

import cn.hutool.db.ds.simple.SimpleDataSource;
import com.plat.PlatApplication;
import com.plat.api.api.action.UserAction;
import org.beetl.sql.core.*;
import org.beetl.sql.core.db.DBStyle;
import org.beetl.sql.core.db.MySqlStyle;
import org.beetl.sql.ext.DebugInterceptor;
import org.beetl.sql.ext.gen.GenConfig;
import org.beetl.sql.ext.spring4.BeetlSqlDataSource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * author ss
 * createTime 2020/5/9
 * package ${com.dtroad.ieasweb.jfeas}
 ***/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PlatApplication.class)
public class UserServiceTest {
    //    @Autowired
//    private UserService userService;
//
//    @Test
//    public void test() {
//        userService.test();
//    }
    @Test
    public void test() throws Exception {

    }

    public static void main(String[] args) throws Exception {
        BeetlSqlDataSource source = new BeetlSqlDataSource();
        source.setMasterSource(new SimpleDataSource("jdbc:mysql://180.76.179.165:3306/plat?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8",
                "root",
                "u^JWSRcCDcR88#HG",
                "com.mysql.cj.jdbc.Driver"));
        ConnectionSource cs = source;
        //设置数据源
        SQLLoader loader = new ClasspathLoader("/sql");
        DBStyle style = new MySqlStyle();
        style.setNameConversion(new UnderlinedNameConversion());
        SQLManager sqlManager = new SQLManager(style, loader, cs, new UnderlinedNameConversion(), new Interceptor[]{new DebugInterceptor()});
        //sql.genPojoCodeToConsole("userRole"); 快速生成，显示到控制台
        // 或者直接生成java文件
        GenConfig config = new GenConfig();
        config.preferBigDecimal(true);
        //config.setBaseClass("com.plat.api.entity.cms.platUser");
        sqlManager.genPojoCode("plat_order", "com.plat.api.entity.cms", config);
        sqlManager.genSQLFile("plat_order",config);
    }
}