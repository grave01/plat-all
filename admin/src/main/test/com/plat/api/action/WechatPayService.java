package com.plat.api.action;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONObject;
import com.github.binarywang.wxpay.bean.order.WxPayMpOrderResult;
import com.github.binarywang.wxpay.bean.request.WxPayRefundRequest;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.github.binarywang.wxpay.bean.result.WxPayRefundResult;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.util.SignUtils;
import com.plat.PlatApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/20
 * Time: 13:42
 * Description: No Description
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PlatApplication.class)
public class WechatPayService {
    @Autowired
    private WxPayService wxService;

    @Test
    public void test() throws WxPayException {
        WxPayUnifiedOrderRequest request = new WxPayUnifiedOrderRequest();
        request.setOpenid("o5oPn5TxuOmFmf3nmUpFiIMEgRN8");
        request.setBody("测试");
        request.setOutTradeNo(RandomUtil.randomNumbers(16));
        request.setTotalFee(100);
        request.setSpbillCreateIp("0.0.0.0");
        request.setNotifyUrl("https://loan.bangnila.com");
        request.setTradeType("JSAPI");
        WxPayMpOrderResult wxPayMpOrderResult = wxService.createOrder(request);
        System.out.printf(JSONObject.toJSONString(wxPayMpOrderResult));
    }

    @Test
    public void test_1() throws WxPayException {
        WxPayRefundRequest wxPayRefundRequest = new WxPayRefundRequest();
        //支付流水号 order_sn
        wxPayRefundRequest.setTransactionId("");
        //外部订单号 order_no
        wxPayRefundRequest.setOutTradeNo("");
        //退款单号----生成的的退款单号
        wxPayRefundRequest.setOutRefundNo("");
        //原单号金额----原单号金额
        wxPayRefundRequest.setTotalFee(0);
        //原单号金额----原单号金额
        wxPayRefundRequest.setRefundFee(0);
        //退款回调通知
        wxPayRefundRequest.setNotifyUrl("");
        //随机字符串生成
        String nonce_str = RandomUtil.randomNumbers(16);
        wxPayRefundRequest.setNonceStr(nonce_str);
        wxPayRefundRequest.setSign(wxPayRefundRequest.getSign());
        wxPayRefundRequest.setSignType("MD5");
        WxPayRefundResult refund = wxService.refund(wxPayRefundRequest);
    }
    @Test
    public void share() throws Exception {

    }

    /**
     * 获取水印文字总长度
     *@paramwaterMarkContent水印的文字
     *@paramg
     *@return水印文字总长度
     */
    public static int getWatermarkLength(String waterMarkContent, Graphics g) {
        return g.getFontMetrics(g.getFont()).charsWidth(waterMarkContent.toCharArray(),0, waterMarkContent.length());
    }
    public static int getCharLen(char c, Graphics g) {
        return g.getFontMetrics(g.getFont()).charWidth(c);
    }
    public static void main(String[] args) throws IOException {
        //创建图片
        BufferedImage img = new BufferedImage(750, 1334, BufferedImage.TYPE_INT_RGB);
        //开启画图
        Graphics g = img.getGraphics();
        //背景 -- 读取互联网图片
        BufferedImage back  = ImageIO.read(new URL("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1484582668,772122840&fm=26&gp=0.jpg"));
        g.drawImage(back.getScaledInstance(750, 1334, Image.SCALE_DEFAULT), 0, 0, null); // 绘制缩小后的图
        //商品  banner图
        //读取互联网图片
        BufferedImage priductUrl = ImageIO.read(new URL("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1600665537736&di=d65f3bd9b31a0205a7b6db80a7604f72&imgtype=0&src=http%3A%2F%2Fn.sinaimg.cn%2Fsinacn17%2F287%2Fw1610h1077%2F20180730%2Ff1b9-hhacrcc6229658.jpg")); //TODO
        g.drawImage(priductUrl.getScaledInstance(690,516,Image.SCALE_DEFAULT),29,61,null);
        //文案标题
        g.setFont(new Font("微软雅黑", Font.BOLD, 34));
        g.setColor(new Color(29,29,29));
        //绘制文字
        g.drawString("填写文案标题", 31, 638);//TODO
        //文案
        g.setFont(new Font("微软雅黑", Font.PLAIN, 30));
        g.setColor(new Color(47,47,47));
        int fontlen = getWatermarkLength("填写文内容", g);//TODO
        //文字长度相对于图片宽度应该有多少行
        int line = fontlen / (back.getWidth() - 90);
        //高度
        int y = back.getHeight() - (line + 1) * 30 - 500;
        //文字叠加,自动换行叠加
        int tempX = 32;
        int tempY = y;
        //单字符长度
        int tempCharLen = 0;
        //单行字符总长度临时计算
        int tempLineLen = 0;
        StringBuffer sb =new StringBuffer();
        for(int i=0; i < "填写文内容".length(); i++) {//TODO
            char tempChar = "填写文内容".charAt(i);//TODO
            tempCharLen = getCharLen(tempChar, g);
            tempLineLen += tempCharLen;
            if(tempLineLen >= (back.getWidth()-90)) {
                //长度已经满一行,进行文字叠加
                g.drawString(sb.toString(), tempX, tempY + 50);
                //清空内容,重新追加
                sb.delete(0, sb.length());
                //每行文字间距50
                tempY += 50;
                tempLineLen =0;
            }
            //追加字符
            sb.append(tempChar);
        }
        //最后叠加余下的文字
        g.drawString(sb.toString(), tempX, tempY + 50);

        //价格背景
        //读取互联网图片
        BufferedImage bground  = ImageIO.read(new URL("https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2963502002,526075463&fm=26&gp=0.jpg"));//TODO
        // 绘制缩小后的图
        g.drawImage(bground.getScaledInstance(160, 40, Image.SCALE_DEFAULT), 30, 1053, null);

        //限时促销价
        g.setFont(new Font("微软雅黑", Font.PLAIN, 24));
        g.setColor(new Color(255,255,255));
        g.drawString("限时促销价", 50, 1080);

        //价格
        g.setFont(new Font("微软雅黑", Font.PLAIN, 50));
        g.setColor(new Color(249, 64, 64));
        g.drawString("¥" + "填写商品的价格", 29, 1162);//TODO

        //原价
        g.setFont(new Font("微软雅黑", Font.PLAIN, 36));
        g.setColor(new Color(171,171,171));
        String price = "¥" + "填写商品的原价";//TODO
        g.drawString(price, 260, 1160);
        g.drawLine(250,1148,260+150,1148);

        //商品名称
        g.setFont(new Font("微软雅黑", Font.PLAIN, 32));
        g.setColor(new Color(29,29,29));
        g.drawString("填写商品名称", 30, 1229);//TODO

        //生成二维码返回链接
        String url = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1600665455065&di=2d3e041b6f49dd169956214e1408f2fc&imgtype=0&src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F01%2F37%2F19%2F19573c400cdd9b2.jpg";//TODO
        //读取互联网图片
        BufferedImage qrCode  = ImageIO.read(new URL(url));
        // 绘制缩小后的图
        g.drawImage(qrCode.getScaledInstance(174, 174, Image.SCALE_DEFAULT), 536, 1057, null);

        //二维码字体
        g.setFont(new Font("微软雅黑", Font.PLAIN, 25));
        g.setColor(new Color(171,171,171));
        //绘制文字
        g.drawString("扫描或长按小程序码", 515, 1260);

        g.dispose();
        //保存到本地 生成文件名字
        String iconKey = RandomUtil.randomString(32) +".png"; //TODO
        //先将画好的海报写到本地
        String picUrl = "F:/2345Downloads/" + iconKey; // TODO
        File file = new File(picUrl);
        ImageIO.write(img, "jpg",file);
        //再将file上传至七牛返回链接存入数据库
        //end
        // TODO
    }
}
