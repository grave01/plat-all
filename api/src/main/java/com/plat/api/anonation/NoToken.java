package com.plat.api.anonation;

import java.lang.annotation.*;

/**
 * author ss
 * createTime 2020/5/8
 * package ${com.example.demo.anonation}
 ***/
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NoToken {
}
