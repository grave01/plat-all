package com.plat.api.api.action;

import java.util.Date;

import com.google.common.collect.Maps;

import com.alibaba.fastjson.JSONObject;
import com.plat.api.dao.api.PlatAddressDao;
import com.plat.api.dao.api.PlatExpressAddressDao;
import com.plat.api.dao.api.TokenDao;
import com.plat.api.entity.api.PlatAddressEntity;
import com.plat.api.entity.api.PlatExpressAddressEntity;
import com.plat.api.entity.api.TokenEntity;
import com.plat.api.exception.PlatException;
import io.swagger.models.auth.In;
import org.beetl.sql.core.query.LambdaQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/30
 * Time: 18:03
 * Description: No Description
 */
@Service
public class AddressAction {
    @Autowired
    private TokenDao tokenDao;
    @Autowired
    private PlatAddressDao platAddressDao;
    @Autowired
    private PlatExpressAddressDao expressAddressDao;

    public Object getList(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        String token = jsonObject.getJSONObject("head").getString("token");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", token).single();
        LambdaQuery lambdaQuery = expressAddressDao.createLambdaQuery().andEq("user_id", tokenEntity.getUserId());
        List<PlatExpressAddressEntity> list = lambdaQuery.page(1, 100).getList();
        return list;
    }

    //添加地址
    public Object addOrUpdate(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        Integer id = body.getInteger("id");
        String token = jsonObject.getJSONObject("head").getString("token");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", token).single();
        LambdaQuery lambdaQuery = expressAddressDao.createLambdaQuery().andEq("user_id", tokenEntity.getUserId());
        List<PlatExpressAddressEntity> list = lambdaQuery.page(1, 100).getList();
        if (list.size() == 10) {
            throw new PlatException("最多可添加10个收货地址");
        }
        PlatExpressAddressEntity addressEntity = null;
        if (id != null) {
            addressEntity = expressAddressDao.unique(id);
        }
        if (addressEntity == null) {
            addressEntity = new PlatExpressAddressEntity();
            addressEntity.setConsignee(body.getString("consignee"));
            addressEntity.setMobile(body.getString("mobile"));
            addressEntity.setTransportDay(body.getString("transportDay"));
            addressEntity.setProvinceName(body.getString("provinceName"));
            addressEntity.setCityName(body.getString("cityName"));
            addressEntity.setCountyName(body.getString("countyName"));
            addressEntity.setAddress(body.getString("address"));
            addressEntity.setIsDefault(0);
            addressEntity.setUserId(tokenEntity.getUserId());
        }
        int num = 0;
        if (id == null) {
            expressAddressDao.insert(addressEntity);
        } else {
            num = expressAddressDao.updateById(addressEntity);
            if (num == 0) {
                throw new PlatException("更新失败");
            }
        }

        JSONObject json = new JSONObject();
        json.put("object", addressEntity);
        return json;
    }

    public Object del(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        Integer id = body.getInteger("id");
        expressAddressDao.deleteById(id);
        return true;
    }

    public Object setDefault(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        Integer id = body.getInteger("id");
        String token = jsonObject.getJSONObject("head").getString("token");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", token).single();
        LambdaQuery lambdaQuery = expressAddressDao.createLambdaQuery().andEq("user_id", tokenEntity.getUserId());
        List<PlatExpressAddressEntity> list = lambdaQuery.page(1, 100).getList();
        PlatExpressAddressEntity addressEntity = new PlatExpressAddressEntity();
        for (PlatExpressAddressEntity platExpressAddressEntity : list) {
            if (platExpressAddressEntity.getId().equals(id)) {
                platExpressAddressEntity.setIsDefault(1);
                addressEntity = platExpressAddressEntity;
            } else {
                platExpressAddressEntity.setIsDefault(0);
            }
            expressAddressDao.updateById(platExpressAddressEntity);
        }
        return addressEntity;
    }

    public Object area(JSONObject jsonObject) {
        String jsonStr = "";
        try {
            File jsonFile = ResourceUtils.getFile("classpath:address.json");
            FileReader fileReader = new FileReader(jsonFile);
            Reader reader = new InputStreamReader(new FileInputStream(jsonFile), "utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            fileReader.close();
            reader.close();
            jsonStr = sb.toString();
            return jsonStr;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
