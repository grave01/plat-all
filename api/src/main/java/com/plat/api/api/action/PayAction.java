package com.plat.api.api.action;

import com.plat.api.entity.api.PlatOrderEntity;
import com.plat.api.dao.api.PlatOrderDetailDao;
import com.plat.api.entity.api.UserEntity;
import com.plat.api.dao.api.PlatOrderDao;

import com.alibaba.fastjson.JSONObject;
import com.github.binarywang.wxpay.bean.order.WxPayMpOrderResult;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.plat.api.api.action.common.BuildOrderParam;
import com.plat.api.api.action.common.CommonMethod;
import com.plat.api.common.OrderType;
import com.plat.api.common.RespCode;
import com.plat.api.dao.api.*;
import com.plat.api.entity.api.*;
import com.plat.api.exception.PlatException;
import com.plat.api.redis.RedisCache;
import com.plat.api.util.SnowflakeIdWorker;
import com.plat.api.util.YmalPropertiesUtil;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/6/14
 * Time: 0:32
 * Description: No Description
 */
@Service
public class PayAction implements OrderType {
    @Autowired
    private WxPayService wxService;
    @Autowired
    private UserDao userDao;
    @Autowired
    private TokenDao tokenDao;
    @Autowired
    private PlatVideoDao platVideoDao;
    @Autowired
    private PlatOrderDao platOrderDao;
    @Autowired
    private PlatOrderDetailDao platOrderDetailDao;
    @Autowired
    private PlatAuctionDao platAuctionDao;
    @Autowired
    private PlatAuctionOrderDao platAuctionOrderDao;
    @Autowired
    private PlatAuctionOrderRecordDao platAuctionOrderRecordDao;

    @Autowired
    private RedisCache redisCache;
    @Autowired
    private PlatAddressDao platAddressDao;

    /**
     * 直接支付时候创建订单
     * 订单开启事务支持
     */
    @Transactional
    public JSONObject CreatePayOrder(JSONObject paramJson) throws WxPayException {
        JSONObject body = paramJson.getJSONObject("body");
        Integer id = body.getInteger("id");
        UserEntity userEntity = CommonMethod.getUserEntity(tokenDao, userDao, paramJson);
        PlatVideoEntity platVideoEntity = platVideoDao.getSQLManager().lambdaQuery(PlatVideoEntity.class).andEq("id", id).single();
        WxPayUnifiedOrderRequest request = new WxPayUnifiedOrderRequest();
        String outTradeNo = SnowflakeIdWorker.getId();
        request.setOpenid(userEntity.getOpenId());
        String payBody = StringUtils.substring(platVideoEntity.getDetail(), 0, 25) + "...";
        request.setBody(payBody);
        request.setOutTradeNo(outTradeNo);
        Double p = platVideoEntity.getPrice() * 100;
        request.setTotalFee(p.intValue());
        request.setSpbillCreateIp("0.0.0.0");
        request.setNotifyUrl(YmalPropertiesUtil.getValue("wxpay.notify-url").toString());
        request.setTradeType("JSAPI");
        WxPayMpOrderResult wxPayMpOrderResult = wxService.createOrder(request);
        if (wxPayMpOrderResult != null) {
            redisCache.setCacheObject(outTradeNo, outTradeNo);
            PlatOrderEntity order = new PlatOrderEntity();
            BuildOrderParam buildOrderParam = new BuildOrderParam();
            buildOrderParam.setGoodsName(platVideoEntity.getVideoName());
            buildOrderParam.setGoodsDetail(platVideoEntity.getDetail());
            buildOrderParam.setOrder(order);
            buildOrderParam.setPlatOrderDao(platOrderDao);
            buildOrderParam.setPlatOrderDetailDao(platOrderDetailDao);
            buildOrderParam.setUserEntity(userEntity);
            buildOrderParam.setOutTradeNo(outTradeNo);
            buildOrderParam.setPrice(platVideoEntity.getPrice());
            buildOrderParam.setGoodsId(platVideoEntity.getId());
            buildOrderParam.setOrderType(OrderType.VIDE0);
            buildOrderParam.setExpressFee(new BigDecimal(0));
            CommonMethod.createOrder(buildOrderParam);
        } else {
            //String code, String msg, String desc,Object param
            throw new PlatException(RespCode.CREATE_ORDER_FAIL.getCode(), "微信支付创建订单失败", RespCode.CREATE_ORDER_FAIL.getError_msg(), RespCode.CREATE_ORDER_FAIL.getError_msg());
        }
        JSONObject json = new JSONObject();
        json.put("payInfo", wxPayMpOrderResult);
        return json;
    }

    private UserEntity getUserEntity(JSONObject paramJson) {
        String token = paramJson.getJSONObject("head").getString("token");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", token).single();
        return userDao.getSQLManager().lambdaQuery(UserEntity.class).andEq("id", tokenEntity.getUserId()).single();
    }


    /**
     * 创建实物订单
     */
    @Transactional
    public JSONObject createRealOrder(JSONObject paramJson) throws WxPayException {
        JSONObject body = paramJson.getJSONObject("body");
        Integer id = body.getInteger("id");
        Integer addressId = body.getInteger("addressId");
        Integer num = body.getInteger("num");//加价幅度*加价格次数 前端调整次数 最低一分钱
        UserEntity userEntity = getUserEntity(paramJson);
        PlatAuctionOrderEntity auctionOrder = platAuctionOrderDao.getSQLManager().lambdaQuery(PlatAuctionOrderEntity.class).andEq("goods_id", id).andEq("user_id", userEntity.getId()).single();
        if (auctionOrder == null) {
            if (addressId == null) {
                throw new PlatException(RespCode.CREATE_ORDER_ADDRESS_NULL.getCode(), RespCode.CREATE_ORDER_ADDRESS_NULL.getError_msg());
            }
        } else {
            if (addressId == null) {
                if (auctionOrder.getAddressId() == null) {
                    throw new PlatException(RespCode.CREATE_ORDER_ADDRESS_NULL.getCode(), RespCode.CREATE_ORDER_ADDRESS_NULL.getError_msg());
                }
            }
        }
        AuctionEntity auctionEntity = platAuctionDao.unique(id);
        WxPayUnifiedOrderRequest request = new WxPayUnifiedOrderRequest();
        //计算最终需要支付的金额
        //TODO 计算金额
        Double amount = auctionEntity.getRangePrice() * num * 100;
        String outTradeNo = SnowflakeIdWorker.getId();
        request.setOpenid(userEntity.getOpenId());
        String payBody = StringUtils.substring(auctionEntity.getDetail(), 0, 25) + "...";
        request.setBody(payBody);
        request.setOutTradeNo(outTradeNo);
        request.setTotalFee(amount.intValue());
        request.setSpbillCreateIp("0.0.0.0");
        request.setNotifyUrl(YmalPropertiesUtil.getValue("wxpay.notify-url").toString());
        request.setTradeType("JSAPI");
        WxPayMpOrderResult wxPayMpOrderResult = wxService.createOrder(request);
        if (wxPayMpOrderResult != null) {
            redisCache.setCacheObject(outTradeNo, outTradeNo);
            addressId = auctionOrder == null ? addressId : auctionOrder.getAddressId() == null ? addressId : auctionOrder.getAddressId();
            createAuctionRecord(auctionEntity, userEntity, outTradeNo, Double.valueOf(amount));
            PlatOrderEntity order = new PlatOrderEntity();
            order.setDeliveryId(addressId);
            BuildOrderParam buildOrderParam = new BuildOrderParam();
            buildOrderParam.setGoodsName(auctionEntity.getName());
            buildOrderParam.setGoodsDetail(auctionEntity.getDetail());
            buildOrderParam.setOrder(order);
            buildOrderParam.setPlatOrderDao(platOrderDao);
            buildOrderParam.setPlatOrderDetailDao(platOrderDetailDao);
            buildOrderParam.setUserEntity(userEntity);
            buildOrderParam.setOutTradeNo(outTradeNo);
            buildOrderParam.setPrice(auctionEntity.getBasePrice());
            buildOrderParam.setGoodsId(auctionEntity.getId());
            buildOrderParam.setOrderType(OrderType.AUCTION);
            buildOrderParam.setExpressFee(new BigDecimal(0));
            CommonMethod.createOrder(buildOrderParam);
        } else {
            //String code, String msg, String desc,Object param
            throw new PlatException(RespCode.CREATE_ORDER_FAIL.getCode(), "微信支付创建订单失败", RespCode.CREATE_ORDER_FAIL.getError_msg(), RespCode.CREATE_ORDER_FAIL.getError_msg());
        }
        JSONObject json = new JSONObject();
        json.put("payInfo", wxPayMpOrderResult);
        return json;
    }


    // !@#$%^12*cxcq12()
    private PlatAuctionOrderRecordEntity createAuctionRecord(AuctionEntity auctionEntity, UserEntity userEntity, String outTradeNo, Double amount) {
        PlatAuctionOrderRecordEntity record = new PlatAuctionOrderRecordEntity();
        Double am = amount / 100;
        record.setPayMoney(am);
        record.setPayStatus(NO_PAY);
        record.setPayTime(new Date());
        record.setAuctionMainOrder(outTradeNo);
        record.setUserId(userEntity.getId());
        record.setCreateTime(new Date());
        record.setOutTradeNo(outTradeNo);
        record.setPaySn(null);
        record.setRefundStatus(ORDER_NO_REFUND);
        record.setRefundTime(new Date());
        record.setGoodsId(Integer.valueOf(auctionEntity.getId().toString()));
        platAuctionOrderRecordDao.insert(record);
        return record;
    }
}
