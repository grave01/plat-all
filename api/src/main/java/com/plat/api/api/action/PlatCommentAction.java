package com.plat.api.api.action;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.plat.api.anonation.Token;
import com.plat.api.dao.api.PlatCommentDao;
import com.plat.api.dao.api.TokenDao;
import com.plat.api.dao.api.UserDao;
import com.plat.api.dao.param.SysPlatParamDao;
import com.plat.api.entity.api.PlatCommentEntity;
import com.plat.api.entity.api.TokenEntity;
import com.plat.api.entity.api.UserEntity;
import com.plat.api.exception.PlatException;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.query.LambdaQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/10
 * Time: 12:45
 * Description: No Description
 */
@Service
@Token
public class PlatCommentAction {
    @Autowired
    private TokenDao tokenDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private PlatCommentDao platCommentDao;
    @Autowired
    private SysPlatParamDao sysPlatParamDao;

    public JSONObject getCommentList(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        Integer targetId = body.getInteger("targetId");
        Integer page = body.getInteger("page");
        Integer pageSize = body.getInteger("pageSize");
        LambdaQuery<PlatCommentEntity> query = platCommentDao.createLambdaQuery().andEq("target_id", targetId).orderBy("id desc");
        List<PlatCommentEntity> list = query.page(page, pageSize).getList();
        JSONObject json = new JSONObject();
        for (int i =0;i<list.size();i++){
            UserEntity userEntity = userDao.createLambdaQuery().andEq("id",(list.get(i).getOwnerId())).single();
            list.get(i).setCommentTime( DateUtil.format(list.get(i).getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
            list.get(i).setUserEntity(userEntity);
        }
        json.put("list", list);
        return json;
    }

    public JSONObject comment(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        JSONObject head = jsonObject.getJSONObject("head");
        Integer replayId = body.getInteger("replayId");
//        UserEntity replyUserEntity = userDao.unique(replayId);
        String content = body.getString("content");
        String photo = body.getString("photo");
        if(content.isEmpty()){
            throw new PlatException("输入评论信息");
        }
        Integer type = body.getInteger("type");
        Integer targetId = body.getInteger("targetId");
        String replyName = body.getString("replyName");
        TokenEntity tokenEntity = tokenDao.createLambdaQuery().andEq("token", head.getString("token")).single();
        UserEntity userEntity = userDao.unique(tokenEntity.getUserId());
        String nickname = userEntity.getNickname();
        PlatCommentEntity platCommentEntity = new PlatCommentEntity();
//        platCommentEntity.setReplayId(replyUserEntity.getId());
        platCommentEntity.setContent(content);
        platCommentEntity.setNickname(nickname);
        platCommentEntity.setPhoto(photo);
//        PlatParamEntity platParamEntity = sysPlatParamDao.getSQLManager().lambdaQuery(PlatParamEntity.class).andEq("key", type).single();
//        if (null == platParamEntity) {
//            throw new PlatException("不支持的类型评论，请在系统参数配置评论类型");
//        } else {
//            platCommentEntity.setCom_target_type(Integer.valueOf(platParamEntity.getValue()));
//        }
        platCommentEntity.setTargetId(targetId);
        platCommentEntity.setCreateTime(new Date());
        platCommentEntity.setReplyName(replyName);
        platCommentEntity.setOwnerId(userEntity.getId());
        platCommentDao.insert(platCommentEntity);
        platCommentEntity.setUserEntity(userEntity);
        platCommentEntity.setCommentTime(DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        JSONObject json = new JSONObject();
        json.put("object", platCommentEntity);
        return json;
    }

    public List getCommentByTargetId(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        Integer targetId = body.getInteger("targetId");
        Integer page = body.getInteger("page");
        Integer pageSize = body.getInteger("pageSize");
        LambdaQuery<PlatCommentEntity> query = platCommentDao.getSQLManager().lambdaQuery(PlatCommentEntity.class);
        PageQuery<PlatCommentEntity> list = query.andEq("target_id", targetId).orderBy(" create_time desc ").page(page, pageSize);
        List<PlatCommentEntity> list1 = list.getList();
        for (int i =0;i<list1.size();i++){
            UserEntity userEntity = userDao.unique(list1.get(i).getOwnerId());
            list1.get(i).setCommentTime( DateUtil.format(list1.get(i).getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
            list1.get(i).setUserEntity(userEntity);
        }
        return list.getList();
    }
}
