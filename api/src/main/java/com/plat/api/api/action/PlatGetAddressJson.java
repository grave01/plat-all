package com.plat.api.api.action;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.plat.api.anonation.NoToken;
import com.plat.api.anonation.Token;
import com.plat.api.dao.api.PlatAddressJsonDao;
import com.plat.api.entity.api.Address2;
import com.plat.api.entity.api.PlatAddressJsonEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/6/9
 * Time: 23:39
 * Description: No Description
 */
@Service("platGetAddressJson")
@Token
public class PlatGetAddressJson {
    @Autowired
    private PlatAddressJsonDao platAddressJsonDao;

    @NoToken
    public JSONObject getAddress(JSONObject jsonObject) {
        JSONObject json = new JSONObject();
        List<PlatAddressJsonEntity> list = platAddressJsonDao.all();
        ArrayList<Address2>  address2s =  paresAddress(list.get(0).getJson());
        json.put("json",address2s);
        return json;
    }

    private   ArrayList<Address2> paresAddress(String json) throws JSONException {
        ArrayList<Address2> proList = new ArrayList<>();
        JSONArray jsonArray = JSONArray.parseArray(json);
        for (int i = 0; i < jsonArray.size(); i++) {
            Address2 address2 = new Address2();
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            address2.setName(jsonObject.getString("name"));
            JSONArray city = jsonObject.getJSONArray("city");
            List<Address2.CityBean> cityList = new ArrayList<>();
            for (int j = 0; j < city.size(); j++) {
                Address2.CityBean cityBean = new Address2.CityBean();
                JSONObject jsonObject1 = city.getJSONObject(j);
                cityBean.setName(jsonObject1.getString("name"));
                List<String> dis = new ArrayList<>();
                String[] area = jsonObject1.getString("area").replaceAll("\"", "").replaceAll("\\[", "").replaceAll("\\]", "").split(",");
                for (String sa : area) {
                    dis.add(sa);
                }
                cityBean.setArea(dis);
                cityList.add(cityBean);
            }
            address2.setCity(cityList);
            proList.add(address2);
        }
        return proList;
    }
}
