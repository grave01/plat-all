package com.plat.api.api.action;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.plat.api.anonation.NoToken;
import com.plat.api.anonation.Token;
import com.plat.api.dao.api.PlatPeopleDao;
import com.plat.api.dao.api.PlatVideoDao;
import com.plat.api.entity.api.PlatPeopleEntity;
import com.plat.api.entity.api.PlatVideoEntity;
import com.plat.api.util.StringUtil;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.query.LambdaQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/6/4
 * Time: 13:48
 * Description: No Description
 */
@Service
@Token
public class PlatPeopleAction {
    @Autowired
    private PlatPeopleDao platPeopleDao;
    @Autowired
    private PlatVideoDao platVideoDao;

    @NoToken
    public JSONObject getList(JSONObject jsonObject) {
        JSONObject param = jsonObject.getJSONObject("body");
        List<PlatPeopleEntity> list = platPeopleDao.all(param.getInteger("start"), 10);
        JSONObject json = new JSONObject();
        List<PlatPeopleEntity> li = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            list.get(i).getMainPhoto();
            list.get(i).setMainPhoto(StringUtil.solveString(list.get(i).getMainPhoto()));
            list.get(i).setAlbumId(StringUtil.solveString(list.get(i).getAlbumId()));
            list.get(i).setWorkAlbumId(StringUtil.solveString(list.get(i).getWorkAlbumId()));
            li.add(list.get(i));
        }
        json.put("list", li);
        return json;
    }

    @NoToken
    public JSONObject getPeople(JSONObject jsonObject) {
        JSONObject param = jsonObject.getJSONObject("body");
        PlatPeopleEntity peopleEntity = platPeopleDao.single(param.getInteger("id"));
        peopleEntity.setWorkAlbumId(StringUtil.solveString(peopleEntity.getWorkAlbumId()));
        peopleEntity.setAlbumId(StringUtil.solveString(peopleEntity.getAlbumId()));
        peopleEntity.setMainPhoto(StringUtil.solveString(peopleEntity.getMainPhoto()));
        LambdaQuery<PlatVideoEntity> lambdaQuery = platVideoDao.getSQLManager().lambdaQuery(PlatVideoEntity.class);

        PageQuery<PlatVideoEntity> list = lambdaQuery
                .andEq("create_user", param.getInteger("id"))
                .orderBy("is_top = 1 desc")
                .page(param.getInteger("page"), 10);
        JSONObject json = new JSONObject();
        List<String> list1 = new ArrayList<>();
        List<String> list2 = new ArrayList<>();
        List<String> list3 = new ArrayList<>();
        String[] albumUrl = peopleEntity.getAlbumId().split(",");
        String[] workUrl = peopleEntity.getWorkAlbumId().split(",");
        for (int i = 0; i < albumUrl.length; i++) {
            list1.add(albumUrl[i]);
        }
        for (int i = 0; i < workUrl.length; i++) {
            list2.add(workUrl[i]);
        }
        peopleEntity.setListAlbumUrl(list1);
        peopleEntity.setListWorkUrl(list2);
        for (int i=0;i<list2.size();i++){
            list3.add(list2.get(i));
        }
        for (int i=0;i<list1.size();i++){
            list3.add(list1.get(i));
        }
        List<PlatVideoEntity> lis =new ArrayList();
        for (int i = 0; i < list.getList().size(); i++) {
            list.getList().get(i).setPoster(StringUtil.solveString(list.getList().get(i).getPoster()));
            list.getList().get(i).setImageId(StringUtil.solveString(list.getList().get(i).getImageId()));
            list.getList().get(i).setPublishTime(DateUtil.format(list.getList().get(i).getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
            lis.add(list.getList().get(i));
        }
        peopleEntity.setAllUrl(list3);
        json.put("object", peopleEntity);
        json.put("list", lis);
        return json;
    }
}
