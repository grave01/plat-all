package com.plat.api.api.action;

import java.util.Date;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.plat.api.api.action.common.CommonMethod;
import com.plat.api.common.OrderType;
import com.plat.api.common.RespCode;
import com.plat.api.dao.api.PlatOrderDao;
import com.plat.api.dao.api.TokenDao;
import com.plat.api.dao.api.UserDao;
import com.plat.api.entity.api.PlatOrderEntity;
import com.plat.api.entity.api.PlatRefundEntity;
import com.plat.api.entity.api.UserEntity;
import com.plat.api.exception.PlatException;
import com.plat.api.mapper.PlatRefundMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/9/15
 * Time: 15:21
 * Description: No Description
 */
@Service
public class PlatRefundAction {

    @Autowired
    private UserDao userDao;
    @Autowired
    private TokenDao tokenDao;
    @Autowired
    private PlatRefundMapper refundMapper;
    @Autowired
    private PlatOrderDao orderDao;

    public JSONObject apply(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        String orderNo = body.getString("orderNo");
        UserEntity userEntity = CommonMethod.getUserEntity(tokenDao, userDao, jsonObject);
        PlatOrderEntity orderEntity = orderDao.createLambdaQuery().andEq("order_no", orderNo).single();
        JSONObject result = new JSONObject();
        if (orderEntity == null) {
            throw new PlatException(RespCode.ORDER_NOT_EXIST.getCode(), "订单不存在");
        }
        QueryWrapper<PlatRefundEntity> queryWrapper = new QueryWrapper();
        queryWrapper.eq("order_no", orderNo);
        queryWrapper.eq("refund_status", OrderType.ORDER_REFUNDING);
        PlatRefundEntity refundEntity = refundMapper.selectOne(queryWrapper);
        if (refundEntity != null) {
            throw new PlatException(RespCode.REFUND_ORDER_HAS_EXSIT.getCode(), "退款订单已经存在");
        } else {
            refundEntity = new PlatRefundEntity();
            refundEntity.setOrderNo(orderEntity.getOrderNo());
            refundEntity.setRefundNo(RandomUtil.randomNumbers(18));
            refundEntity.setCreateTime(new Date());
            refundEntity.setRefundAmt(orderEntity.getOrderRealPrice().doubleValue());
            refundEntity.setRefundRealAmt(orderEntity.getOrderRealPrice().doubleValue());
            refundEntity.setRefundStatus(OrderType.ORDER_REFUNDING);
            refundEntity.setRefundReason("商品退款");
            refundEntity.setUserId(userEntity.getId());
            refundEntity.setUserName(userEntity.getName());
            refundEntity.setPhone(userEntity.getPhone());
            refundMapper.insert(refundEntity);
            orderEntity.setRefundStatus(OrderType.ORDER_REFUNDING);
            orderDao.updateById(orderEntity);
            result.put("object", refundEntity);
        }
        return result;
    }
}
