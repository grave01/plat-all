package com.plat.api.api.action;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.plat.api.anonation.NoToken;
import com.plat.api.anonation.Token;
import com.plat.api.api.action.common.IndexIVideoResult;
import com.plat.api.api.action.common.IndexType;
import com.plat.api.common.OrderType;
import com.plat.api.common.VideoStatus;
import com.plat.api.dao.api.*;
import com.plat.api.entity.api.PlatOrderDetailEntity;
import com.plat.api.entity.api.PlatOrderEntity;
import com.plat.api.entity.api.PlatVideoEntity;
import com.plat.api.entity.api.TokenEntity;
import com.plat.api.mapper.PlatOrderDetailMapper;
import com.plat.api.mapper.PlatVideoMapper;
import com.plat.api.util.StringUtil;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.query.LambdaQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/10
 * Time: 8:49
 * Description: No Description
 */
@Service
@Token
public class PlatVideoAction implements VideoStatus, OrderType {
    @Autowired
    private PlatVideoDao platVideoDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private TokenDao tokenDao;
    private static final Integer is_recommend = 1; //是否推荐
    @Autowired
    private PlatOrderDao platOrderDao;
    @Autowired
    private PlatOrderDetailDao platOrderDetailDao;
    @Autowired
    private PlatOrderDetailMapper platOrderDetailMapper;
    @Autowired
    private PlatVideoMapper platVideoMapper;

    @NoToken
    public JSONObject getVideoList(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        Integer page = body.getInteger("page");
        Integer pageSize = body.getInteger("pageSize");
        Integer category = body.getInteger("category");
        LambdaQuery<PlatVideoEntity> query = platVideoDao.getSQLManager().lambdaQuery(PlatVideoEntity.class);
        PageQuery<PlatVideoEntity> list;
        if (category!=null){
            if(category==0){
                list  = query.orderBy("is_top desc ").page(page, pageSize);
            }else {
                list  = query.andLike("category","%"+category+"%").orderBy("is_top desc").orderBy("is_top desc ").page(page, pageSize);
            }
        }else {
            list = query.orderBy("is_top desc").andEq("is_recommend", 1).orderBy("is_top desc ").page(page, pageSize);
        }
        List<IndexIVideoResult> lis = new ArrayList<>();
        for (PlatVideoEntity platVideoEntity : list.getList()) {
            platVideoEntity.setPoster(StringUtil.solveString(platVideoEntity.getPoster()));
            platVideoEntity.setImageId(StringUtil.solveString(platVideoEntity.getImageId()));
            platVideoEntity.setPublishTime(DateUtil.format(platVideoEntity.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
            IndexIVideoResult indexIVideoResult = new IndexIVideoResult();
            indexIVideoResult.setObject(platVideoEntity);
            indexIVideoResult.setType(IndexType.VIDEO);
            if (platVideoEntity.getVideoInfo() != null) {
                JSONObject jsonObject1 = JSONObject.parseObject(platVideoEntity.getVideoInfo());
                platVideoEntity.setVideoInfo(jsonObject1.getJSONObject("format").toJSONString());
            }
            lis.add(indexIVideoResult);
        }
        JSONObject json =new JSONObject();
        json.put("list", lis);
        return json;
    }

    /**
     * 搜索视频
     */
    public JSONObject search(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        String keyword = body.getString("keyword");
        Integer current = body.getInteger("page");
        Integer pageSize = body.getInteger("pageSize");
        QueryWrapper<PlatVideoEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("video_name", keyword).or().like("detail", keyword).or().like("create_username",keyword);
        //总页数+总记录数
        Page<PlatVideoEntity> page= new Page<>(current, pageSize);
        //调用自定义sql
        IPage<PlatVideoEntity> iPage = platVideoMapper.selectPage(page, queryWrapper);
        JSONObject json = new JSONObject();
        for (PlatVideoEntity platVideoEntity : iPage.getRecords()) {
            platVideoEntity.setPoster(StringUtil.solveString(platVideoEntity.getPoster()));
            platVideoEntity.setImageId(StringUtil.solveString(platVideoEntity.getImageId()));
            platVideoEntity.setPublishTime(DateUtil.format(platVideoEntity.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
        }
        json.put("list", iPage.getRecords());
        return json;
    }

    @NoToken
    public JSONObject getRecommendVideoList(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        String keyword = body.getString("keyword");
        Integer page = body.getInteger("page");
        Integer is_free = body.getInteger("is_free");
        Date create_time = body.getDate("create_time");
        String category = body.getString("category");
        List<String> li = new ArrayList<>();
        for (int i = 0; i < category.split(",").length; i++) {
            li.add(category.split(",")[i]);
        }
        Integer type = body.getInteger("type");
        Integer pageSize = body.getInteger("pageSize");
        LambdaQuery<PlatVideoEntity> query = platVideoDao.getSQLManager().lambdaQuery(PlatVideoEntity.class);
        PageQuery<PlatVideoEntity> list = query.andLike("name", keyword)
                .andLike("desc", keyword)
                .andEq("is_free", is_free)
                .andEq("create_time", create_time)
                .andEq("type", type).andEq("is_recommend", is_recommend)
                .andIn("category", li).orderBy("is_recommend desc")
                .page(page, pageSize);
        JSONObject json = new JSONObject();
        json.put("list", list.getList());
        return json;
    }

    @NoToken
    public JSONObject getMyVideoList(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        JSONObject head = jsonObject.getJSONObject("head");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", head.getString("token")).single();
        String keyword = body.getString("keyword");
        Integer page = body.getInteger("page");
        Integer is_free = body.getInteger("is_free");
        Date create_time = body.getDate("create_time");
        String category = body.getString("category");
        List<String> li = new ArrayList<>();
        for (int i = 0; i < category.split(",").length; i++) {
            li.add(category.split(",")[i]);
        }
        Integer type = body.getInteger("type");
        Integer pageSize = body.getInteger("pageSize");
        LambdaQuery<PlatVideoEntity> query = platVideoDao.getSQLManager().lambdaQuery(PlatVideoEntity.class);
        PageQuery<PlatVideoEntity> list = query.andLike("name", keyword)
                .andLike("desc", keyword)
                .andEq("is_free", is_free)
                .andEq("is_free", tokenEntity.getUserId())
                .andEq("create_time", create_time)
                .andEq("type", type).andEq("is_recommend", is_recommend)
                .andIn("category", li).orderBy("is_recommend desc")
                .page(page, pageSize);
        JSONObject json = new JSONObject();
        json.put("list", list.getList());
        return json;
    }

    @NoToken
    public JSONObject getVideo(JSONObject jsonObject) {
        JSONObject param = jsonObject.getJSONObject("body");
        Integer id = param.getInteger("id");
        PlatVideoEntity platVideoEntity = platVideoDao.unique(id);
        JSONObject json = new JSONObject();
        platVideoEntity.setPublishTime(DateUtil.format(platVideoEntity.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
        LambdaQuery<PlatVideoEntity> lambdaQuery = platVideoDao.createLambdaQuery().andLike("category", "%" + StringUtil.solveString(platVideoEntity.getCategory()).split(",")[0] + "%");
        PageQuery<PlatVideoEntity> pageQuery = lambdaQuery.page(1, 10);
        platVideoEntity.setNum(platVideoEntity.getNum() + 1);
        platVideoDao.updateById(platVideoEntity);//修改播放量
        if (null != platVideoEntity.getM3u8Url()) {
            // platVideoEntity.setUrl(platVideoEntity.getM3u8Url());
            platVideoEntity.setUrl(platVideoEntity.getUrl());
        }
        JSONObject head = jsonObject.getJSONObject("head");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", head.getString("token")).single();

        if (platVideoEntity.getIsFree().equals(FREE)) {
            platVideoEntity.setShowFreeItem(1);
        } else {
//            LambdaQuery<PlatOrderDetailEntity> lambdaQuery1 = platOrderDetailDao.createLambdaQuery().andEq("user_id", tokenEntity.getUserId()).andEq("goods_id", platVideoEntity.getId()).andEq("order_type", OrderType.VIDE0);
//            PlatOrderDetailEntity platOrderDetailEntity = lambdaQuery1.single();
            //0是未付费 1是完成支付 按钮置灰2 存在订单但是支付状态是0 显示支付按钮
            PlatOrderDetailEntity detailEntity = platOrderDetailMapper.selectDetail(tokenEntity.getUserId(), platVideoEntity.getId(), OrderType.VIDE0);
            if (detailEntity == null) {
                platVideoEntity.setShowFreeItem(0);
            } else {
                platVideoEntity.setIsFree(1);
                platVideoEntity.setShowFreeItem(2);
            }
        }
        json.put("object", platVideoEntity);
        List<PlatVideoEntity> list = pageQuery.getList();
        List<PlatVideoEntity> lis = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setPoster(StringUtil.solveString(list.get(i).getPoster()));
            list.get(i).setImageId(StringUtil.solveString(list.get(i).getImageId()));
            list.get(i).setPublishTime(DateUtil.format(list.get(i).getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
            if (null != list.get(i).getM3u8Url()) {
                // list.get(i).setUrl(StringUtil.solveString(list.get(i).getM3u8Url()));
            }
            lis.add(list.get(i));
        }
        json.put("list", lis);
        return json;
    }
}
