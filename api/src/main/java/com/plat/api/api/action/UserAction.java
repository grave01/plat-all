package com.plat.api.api.action;

import java.util.Date;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Maps;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.plat.api.anonation.Token;
import com.plat.api.common.OrderType;
import com.plat.api.dao.api.*;
import com.plat.api.entity.api.TokenEntity;
import com.plat.api.entity.api.UserEntity;
import com.plat.api.mapper.SysConfigMapper;
import com.plat.api.sys.entity.SysConfig;
import com.plat.api.sys.service.ISysConfigService;
import com.plat.api.util.StringUtil;
import com.plat.api.util.YmalPropertiesUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author ss
 * createTime 2020/5/8
 * package ${com.example.demo.controller.action}
 ***/
@Service
@Token
public class UserAction {
    @Autowired
    private UserDao userDao;
    @Autowired
    private TokenDao tokenDao;
    @Autowired
    private PlatAuctionDao platAuctionDao;
    @Autowired
    private PlatOrderDao orderDao;
    @Autowired
    private PlatOrderDetailDao detailDao;
    @Autowired
    private SysConfigMapper configMapper;

    // @NoToken
    public JSONObject getName(JSONObject jsonObject) {
        JSONObject json = new JSONObject();
        json.put("name", "UserService.getName");
        return json;
    }

    public JSONObject getUserInfo(JSONObject jsonObject) {
        String token = jsonObject.getJSONObject("head").getString("token");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", token).single();
        UserEntity userEntity = userDao.getSQLManager().lambdaQuery(UserEntity.class).andEq("id", tokenEntity.getUserId()).single();

        long count = platAuctionDao.createLambdaQuery().andEq("auction_owner_id", tokenEntity.getUserId()).count();
        JSONObject json = new JSONObject();
        userEntity.setCount(count);
        json.put("userInfo", userEntity);
        json.put("countInfo", getCount(userEntity));
        Map<String, Object> map = new HashMap<>();
        QueryWrapper<SysConfig> qw = new QueryWrapper();
        qw.eq("config_key", "plat.about");
        String version = RandomUtil.randomNumbers(1) + "." + RandomUtil.randomNumbers(1) + "." + RandomUtil.randomNumbers(1);
        map.put("about", configMapper.selectOne(qw).getConfigValue() + "?v=" + version);
        QueryWrapper<SysConfig> qw1 = new QueryWrapper();
        qw1.eq("config_key", "plat.license");
        map.put("license", configMapper.selectOne(qw1).getConfigValue() + "?v=" + version);
        json.put("xcx", map);
        return json;
    }

    /**
     * 统计数量
     **/
    public Map getCount(UserEntity userEntity) {
        Map<String, Object> map = new HashMap<>();
        //全部
        long count1 = orderDao.createLambdaQuery().andEq("user_id", userEntity.getId()).andNotEq("order_type",OrderType.VIDE0)
                .andNotEq("order_type",OrderType.LIVE).count();
        map.put("all", count1);
        //待支付
        long count2 = orderDao.createLambdaQuery().andEq("user_id", userEntity.getId()).andEq("pay_status", 0).andNotEq("order_status",OrderType.ORDER_CANCEL).andNotEq("order_type",OrderType.VIDE0)
                .andNotEq("order_type",OrderType.LIVE).count();
        map.put("waitingPay", count2);
        //待发货
        long count3 = orderDao.createLambdaQuery().andEq("user_id", userEntity.getId())
                .andEq("pay_status", 1).andEq("express_status", OrderType.ORDER_EXPRESS_NO_DISTRIBUTE).andEq("refund_status", OrderType.ORDER_NO_REFUND)
                .andNotEq("order_type",OrderType.VIDE0)
                .andNotEq("order_type",OrderType.LIVE).count();
        map.put("waitingExpress", count3);
        //待收货
        long count4 = orderDao.createLambdaQuery().andEq("user_id", userEntity.getId())
                .andEq("pay_status", 1).andEq("express_status", OrderType.ORDER_EXPRESS_DISTRIBUTE).andEq("refund_status", OrderType.ORDER_NO_REFUND).andNotEq("order_status",OrderType.ORDER_FINISH).count();
        map.put("waitingExpressFinish", count4);
        //完成
        long count5 = orderDao.createLambdaQuery().andEq("user_id", userEntity.getId())
                .andEq("pay_status", 1).andEq("order_status", OrderType.ORDER_FINISH).count();
        map.put("finish", count5);
        Map<String, Object> map1 = new HashMap<>();
        map1.put("countInfo", map);
        return map1;
    }

    public void test() {
        List<UserEntity> list = userDao.all();
        System.out.println(list);
    }

    public JSONObject apply(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        String token = jsonObject.getJSONObject("head").getString("token");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", token).single();
        UserEntity userEntity = userDao.getSQLManager().lambdaQuery(UserEntity.class).andEq("id", tokenEntity.getUserId()).single();
        userEntity.setAuthStatus(3);//等待认证

        userEntity.setIdFront(body.getString("front"));
        userEntity.setIdBack(body.getString("back"));
        userEntity.setAuthStatus(3);
        Integer type = 0;
        if (body.getString("type").equals("教师")) {
            type = 1;
        }
        if (body.getString("type").equals("个人")) {
            type = 0;
        }
        if (body.getString("type").equals("企业")) {
            type = 2;
        }
        if (body.getString("type").equals("机构")) {
            type = 3;
        }
        userEntity.setType(type);
        String[] strings = StringUtil.solveString(body.getString("field")).split(",");
        StringUtils.join(strings, ",");
        userEntity.setField(StringUtils.join(strings, ","));
        userEntity.setLicense(body.getString("license"));
        userEntity.setAuthStatus(3);//等待认证
        userDao.updateById(userEntity);
        JSONObject result = new JSONObject();
        result.put("object", userEntity);
        return result;
    }

    public JSONObject userInfo(JSONObject param) {
        String token = param.getJSONObject("head").getString("token");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", token).single();
        UserEntity userEntity = userDao.getSQLManager().lambdaQuery(UserEntity.class).andEq("id", tokenEntity.getUserId()).single();
        JSONObject result = new JSONObject();
        result.put("object", userEntity);
        return result;
    }
}
