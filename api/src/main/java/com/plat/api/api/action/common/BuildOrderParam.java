package com.plat.api.api.action.common;

import com.plat.api.dao.api.PlatOrderDao;
import com.plat.api.dao.api.PlatOrderDetailDao;
import com.plat.api.entity.api.PlatOrderEntity;
import com.plat.api.entity.api.UserEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/9/16
 * Time: 20:50
 * Description: No Description
 * desc 构建创建订单的必要参数
 */
@Data
public class BuildOrderParam {
    String goodsName;
    String goodsDetail;
    PlatOrderEntity order;
    PlatOrderDao platOrderDao;
    PlatOrderDetailDao platOrderDetailDao;
    UserEntity userEntity;
    String outTradeNo;
    Double price;
    Long goodsId;
    String orderType;
    BigDecimal expressFee;
}
