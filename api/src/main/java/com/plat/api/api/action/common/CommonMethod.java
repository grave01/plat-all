package com.plat.api.api.action.common;

import com.alibaba.fastjson.JSONObject;
import com.plat.api.common.OrderType;
import com.plat.api.dao.api.PlatOrderDao;
import com.plat.api.dao.api.PlatOrderDetailDao;
import com.plat.api.dao.api.TokenDao;
import com.plat.api.dao.api.UserDao;
import com.plat.api.entity.api.PlatOrderDetailEntity;
import com.plat.api.entity.api.PlatOrderEntity;
import com.plat.api.entity.api.TokenEntity;
import com.plat.api.entity.api.UserEntity;

import java.math.BigDecimal;
import java.util.Date;

import static com.plat.api.common.OrderType.WECHAT_PAY;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/9/6
 * Time: 0:37
 * Description: No Description
 */
public class CommonMethod {
    public static void createOrder(BuildOrderParam buildOrderParam) {
        PlatOrderEntity order = buildOrderParam.getOrder();
        UserEntity userEntity = buildOrderParam.getUserEntity();
        order.setDeliveryId(order.getDeliveryId());
        order.setOrderStatus(OrderType.CREATE_ORDER);
        order.setPayStatus(OrderType.NO_PAY);
        order.setUserId(userEntity.getId());
        order.setExpressFee(buildOrderParam.getExpressFee());
        order.setOrderCouponPrice(new BigDecimal("0"));
        order.setOrderNo(buildOrderParam.outTradeNo);
        Double p = buildOrderParam.price;
        order.setOrderPrice(new BigDecimal(p.doubleValue()));
        order.setOrderRealPrice(new BigDecimal(p.doubleValue()));
        order.setOrderType(buildOrderParam.orderType);
        order.setPhone(userEntity.getPhone());
        order.setCreateTime(new Date());
        order.setPayTime(null);
        order.setExpressStatus(OrderType.ORDER_EXPRESS_NO_DISTRIBUTE);
        order.setRefundStatus(OrderType.ORDER_NO_REFUND);
        order.setUserName(userEntity.getNickname());
        order.setPayType(WECHAT_PAY);
        buildOrderParam.getPlatOrderDao().insert(order);
        //插入订单明细
        PlatOrderDetailEntity detail = new PlatOrderDetailEntity();
        detail.setOrderNo(order.getOrderNo());
        detail.setGoodsId(buildOrderParam.goodsId);
        detail.setCreateTime(new Date());
        detail.setPublisherId(userEntity.getId());
        detail.setCreateTime(new Date());
        detail.setUserId(userEntity.getId());
        detail.setOrderType(buildOrderParam.orderType);
        detail.setGoodsType(buildOrderParam.orderType);
        detail.setGoodsName(buildOrderParam.goodsName);
        detail.setGoodsDetail(buildOrderParam.goodsDetail);
        buildOrderParam.getPlatOrderDetailDao().insert(detail);
    }

    public static UserEntity getUserEntity(TokenDao tokenDao, UserDao userDao, JSONObject paramJson) {
        String token = paramJson.getJSONObject("head").getString("token");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", token).single();
        return userDao.getSQLManager().lambdaQuery(UserEntity.class).andEq("id", tokenEntity.getUserId()).single();
    }

}
