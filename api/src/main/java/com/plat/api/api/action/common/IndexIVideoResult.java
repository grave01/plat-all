package com.plat.api.api.action.common;


import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/9/2
 * Time: 14:27
 * Description: 返回一个对象--对象类型，根据
 */
@Data
public class IndexIVideoResult {
    /**
     * 是视频：1，直播：2，头条：43，新闻：4，通知：5，推荐机构：6，广告：7
     */
    private Integer type;
    /**
     * 返回对象
     */
    private Object object;
}
