package com.plat.api.api.action.common;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/9/2
 * Time: 14:35
 * Description: 首页推荐类型
 */
public interface IndexType {
    /**
     * 是视频：1，直播：2，头条：43，新闻：4，通知：5，推荐机构：6，广告：7
     */
    Integer VIDEO = 1;
    Integer LIVE = 2;
    Integer TOU = 3;
    Integer NEWS = 4;
    Integer NOTIFY = 5;
    Integer REC_ORG = 6;
    Integer AD = 7;
}
