package com.plat.api.api.action.common;

import com.plat.api.entity.api.PlatWechatLiveRoomEntity;
import com.plat.api.entity.api.UserEntity;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/9/2
 * Time: 15:29
 * Description: 直播间结果集
 */
public class LiveRoomResult {
    /**
     * 直播间
     */
    private PlatWechatLiveRoomEntity liveRoomEntity;
    /**
     * 发布直播间的用户信息
     */
    private UserEntity userEntity;
}
