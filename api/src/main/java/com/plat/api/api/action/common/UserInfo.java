package com.plat.api.api.action.common;

import com.alibaba.fastjson.JSONObject;
import com.plat.api.dao.api.TokenDao;
import com.plat.api.dao.api.UserDao;
import com.plat.api.entity.api.TokenEntity;
import com.plat.api.entity.api.UserEntity;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/9
 * Time: 14:54
 * Description: No Description
 */
public class UserInfo {
    public static UserEntity getUserEntity(JSONObject paramJson, UserDao userDao, TokenDao tokenDao) {
        String token = paramJson.getJSONObject("head").getString("token");
        TokenEntity tokenEntity = tokenDao.getSQLManager().lambdaQuery(TokenEntity.class).andEq("token", token).single();
        return userDao.getSQLManager().lambdaQuery(UserEntity.class).andEq("id", tokenEntity.getUserId()).single();
    }
}
