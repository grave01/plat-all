package com.plat.api.api.action.common.constant;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/5
 * Time: 19:14
 * Description: No Description
 */
public interface GoodsType {
    /**
     * 视频
     */
    String VIDEO = "VIDEO";
    /**
     * 拍卖
     */
    String PAIMAI = "PAIMAI";
    /**
     * 实物
     */
    String NORMAL = "NORMAL";
}
