package com.plat.api.cms.action;

import com.alibaba.fastjson.JSONObject;
import com.plat.api.anonation.NoToken;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/10
 * Time: 19:07
 * Description: No Description
 */
@Service
@NoToken
public class CmsWebInfoAction {

    //获取信息
    public JSONObject getInfo(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        JSONObject json = new JSONObject();
        JSONObject webInfo = new JSONObject();
        JSONObject videoList = new JSONObject();
        JSONObject group = new JSONObject();
        JSONObject devLine = new JSONObject();
        JSONObject honor = new JSONObject();
        JSONObject work = new JSONObject();//作品有分类
        JSONObject photo = new JSONObject();
        JSONObject menu = new JSONObject();
        json.put("webInfo", webInfo);
        json.put("videoList", videoList);
        json.put("group", group);
        json.put("devLine", devLine);
        json.put("honor", honor);
        json.put("work", work);
        json.put("photo", photo);
        json.put("menu", menu);
        return null;
    }

    //获取文章
    public JSONObject getArticle(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        String email = body.getString("email");
        return null;
    }

    //订阅邮件
    public JSONObject subEmail(JSONObject jsonObject) {
        JSONObject body = jsonObject.getJSONObject("body");
        String email = body.getString("email");
        return null;
    }

}
