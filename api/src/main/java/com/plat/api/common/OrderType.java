package com.plat.api.common;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/6/18
 * Time: 23:35
 * Description: No Description
 */
public interface OrderType {
    /**
     * 订单类型
     */
    String VIDE0 = "VIDEO";//视频
    String AUCTION = "AUCTION";//拍卖
    String LIVE = "LIVE";//直播
    String YSP = "YSP";//艺术品
    /**
     * 订单状态
     */
    Integer CREATE_ORDER = 101; //创建订单
    Integer ORDER_PAY = 102; //订单支付
    Integer ORDER_REFUND = 103; //订单退款
    Integer ORDER_FINISH = 104; //订单完成
    Integer ORDER_EXPIRE = 105; //订单过期
    Integer ORDER_CANCEL = 106; //订单取消
    /**
     * 发货运输状态
     */
    Integer ORDER_EXPRESS_NO_DISTRIBUTE = 201; //新进订单需要打包
    Integer ORDER_EXPRESS_DISTRIBUTE = 202; //订单发货
    Integer ORDER_EXPRESS_WAITING = 203; //订单运输途中
    Integer ORDER_EXPRESS_FINISH = 204; //订单收货
    Integer NOT_SUPPORTED_EXPRESS = 205; //改单不支持发货
    /**
     * 退款相关状态
     */
    Integer ORDER_REFUNDING = 300; //退款申请中
    Integer ORDER_NO_REFUND = 301; //该笔允许退款申请
    Integer ORDER_REFUND_WAITING = 302; //退款失败订单
    Integer ORDER_REFUND_FINISH = 303; //订单提交审核完成并且退款
    Integer ORDER_REFUND_REJECT = 304; //订单提交审核拒绝 拒绝退款订单中必须 说明原因 说明原因项目不能为空 例如超过退款时间30天
    /**
     * 支付状态
     */
    Integer FINISH_PAY = 1; //支付成功
    Integer NO_PAY = 0;//没有支付。默认值
    Integer PAY_REFUND = 2;//成功支付退款成功

    /**
     * 拍卖状态状态
     */
    Integer AUCTION_SUCCESS = 401;//拍卖成功
    Integer AUCTION_FAIL = 402;//拍卖成功
    Integer AUCTION_CREATE_ORDER = 403;//拍卖创建订单
    Integer AUCTION_CREATE_RUNNING = 405;//拍卖进行中

    String WECHAT_PAY = "WECHAT_PAY";
}
