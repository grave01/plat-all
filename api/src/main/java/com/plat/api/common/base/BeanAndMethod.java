package com.plat.api.common.base;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/15
 * Time: 16:19
 * Description: 构造请求的类和方法名称
 */
@Data
public class BeanAndMethod {
    /**
     * beanName
     */
    private String beanName;
    /**
     * methodName
     */
    private String methodName;

    private String cmd;
}
