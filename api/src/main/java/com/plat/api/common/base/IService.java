package com.plat.api.common.base;

import com.alibaba.fastjson.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/10
 * Time: 19:08
 * Description: No Description
 */
public interface IService {
    public JSONObject cmsGet(JSONObject jsonObject);
}
