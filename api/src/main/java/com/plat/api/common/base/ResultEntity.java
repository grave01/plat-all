package com.plat.api.common.base;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/15
 * Time: 16:47
 * Description: No Description
 */
@Data
public class ResultEntity {
    /**
     * 返回的结果对象
     */
    private Object result;
    /**
     * 请求参数
     */
    private Object param;

}
