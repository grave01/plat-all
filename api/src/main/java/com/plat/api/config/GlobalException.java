package com.plat.api.config;

import com.plat.api.common.Msg;
import com.plat.api.common.base.ERROR;
import com.plat.api.dao.sys.PlatReqInfoDao;
import com.plat.api.entity.sys.PlatReqInfo;
import com.plat.api.exception.PlatException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/28
 * Time: 9:24
 * Description: No Description
 */
@Slf4j
@RestControllerAdvice
public class GlobalException {
    @Autowired
    private PlatReqInfoDao reqInfoDao;

    /**
     * 系统异常处理，比如：404,500
     *
     * @param req
     * @param resp
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public void defaultErrorHandler(HttpServletRequest req, HttpServletResponse resp, Exception e) throws Exception {
        //log.error("", e);
        if (e instanceof org.springframework.web.servlet.NoHandlerFoundException) {
            log.info(e.getMessage());
            ERROR.error(resp, new PlatException("请求不存在"), "cmd");
//            PlatReqInfo platReqInfo = new PlatReqInfo();
//            platReqInfo.setReq_body("请求不存在");
//            platReqInfo.setUrl(req.getRequestURL().toString());
//            platReqInfo.setCreate_time(new Date());
//            reqInfoDao.insert(platReqInfo);
        }
    }

}
