package com.plat.api.config;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * author ss
 * createTime 2020/5/9
 * package ${com.plat.api.config}
 ***/
@Configuration
@Slf4j
public class WxConfig {
    public WxConfig() {
        log.info("加载wx支付相关到容器" + "包名称：" + WxConfig.class.getPackage() + "方法：wxMaService、wxPayService");
    }

    @Bean
    @ConditionalOnMissingBean(WxMaService.class)
    public WxMaService wxMaService() {
        WxMaDefaultConfigImpl config = new WxMaDefaultConfigImpl();
        config.setAppid(StringUtils.trimToNull("wxe461b76c983ac6bb"));
        config.setSecret(StringUtils.trimToNull("f6dbdc608e804ae2f6bd372a0f56024c"));
        config.setToken(StringUtils.trimToNull("this.properties.getToken()"));
        config.setAesKey(StringUtils.trimToNull("this.properties.getAesKey()"));
        config.setMsgDataFormat(StringUtils.trimToNull("this.properties.getMsgDataFormat()"));
        final WxMaServiceImpl service = new WxMaServiceImpl();
        service.setWxMaConfig(config);
        return service;
    }

    @Bean
    @ConditionalOnMissingBean(WxPayService.class)
    public WxPayService wxPayService() {
        final WxPayServiceImpl wxPayService = new WxPayServiceImpl();
        wxPayService.setConfig(WxPayCommentConfig.getCreateOrderConfig());
        return wxPayService;
    }
}
