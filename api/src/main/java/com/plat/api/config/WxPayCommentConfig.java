package com.plat.api.config;

import com.github.binarywang.wxpay.config.WxPayConfig;
import com.plat.api.util.YmalPropertiesUtil;
import org.apache.commons.lang3.StringUtils;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/21
 * Time: 16:38
 * Description: 微信支付相关配置信息
 */
public class WxPayCommentConfig {

    /**
     * 默认付款回调配置
     */
    public static WxPayConfig getCreateOrderConfig() {
        WxPayConfig payConfig = getWxPayConfig();
        payConfig.setNotifyUrl(YmalPropertiesUtil.getValue("wxpay.notify-url").toString());
        return payConfig;
    }

    /**
     * 默认退款回调配置
     */
    public static WxPayConfig getRefundConfig() {
        WxPayConfig payConfig = getWxPayConfig();
        payConfig.setNotifyUrl(YmalPropertiesUtil.getValue("wxpay.refund-notify-url").toString());
        return payConfig;
    }

    /**
     * 拍卖付款回调配置
     */
    public static WxPayConfig getAuctionConfig() {
        WxPayConfig payConfig = getWxPayConfig();
        payConfig.setNotifyUrl(YmalPropertiesUtil.getValue("wxpay.auction-notify-url").toString());
        return payConfig;
    }
    /**
     * 订阅回调通知
     */
    public static WxPayConfig getSubNotifyConfig() {
        WxPayConfig payConfig = getWxPayConfig();
        payConfig.setNotifyUrl(YmalPropertiesUtil.getValue("wxpay.sub-notify-url").toString());
        return payConfig;
    }
    /**
     * 购买商品回调通知
     */
    public static WxPayConfig getYSPNotifyConfig() {
        WxPayConfig payConfig = getWxPayConfig();
        payConfig.setNotifyUrl(YmalPropertiesUtil.getValue("wxpay.buy-goods-notify-url").toString());
        return payConfig;
    }
    /**
     * 购买视频回调通知
     */
    public static WxPayConfig getVideoNotifyConfig() {
        WxPayConfig payConfig = getWxPayConfig();
        payConfig.setNotifyUrl(YmalPropertiesUtil.getValue("wxpay.notify-url").toString());
        return payConfig;
    }

    private static WxPayConfig getWxPayConfig() {
        WxPayConfig payConfig = new WxPayConfig();
        payConfig.setAppId(StringUtils.trimToNull("wxe461b76c983ac6bb"));
        payConfig.setMchId(StringUtils.trimToNull("1583531981"));
        payConfig.setMchKey(StringUtils.trimToNull("SQgxqGozx2EDtHfLCHfthnhRAQq65Spl"));
        payConfig.setKeyPath(StringUtils.trimToNull("classpath:cert/apiclient_cert.p12"));
        return payConfig;
    }

}
