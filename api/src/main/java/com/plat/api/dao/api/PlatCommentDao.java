package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatCommentEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/10
 * Time: 12:45
 * Description: No Description
 */
public interface PlatCommentDao extends BaseMapper<PlatCommentEntity> {
}
