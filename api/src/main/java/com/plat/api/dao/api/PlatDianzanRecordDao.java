package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatDianzanRecordEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/5
 * Time: 15:11
 * Description: 点赞记录操做
 */
public interface PlatDianzanRecordDao extends BaseMapper<PlatDianzanRecordEntity> {
}
