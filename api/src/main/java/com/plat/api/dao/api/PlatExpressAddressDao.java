package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatExpressAddressEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/28
 * Time: 0:04
 * Description: No Description
 */
public interface PlatExpressAddressDao extends BaseMapper<PlatExpressAddressEntity> {
}
