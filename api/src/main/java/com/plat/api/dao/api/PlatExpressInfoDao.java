package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatExpressInfoEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/9
 * Time: 15:49
 * Description: No Description
 */
public interface PlatExpressInfoDao extends BaseMapper<PlatExpressInfoEntity> {
}
