package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatMyWorldEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/10
 * Time: 11:55
 * Description: No Description
 */
public interface PlatMyWorldDao extends BaseMapper<PlatMyWorldEntity> {
}
