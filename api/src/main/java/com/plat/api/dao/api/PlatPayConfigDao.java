package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatPayConfigEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/6
 * Time: 23:26
 * Description: 获取支付配置信息
 */
public interface PlatPayConfigDao extends BaseMapper<PlatPayConfigEntity> {

}
