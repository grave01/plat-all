package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatRefundApplyEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/10
 * Time: 17:44
 * Description: No Description
 */
public interface PlatRefundApplyDao extends BaseMapper<PlatRefundApplyEntity> {
}
