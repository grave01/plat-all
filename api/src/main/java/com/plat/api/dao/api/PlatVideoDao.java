package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatVideoEntity;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/10
 * Time: 8:48
 * Description: No Description
 */
public interface PlatVideoDao extends BaseMapper<PlatVideoEntity> {
    List<PlatVideoEntity> getList(String keyword,int page,int pageSize);
}
