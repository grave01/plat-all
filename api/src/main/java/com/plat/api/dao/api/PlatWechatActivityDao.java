package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatWechatActivityEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/2
 * Time: 23:46
 * Description: No Description
 */
public interface PlatWechatActivityDao extends BaseMapper<PlatWechatActivityEntity> {
}
