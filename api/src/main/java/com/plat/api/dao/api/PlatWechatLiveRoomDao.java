package com.plat.api.dao.api;

import com.plat.api.entity.api.PlatWechatLiveRoomEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/11
 * Time: 17:08
 * Description: No Description
 */
public interface PlatWechatLiveRoomDao extends BaseMapper<PlatWechatLiveRoomEntity> {
}
