package com.plat.api.dao.api;

import com.plat.api.entity.api.UserEntity;
import org.beetl.sql.core.annotatoin.Sql;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * author ss
 * createTime 2020/5/9
 * package ${com.plat.api.dao.api}
 ***/
public interface UserDao extends BaseMapper<UserEntity> {
    @Sql("select * from plat_user as a where a.open_id =?")
    UserEntity getUserInfo(String key1);

    @Sql("select * from plat_user as a where a.email =? or a.phone =? and a.password =?")
    UserEntity getUserInfo(String email, String phone, String pwd);
}
