package com.plat.api.dao.param;

import com.plat.api.entity.sys.PlatParamEntity;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/10
 * Time: 13:40
 * Description: No Description
 */
public interface SysPlatParamDao extends BaseMapper<PlatParamEntity> {
}
