package com.plat.api.dao.sys;

import com.plat.api.entity.sys.PlatReqInfo;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/28
 * Time: 10:07
 * Description: No Description
 */
public interface PlatReqInfoDao extends BaseMapper<PlatReqInfo> {

}
