package com.plat.api.entity.api;

import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/6/9
 * Time: 23:53
 * Description: No Description
 */
@Data
public class Address2 extends BaseEntity {
    private String name;
    private  List<CityBean> city;

    public static class CityBean {
        private String name;
        private  List<String> area;

        public List<String> getArea() {
            return area;
        }

        public void setArea(List<String> area) {
            this.area = area;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
}
