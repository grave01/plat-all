package com.plat.api.entity.api;

import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/28
 * Time: 0:00
 * Description: No Description
 */
@Table(name = "plat_address")
@Data
public class PlatAddressEntity extends BaseEntity {

    private Integer id;
    private String platAddressName;
    private Integer userId;
    private String province;
    private String city;
    private String county;
    private String addressDetail;
    private String areaCode;
    private String postalCode;
    private String tel;
    private Integer isDefault;
    private Integer deleted;
    private String platAddressCode;
}
