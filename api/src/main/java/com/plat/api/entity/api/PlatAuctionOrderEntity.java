package com.plat.api.entity.api;

import com.baomidou.mybatisplus.annotation.TableName;
import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/21
 * Time: 13:59
 * Description: No Description
 */
@Data
@Table(name = "plat_auction_order")
@TableName(value = "plat_auction_order")
public class PlatAuctionOrderEntity extends BaseEntity {
    private Integer id;
    private Double lastPrice;
    private Double lastAddPrice;
    private String goodsCode;
    private String mainOderNo;
    private Integer userId;
    private Integer goodsId;
    private Date createTime;
    private Date updateTime;
    private Integer orderStatus;
    private Integer addressId;
    private String addressJson;
}
