package com.plat.api.entity.api;

import com.baomidou.mybatisplus.annotation.TableName;
import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/21
 * Time: 13:59
 * Description: No Description
 */
@Data
@Table(name = "plat_auction_record")
@TableName(value = "plat_auction_record")
public class PlatAuctionOrderRecordEntity {
    private Integer id;
    private Double payMoney;
    private Integer payStatus;
    private Date payTime;
    private String auctionMainOrder;
    private Integer userId;
    private Date createTime;
    private String pTime;
    private String outTradeNo;
    private String paySn;
    private Integer refundStatus;
    private Date refundTime;
    private Integer goodsId;
    private UserEntity userEntity;
    private AuctionEntity auctionEntity;
    private Double thisAmount;
    private Integer lastPrice;
}
