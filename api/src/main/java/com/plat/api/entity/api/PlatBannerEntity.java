package com.plat.api.entity.api;

import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.AutoID;
import org.beetl.sql.core.annotatoin.Table;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/15
 * Time: 23:10
 * Description: No Description
 */
@Data
@Table(name = "plat_banner")
public class PlatBannerEntity extends BaseEntity {
    @AutoID
    private Integer id;
    private String url;
    private String imageUrl;
    private String detail;
    private Integer orderNum;
     /**
     * 是否是H5页面
     */
    private Integer isH5;
}
