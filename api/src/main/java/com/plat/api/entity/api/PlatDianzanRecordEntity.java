package com.plat.api.entity.api;

import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.AutoID;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/5
 * Time: 15:08
 * Description: 点赞记录表
 */
@Data
@Table(name = "plat_dianzan_record")
public class PlatDianzanRecordEntity extends BaseEntity {
    /**
     * $column.columnComment
     */
    @AutoID
    private Long id;
    //点赞对象
    private Long targetId;
    //用户id
    private Integer userId;
    //点赞时间
    private Date dianzanTime;
}
