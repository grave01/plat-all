package com.plat.api.entity.api;

import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/28
 * Time: 0:00
 * Description: No Description
 */
@Table(name = "plat_express_address")
@Data
public class PlatExpressAddressEntity {
    private Integer id;//主键
    private String consignee;//收件人
    private String mobile;//手机号
    private String transportDay;//配送日期
    private String provinceName;//省份
    private String cityName;//城市
    private String countyName;//地区
    private String address;//详细地址
    private Integer isDefault;//是否默认
    private Integer userId;//用户id
}
