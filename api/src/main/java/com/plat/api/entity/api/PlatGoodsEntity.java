package com.plat.api.entity.api;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/9/10
 * Time: 23:36
 * Description: No Description
 */
@Data
@TableName(value = "plat_goods")
public class PlatGoodsEntity {
    @TableId(type = IdType.AUTO)
    private Long id;
    private String goodsName;
    private Integer goodsType;
    private String goodsDesc;
    private Integer goodsStatus;
    private Integer userId;
    private String userName;
    private Double goodsPrice;
    private Double discountPrice;
    private Date createTime;
    private Date updateTime;
    private String goodsImage;
    private String goodsDeatail;
    private String goodsSn;
    private String category;//类型艺术品YSP
    @TableField(exist = false)
    private String endTime;
    private Integer num;//商品数量
    @TableField(exist = false)
    private Integer percent;//百分比
    private Integer initNum;//初始化库存
    private Double expressFee;//运费

}
