package com.plat.api.entity.api;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/9/5
 * Time: 22:53
 * Description: No Description
 */
@Data
@Table(name = "plat_live_sub")
@TableName(value = "plat_live_sub")
public class PlatLiveSubEntity {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private Long liveId;
    private Integer userId;
    private Date startTime;
    private Integer payStatus;
    private Date createTime;
    private String outTradeNo;
    private String payNo;
}
