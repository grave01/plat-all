package com.plat.api.entity.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.AutoID;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/10
 * Time: 11:32
 * Description: No Description
 */
@Data
@Table(name = "plat_my_world")
public class PlatMyWorldEntity extends BaseEntity {

    /** $column.columnComment */
    @AutoID
    private Long id;

    /** 发布的信息 */
    private String content;

    /** 图片地址json */
    private String url;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date publishTime;

    /** 0是未发布1是发布 */
    private Long isPublish;

    /** 发布者id */
    private Integer userId;

    /** 是否私密 */
    private Integer isSelf;

    /** 只允许谁看 */
    private String viewUserId;

    /** 不给谁看 */
    private String noViewUserId;

    private Integer readNum;

    private Integer likeNum;

    private Integer noLikeNum;

    private Integer tapNum;

    private UserEntity user;
    private String pubTime;
    public String longTime;
    //是否置顶
    private Integer isTop;
    //是否广告
    private Integer isAdd;
    //评论人数
    private Long commentNum;

    private Integer isTap;
    //发布地址
    private String publishAddress;
    //发布纬度
    private String latitude;
    //发布经度
    private String longitude;
}
