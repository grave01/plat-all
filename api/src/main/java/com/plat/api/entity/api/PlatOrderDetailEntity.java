package com.plat.api.entity.api;

import lombok.Data;
import org.beetl.sql.core.annotatoin.AutoID;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/21
 * Time: 9:55
 * Description: No Description
 */
@Data
@Table(name = "plat_order_detail")
public class PlatOrderDetailEntity {
    @AutoID
    private Integer id;
    private String  orderNo;
    private long    goodsId;
    private Date    createTime;
    private Integer publisherId;
    private Integer userId;
    private String  orderType;
    /**
     * 商品类型 商品类型VIDEO-视频，PAIMAI-拍卖，NORMAL-普通实物
     */
    private String goodsType;
    private String goodsName;
    private String goodsDetail;
}
