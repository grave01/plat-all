package com.plat.api.entity.api;

import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

import java.math.BigDecimal;
import java.util.Date;


/*
 *
 * gen by beetlsql 2020-07-20
 */
@Data
@Table(name = "plat_order")
public class PlatOrderEntity extends BaseEntity {


    private Integer id;
    /*
    配送地址id
    */
    private Integer deliveryId;
    /*
    订单优惠id
    */
    private Integer orderCouponId;
    /*
    订单状态100完成，200发货，300收货，400退款，500待发货600未发货
    */
    private Integer orderStatus;
    /*
    支付状态1支付0是未支付2是退款
    */
    private Integer payStatus;
    /*
    支付方式
    */
    private String payType;
    /*
    用户id
    */
    private Integer userId;
    /*
    顶大优惠金额
    */
    private BigDecimal orderCouponPrice;
    /*
    订单号码
    */
    private String orderNo;
    /*
    订单金额
    */
    private BigDecimal orderPrice;
    /*
    订单实际金额
    */
    private BigDecimal orderRealPrice;
    /*
    订单流水
    */
    private String orderSn;
    /*
    类型vitual,real  VIRTUAL,REAL
    */
    private String orderType;
    /*
    下单手机号
    */
    private String phone;
    /*
    创建时间
    */
    private Date createTime;
    /*
    支付时间
    */
    private Date payTime;
    /*
   发货状态
   */
    private Integer expressStatus;
    /*
    退货货状态
     */
    private Integer refundStatus;
    /*
    下单者昵称
     */
    private String  userName;
    private BigDecimal lastAddPrice;
    private BigDecimal expressFee;

}
