package com.plat.api.entity.api;

import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/6
 * Time: 23:20
 * Description: No Description
 */

@Data
@Table(name = "plat_pay_config")
public class PlatPayConfigEntity extends BaseEntity {
    private Integer id;
    private String detail;//详细描述
    private String mchId;//商户号
    private String mchKey;//微信商户密钥
    private String privateKey;//支付宝商户密钥
    private String publickey;//支付宝商户公钥
    private String mPublicKey;//支付宝平台公钥
    private String payType;//支付宝类型
    private Date createTime;//创建时间
    private String isUse;//是否启用1-启用0是关闭2-是停用3-冻结
    private String appId;//appid
}
