package com.plat.api.entity.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.AutoID;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;
import java.util.List;

/**
 * 平台相关的人物对象 plat_people
 * 
 * @author plat
 * @date 2020-05-31
 */
@Data
@Table(name = "plat_people")
public class PlatPeopleEntity
        extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** id */
    @AutoID
    private Long id;

    /** 人物名称 */
    private String userName;

    /** 昵称 */
    private String nickname;

    /** 分类逗号隔开 */
    private String category;
    /** 分类逗号隔开 */
    private String categoryName;
    /** 人物描述 */
    private String detail;

    /** 个人照片 */
    private String albumId;

    /** 作品集合 */
    private String workAlbumId;

    /** 绑定系统用户id */
    private Long userId;

    /** 手机号 */
    private String phone;

    /** qq */
    private String qq;

    /** 微信 */
    private String wx;

    /** 邮箱 */
    private String email;

    /** 主图 头像url */
    private String mainPhoto;

    /** 是否解雇 */
    private Integer isFire;

    /** 专业 */
    private String pro;

    /** 生日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birth;

    /** 身份证号 */
    private String idNo;

    /** 家庭住址 */
    private String address;

    /** 目前住址 */
    private String nowAddress;

    private List<Long> platCategory;
    /** 个人 */
    private List<String> listAlbumUrl;
    /** 作品 */
    private List<String> listWorkUrl;
    private List<String> allUrl;
}
