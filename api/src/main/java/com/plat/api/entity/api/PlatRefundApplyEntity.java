package com.plat.api.entity.api;

import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/6/9
 * Time: 23:53
 * Description: 退款申请
 */
@Data
public class PlatRefundApplyEntity extends BaseEntity {
    private String orderNo;
    private String refundNo;
    private Date createTime;
    private Double refundAmt;
    private Double refundRealAmt;
    private Date finishTime;
    private Integer refundStatus;
    private String refundMsg;
    private String refund_reason;
    private String refundInfo;
    private Integer userId;
    private String userName;
    private String rejectReason;
    private String phone;
}
