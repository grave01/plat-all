package com.plat.api.entity.api;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/9/15
 * Time: 15:24
 * Description: No Description
 */
@Data
@TableName(value = "plat_order_refund")
public class PlatRefundEntity {
    private int id; //id
    private String orderNo;//主单号
    private String refundNo;//退款单号
    private Date createTime;//
    private Double refundAmt;//
    private Double refundRealAmt;//实际退款
    private Date finishTime;//完成时间
    private int refundStatus;//退款状态
    private String refundReason;//退款理由
    private String refundInfo;//退款信息（全部信息，以json保存）
    private int userId;//申请用户id
    private String userName;//申请用户名
    private String rejectReason;//拒绝原因
    private String phone;//手机号

}
