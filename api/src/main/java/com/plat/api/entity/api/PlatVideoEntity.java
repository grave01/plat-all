package com.plat.api.entity.api;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/10
 * Time: 8:45
 * Description: No Description
 */
@Data
@Table(name = "plat_video")
@TableName(value = "plat_video")
public class PlatVideoEntity {
    private static final long serialVersionUID = 1L;
    @TableId(value = "id",type = IdType.AUTO)
    private long id;//主键
    private String videoName; //视频名称
    private String url; //地址
    private String detail; //描述
    private Integer isFree; //是否付费
    private Double price; //原价格
    private Date createTime; //创建时间
    private Date updateTime; //更新时间
    private Integer isDelete; //是否删除
    private Integer createUser; //创建者id
    private String createUsername; //创建者
    private String category; //分类
    private Integer videoType; //类型1001-长视频,1002-短视频
    private Integer isCheck; //审核1是审核0是未审核2是拒绝3是下架
    private Integer isTop; //是否置顶
    private Integer isRecommend; //是否推荐
    private String adId; //广告码
    private Integer freeTime; //免费时长s为单位
    private String poster; //封面图片
    private String imageId; //相片集合
    private Integer num; //播放量
    private String m3u8Url; //m3u8视频地址
    private String goodsType; //商品类型VIDEO-视频，PAIMAI-拍卖，NORMAL-普通实物
    private String videoInfo; //七牛云视频信息
    @TableField(exist = false)
    private String src; //视频地址
    @TableField(exist = false)
    private String title; //标题
    @TableField(exist = false)
    private String publishTime; //七牛云视频信息
    @TableField(exist = false)
    private Integer showFreeItem; //显示支付按钮

}