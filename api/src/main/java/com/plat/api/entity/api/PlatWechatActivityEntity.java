package com.plat.api.entity.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * 活动对象 plat_wechat_activity
 *
 * @author plat
 * @date 2020-08-04
 */
@Data
@Table(name = "plat_wechat_activity")
public class PlatWechatActivityEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * $column.columnComment
     */
    private String title;

    /**
     * 方法名称和参数
     */
    private String tap;

    /**
     * 活动描述
     */
    private String detail;

    /**
     * 开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
     * 是否删除
     */
    private Integer isDelete;

    /**
     * 海报图片
     */
    private String img;

    /**
     * H5活动地址
     */
    private String url;

    /**
     * 是否h5页面
     */
    private Integer isH5;

    /**
     * 点击浏览量
     */
    private Long tapNum;


}
