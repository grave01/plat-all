package com.plat.api.entity.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/11
 * Time: 17:07
 * Description: No Description
 */
@Table(name = "plat_wechat_live_room")
@Data
public class PlatWechatLiveRoomEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 直播间名称 */
    private String name;

    /** 封面 */
    private String coverImg;

    /** 开播时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;
    private String start_Time;

    private String end_Time;

    /** 主播昵称 */
    private String anchorName;

    /** 主播微信号 */
    private String anchorWechat;

    /** 分享照片 */
    private String shareImg;

    /** 直播类型 直播类型，1 推流 0 手机直播 */
    private Integer type;

    /** 1：横屏 0：竖屏 */
    private Integer screenType;

    /** 是否 关闭点赞 1 关闭 */
    private Integer closeLike;

    /** 是否 关闭商品货架，1：关闭 */
    private Integer closeGoods;

    /** 是否开启评论，1：关闭 */
    private Integer closeComment;

    /** 密码口令观看 */
    private String password;

    /** 置顶直播 */
    private Integer isTop;

    /** 直播详细描述 */
    private String detail;

    /** 直播图片集合 */
    private String imageUrls;

    /** 是否检查0未检查1是检查 */
    private Integer isCheck;

    /** 房间号 微信返回 */
    private Long roomId;

    /** 是否下架 */
    private Integer isOffonline;

    /** 是否免费 */
    private Integer isFree;

    /** 价格 */
    private Double price;

    /** 封面 */
    private String poster;

    /** 回放视频地址 */
    private String mediaUrl;

    /** 直播状态 */
    private Integer liveStatus;
    /** 直播状态 */
    private Integer createUser;

    /** 直播状态名称 */
    private String liveStatusName;
    private String  coverImgUrl;
    private String shareImgUrl;
    /** 是否已经订阅 */
    private Integer isSub;
    private UserEntity  user;
    private Integer  subNum;
}
