package com.plat.api.entity.api;

import com.plat.api.entity.RepGoodsInfo;
import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/9
 * Time: 15:18
 * Description: No Description
 */
@Data
public class RepOrderDetail extends BaseEntity {

    private  String orderNo;
    private Integer orderStatus;
    private  List<RepGoodsInfo> goodsList;
    private Integer  count;
    private BigDecimal countMoney;
    private Integer payStatus;
    private Integer status;//订单状态
    private String creatTime;//订单创建时间
    private String payTime;//订单支付时间
    private Object user;
    private Integer refundStatus;//退款状态
    private String orderType;//订单类型
    private Integer expressStatus;//发货状态
    private String orderTypeName;//订单类型名称
}


