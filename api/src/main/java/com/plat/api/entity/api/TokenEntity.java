package com.plat.api.entity.api;

import com.plat.api.sys.entity.BaseEntity;
import lombok.Data;
import org.beetl.sql.core.annotatoin.AutoID;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/9
 * Time: 22:44
 * Description: No Description
 */
@Data
@Table(name = "plat_token")
public class TokenEntity  extends BaseEntity {
    @AutoID
    private Integer id;
    private Integer userId;
    private String token;
    private String refreshToken;
    private Date createTime;
    private Date expireTime;
    private Date refreshTime;
}
