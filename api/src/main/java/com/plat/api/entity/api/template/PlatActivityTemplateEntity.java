package com.plat.api.entity.api.template;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/3
 * Time: 1:38
 * Description: No Description
 */
@Data
public class PlatActivityTemplateEntity {
    private String name;
    private Item item;
    private String tap;

}
