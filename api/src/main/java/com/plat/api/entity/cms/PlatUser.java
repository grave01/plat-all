package com.plat.api.entity.cms;

import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;


/* 
* 
* gen by beetlsql 2020-07-13
*/
@Table(name="plat.plat_user")
public class PlatUser   {
	
	// alias
	public static final String ALIAS_id = "id";
	public static final String ALIAS_auth_status = "auth_status";
	public static final String ALIAS_field_id = "field_id";
	public static final String ALIAS_gender = "gender";
	public static final String ALIAS_has_auth = "has_auth";
	public static final String ALIAS_has_edit = "has_edit";
	public static final String ALIAS_is_lock = "is_lock";
	public static final String ALIAS_is_vip = "is_vip";
	public static final String ALIAS_type = "type";
	public static final String ALIAS_vip_level = "vip_level";
	public static final String ALIAS_account = "account";
	public static final String ALIAS_account_index = "account_index";
	public static final String ALIAS_address = "address";
	public static final String ALIAS_email = "email";
	public static final String ALIAS_field = "field";
	public static final String ALIAS_id_back = "id_back";
	public static final String ALIAS_id_front = "id_front";
	public static final String ALIAS_id_no = "id_no";
	public static final String ALIAS_is_import = "is_import";
	public static final String ALIAS_medal = "medal";
	public static final String ALIAS_name = "name";
	public static final String ALIAS_nickname = "nickname";
	public static final String ALIAS_now_address = "now_address";
	public static final String ALIAS_open_id = "open_id";
	public static final String ALIAS_password = "password";
	public static final String ALIAS_phone = "phone";
	public static final String ALIAS_userInfo = "userInfo";
	public static final String ALIAS_wx_head_photo = "wx_head_photo";
	public static final String ALIAS_auth_time = "auth_time";
	public static final String ALIAS_create_time = "create_time";
	
	private Integer id ;
	/*
	认证状态1是认证2是拒绝0是未认证3是等待认证
	*/
	private Integer authStatus ;
	/*
	领域id
	*/
	private Integer fieldId ;
	/*
	1是男 2是女 0是未知
	*/
	private Integer gender ;
	/*
	是否认证完毕选择is_import其他人物 未认真是普通人，可以人工处理认证
	*/
	private Integer hasAuth ;
	/*
	信息已编辑
	*/
	private Integer hasEdit ;
	/*
	账户是否被锁
	*/
	private Integer isLock ;
	/*
	平台vip会员(购买或者赠送得到)
	*/
	private Integer isVip ;
	/*
	账户类型1是微信2是支付宝3是其他
	*/
	private Integer type ;
	/*
	vip等级
	*/
	private Integer vipLevel ;
	/*
	账户 一年范围内改一次
	*/
	private String account ;
	/*
	账户索引 这个永不更改（平台唯一标识，非技术员不可见）
	*/
	private String accountIndex ;
	/*
	所在地址（认证之后为身份证地址）
	*/
	private String address ;
	/*
	邮箱
	*/
	private String email ;
	/*
	领域
	*/
	private String field ;
	/*
	身份证后照
	*/
	private String idBack ;
	/*
	身份证前照
	*/
	private String idFront ;
	/*
	身份证
	*/
	private String idNo ;
	/*
	3 普通人 4 公众人物（明星4-1，网红4-2）  6 军人 7 港澳同胞 8 台湾同胞 
	*/
	private String isImport ;
	/*
	拥有勋章（存储json图片地址）
	*/
	private String medal ;
	/*
	昵称
	*/
	private String name ;
	/*
	昵称
	*/
	private String nickname ;
	/*
	现居住地
	*/
	private String nowAddress ;
	/*
	微信openid
	*/
	private String openId ;
	/*
	密码
	*/
	private String password ;
	/*
	手机号
	*/
	private String phone ;
	/*
	微信用户信息
	*/
	private String userinfo ;
	/*
	微信头像地址
	*/
	private String wxHeadPhoto ;
	/*
	认证时间
	*/
	private Date authTime ;
	/*
	注册时间
	*/
	private Date createTime ;
	
	public PlatUser() {
	}
	
	public Integer getId(){
		return  id;
	}
	public void setId(Integer id ){
		this.id = id;
	}
	
	/**
	* 认证状态1是认证2是拒绝0是未认证3是等待认证
	*@return 
	*/
	public Integer getAuthStatus(){
		return  authStatus;
	}
	/**
	* 认证状态1是认证2是拒绝0是未认证3是等待认证
	*@param  authStatus
	*/
	public void setAuthStatus(Integer authStatus ){
		this.authStatus = authStatus;
	}
	
	/**
	* 领域id
	*@return 
	*/
	public Integer getFieldId(){
		return  fieldId;
	}
	/**
	* 领域id
	*@param  fieldId
	*/
	public void setFieldId(Integer fieldId ){
		this.fieldId = fieldId;
	}
	
	/**
	* 1是男 2是女 0是未知
	*@return 
	*/
	public Integer getGender(){
		return  gender;
	}
	/**
	* 1是男 2是女 0是未知
	*@param  gender
	*/
	public void setGender(Integer gender ){
		this.gender = gender;
	}
	
	/**
	* 是否认证完毕选择is_import其他人物 未认真是普通人，可以人工处理认证
	*@return 
	*/
	public Integer getHasAuth(){
		return  hasAuth;
	}
	/**
	* 是否认证完毕选择is_import其他人物 未认真是普通人，可以人工处理认证
	*@param  hasAuth
	*/
	public void setHasAuth(Integer hasAuth ){
		this.hasAuth = hasAuth;
	}
	
	/**
	* 信息已编辑
	*@return 
	*/
	public Integer getHasEdit(){
		return  hasEdit;
	}
	/**
	* 信息已编辑
	*@param  hasEdit
	*/
	public void setHasEdit(Integer hasEdit ){
		this.hasEdit = hasEdit;
	}
	
	/**
	* 账户是否被锁
	*@return 
	*/
	public Integer getIsLock(){
		return  isLock;
	}
	/**
	* 账户是否被锁
	*@param  isLock
	*/
	public void setIsLock(Integer isLock ){
		this.isLock = isLock;
	}
	
	/**
	* 平台vip会员(购买或者赠送得到)
	*@return 
	*/
	public Integer getIsVip(){
		return  isVip;
	}
	/**
	* 平台vip会员(购买或者赠送得到)
	*@param  isVip
	*/
	public void setIsVip(Integer isVip ){
		this.isVip = isVip;
	}
	
	/**
	* 账户类型1是微信2是支付宝3是其他
	*@return 
	*/
	public Integer getType(){
		return  type;
	}
	/**
	* 账户类型1是微信2是支付宝3是其他
	*@param  type
	*/
	public void setType(Integer type ){
		this.type = type;
	}
	
	/**
	* vip等级
	*@return 
	*/
	public Integer getVipLevel(){
		return  vipLevel;
	}
	/**
	* vip等级
	*@param  vipLevel
	*/
	public void setVipLevel(Integer vipLevel ){
		this.vipLevel = vipLevel;
	}
	
	/**
	* 账户 一年范围内改一次
	*@return 
	*/
	public String getAccount(){
		return  account;
	}
	/**
	* 账户 一年范围内改一次
	*@param  account
	*/
	public void setAccount(String account ){
		this.account = account;
	}
	
	/**
	* 账户索引 这个永不更改（平台唯一标识，非技术员不可见）
	*@return 
	*/
	public String getAccountIndex(){
		return  accountIndex;
	}
	/**
	* 账户索引 这个永不更改（平台唯一标识，非技术员不可见）
	*@param  accountIndex
	*/
	public void setAccountIndex(String accountIndex ){
		this.accountIndex = accountIndex;
	}
	
	/**
	* 所在地址（认证之后为身份证地址）
	*@return 
	*/
	public String getAddress(){
		return  address;
	}
	/**
	* 所在地址（认证之后为身份证地址）
	*@param  address
	*/
	public void setAddress(String address ){
		this.address = address;
	}
	
	/**
	* 邮箱
	*@return 
	*/
	public String getEmail(){
		return  email;
	}
	/**
	* 邮箱
	*@param  email
	*/
	public void setEmail(String email ){
		this.email = email;
	}
	
	/**
	* 领域
	*@return 
	*/
	public String getField(){
		return  field;
	}
	/**
	* 领域
	*@param  field
	*/
	public void setField(String field ){
		this.field = field;
	}
	
	/**
	* 身份证后照
	*@return 
	*/
	public String getIdBack(){
		return  idBack;
	}
	/**
	* 身份证后照
	*@param  idBack
	*/
	public void setIdBack(String idBack ){
		this.idBack = idBack;
	}
	
	/**
	* 身份证前照
	*@return 
	*/
	public String getIdFront(){
		return  idFront;
	}
	/**
	* 身份证前照
	*@param  idFront
	*/
	public void setIdFront(String idFront ){
		this.idFront = idFront;
	}
	
	/**
	* 身份证
	*@return 
	*/
	public String getIdNo(){
		return  idNo;
	}
	/**
	* 身份证
	*@param  idNo
	*/
	public void setIdNo(String idNo ){
		this.idNo = idNo;
	}
	
	/**
	* 3 普通人 4 公众人物（明星4-1，网红4-2）  6 军人 7 港澳同胞 8 台湾同胞 
	*@return 
	*/
	public String getIsImport(){
		return  isImport;
	}
	/**
	* 3 普通人 4 公众人物（明星4-1，网红4-2）  6 军人 7 港澳同胞 8 台湾同胞 
	*@param  isImport
	*/
	public void setIsImport(String isImport ){
		this.isImport = isImport;
	}
	
	/**
	* 拥有勋章（存储json图片地址）
	*@return 
	*/
	public String getMedal(){
		return  medal;
	}
	/**
	* 拥有勋章（存储json图片地址）
	*@param  medal
	*/
	public void setMedal(String medal ){
		this.medal = medal;
	}
	
	/**
	* 昵称
	*@return 
	*/
	public String getName(){
		return  name;
	}
	/**
	* 昵称
	*@param  name
	*/
	public void setName(String name ){
		this.name = name;
	}
	
	/**
	* 昵称
	*@return 
	*/
	public String getNickname(){
		return  nickname;
	}
	/**
	* 昵称
	*@param  nickname
	*/
	public void setNickname(String nickname ){
		this.nickname = nickname;
	}
	
	/**
	* 现居住地
	*@return 
	*/
	public String getNowAddress(){
		return  nowAddress;
	}
	/**
	* 现居住地
	*@param  nowAddress
	*/
	public void setNowAddress(String nowAddress ){
		this.nowAddress = nowAddress;
	}
	
	/**
	* 微信openid
	*@return 
	*/
	public String getOpenId(){
		return  openId;
	}
	/**
	* 微信openid
	*@param  openId
	*/
	public void setOpenId(String openId ){
		this.openId = openId;
	}
	
	/**
	* 密码
	*@return 
	*/
	public String getPassword(){
		return  password;
	}
	/**
	* 密码
	*@param  password
	*/
	public void setPassword(String password ){
		this.password = password;
	}
	
	/**
	* 手机号
	*@return 
	*/
	public String getPhone(){
		return  phone;
	}
	/**
	* 手机号
	*@param  phone
	*/
	public void setPhone(String phone ){
		this.phone = phone;
	}
	
	/**
	* 微信用户信息
	*@return 
	*/
	public String getUserinfo(){
		return  userinfo;
	}
	/**
	* 微信用户信息
	*@param  userinfo
	*/
	public void setUserinfo(String userinfo ){
		this.userinfo = userinfo;
	}
	
	/**
	* 微信头像地址
	*@return 
	*/
	public String getWxHeadPhoto(){
		return  wxHeadPhoto;
	}
	/**
	* 微信头像地址
	*@param  wxHeadPhoto
	*/
	public void setWxHeadPhoto(String wxHeadPhoto ){
		this.wxHeadPhoto = wxHeadPhoto;
	}
	
	/**
	* 认证时间
	*@return 
	*/
	public Date getAuthTime(){
		return  authTime;
	}
	/**
	* 认证时间
	*@param  authTime
	*/
	public void setAuthTime(Date authTime ){
		this.authTime = authTime;
	}
	
	/**
	* 注册时间
	*@return 
	*/
	public Date getCreateTime(){
		return  createTime;
	}
	/**
	* 注册时间
	*@param  createTime
	*/
	public void setCreateTime(Date createTime ){
		this.createTime = createTime;
	}
	

}
