package com.plat.api.entity.sys;

import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/10
 * Time: 13:28
 * Description: No Description
 */
@Data
@Table(name = "plat_param")
public class PlatParamEntity {
    /** $column.columnComment */
    private Long id;

    /** 键 */
    private String key;

    /** 值 */
    private String value;

    /** 描述 */
    private String desc;
}
