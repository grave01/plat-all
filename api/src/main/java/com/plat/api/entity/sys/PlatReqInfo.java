package com.plat.api.entity.sys;

import lombok.Data;
import org.beetl.sql.core.annotatoin.Table;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/28
 * Time: 10:05
 * Description: No Description
 */
@Data
@Table(name = "plat_req_info")
public class PlatReqInfo {
    private Integer id;
    private String req_body;//请求报文
    private String url;//请求报文
    private Date create_time;//创建时间
}
