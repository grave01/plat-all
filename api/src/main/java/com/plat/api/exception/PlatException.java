package com.plat.api.exception;

import com.plat.api.common.RespCode;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/9
 * Time: 0:15
 * Description: No Description
 */
public class PlatException extends RuntimeException {
    private String code;
    private String desc;
    private Object param;
    public PlatException(String code, String msg, String desc) {
        super(msg);
        this.code = code;
        this.desc = desc;
    }
    public PlatException(String code, String msg, String desc,Object param) {
        super(msg);
        this.code = code;
        this.desc = desc;
        this.param = param;
    }

    public PlatException(String code, String msg) {
        super(msg);
        this.code = code;
    }

    public PlatException(String msg) {
        super(msg);
        this.code = RespCode.SYS_ERROR.getCode();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Object getParam() {
        return param;
    }
}
