package com.plat.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.plat.api.entity.api.PlatAuctionOrderEntity;
import com.plat.api.entity.api.PlatAuctionOrderRecordEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/28
 * Time: 18:01
 * Description: No Description
 */
public interface PlatACRMapper extends BaseMapper<PlatAuctionOrderRecordEntity> {
    PlatAuctionOrderRecordEntity getMAXAmount(@Param("id") Integer id);

    Double getPaySumMoney(@Param("id")Integer id, @Param("userId")Integer userId);

    List<PlatAuctionOrderRecordEntity> getPayList(@Param("id")Integer id);
}
