package com.plat.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.plat.api.entity.api.PlatGoodsEntity;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/9/10
 * Time: 23:54
 * Description: No Description
 */
@Transactional
public interface PlatGoodMapper extends BaseMapper<PlatGoodsEntity> {
}
