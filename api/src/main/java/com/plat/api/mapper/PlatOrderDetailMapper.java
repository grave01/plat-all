package com.plat.api.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.plat.api.entity.api.PlatOrderDetailEntity;
import org.apache.ibatis.annotations.Param;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/28
 * Time: 17:53
 * Description: No Description
 */
public interface PlatOrderDetailMapper extends BaseMapper<PlatOrderDetailEntity> {
    Integer getAuctionNum(@Param("id") int id,@Param("type") String type);

    PlatOrderDetailEntity  selectDetail(@Param("userId") Integer userId, @Param("goodsId")  Long goodsId, @Param("videoType") String videoType);
}
