package com.plat.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.plat.api.entity.api.PlatRefundEntity;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/9/15
 * Time: 15:37
 * Description: No Description
 */
@Transactional
public interface PlatRefundMapper extends BaseMapper<PlatRefundEntity> {
}
