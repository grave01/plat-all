package com.plat.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.plat.api.entity.api.PlatLiveSubEntity;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/9/5
 * Time: 22:58
 * Description: No Description
 */
@Transactional
public interface PlatSubLiveMapper extends BaseMapper<PlatLiveSubEntity> {
}
