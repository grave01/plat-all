package com.plat.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.plat.api.entity.api.PlatVideoEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/29
 * Time: 11:58
 * Description: No Description
 */
public interface PlatVideoMapper extends BaseMapper<PlatVideoEntity> {
}
