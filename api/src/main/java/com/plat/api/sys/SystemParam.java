package com.plat.api.sys;

import com.plat.api.sys.entity.SysConfig;
import com.plat.api.sys.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/5/26
 * Time: 23:40
 * Description: No Description
 */
//@Component
//public class SystemParam {
//    @Autowired
//    private ISysConfigService iSysConfigService;
//    public static Map<String, String> sysParam = new HashMap<>();
//
//    public  void init() {
//        List<SysConfig> list = iSysConfigService.findAll();
//        for (SysConfig sysConfig : list) {
//            sysParam.put(sysConfig.getConfigKey(), sysConfig.getConfigValue());
//        }
//    }
//
//    public  String getParam(String key) {
//        return sysParam.get(key);
//    }
//}
