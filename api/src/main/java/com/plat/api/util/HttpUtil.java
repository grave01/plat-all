package com.plat.api.util;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/8/28
 * Time: 8:57
 * Description: No Description
 */
public class HttpUtil {
    public static String getRequestData(HttpServletRequest httpServletRequest){
        HttpServletRequestWrapper httpServletRequestWrapper = new HttpServletRequestWrapper(httpServletRequest);
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = null;
        InputStreamReader inputStreamReader=null;
        ServletInputStream servletInputStream =null;
        try {
            servletInputStream =  httpServletRequestWrapper.getInputStream();
            inputStreamReader=new InputStreamReader (servletInputStream, Charset.forName("UTF-8"));
            reader = new BufferedReader(inputStreamReader);
            String line = "";
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            return "";
        }finally {
            try {
                if(servletInputStream!=null){
                    servletInputStream.close();
                }
                if(inputStreamReader!=null){
                    inputStreamReader.close();
                }
                if(reader!=null){
                    reader.close();
                }
            } catch (IOException e) {

            }
        }
        return sb.toString ();
    }
}
