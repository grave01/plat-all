package com.plat.api.util;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/9/2
 * Time: 15:53
 * Description: 随机数工具
 */
public class RandomUtil {
    /**
     * 生成一个一个范围之内的随机数
     */
    public static Integer getNum(Integer min, Integer max) {
        Random random = new Random();
        int s = random.nextInt(max) % (max - min + 1) + min;
        return s;
    }

    ;
}
