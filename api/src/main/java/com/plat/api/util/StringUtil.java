package com.plat.api.util;

import org.apache.commons.lang3.StringUtils;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/6/4
 * Time: 14:13
 * Description: No Description
 */
public class StringUtil {
    public static String solveString(String str){
        String string = StringUtils.strip(str,"[]").replaceAll("\\\"", "");;
        return  string;
    }
    public static String solveString(String str,String format){
        String string = StringUtils.strip(str,format);
        return  string;
    }

    public static void main(String[] args) {
        String s = "[\"写真\",\"色彩\",\"少儿\"]";
        String s1 = StringUtil.solveString(s);
        System.out.println(s1);
    }
}
