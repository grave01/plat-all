package com.plat.api.util;


import cn.hutool.core.util.RandomUtil;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/9/23
 * Time: 22:39
 * Description: No Description
 */
public class UrlUtil {
    public static String encode(String url) throws Exception {
        String urlString;
        try {
            // 将普通字符创转换成application/x-www-from-urlencoded字符串
            urlString = URLEncoder.encode(url, "GBK");  //输出%C4%E3%BA%C3

            System.out.println(urlString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw e;
        }
        return urlString;
    }

    /**
     * html防止浏览器缓存---加入版本号
     *
     * @param url 初始化url地址
     */
    public static String addVersion(String url) throws Exception {
        String realUrl;
        if (url.contains("?")) {
            realUrl = url + "&v" + cn.hutool.core.util.RandomUtil.randomNumbers(1) + "." + cn.hutool.core.util.RandomUtil.randomNumbers(1) + "." + RandomUtil.randomNumbers(1);
        } else {
            realUrl = url + "?v" + cn.hutool.core.util.RandomUtil.randomNumbers(1) + "." + cn.hutool.core.util.RandomUtil.randomNumbers(1) + "." + RandomUtil.randomNumbers(1);
        }
        return realUrl;
    }
}
