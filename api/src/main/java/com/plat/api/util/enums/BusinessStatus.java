package com.plat.api.util.enums;

/**
 * 操作状态
 * 
 * @author plat
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
