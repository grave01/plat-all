package com.plat.api.util.file;


/**
 * 文件信息异常类
 * 
 * @author plat
 */
public class FileException extends BaseException
{
    private static final long serialVersionUID = 1L;

    public FileException(String code, Object[] args)
    {
        super("file", code, args, null);
    }

}
