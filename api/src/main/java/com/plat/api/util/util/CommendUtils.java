package com.plat.api.util.util;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/6
 * Time: 22:59
 * Description: No Description
 */
@Slf4j
public class CommendUtils {

    public static void commend(String cmd) throws IOException {
        Date startTime = new Date();
        Long st = System.currentTimeMillis();
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command(cmd);
        processBuilder.redirectErrorStream(true);
        InputStream inputStream = null;
        try {
            //启动进程
            Process start = processBuilder.start();
            //获取输入流
            inputStream = start.getInputStream();
            //转成字符输入流
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "gbk");
            int len = -1;
            char[] c = new char[1024];
            StringBuffer outputString = new StringBuffer();
            //读取进程输入流中的内容
            while ((len = inputStreamReader.read(c)) != -1) {
                String s = new String(c, 0, len);
                outputString.append(s);
                log.info("[In process of responding cmd line is {}]", s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        log.info("[Executing cmd has finished,cmd is {},Start execute time is {},Finished time is {} ,Spend time is {}ms]",
                cmd, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startTime),
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()),
                System.currentTimeMillis() - st);
    }

    public static void commend(List<String> cmd) throws IOException {
        Date startTime = new Date();
        Long st = System.currentTimeMillis();
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command(cmd);
        processBuilder.redirectErrorStream(true);
        InputStream inputStream = null;
        try {
            //启动进程
            Process start = processBuilder.start();
            //获取输入流
            inputStream = start.getInputStream();
            //转成字符输入流
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "gbk");
            int len = -1;
            char[] c = new char[1024];
            StringBuffer outputString = new StringBuffer();
            //读取进程输入流中的内容
            while ((len = inputStreamReader.read(c)) != -1) {
                String s = new String(c, 0, len);
                outputString.append(s);
                log.info("[In process of responding cmd line is {}]", s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        log.info("[Executing cmd has finished,cmd is {},Start execute time is {},Finished time is {} ,Spend time is {}ms]",
                cmd, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startTime),
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()),
                System.currentTimeMillis() - st);
    }

    public static void main(String[] args) throws IOException {
        commend("ipconfig");
    }
}
