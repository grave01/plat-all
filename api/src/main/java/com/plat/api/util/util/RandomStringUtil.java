package com.plat.api.util.util;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;

/**
 * Created with IntelliJ IDEA.
 * User: shish
 * Date: 2020/7/5
 * Time: 1:16
 * Description: No Description
 */
public class RandomStringUtil {
    public static String uniqKey(){
        return IdUtil.simpleUUID()+ RandomUtil.randomString(8);
    }
}
