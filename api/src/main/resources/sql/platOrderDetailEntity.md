getAuctionNum
===
SELECT
 count(1) as count
FROM
	plat_order_detail AS a
	LEFT JOIN plat_order AS b ON a.order_no = b.order_no
	WHERE  a.goods_id = #id#
	 AND a.goods_type='AUCTION'