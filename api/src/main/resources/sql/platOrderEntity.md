getMAXAmount
===
SELECT
	max(a.order_real_price)
FROM
	plat_order AS a
	LEFT JOIN plat_order_detail as b ON a.order_no = b.order_no
WHERE
	b.goods_type = 'AUCTION' AND b.goods_id =  #id#
	
getSumAmount
===
SELECT 
a.order_price
FROM
	plat_order AS a
	LEFT JOIN plat_order_detail AS b ON a.order_no = b.order_no
	LEFT JOIN plat_auction_record AS c ON b.goods_id = c.goods_id 
	AND b.user_id = c.user_id 
WHERE
	c.goods_id = #id# 
	AND c.user_id =  #userId# 
	AND b.goods_type = "AUCTION" 
ORDER BY
	c.id DESC 
	LIMIT 1