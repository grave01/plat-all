package com.plat;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.plat.api.entity.api.PlatVideoEntity;
import com.plat.api.mapper.PlatVideoMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class PlatApplicationTests {
    @Autowired
    private PlatVideoMapper platVideoMapper;

    @Test
    void contextLoads() {
        QueryWrapper<PlatVideoEntity> queryWrapper = new QueryWrapper<>();
//        queryWrapper.ge("age", 26);

        //总页数+总记录数
        Page<PlatVideoEntity> page = new Page<>(1, 2);

        //调用自定义sql
        IPage<PlatVideoEntity> iPage = platVideoMapper.selectPage(page, queryWrapper);

        System.out.println("总页数:" + iPage.getPages());
        System.out.println("总记录数:" + iPage.getTotal());
        List<PlatVideoEntity> userList = iPage.getRecords();
       // platVideoMapper.selectPage();
    }

    public static void main(String[] args) {
      String s =HttpUtil.get("https://apis.map.qq.com/ws/district/v1/list?key=H6VBZ-4CGW6-UFDSP-EUVUD-GNTF5-YTBOX&id=110000");
        JSONObject json = JSONObject.parseObject(s);
        System.out.println(json);
    }


}
