import request from '@/utils/request'

// 查询广告管理列表
export function listAds(query) {
  return request({
    url: '/content/ads/list',
    method: 'get',
    params: query
  })
}

// 查询广告管理详细
export function getAds(id) {
  return request({
    url: '/content/ads/' + id,
    method: 'get'
  })
}

// 新增广告管理
export function addAds(data) {
  return request({
    url: '/content/ads',
    method: 'post',
    data: data
  })
}

// 修改广告管理
export function updateAds(data) {
  return request({
    url: '/content/ads',
    method: 'put',
    data: data
  })
}

// 删除广告管理
export function delAds(id) {
  return request({
    url: '/content/ads/' + id,
    method: 'delete'
  })
}

// 导出广告管理
export function exportAds(query) {
  return request({
    url: '/content/ads/export',
    method: 'get',
    params: query
  })
}