import request from '@/utils/request'

// 查询拍卖管理列表
export function listAuction(query) {
  return request({
    url: '/content/auction/list',
    method: 'get',
    params: query
  })
}

// 查询拍卖管理详细
export function getAuction(id) {
  return request({
    url: '/content/auction/' + id,
    method: 'get'
  })
}

// 新增拍卖管理
export function addAuction(data) {
  return request({
    url: '/content/auction',
    method: 'post',
    data: data
  })
}

// 修改拍卖管理
export function updateAuction(data) {
  return request({
    url: '/content/auction',
    method: 'put',
    data: data
  })
}

// 删除拍卖管理
export function delAuction(id) {
  return request({
    url: '/content/auction/' + id,
    method: 'delete'
  })
}

// 导出拍卖管理
export function exportAuction(query) {
  return request({
    url: '/content/auction/export',
    method: 'get',
    params: query
  })
}
