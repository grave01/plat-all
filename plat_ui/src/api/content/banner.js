import request from '@/utils/request'

// 查询前台banner列表
export function listBanner(query) {
  return request({
    url: '/content/banner/list',
    method: 'get',
    params: query
  })
}

// 查询前台banner详细
export function getBanner(id) {
  return request({
    url: '/content/banner/' + id,
    method: 'get'
  })
}

// 新增前台banner
export function addBanner(data) {
  return request({
    url: '/content/banner',
    method: 'post',
    data: data
  })
}

// 修改前台banner
export function updateBanner(data) {
  return request({
    url: '/content/banner',
    method: 'put',
    data: data
  })
}

// 删除前台banner
export function delBanner(id) {
  return request({
    url: '/content/banner/' + id,
    method: 'delete'
  })
}

// 导出前台banner
export function exportBanner(query) {
  return request({
    url: '/content/banner/export',
    method: 'get',
    params: query
  })
}