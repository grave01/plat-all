import request from '@/utils/request'

// 查询视频管理列表
export function listContent(query) {
  return request({
    url: '/content/content/list',
    method: 'get',
    params: query
  })
}

// 查询视频管理详细
export async function getContent(id) {
  return await request({
    url: '/content/content/' + id,
    method: 'get'
  })
}

// 新增视频管理
export function addContent(data) {
  return request({
    url: '/content/content',
    method: 'post',
    data: data
  })
}

// 修改视频管理
export function updateContent(data) {
  return request({
    url: '/content/content',
    method: 'put',
    data: data
  })
}

// 删除视频管理
export function delContent(id) {
  return request({
    url: '/content/content/' + id,
    method: 'delete'
  })
}

// 导出视频管理
export function exportContent(query) {
  return request({
    url: '/content/content/export',
    method: 'get',
    params: query
  })
}
export function uploadFile(file) {
  console.log(file);
  const data = new FormData();
  data.append('file', file);
  return request({
    url: '/sys/file/upload',
    method: "post",
    headers: {'Content-Type': 'multipart/form-data'},
    params: data
  })
}
