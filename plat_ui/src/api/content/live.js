import request from '@/utils/request'

// 查询直播间管理列表
export function listLive(query) {
  return request({
    url: '/wechat/live/list',
    method: 'get',
    params: query
  })
}

// 查询直播间管理详细
export function getLive(id) {
  return request({
    url: '/wechat/live/' + id,
    method: 'get'
  })
}

// 新增直播间管理
export function addLive(data) {
  return request({
    url: '/wechat/live',
    method: 'post',
    data: data
  })
}

// 修改直播间管理
export function updateLive(data) {
  return request({
    url: '/wechat/live',
    method: 'put',
    data: data
  })
}

// 删除直播间管理
export function delLive(id) {
  return request({
    url: '/wechat/live/' + id,
    method: 'delete'
  })
}

// 导出直播间管理
export function exportLive(query) {
  return request({
    url: '/wechat/live/export',
    method: 'get',
    params: query
  })
}