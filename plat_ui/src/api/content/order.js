import request from '@/utils/request'

// 查询顶大列表
export function listOrder(query) {
  return request({
    url: '/content/order/list',
    method: 'get',
    params: query
  })
}

// 查询顶大详细
export function getOrder(id) {
  return request({
    url: '/content/order/' + id,
    method: 'get'
  })
}

// 新增顶大
export function addOrder(data) {
  return request({
    url: '/content/order',
    method: 'post',
    data: data
  })
}

// 修改顶大
export function updateOrder(data) {
  return request({
    url: '/content/order',
    method: 'put',
    data: data
  })
}

// 删除顶大
export function delOrder(id) {
  return request({
    url: '/content/order/' + id,
    method: 'delete'
  })
}

// 导出顶大
export function exportOrder(query) {
  return request({
    url: '/content/order/export',
    method: 'get',
    params: query
  })
}

// 查看订单详情
export function orderDetailList(data) {
  return request({
    url: '/content/order/getOrderDetailList',
    method: 'post',
    data: data
  })
}

// 查看订单发货详情
export function getOrderExpress(data) {
  return request({
    url: '/content/express/getOrderExpress/'+data.orderNo,
    method: 'get'
  })
}

// 发货
export function insertOrderExpress(data) {
  return request({
    url: '/content/express/insertOrderExpress',
    method: 'post',
    data: data
  })
}

export function refund(data) {
  return request({
    url: '/content/order/refund',
    method: 'post',
    data: data
  })
}