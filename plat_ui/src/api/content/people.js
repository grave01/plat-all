import request from '@/utils/request'

// 查询平台相关的人物列表
export async function listPeople(query) {
  return await request({
    url: '/content/people/list',
    method: 'get',
    params: query
  })
}

// 查询平台相关的人物详细
export async function getPeople(id) {
  return await request({
    url: '/content/people/' + id,
    method: 'get'
  })
}

// 新增平台相关的人物
export function addPeople(data) {
  return request({
    url: '/content/people',
    method: 'post',
    data: data
  })
}

// 修改平台相关的人物
export function updatePeople(data) {
  return request({
    url: '/content/people',
    method: 'put',
    data: data
  })
}

// 删除平台相关的人物
export function delPeople(id) {
  return request({
    url: '/content/people/' + id,
    method: 'delete'
  })
}

// 导出平台相关的人物
export function exportPeople(query) {
  return request({
    url: '/content/people/export',
    method: 'get',
    params: query
  })
}
