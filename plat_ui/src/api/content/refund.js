import request from '@/utils/request'

// 查询订单申请列表
export function listRefund(query) {
  return request({
    url: '/content/refund/list',
    method: 'get',
    params: query
  })
}

// 查询订单申请详细
export function getRefund(id) {
  return request({
    url: '/content/refund/' + id,
    method: 'get'
  })
}

// 新增订单申请
export function addRefund(data) {
  return request({
    url: '/content/refund',
    method: 'post',
    data: data
  })
}


// 修改订单申请
export function updateRefund(data) {
  return request({
    url: '/content/refund',
    method: 'put',
    data: data
  })
}

// 删除订单申请
export function delRefund(id) {
  return request({
    url: '/content/refund/' + id,
    method: 'delete'
  })
}

// 导出订单申请
export function exportRefund(query) {
  return request({
    url: '/content/refund/export',
    method: 'get',
    params: query
  })
}