import request from '@/utils/request'

// 查询活动样式模板列表
export function listTemplate(query) {
  return request({
    url: '/content/template/list',
    method: 'get',
    params: query
  })
}

// 查询活动样式模板详细
export function getTemplate(activityId) {
  return request({
    url: '/content/template/' + activityId,
    method: 'get'
  })
}

// 新增活动样式模板
export function addTemplate(data) {
  return request({
    url: '/content/template',
    method: 'post',
    data: data
  })
}

// 修改活动样式模板
export function updateTemplate(data) {
  return request({
    url: '/content/template',
    method: 'put',
    data: data
  })
}

// 删除活动样式模板
export function delTemplate(activityId) {
  return request({
    url: '/content/template/' + activityId,
    method: 'delete'
  })
}

// 导出活动样式模板
export function exportTemplate(query) {
  return request({
    url: '/content/template/export',
    method: 'get',
    params: query
  })
}