import request from '@/utils/request'

// 获取上传token
export function getUploadToken(data) {
    return request({
      url: '/sys/file/upload/getToken',
      method: 'post',
      data: data
    })
  }