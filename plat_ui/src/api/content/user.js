import request from '@/utils/request'

// 查询用户认证列表
export function listUser(query) {
  return request({
    url: '/content/user/list',
    method: 'get',
    params: query
  })
}

// 查询用户认证详细
export function getUser(id) {
  return request({
    url: '/content/user/' + id,
    method: 'get'
  })
}

// 新增用户认证
export function addUser(data) {
  return request({
    url: '/content/user',
    method: 'post',
    data: data
  })
}

// 修改用户认证
export function updateUser(data) {
  return request({
    url: '/content/user',
    method: 'put',
    data: data
  })
}

// 删除用户认证
export function delUser(id) {
  return request({
    url: '/content/user/' + id,
    method: 'delete'
  })
}


// 导出用户认证
export function exportUser(query) {
  return request({
    url: '/content/user/export',
    method: 'get',
    params: query
  })
}