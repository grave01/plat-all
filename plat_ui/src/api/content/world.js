import request from '@/utils/request'

// 查询圈子管理列表
export function listWorld(query) {
  return request({
    url: '/content/world/list',
    method: 'get',
    params: query
  })
}

// 查询圈子管理详细
export function getWorld(id) {
  return request({
    url: '/content/world/' + id,
    method: 'get'
  })
}

// 新增圈子管理
export function addWorld(data) {
  return request({
    url: '/content/world',
    method: 'post',
    data: data
  })
}

// 修改圈子管理
export function updateWorld(data) {
  return request({
    url: '/content/world',
    method: 'put',
    data: data
  })
}

// 删除圈子管理
export function delWorld(id) {
  return request({
    url: '/content/world/' + id,
    method: 'delete'
  })
}

// 导出圈子管理
export function exportWorld(query) {
  return request({
    url: '/content/world/export',
    method: 'get',
    params: query
  })
}


// 处理违规
export  function solve(url,d) {
  var data ={
    url:url,
    id:d.id,
    platMyWorld:d
    }
  return request({
    url: '/content/world/solve',
    method: 'post',
    data: data
  })
}

// 处理违规发圈
export  function solveTxt(d) {

  return request({
    url: '/content/world/solveTxt',
    method: 'post',
    data: d
  })
}


