//app.js
import page from './utils/page';
var util = require('./utils/util.js');
//app.js
App({
    onLaunch: function () {
      // 展示本地存储能力
      var logs = wx.getStorageSync('logs') || []
      logs.unshift(Date.now())
      wx.setStorageSync('logs', logs)
      util.request("userAction.getUserInfo").then(function(res){
        console.log(res)
        wx.setStorageSync("has_edit",res.body.userInfo.hasEdit)
        wx.setStorageSync("has_live",res.body.userInfo.hasLive)
        wx.setStorageSync("has_ysp",res.body.userInfo.hasYsp)
        wx.setStorageSync("has_my_world",res.body.userInfo.hasMyWorld)
        wx.setStorageSync("about",res.body.xcx.about)
        wx.setStorageSync("license",res.body.xcx.license)
    })
      // util.request("addressAction.area",{}).then(function(res){
      //   console.log(res)
      //    wx.setStorageSync('area', JSON.parse(res.body))
      // })
      // 登录
      wx.login({
        success: res => {
          // 发送 res.code 到后台换取 openId, sessionKey, unionId
        }
      })
      // 获取用户信息
      wx.getSetting({
        success: res => {
          if (res.authSetting['scope.userInfo']) {
            // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
            wx.getUserInfo({
              success: res => {
                // 可以将 res 发送给后台解码出 unionId
                this.globalData.userInfo = res.userInfo
  
                // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
                // 所以此处加入 callback 以防止这种情况
                if (this.userInfoReadyCallback) {
                  this.userInfoReadyCallback(res)
                }
              }
            })
          }
        }
      })

       const { statusBarHeight, platform } = wx.getSystemInfoSync()
    const { top, height } = wx.getMenuButtonBoundingClientRect()

    // 状态栏高度
    wx.setStorageSync('statusBarHeight', statusBarHeight)
    // 胶囊按钮高度 一般是32 如果获取不到就使用32
    wx.setStorageSync('menuButtonHeight', height ? height : 32)
    
    // 判断胶囊按钮信息是否成功获取
    if (top && top !== 0 && height && height !== 0) {
        const navigationBarHeight = (top - statusBarHeight) * 2 + height
        // 导航栏高度
        wx.setStorageSync('navigationBarHeight', navigationBarHeight)
    } else {
        wx.setStorageSync(
          'navigationBarHeight',
          platform === 'android' ? 48 : 40
        )
    }
    },
    globalData: {
      userInfo: null
    }
  })