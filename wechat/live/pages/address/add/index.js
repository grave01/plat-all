var util = require('../../../utils/util.js')
var areaInfo = []; //所有省市区县数据
var provinces = []; //省
var provinceNames = []; //省名称
var citys = []; //城市
var cityNames = []; //城市名称
var countys = []; //区县
var countyNames = []; //区县名称
var value = [0, 0, 0]; //数据位置下标
var addressList = null;
import {$wuxDialog} from '../../../dist/index'
const alert = (content) => {
  $wuxDialog('#wux-dialog--alert').alert({
    resetOnClose: true,
    title: '提示',
    content: content,
  })
}
import { $wuxLoading } from '../../../dist/index'
const loaing = (content) => {
  $wuxLoading().show({
    text: content,
})
}
const hide = () => {
  $wuxLoading().hide();
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    transportValues: ["收货时间不限", "周六日/节假日收货", "周一至周五收货"],
    transportIndex: 0,
    provinceIndex: 0, //省份
    cityIndex: 0, //城市
    countyIndex: 0, //区县,
    phone:"",
    region:[]
  },
  bindRegionChange: function (e) {
    console.log(e)
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      region: e.detail.value
    })
    // this.getDetailPlace(e.detail.code)
  },
  getPhoneNumber:function(e){
    var that = this
    wx.login({
         success:function(res){
           var param ={
             code:res.code,
             iv :e.detail.iv,
              encryptedData:e.detail.encryptedData
            }
            util.request("loginAction.getPhoneNumber",param).then(function(res){
              console.log(res)
              that.setData({
               phone:res.body.object.phoneNumber
              })
            })
         }
    })
  
   },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // loaing("加载地址中")
    // util.request("addressAction.area",{}).then(function(res){
    //     console.log(res)
    //     hide()
    //      wx.setStorageSync('area', JSON.parse(res.body))
    //   })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    areaInfo = wx.getStorageSync('area');
    // that.getProvinceData();

  },
  // 获取省份数据
  getProvinceData: function() {
    var that = this;
    var s;
    provinces = [];
    provinceNames = [];
    var num = 0;
    for (var i = 0; i < areaInfo.length; i++) {
      s = areaInfo[i];
      if (s.di == "00" && s.xian == "00") {
        provinces[num] = s;
        provinceNames[num] = s.name;
        num++;
      }
    }
    that.setData({
      provinceNames: provinceNames
    })

    that.getCityArr();
    that.getCountyInfo();
  },

  // 获取城市数据
  getCityArr: function(count = 0) {
    var c;
    citys = [];
    cityNames = [];
    var num = 0;
    for (var i = 0; i < areaInfo.length; i++) {
      c = areaInfo[i];
      if (c.xian == "00" && c.sheng == provinces[count].sheng && c.di != "00") {
        citys[num] = c;
        cityNames[num] = c.name;
        num++;
      }
    }
    if (citys.length == 0) {
      citys[0] = {
        name: ''
      };
      cityNames[0] = {
        name: ''
      };
    }
    var that = this;
    that.setData({
      citys: citys,
      cityNames: cityNames
    })
    console.log('cityNames:' + cityNames);
    that.getCountyInfo(count, 0);
  },

  // 获取区县数据
  getCountyInfo: function(column0 = 0, column1 = 0) {
    var c;
    countys = [];
    countyNames = [];
    var num = 0;
    for (var i = 0; i < areaInfo.length; i++) {
      c = areaInfo[i];
      if (c.xian != "00" && c.sheng == provinces[column0].sheng && c.di == citys[column1].di) {
        countys[num] = c;
        countyNames[num] = c.name;
        num++;
      }
    }
    if (countys.length == 0) {
      countys[0] = {
        name: ''
      };
      countyNames[0] = {
        name: ''
      };
    }
    console.log('countyNames:' + countyNames);
    var that = this;
    // value = [column0, column1, 0];

    that.setData({
      countys: countys,
      countyNames: countyNames,
      // value: value,
    })
  },

  bindTransportDayChange: function(e) {
    console.log('picker country 发生选择改变，携带值为', e.detail.value);
    this.setData({
      transportIndex: e.detail.value
    })
  },

  bindProvinceNameChange: function(e) {
    var that = this;
    console.log('picker province 发生选择改变，携带值为', e.detail.value);
    var val = e.detail.value
    that.getCityArr(val); //获取地级市数据
    that.getCountyInfo(val, 0); //获取区县数据

    value = [val, 0, 0];
    this.setData({
      provinceIndex: e.detail.value,
      cityIndex: 0,
      countyIndex: 0,
      value: value
    })

  },

  bindCityNameChange: function(e) {
    var that = this;
    console.log('picker city 发生选择改变，携带值为', e.detail.value);

    var val = e.detail.value
    that.getCountyInfo(value[0], val); //获取区县数据
    value = [value[0], val, 0];
    this.setData({
      cityIndex: e.detail.value,
      countyIndex: 0,
      value: value
    })
  },

  bindCountyNameChange: function(e) {
    var that = this;
    console.log('picker county 发生选择改变，携带值为', e.detail.value);
    this.setData({
      countyIndex: e.detail.value
    })
  },

  saveAddress: function(e) {
    var that =this;
    var consignee = e.detail.value.consignee;
    var mobile = that.data.phone;
    var transportDay = e.detail.value.transportDay;
    var provinceName = that.data.region[0];
    var cityName = that.data.region[1];
    var countyName = that.data.region[2];
    var address = e.detail.value.address;

    console.log(transportDay + "," + provinceName + "," + cityName + "," + countyName + "," + address); //输出该文本 
    
    var arr = wx.getStorageSync('addressList') || [];
    console.log("arr,{}", arr);
    addressList = {
      consignee: consignee,
      mobile: mobile,
      address: provinceName + cityName + countyName+address,
      transportDay: transportDay
    }
      arr.push(addressList);
      if(util.isNull(consignee)){
        alert("收件人不能为空")
        return;
      }
      if(util.isNull(mobile)){
        alert("手机号不能为空")
        return;
      }
      if(that.data.region.length==0){
          alert("请选择地址")
        return;
      }
      if(util.isNull(address)){
        alert("详细不能为空")
        return;
      }
    wx.setStorageSync('addressList', arr);
    var ads={
      consignee:consignee,
      mobile:mobile,
      transportDay:transportDay,
      provinceName:provinceName,
      cityName:cityName,
      countyName:countyName,
      address:address
    }
    util.request("addressAction.addOrUpdate",ads).then(function(res){
       if(res.code=="00000"){
        wx.navigateBack({
      
        })
       }else{
         wx.showToast({
           title: '新增失败',
         })
       }
    })
  },
  handlerGobackClick:function(){
    wx.navigateBack({
      complete: (res) => {},
    })
  }
})