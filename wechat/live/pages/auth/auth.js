import {
  $wuxCountDown
} from '../../dist/index'
var util = require('../../utils/util.js');
Page({
  data: {
    height: '',
    hasEdit:false,
    authBg:""
  },
  onLoad() {
    var that = this;
    var param = {}
    util.request("userAction.getPlatInfo",param).then(function(res){
      console.log(res)
      that.setData({
        authBg:res.body.authBg
      })
  })
  },
  bindGetUserInfo(e) {
    var that =this
    console.log(e.detail.userInfo);
    wx.login({
      complete: (res) => {
        console.log(res)
        var param = {
          "uersInfo": e.detail,
          "code": res.code
        }
        util.request("loginAction.login", param).then(function (res) {
          console.log(res)
          var token = res.body.token;
          var user = res.body.user
          if (res.code == "00000") {
            wx.setStorageSync('plat_token', token);
            wx.setStorageSync('has_edit', res.body.user.hasEdit);
            wx.setStorageSync('openId', res.body.user.openId);
            console.log(user)
            if(res.body.user.isPauseUse==1){
              wx.reLaunch({
                url: '/pages/close/index?msg='+res.body.user.pauseMsg,
              })
            }else{
              wx.reLaunch({
                url: '/pages/index/index',
              })
            }
          }
        })
      },
    })
  },
  handlerGobackClick:function(){
    wx.navigateBack()
  }
})