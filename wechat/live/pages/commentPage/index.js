//index.js
//获取应用实例
//const app = getApp();
var util = require('../../utils/util.js');
import tool from "../../utils/tool.js";
import {
  $startWuxRefresher,
  $stopWuxRefresher,
  $stopWuxLoader
} from '../../dist/index'
import {
  $wuxDialog
} from '../../dist/index'
var page = 1;
var pageSize = 10;
var onshow = false;
const alert = (content) => {
  $wuxDialog('#wux-dialog--alert').alert({
    resetOnClose: true,
    title: '提示',
    content: content,
  })
}
Page({
  data: {
    title: "评论圈子",
    dianzanColor: 'green',
    giveLoveIconColor: "red",
    comment: "red",
    isGetLove: false,
    hidden: true,
    hiddenComment: true,
    hiddenLoadMore: false,
    replyId: "", //回复id
    replyName: "", //回复id
    commentId: "", //评论id,
    content: "", //评论内容
    pics: [],
    item: "",
    x: "",
    y: "",
    screenHeight: "",
    worldList: [],
    isPreview: false,
    targetId: "",
    commentList: [],
    isTapNum: false,
    visable:false,
    inputValue: null,
    dialogShow:false,
    buttons: [{text: '取消'},{text: '确定删除'}],
    error:"",
    isVideo:false,
    isCanDelete:false,
    home:true
  },
  onHome:function(){
    wx.reLaunch({
      url: '/pages/index/index',
    })
  },
  tapDialogButton:function(e){
    console.log(e)
    if(e.detail.index==1){
      var param = {
        targetId:this.data.targetId
      }
      util.request("myGloableAction.delMyWorld",param).then(function(res){
        console.log(res)
        wx.navigateBack()
      })
    }else{
      this.setData({
        dialogShow:false
      })
    }
  },
  del:function(e){
    console.log(e)
    var item  = e.currentTarget.dataset.item
      if(item.isCanDelete==1){
        this.setData({
          dialogShow:true,
          error:"你要确定删除该圈子信息吗？"
        })
      }
  },
  onShow: function (e) {

  },
  onLoad: function (optins) {
    var that = this;
    var item = JSON.parse(decodeURI(optins.item));
    console.log(item)
    console.log()
    that.setData({
      item: item,
      targetId:item.id
    });
    that.getMyWorld(this.data.targetId);
    page = 1;
    that.init();
  },
  dianzan: function () {
    var that = this;
    var color;
    var it = that.data.item
    if (that.data.dianzanColor == "red") {
      //alert("取消点赞")
      it.tapNum = it.tapNum - 1
      color = "green"
    } else {
      //alert("谢谢点赞")
      color = "red",
        it.tapNum = it.tapNum + 1
    }
    that.setData({
      dianzanColor: color,
      item: it
    })
    var param = {
      targetId: that.data.targetId
    }
    util.request("myGloableAction.dianZan", param).then(function (res) {

    })
  },
  getMyWorld: function (v) {
    var that = this;
    var param = {
      id: v
    };
    util.request("myGloableAction.getMyWorld", param).then(function (res) {
      var obj = res.body.object
      obj.url = JSON.parse(obj.url)
      console.log(obj)
      if(obj.isVideo){
        that.setData({
          isVideo:true
        })
      }
      if(obj.isCanDelete==1){
        that.setData({
          isCanDelete:true
        })
      }
      var dianzanColor;
      if (obj.isTap == 1) {
        that.setData({
          item: obj,
          dianzanColor: "red",
          isTapNum: true
        })
      } else {
        that.setData({
          item: obj,
          dianzanColor: "green",
          isTapNum: false
        })
      }
      console.log("asaassa"+obj.publishAddress)
     if(util.isNull(obj.publishAddress)){
      obj.publishAddress = '中国';
      that.setData({
        item:obj
      })
     }
    })
  },
  init: function () {
    var that = this;
    that.getList();
  },
  getList: function () {
    var that = this;
    console.log("加载")
    var param = {
      page: page,
      pageSize: pageSize,
      targetId: that.data.targetId
    }
    util.request("platCommentAction.getCommentList", param).then(function (res) {
      console.log(res)
      var list = res.body.list;
      that.setData({
        commentList: list,
        hiddenLoadMore: true
      })
    })
  },
  handlerGobackClick() {
    wx.navigateBack({
      complete: (res) => {},
    })
  },

  giveLove: function (e) {
    console.log(e.currentTarget.dataset.id);
    var that = this;
    console.log(e.currentTarget.dataset.id);
    that.setData({
      isGetLove: true
    })
    console.log("点赞");
  },
  publish: function () {
    wx.navigateTo({
      url: '/pages/pubMyWorld/publish',
    })
  },
  viewComment: function (e) {
    var that = this;
    console.log(e);
    console.log("查看评论")
    that.setData({
      hidden: false,
      x: e.changedTouches[0].clientX,
      y: e.changedTouches[0].clientY
    })
  },
  closeMask: function () {
    var that = this;
    console.log("关闭评论列表")
    that.setData({
      hidden: true
    })
  },
  getContent: function (e) {
    var that = this;
    that.setData({
      content: e.detail.value
    })
  },
  start: function (e) {
    var that = this
    var param = {
      content: that.data.content,
      targetId: that.data.targetId
    };
    that.setData({
      visable:true
    })
    if (util.isNull(that.data.content)) {
      that.setData({
        visable:false
      })
      alert("评论内容不能为空")
      return;
    }
    util.request("platCommentAction.comment", param).then(function (res) {
      console.log(res)
      if(res.code=="500"){
        that.setData({
          visable:false
        })
        alert("请输入信息")
      }else{
          var list = that.data.commentList;
          var arr = new Array()
          arr.push(res.body.object)
          for (var i = 0; i < list.length; i++) {
            arr.push(list[i])
          }
          list = arr
          that.setData({
            commentList: list
          })
          that.setData({
            visable:false,
            'inputValue': '',
            content:null
          })
        }
    })
  },
  userCommentInput: function (e) {
    var that = this;
    that.setData({
      content: e.detail.value
    })
  },
  // init: function () {
  //   var that = this;
  //   that.setData({
  //     hiddenComment: true,
  //     replyId: "", //回复id
  //     replyName: "", //回复id
  //     commentId: "", //评论id
  //     content: "", //评论内容
  //     pics: [] //评论图片
  //   })
  // },
  closeCommentMask: function () {
    var that = this;
    console.log("关闭评论")
    that.init();
  },
  startComment: function () {
    var that = this;
    console.log("开始评论");
    console.log("内容的id" + that.data.commentId)
    console.log("回复的id" + that.data.replyId)
    console.log("图片urls" + that.data.pics) //以逗号隔开的url地址
    console.log("评论内容" + that.data.content)

    //拿到评论内容的id(必填)
    //拿到回复的id(有的空)
    //拿到回复的图片(有的空)
    //拿到评论内容(必填想)
    //以上交给后台处理完毕--更新对应commentid下面的list
  },
  //预览图片，放大预览
  preview: function (event) {
    var that = this;
    console.log(event.currentTarget.dataset)
    var list = event.currentTarget.dataset.list
    let currentUrl = event.currentTarget.dataset.image;
    onshow = true;
    wx.previewImage({
      current: currentUrl, // 当前显示图片的http链接
      urls: list // 需要预览的图片http链接列表
    })
  },
  //上传图片开始
  chooseImg: function (e) {
    var that = this,
      pics = this.data.pics;
    console.log(pics);
    if (pics.length < 3) {
      wx.chooseImage({
        count: 3, // 最多可以选择的图片张数，默认9
        sizeType: ['original', 'compressed'], // original 原图，compressed 压缩图，默认二者都有
        sourceType: ['album', 'camera'], // album 从相册选图，camera 使用相机，默认二者都有
        success: function (res) {
          // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
          var tempFilePaths = res.tempFilePaths;
          // wx.showToast({
          //   title: '正在上传...',
          //   icon: 'loading',
          //   mask: true,
          //   duration: 10000
          // });
          for (var i = 0; i < tempFilePaths.length; i++) {
            pics.push(tempFilePaths[i]);
          }
          console.log(pics);
          that.setData({
            pics: pics
          })
        },
      });
    } else {
      wx.showToast({
        title: '最多上传3张图片',
        icon: 'none',
        duration: 3000
      });

    }
  },
  // 删除图片
  deleteImg: function (e) {
    var that = this;
    var pics = this.data.pics;
    var index = e.currentTarget.dataset.index;
    pics.splice(index, 1);
    console.log(pics)
    this.setData({
      pics: pics,
    })
  },
  // 刷新相关
  onPulling: function () {
    console.log('onPulling')
  },
  onRefresh() {
    var that = this
    //   setTimeout(() => {
    //     //加载数据
    //     console.log("刷新数据")

    // }, 3000)
    console.log("下拉刷新1")
    page = 1;
    that.init()
    console.log("下拉刷新2")
    $stopWuxRefresher()
  },
  onLoadmore() {
    console.log('onLoadmore')
    setTimeout(() => {}, 3000)
  },
  onReachBottom: function () {
    var that = this;
    console.log("底部刷新")
    page = page + 1;
    var param = {
      "page": page,
      "pageSize": pageSize,
      targetId: that.data.targetId
    }
    that.setData({
      hiddenLoadMore: false
    })
    util.request("platCommentAction.getCommentList", param).then(function (res) {
      console.log(res)
      var list = res.body.list;
      var li = that.data.commentList
      for (var i = 0; i < list.length; i++) {
        li.push(list[i])
      }
      console.log(list)
      if (list.length > 0) {
        that.setData({
          commentList: li,
          hiddenLoadMore: true
        })
      } else {
        page = page - 1;
        setTimeout(() => {
          that.setData({
            hiddenLoadMore: true
          })
        }, 1000)
      }

    })
  },
  // 刷新相关
  handlerGohomeClick() {
    wx.navigateTo({
      url: '/pages/index/index'
    });
  },

});