//index.js
//获取应用实例
//const app = getApp();
var util = require('../../../utils/util.js');
import {
  $startWuxRefresher,
  $stopWuxRefresher,
  $stopWuxLoader
} from '../../../dist/index'
var  page=1;
import { $wuxLoading } from '../../../dist/index'
const loading = (content) => {
  $wuxLoading().show({
    text: content,
})
}
const hide = () => {
  $wuxLoading().hide();
}
import {$wuxDialog} from '../../../dist/index'
const alert = (content) => {
  $wuxDialog('#wux-dialog--alert').alert({
    resetOnClose: true,
    title: '提示',
    content: content,
  })
}
Page({
  data: {
    title: "艺购",
    loading: false,
    color: '#000',
    background: '#fff',
    show: true,
    animated: false,
    hiddenLoadMore: false,
    pageSize: 10,
    list: [],
    canPublish:false,
    height:0,
    width:0,
    isLoad:true
  },
  preview:function(e){
    var url = e.currentTarget.dataset.item;
    var urls = new Array();
    urls.push(url)
    wx.previewImage({
      current:url,
      urls: urls
    })
    this.setData({
      isLoad:false
    })
  },
  bindload:function(e){
    var image = util.imageUtil(e)
    console.log("图片")
    console.log(image)
    if(image.imageHeight<200){
      image.imageHeight=image.imageHeight
    }
    if((image.imageHeight)>300&&(image.imageHeight)<400){
      image.imageHeight =  image.imageHeight-50
    }
    if((image.imageHeight)>400&&(image.imageHeight)<500){
      image.imageHeight =  image.imageHeight-100
    }
    if((image.imageHeight)>500&&(image.imageHeight)<600){
      image.imageHeight =   image.imageHeight/2
    }
    if((image.imageHeight)>600&&(image.imageHeight)<800){
      image.imageHeight =   image.imageHeight/2
    }
    if((image.imageHeight)>800&&(image.imageHeight)<1000){
      image.imageHeight =   image.imageHeight/2-100
    }
    this.setData({
      height:image.imageHeight,
      width:image.imageWidth
    })
  },
  onShow: function () {
    if(this.data.isLoad){
      var that = this
      page = 1
      that.setData({
        hasYsp:wx.getStorageSync('has_ysp')
      })
      that.init()
      that.getUser();
    }
  },
  getUser:function(){
    var that = this
    util.request("userAction.getUserInfo").then(function(res){
      var canPublish
      if(res.body.userInfo.hasYsp==1){
        canPublish = true
      }else{
        canPublish = false
      }
      that.setData({
        canPublish:canPublish,
        isLoad:true
      })
  })
  },
  onLoad: function () {
    var that = this
    that.init()
  
  },
  init: function () {
    var that = this
    var param = {
      "page":page,
      "pageSize": that.data.pageSize
    }
    util.request("platGoodsAction.getGoodsList", param).then(function (res) {
      var li = res.body.list
      console.log(res)
      for (var i = 0; i < li.length; i++) {
        // li.url = JSON.parse( li[i].url);
        console.log(JSON.parse(li[i].goodsImage))
        li[i].image = JSON.parse(li[i].goodsImage);
      }
     console.log(li)
      that.setData({
        list: li,
        hiddenLoadMore:true
      })
    })
  },
  search: function () {
    wx.switchTab({
      url: '/pages/demo1/index',
    })
  },

createPay:function(e){
  var that = this;
  var param= {
    goodsSn:e.currentTarget.dataset.item.goodsSn
  };
  loading("客官，正在帮你抢呢！！！")
  util.request("platGoodsAction.createPay",param).then(function(res){
    hide();
    console.log(res)
    if(res.code=="500"){
      alert(res.body.msg)
      return
    }
    if(res.code=="70001"){
      alert(res.body.msg)
      wx.navigateTo({
        url: '/pages/address/adress?dataObj=1',
      })
      return
    }
    var pay = res.body.payInfo;
    var li = that.data.list
    var object = res.body.object
    for(var i=0;i<li.length;i++){
        if(li[i].goodsSn==object.goodsSn){
          li[i].num= object.num;
          li[i].percent= object.per;
        }
    }
    wx.requestPayment({
      'timeStamp': pay.timeStamp,
      'nonceStr': pay.nonceStr,
      'package': pay.packageValue,
      'signType': 'MD5',
      'appId': pay.appId,
      'paySign': pay.paySign,
      'success': function (res) {
        console.log('success')
        var v = that.data.video
        v.isFree = 1
        v.showFreeItem = 2
        console.log(v)
        that.setData({
          video:v,
          btnVisable:false
        })
      },
      'fail': function (res) {
        console.log('fail')
      },
      'complete': function (res) {
        console.log('complete');
        that.setData({
          list:li
        })
      }
    })
  })
},

  // 发布拍买的东西
  publish: function () {
    var isEditUderInfo = wx.getStorageSync('has_edit');
   if(isEditUderInfo==1){
    wx.navigateTo({
      url: "/pages/goodsList/publish/index",
    })
   }else{
    wx.navigateTo({
      url: '/pages/userInfo/index',
    })
   }
  },
  /**
   * 触底上拉加载
   */
  onReachBottom: function () {
    console.log("assas")
  },

  /**
   * 下拉加载
   */
  onPullDownRefresh: function () {
    console.log("assas")
  },
  onPulling: function () {
    console.log('onPulling')
  },
  onRefresh() {
    var that = this
    //   setTimeout(() => {
    //     //加载数据
    //     console.log("刷新数据")

    // }, 3000)
    console.log("下拉刷新1")
    page = 1
    that.init()
    console.log("下拉刷新2")
    $stopWuxRefresher()
  },
  onLoadmore() {
    console.log('onLoadmore')
    setTimeout(() => {}, 3000)
  },
  onReachBottom: function () {
    var that = this;
    console.log("底部刷新")
   page = page+1;
    var param = {
      "page": page,
      "pageSize": that.data.pageSize
    }
    that.setData({
      hiddenLoadMore: false
    })
    util.request("platGoodsAction.getGoodsList", param).then(function (res) {
      console.log(res)
      var list = res.body.list
      var li = that.data.list
      for (var i = 0; i < list.length; i++) {
        li.push(list[i])
      }
      if (list.length > 0) {
        that.setData({
          list: li,
          hiddenLoadMore: true
        })
      } else {
        // page = page - 1;
        setTimeout(() => {
          that.setData({
            hiddenLoadMore: true
          })
        }, 1000)
      }
    })
  },
});