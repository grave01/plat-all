//index.js
//获取应用实例
//const app = getApp();
var upload = require('../../../pages/publishSale/upload.js');
var util = require('../../../utils/util.js');
import tool from "../../../utils/tool.js";
import {
  $startWuxRefresher,
  $stopWuxRefresher,
  $stopWuxLoader
} from '../../../dist/index'
import {
  $wuxDialog
} from '../../../dist/index'
var page = 1;
var pageSize = 10;
var onshow = false;
const alert = (content) => {
  $wuxDialog('#wux-dialog--alert').alert({
    resetOnClose: true,
    title: '提示',
    content: content,
  })
}
import { $wuxLoading } from '../../../dist/index'
const loading = (content) => {
  $wuxLoading().show({
    text: content,
})
}
const hide = () => {
  $wuxLoading().hide();
}
Page({
    data: {
        minDate:util.formatTime(new Date()),
        btnVisable:false,
        pics: [],
        loading: false,
        color: '#000',
        background: '#fff',
        show: true,
        animated: false,
        startTime: [],
        endTime: [],
        startTimeValue: '请选择',
        endTimeValue: "请选择",
        lang: 'zh_CN',
        fileList: [],
        title:"发布拍卖",
        name:"",
        price: "",
        addPrice: "",
        detail: "",
        num:1,
        expressFee:0
    },
  onLoad:function(options){
      var that =this
    
  },
  titleInput:function(e){
    var that = this
    that.setData({
        name: e.detail.value
    })
  },
    inputAddPrice: function (e) {
        var that = this
        that.setData({
            addPrice: e.detail.value
        })
    },
    inputExpressFee:function(e){
        var that = this
        that.setData({
            expressFee: e.detail.value
        })
    },
    detailInput: function (e) {
        var that = this
        that.setData({
            detail: e.detail.value
        })
    },
    inputPrice: function (e) {
        var that = this
        that.setData({
            price: e.detail.value
        })
    },
    startOnConfirm(e) {
        var that = this
        console.log(e.detail)
        that.setData({
            startTimeValue: e.detail.label
        })
    },
    onChangeNum(e) {
        console.log(e)
        this.setData({
            num: e.detail.value,
        })
    },
    endOnConfirm(e) {
        var that = this
        console.log(e.detail)
        that.setData({
            endTimeValue: e.detail.label
        })
    },

    handlerGobackClick: function () {
        wx.navigateBack();
    },
    onChange(e) {
        console.log('onChange', e)
        const {
            file,
            fileList
        } = e.detail
        if (file.status === 'uploading') {
            this.setData({
                progress: 0,
            })
            loading("上传中")
        } else if (file.status === 'done') {
            this.setData({
                imageUrl: file.url,
            })
        }

        // Controlled state should set fileList
        this.setData({
            fileList
        })
    },
    onSuccess(e) {
        console.log('onSuccess', e)
    },
    onFail(e) {
        console.log('onFail', e)
    },
    onComplete(e) {
        console.log('onComplete', e)
        hide();
    },
    onProgress(e) {
        console.log('onProgress', e)
        this.setData({
            progress: e.detail.file.progress,
        })
    },
    onPreview(e) {
        console.log('onPreview', e)
        const {
            file,
            fileList
        } = e.detail
        wx.previewImage({
            current: file.url,
            urls: fileList.map((n) => n.url),
        })
    },
    onRemove(e) {
        const {
            file,
            fileList
        } = e.detail
        wx.showModal({
            content: '确定删除？',
            success: (res) => {
                if (res.confirm) {
                    this.setData({
                        fileList: fileList.filter((n) => n.uid !== file.uid),
                    })
                }
            },
        })
    },
    publish:tool.debounce(function(){
        var that = this
        var list = that.data.fileList;
        var arr = new Array();
        console.log(that.data.fileList)
        for(var i= 0;i<list.length;i++){
           arr.push(JSON.parse(list[i].res.data).body.url[0])
        }
        if(util.isNull(that.data.detail)){
          alert("详情描述不能为空")
          return;
        }   
        //  if(util.isNull(that.data.endTimeValue)||that.data.endTimeValue=="请选择"){
        //     alert("结束时间不能为空")
        //     return;
        // }   
        if(util.isNull(that.data.price.length)){
            alert("价格不能为空")
            return;
        }     
        if(util.isNull(arr)){
            alert("照片不能为空")
            return;
        }
        // var   end = new   Date(Date.parse(that.data.endTimeValue.replace(/-/g,"/")));
        // var  now = new Date();
        // if(now>end){
        //     alert("结束日期必须大于当前时间")
        //     return;
        // }
        var param = {
            detail:that.data.detail,
            price:that.data.price,
            url:JSON.stringify(arr),
            num:that.data.num,
            expressFee:that.data.expressFee
        };
        that.setData({
            btnVisable:true
        })
        util.request("platGoodsAction.publish",param).then(function(res){ 
           console.log(res)
           that.setData({
            btnVisable:false
          })
           if(res.code=="00000"){
               wx.navigateBack({
                 complete: (res) => {    
                 },
               })
           }else{
               alert(res.msg)
           }
        })
    },1000)
});