//index.js
//获取应用实例
//const app = getApp();
var util = require('../../utils/util.js');
var amp = require("../../utils/amp.js")
import {
  $startWuxRefresher,
  $stopWuxRefresher,
  $stopWuxLoader
} from '../../dist/index'
import {
  $wuxNotification
} from '../../dist/index'
var page = 1;
var amapFile = require('../../lib/amap-wx.js');
var markersData = {
  latitude: '',//纬度
  longitude: '',//经度
  key: "1c6c7dba50dfdba107644ceaa09a16ae"//申请的高德地图key
};
const app = getApp()
Page({
  data: {
    title: "搜索....",
    loading: false,
    color: '#000',
    background: '#fff',
    show: true,
    hiddenLoadMore: false,
    listCategory: [],
    bannerList: [],
    peopleList: [],
    videoList: [],
    // swiper
    swiperH: '', //swiper高度
    nowIdx: 0, //当前swiper索引,
    currentTab: 'index',
    tabList: [{
        key: "index",
        id:1,
        value: "首页"
      },
      {
        key: "live",
        id:2,
        value: "直播"
      }, {
        key: "teacher",
        id:3,
        value: "老师"
      },  {
        key: "news",
        id:4,
        value: "头条"
      },  {
        key: "color",
        id:5,
        value: "色彩"
      },  {
        key: "real",
        id:6,
        value: "写真"
      }
      ,  {
        key: "video",
        id:7,
        value: "小视频"
      }
      ,{
        key: "suxie",
        id:7,
        value: "速写"
      }
      ,{
        key: "child",
        id:7,
        value: "儿童"
      }
    ],
    city:"中国",
    weather:"未知",
    bottomText:"",
    // landscape:false,
    landscape:{},
    animationData: null,
    isAnimationData:false,
    width:0
  },
  openLandScape:function(){
  
  if(this.data.isAnimationData){
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'linear'
    })
    animation.translateX(-60).step();
    this.setData({
      animationData: animation.export(),
      isAnimationData:false
    })
  }else{
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'linear'
    })
    animation.translateX(0).step();
    this.setData({
      animationData: animation.export(),
      isAnimationData:true
    })
  }

  },
  //去活动专场
  goActivity:function(e){
   console.log(e.currentTarget.dataset.item)
   var item = e.currentTarget.dataset.item;
   if(item.isH5==1){
    var url = encodeURIComponent(e.currentTarget.dataset.item.url);
   wx.navigateTo({
   url: '/plat/webview/webview?url='+url
 })}
 else if(item.isH5==0) {
     wx.navigateTo({
     url: e.currentTarget.dataset.item.url,
 })
 }else if(item.isH5==3){
   var arr = new Array();
   arr.push(e.currentTarget.dataset.item.bgUrl)
   wx.previewImage({
     current:e.currentTarget.dataset.item.bgUrl,
     urls: arr,
   })
 }
  },
  onCloseLandscape:function(){
    var that = this
    that.setData({
      'landscape.hidden':false
    })
  },
  bindweather: function () {
    console.log("weather")
  },
  bindlogo: function () {
    console.log("logo")
  },
  onChangeTab(e) {
    console.log('onChange', e)
    this.setData({
      currentTab: e.detail.key,
    })
  },
  //获取swiper高度
  getHeight: function (e) {
    var winWid = wx.getSystemInfoSync().windowWidth - 2 * 50; //获取当前屏幕的宽度
    var imgh = e.detail.height; //图片高度
    var imgw = e.detail.width;
    var sH = winWid * imgh / imgw 
    this.setData({
      swiperH: sH, //设置高度
      width:imgw
    })
  },
  //swiper滑动事件
  swiperChange: function (e) {
    this.setData({
      nowIdx: e.detail.current
    })
  },
  search:function(){
  wx.navigateTo({
    url: '/plat/search/search',
  })
  },
  onLoad: function () {
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'linear'
    })
    animation.translateX(-90).step();
    this.setData({
      animationData: animation.export(),
      isAnimationData:false
    })
    var that = this;
    that.setData({
      bottomText:app.globalData.platInfo.bottomText
    })
    that.init();
    that.getPlaceAndWeather()
  },
  onShow:function(){
    var that = this;
    this.setData({
      msgList: [
        { url: "url", title: "点击查看活动" },{ url: "url", title: "点击查看活动" }]
    });
    that.getPlaceAndWeather();
  },
  getPlaceAndWeather:function(){
  var that =this;
    wx.getSetting({
      success:function(res){
        console.log(res)
        wx.openSetting({
          success:function(res){
            console.log(res)
          }
        })
        wx.getLocation({
          type: 'wgs84',
          success: function (res) {
            // console.log(res);
            var latitude = res.latitude  
            var longitude = res.longitude
            var myAmapFun = new amapFile.AMapWX({ key: markersData.key });
            myAmapFun.getRegeo({
              location: '' + longitude + ',' + latitude + '',//location的格式为'经度,纬度'
              success: function (data) {
                console.log(data);
               
              },
              fail: function (info) { }
            });
            myAmapFun.getWeather({
              location: '' + longitude + ',' + latitude + '',//location的格式为'经度,纬度'
              success: function (data) {
                console.log(data);
                that.setData({
                  city:data.liveData.city,
                  weather:data.liveData.temperature
                })
              },
              fail: function (info) {
                that.setData({
                  city:"未知城市",
                  weather:"未知"
                })
              }
            })
          }
        })
      }
    })
  },
  init() {
    page = 1;
    var that = this;
    that.initIndex();
  },
  initIndex:function(){
    var that =this;
    var param= {
      page:1,
      pageSize:10
    }
    util.request("indexAction.initIndex", param).then(res=>{
       console.log("初始化主页");
       console.log(res);
       var flag;
       if(true){
         flag = true;
       }else{
        flag = false;
       }
       that.setData({
        listCategory: res.body.category,
        bannerList: res.body.banner,
        peopleList: res.body.people,
        videoList: res.body.reComInfo,
        landscape:flag,
        landscape: res.body.reLandscapeInfo,
       })
    })
  },

  async getVideList(page) {
    var that = this
    var param = {
      "keyword": "",
      "page": page,
      "is_free": "1",
      "create_time": "1",
      "category": "1",
      "is_recommend": "1"
    }
    await util.request("platVideoAction.getVideoList", param).then(function (res) {
      console.log(res)
      that.vList = res.body.list;
      that.setData({
        videoList: res.body.list
      })
    })
  },
  goToCategoryList: function (e) {
    console.log("根据分类获取视频"+e.currentTarget.dataset.item.isH5)
    var isH5 = e.currentTarget.dataset.item.isH5;
     if(isH5==1){
       var url = encodeURIComponent(e.currentTarget.dataset.item.xcxUrl);
      wx.navigateTo({
      url: '/plat/webview/webview?url='+url
    })}
    else{
        wx.navigateTo({
      url: '/pages/videoList/list?id=' + e.currentTarget.dataset.item.id,
    })
    }
  },
  goToPlay: function (e) {
    console.log(e.currentTarget.dataset.id)
    wx.navigateTo({
      url: '/pages/video/video?id=' + e.currentTarget.dataset.id,
    })
  },
  goToPeople: function (e) {
    console.log(e.currentTarget.dataset.id)
    console.log("根据人物获取视频")
    wx.navigateTo({
      url: '/pages/people/people?id=' + e.currentTarget.dataset.id,
    })
  },
  //pages/peopleList/peopleList
  goToAllPeople: function (e) {
    console.log(e.currentTarget.dataset.isAll)
    console.log("根据人物获取视频")
    wx.navigateTo({
      url: '/pages/peopleList/peopleList?isALL=' + e.currentTarget.dataset.isAll,
    })
  },

  onPulling: function () {
    console.log('onPulling')
  },
  onRefresh() {
    var that = this
    //   setTimeout(() => {
    //     //加载数据
    //     console.log("刷新数据")

    // }, 3000)
    console.log("下拉刷新1")
    that.init()
    console.log("下拉刷新2")
    $stopWuxRefresher()
  },
  onLoadmore() {
    console.log('onLoadmore')
    setTimeout(() => {}, 3000)
  },
  onReachBottom: function () {
    var that = this;
    that.setData({
      hiddenLoadMore: false
    })
    console.log("底部刷新")
    page = page + 1
    var that = this
    var param = {
      page: page,
      pageSize:10
    }
    util.request("platVideoAction.getVideoList", param).then(function (res) {
      console.log(res)
      var oldList = that.data.videoList;
      var newList = res.body.list;
      for (var item in newList) {
        oldList.push(newList[item]);
      }
      setTimeout(() => {
        that.setData({
          videoList: oldList,
          hiddenLoadMore: true
        })
      }, 1000)
    })
  },
  goBanner(e) {
    console.log(e.currentTarget.dataset.item.url)
    var isH5 = e.currentTarget.dataset.item.isH5;
     if(isH5==1){
      var url = encodeURIComponent(e.currentTarget.dataset.item.url);
     wx.navigateTo({
     url: '/plat/webview/webview?url='+url
   })}else if(isH5==0){
       wx.navigateTo({
       url: e.currentTarget.dataset.item.url,
   })
   }else{
     var arr = new Array();
     arr.push(e.currentTarget.dataset.item.imageUrl)
     wx.previewImage({
       urls: arr,
       current:e.currentTarget.dataset.item.imageUrl
     })
   }
  },
  /**
   * 下拉加载
   */
  onPullDownRefresh: function () {
    console.log("assas")
  },
  goLive: function () {
    // wx.navigateTo({
    //   url: '/pages/vshow/index',
    // })
    wx.navigateTo({
      url: '/shortVideo/pages/short/index',
    })
  },
  workAlbum: function () {
    this.showNotification()
  },
  famous: function () {
    this.showNotification()
  },
  activity: function () {
    wx.navigateTo({
      url: '/plat/activity/index',
    })
  },
  showNotification() {
    this.closeNotification = $wuxNotification().show({
      image: "   ",
      title: ' ---------- ',
      text: '\n\n客官\n\n该功能暂未开放，敬请期待',
      data: {
        message: '该功能暂时不开放!!!'
      },
      duration: 3000,
      onClick(data) {
        console.log(data)
      },
      onClose(data) {
        console.log(data)
      },
    })
  },
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '志合云艺术',
      path: '/pages/index/index'
    }
  },
  onShareTimeline(){
    
  }
});