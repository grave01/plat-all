var util = require('../../../utils/util.js');
// pages/live/publish/index.js
import {$wuxDialog} from '../../../dist/index'
const alert = (content) => {
  $wuxDialog('#wux-dialog--alert').alert({
    resetOnClose: true,
    title: '提示',
    content: content,
  })
}
import { $wuxLoading } from '../../../dist/index'
const loading = (content) => {
  $wuxLoading().show({
    text: content,
})
}
const hide = () => {
  $wuxLoading().hide();
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    minDate: util.formatTime(new Date()),
    btnVisable:false,
    name: "",
    anchorName: "",
    anchorWechat: "",
    detail: "",
    startTime: "",
    endTime: "",
    type: "",
    screenType: "",
    coverImg: "",
    shareImg: "",
    coverImgUrl: "",
    shareImgUrl: "",
    closeLike: "",
    closeGoods: "",
    closeComment: "",
    tempCoverImg: "",
    tempShareImg: "",
    isTempCoverImg: false,
    isTempShareImg: false,
    spinning1: false,
    spinning2: false,
    value9: [],
    displayValue9: '请选择',
    value10: [],
    displayValue10: '请选择',
    lang: 'zh_CN',
    value4: '0',
    value5: '0',
    value6: '0',
    value7: '0',
    value8: '0',
    value9: '1',
    value101: '1',
    price: 0,
    isFree: true,
    fileList:[],
    dialogShow:false,
    buttons: [{text: '取消'}, {text: '去实名'}],
    error:""
  },
  tapDialogButton(e) {
    console.log(e)
    this.setData({
        dialogShow: false,
    })
    var index = e.detail.index
    if(index==1){
      wx.navigateToMiniProgram({
        appId: 'wxcbbd86b156ae4441',
      })
    }
},
  onChange1(e) {
    console.log('onChange', e)
    const {
        file,
        fileList
    } = e.detail
    if (file.status === 'uploading') {
        this.setData({
            progress: 0,
        })
        loading("上传中");
    } else if (file.status === 'done') {
        this.setData({
            imageUrl: file.url,
        })
    }

    // Controlled state should set fileList
    this.setData({
        fileList
    })
},
onSuccess(e) {
    console.log('onSuccess', e)
},
onFail(e) {
    console.log('onFail', e)
},
onComplete(e) {
    console.log('onComplete', e)
    hide();
},
onProgress(e) {
    console.log('onProgress', e)
    this.setData({
        progress: e.detail.file.progress,
    })
},
onPreview(e) {
    console.log('onPreview', e)
    const {
        file,
        fileList
    } = e.detail
    wx.previewImage({
        current: file.url,
        urls: fileList.map((n) => n.url),
    })
},
onRemove(e) {
    const {
        file,
        fileList
    } = e.detail
    wx.showModal({
        content: '确定删除？',
        success: (res) => {
            if (res.confirm) {
                this.setData({
                    fileList: fileList.filter((n) => n.uid !== file.uid),
                })
            }
        },
    })
},


  onChange(e) {
    console.log(e)
    const {
      key,
      values
    } = e.detail
    const lang = values[key]

    this.setData({
      lang,
    })
  },
  setValue(values, key, mode) {
    this.setData({
      [`value${key}`]: values.value,
      [`displayValue${key}`]: values.label,
      // [`displayValue${key}`]: values.displayValue.join(' '),
    })
  },
  onConfirm(e) {
    const {
      index,
      mode
    } = e.currentTarget.dataset
    this.setValue(e.detail, index, mode)
    console.log(`onConfirm${index}`, e.detail)
  },
  onVisibleChange(e) {
    this.setData({
      visible: e.detail.visible
    })
  },
  onClick() {
    this.setData({
      visible: true
    })
  },
  onChangekey(field, e) {
    this.setData({
      [field]: e.detail.value
    })

    console.log('radio发生change事件，携带value值为：', e.detail)
  },
  onChange4(e) {
    this.onChangekey('value4', e)
  },
  onChange5(e) {
    this.onChangekey('value5', e)
  },
  onChange6(e) {
    this.onChangekey('value6', e)
  },
  onChange7(e) {
    this.onChangekey('value7', e)
  },
  onChange8(e) {
    this.onChangekey('value8', e)
  },
  onChange9(e) {
    var that = this
    console.log(e)
    var val = e.detail.value
    if (val == 0) {
      that.setData({
        isFree: false,
        price: 0
      })
    } else {
      that.setData({
        isFree: true
      })
    }
    this.onChangekey('value101', e)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  handlerGobackClick: function () {
    wx.navigateBack({
      complete: (res) => {},
    })
  },

  inputChange1(e) {
    var that = this
    console.log(e)
    that.setData({
      name: e.detail.value
    })
  },
  inputChange2(e) {
    var that = this
    console.log(e)
    that.setData({
      anchorName: e.detail.value
    })
  },
  inputChange3(e) {
    var that = this
    console.log(e)
    that.setData({
      anchorWechat: e.detail.value
    })
  },
  inputChange4(e) {
    var that = this
    console.log(e)
    that.setData({
      detail: e.detail.value
    })
  },
  inputChange5(e) {
    var that = this
    console.log(e)
    that.setData({
      price: e.detail.value
    })
  },
  publish: function () {
    var that = this;
    console.log("封面");
    console.log(that.data.fileList[0])
    if(util.isNull(that.data.fileList)){
      alert("封面必须上传")
      return;
    }
    // if(util.isNull(that.data.fileList2)){
    //   alert("分享照片必须上传")
    //   return;
    // }
    var list = that.data.fileList;
    var arr = new Array();
    console.log(that.data.fileList)
    for(var i= 0;i<list.length;i++){
       arr.push(JSON.parse(list[i].res.data).body.url[0])
    }
    that.setData({
      coverImg:arr[0]
    })
    if (util.isNull(that.data.name)) {
      alert("直播间名称不能为空")
      return;
    }
    if (util.isNull(that.data.coverImg)) {
      alert("封面必须上传")
      return;
    }
    if (util.isNull(that.data.displayValue9) || that.data.displayValue9 == "请选择") {
      alert("开播时间必须选择")
      return;
    }
    if (util.isNull(that.data.displayValue10) || that.data.displayValue10 == "请选择") {
      alert("下播时间必须选择")
      return;
    }
    if (util.isNull(that.data.anchorName)) {
      alert("请输入主播昵称")
      return;
    }
    if (util.isNull(that.data.anchorWechat)) {
      alert("请输入主播微信号方可开启直播")
      return;
    }
    if (util.isNull(that.data.detail)) {
      alert("请输入直播内容")
      return;
    }
    var stTime  = new Date(that.data.displayValue9).getTime()/(1000*60)
    var edTime  = new Date(that.data.displayValue10).getTime()/(1000*60)
    var beginTime  =stTime-new Date().getTime()/(1000*60)
    console.log(beginTime)
     if(beginTime<10){
      alert("请设置开播时间在十分钟之后")
      return;
     }
     var zhouqi = edTime-stTime;
     if((edTime-stTime)<0){
      alert("下播时间必须大于开播时间")
      return;
     }
     console.log("剩余几个月"+beginTime/(60*24))
     if(beginTime/(60*24)>180){
      alert("开播计划不能六个月之后")
      return;
     }
     if(zhouqi/60>24){
      alert("直播时长不能超过24小时")
      return;
     }
    var param = {
      name: that.data.name,
      coverImgUrl: that.data.coverImg,
      startTime: that.data.displayValue9,
      endTime: that.data.displayValue10,
      anchorName: that.data.anchorName,
      anchorWechat: that.data.anchorWechat,
      type: that.data.value4,
      screenType: that.data.value5,
      closeLike: that.data.value6,
      closeGoods: that.data.value7,
      closeComment: that.data.value8,
      detail: that.data.detail,
      isFree: that.data.value101,
      price: that.data.price
    }
    that.setData({
      btnVisable:true
    })
    console.log(param)
    loading("发布中")
    util.request("platLiveAction.publish", param).then(function (res) {
      console.log(res)
      that.setData({
        btnVisable:false
      })
      if (res.code == "00000") {
        wx.navigateBack({
          complete: (res) => {},
        })
      }else{
        //alert(res.desc);  buttons: [{text: '取消'}, {text: '去实名'}],
        var btn = [];
        if(res.code=='300036'){
           btn = [{text: '取消'}, {text: '去实名'}]
        }else{
          btn=[{text: '取消'}]
        }
        that.setData({
          error:res.desc,
          dialogShow:true,
          buttons:btn
        })
        hide()
      }
    })
  }
})