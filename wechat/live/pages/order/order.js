
//JS
var util = require('../../utils/util.js');
var page = 1;
var pageSize=10;
var app = getApp()
import { $wuxLoading } from '../../dist/index'
import { $startWuxRefresher, $stopWuxRefresher, $stopWuxLoader } from '../../dist/index'
const loaing = (content) => {
  $wuxLoading().show({
    text: content,
})
}
const hide = () => {
  $wuxLoading().hide();
}

Page({
  data: {
    // 顶部菜单切换
    navbar: ['全部', '待付款', "待发货", "待收货", "已完成","退货"],
    // 默认选中菜单
    currentTab: 0,
    index: 0,
    pick_name: "",
    // list数据
    list: [],
    type:0,
    all:[],
    waitPay:[],
    waitExpress:[],
    waitTake:[],
    finish:[],
    refund:[],
  },
  onChangeTab(e) {
    console.log('onChange', e)
    page = 1
    this.setData({
      currentTab: e.detail.key,
    })
    console.log(this.data.all.length)
  // if( e.detail.key==0){
  //   if(this.data.all.length==0){
  //     this.initOrderList(e.detail.key)
  //    }
  // }
  // if( e.detail.key==1){
  //  if(this.data.waitPay.length==0){
  //   this.initOrderList(e.detail.key)
  //  }}
  //    if( e.detail.key==2){
  //  if(this.data.waitExpress.length==0){
  //   this.initOrderList(e.detail.key)
  //  }}
  //  if( e.detail.key==3){
  //  if(this.data.waitTake.length==0){
  //   this.initOrderList(e.detail.key)
  //  }}
  //  if(e.detail.key==4){
  //  if(this.data.finish.length==0){
  //   this.initOrderList(e.detail.key)
  //  }}
  //  if( e.detail.key==5){
  //  if(this.data.refund.length==0){
  //   this.initOrderList(e.detail.key)
  //  }}
  this.initOrderList(e.detail.key)
},
  // 初始化加载
  onLoad: function(options) {
    var that = this;
    var type =options.type
    console.log(type) 
     that.setData({
       currentTab:type,
       index:type
     })
  },
  onShow:function(){
    var that = this;
    //that.getOrderList(that.data.type);
    page = 1;
    that.initOrderList(that.data.index);
  },
  initOrderList:function(type){
    var that = this;
    var param ={
     page:page,
     pageSize:pageSize,
     type:type
    }
    loaing("加载中")
    util.request("orderAction.orderList",param).then(function(res){
      console.log(res)
      console.log(that.data.currentTab)
      hide();
      that.setData({
       all:res.body.all,
       waitPay:res.body.waitPay,
       waitExpress:res.body.waitExpress,
       waitTake:res.body.waitTake,
       finish:res.body.finish,
       refund:res.body.refund
      })
    })
  },
  getOrderList:function(type){
    var that = this;
    var param ={
     page:page,
     pageSize:pageSize,
     type:type
    }
    util.request("orderAction.orderList",param).then(function(res){
      console.log(res)
      that.setData({
        list:res.body.list,
        type:type
      })
    })
  },
 
  //顶部tab切换
  navbarTap: function(e) {
    var that = this;
    this.setData({
      currentTab: e.currentTarget.dataset.idx
    })
    //['全部', '待付款', "待发货", "待收货", "已完成"],
    console.log(e.currentTarget.dataset.idx)
    // var value = e.currentTarget.dataset.idx;
    // that.getOrderList(value)
  },
 
  handlerGobackClick:function(){
    wx.navigateBack({
      complete: (res) => {},
    })
  },
  // 发起再支付
  goRepay:function(e){
    var that =this;
    var item = e.currentTarget.dataset.item
    console.log("发起再次支付"+item)
    console.log(item)
    var param = {
      orderNo:item.orderNo
    }
    loaing("去支付中")
    util.request("orderAction.repay",param).then(function(res){
      console.log(res)
      var pay = res.body.payInfo
      wx.requestPayment({
        'timeStamp': pay.timeStamp,
        'nonceStr': pay.nonceStr,
        'package': pay.packageValue,
        'signType': 'MD5',
        'appId': pay.appId,
        'paySign': pay.paySign,
        'success': function (res) {
          console.log('success')
        },
        'fail': function (res) {
          console.log('fail')
        },
        'complete': function (res) {
          console.log('complete')
          hide()
          // that.initOrderList(that.data.index);
        }
      })
    })
  },
   // 确认完成订单
  confirmOrder:function(e){
    var that = this;
    var item = e.currentTarget.dataset.item
    console.log("确认完成订单"+item)
    console.log(item)
    var param = {
      orderNo:item.orderNo
    }
    loaing("确认完成中")
    util.request("orderAction.confirmOrder",param).then(function(res){
      console.log(res)
      if(that.data.currentTab=="0"){
        var all = that.data.all
        for(var i = 0;i<all.length;i++){
           if( all[i].orderNo==item.orderNo){
            all[i].orderStatus =104
           }
        }
        that.setData({
          all:all
        })
      }
      if(that.data.currentTab=="3"){
        var waitTake =  that.data.waitTake;
        for(var i = 0;i<waitTake.length;i++){
          if(waitTake[i].orderNo==item.orderNo){
            waitTake[i].orderStatus =104 
          }
        }
        that.setData({
          waitTake:waitTake
        })
      }
      hide()
    })
  },
     // 查看物流信息
  getExpressInfo:function(e){
    var that =this;
    var item = e.currentTarget.dataset.item
    console.log("查看物流信息"+item)
    console.log(item)
    var param = {
      orderNo:item.orderNo
    }
    loaing("查看物流中")
    util.request("orderAction.getExpressInfo",param).then(function(res){
      console.log(res)
      // that.initOrderList(that.data.index);
      hide()
    })
  },
    // 取消订单
       cancelOrder:function(e){
        var that =this;
        var item = e.currentTarget.dataset.item
        console.log("取消订单"+item)
        console.log(item)
        var param = {
          orderNo:item.orderNo
        }
        loaing("取消订单中")
        util.request("orderAction.cancelOrder",param).then(function(res){
          console.log(res)
          // that.initOrderList(that.data.index);
          if(that.data.currentTab=="0"){
            var all = that.data.all
            for(var i = 0;i<all.length;i++){
               if( all[i].orderNo==item.orderNo){
                all[i].orderStatus =106
               }
            }
            that.setData({
              all:all
            })
          }
          console.log("切换"+that.data.currentTab)
          if(that.data.currentTab=="1"){
            var waitPay =  that.data.waitPay;
            for(var i = 0;i<waitPay.length;i++){
              if(waitPay[i].orderNo==item.orderNo){
                waitPay[i].orderStatus =106 
              }
            }
            that.setData({
              waitPay:waitPay
            })
          }
          hide()
        })
      },
  applyRefund:function(e){
    var that = this;
    loaing("申请中");
    var param = {
      orderNo:e.currentTarget.dataset.item.orderNo
    }
    util.request("platRefundAction.apply",param).then(function(res){
       if(res.code=="00000"){
        if(that.data.currentTab=="0"){
          var all = that.data.all
          for(var i = 0;i<all.length;i++){
             if( all[i].orderNo==item.orderNo){
              all[i].refundStatus =300
             }
          }
          that.setData({
            all:all
          })
        }
        if(that.data.currentTab=="2"){
          var waitExpress =  that.data.waitExpress;
          for(var i = 0;i<waitExpress.length;i++){
            if(waitExpress[i].orderNo==item.orderNo){
              waitExpress[i].refundStatus =300 
            }
          }
          that.setData({
            waitExpress:waitExpress
          })
        }
       }
       hide()
    })
  },
    // 刷新相关
    onPulling: function () {
      console.log('onPulling')
    },
    onRefresh() {
      var that = this
    //   setTimeout(() => {
    //     //加载数据
    //     console.log("刷新数据")
      
    // }, 3000)
    console.log("下拉刷新1")
    page = 1;
    that.initOrderList()
    console.log("下拉刷新2")
    $stopWuxRefresher()
    },
    onLoadmore() {
      console.log('onLoadmore')
      setTimeout(() => {}, 3000)
    },
    onReachBottom: function () {
      var that = this;
      console.log("底部刷新")
      page = page+1;
      var param ={
        page:page,
        pageSize:pageSize,
        type:that.data.currentTab
       }
       loaing("加载中")
       console.log(param)
       util.request("orderAction.orderList",param).then(function(res){
         console.log(res)
         console.log(that.data.currentTab)
         hide();
        //  var li = new Array();
        //  li[0] = res.body.all,
        //  li[1] = res.body.waitPay,
        //  li[2] = res.body.waitExpress,
        //  li[3] = res.body.waitTake,
        //  li[4] = res.body.finish,
        //  li[5] = res.body.refund,
          that.data.all.join(res.body.all)
          var a_1= that.data.all;
          var a_2= that.data.waitPay;
          var a_3=that.data.waitExpress;
          var a_4= that.data.waitTake;
          var a_5= that.data.finish;
          var a_6= that.data.refund;
          for(var i = 0;i<res.body.all.length;i++){
            a_1.push(res.body.all[i])
          }
          for(var i = 0;i<res.body.waitPay.length;i++){
            a_2.push(res.body.waitPay[i])
          }
          for(var i = 0;i<res.body.waitExpress.length;i++){
            a_3.push(res.body.waitExpress[i])
          }
          for(var i = 0;i<res.body.waitTake.length;i++){
            a_4.push(res.body.waitTake[i])
          }
          for(var i = 0;i<res.body.finish.length;i++){
            a_5.push(res.body.finish[i])
          }
          for(var i = 0;i<res.body.refund.length;i++){
            a_6.push(res.body.refund[i])
          }
          // that.data.waitPay.join(res.body.waitPay)
          // that.data.waitExpress.join(res.body.waitExpress)
          // that.data.waitTake.join(res.body.waitTake)
          // that.data.finish.join(res.body.finish)
          // that.data.refund.join(res.body.refund)
         that.setData({
          all:a_1,
          waitPay:a_2,
          waitExpress:a_3,
          waitTake:that.a_4,
          finish:a_5,
          refund:a_6
         })
       })
  
    },
})