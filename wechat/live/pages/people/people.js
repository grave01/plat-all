// pages/video/video.js
var util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    url: "",
    name: "",
    peopleId: "",
    people: "",
    videoList: [],
    home:true
  },
  onHome:function(){
    wx.reLaunch({
      url: '/pages/index/index',
    })
  },
 //图片点击事件
 imgYu: function (event) {
  var src = event.currentTarget.dataset.url;//获取data-src
  var imgList = event.currentTarget.dataset.list;//获取data-src
  //图片预览
  wx.previewImage({
    current: src, // 当前显示图片的http链接
    urls: imgList // 需要预览的图片http链接列表
  })
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    console.log("获取");
    console.log(options.id) //开始处理相关请求
    that.setData({
      peopleId: options.id
    })
    wx.getSystemInfoSync();
    that.init();
  },
  init() {
    var that = this;
    that.getPeople()
  },
  getPeople() {
    var that = this;
    var param = {
      "id": that.data.peopleId,
      "page":1,
      "is_recommend":"1"
    }
    util.request("platPeopleAction.getPeople", param).then(function (res) {
      console.log(res)
      that.setData({
        people: res.body.object,
        videoList: res.body.list
      })
    })
  },
  goToPlay: function (e) {
    console.log(e.currentTarget.dataset.id)
    wx.navigateTo({
      url: '/pages/video/video?id=' + e.currentTarget.dataset.id,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  handlerGohomeClick() {
    wx.navigateBack()
  }
})