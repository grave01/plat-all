// pages/mycenter/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperH:'',//swiper高度
    nowIdx:0,//当前swiper索引
    imgList:[//图片列表
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1597343579818&di=979423712a6fbc3db0aff565ae93ae15&imgtype=0&src=http%3A%2F%2Ft9.baidu.com%2Fit%2Fu%3D3923875871%2C1613462878%26fm%3D79%26app%3D86%26f%3DJPEG%3Fw%3D1280%26h%3D854",
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1597343579818&di=84a4bc131407c696fca0ec2952449179&imgtype=0&src=http%3A%2F%2Ft9.baidu.com%2Fit%2Fu%3D2266751744%2C4253267866%26fm%3D79%26app%3D86%26f%3DJPEG%3Fw%3D1280%26h%3D854",
        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1597343579818&di=fcc9c759297673d5e8d7798fcb6fd1db&imgtype=0&src=http%3A%2F%2Ft9.baidu.com%2Fit%2Fu%3D3949188917%2C63856583%26fm%3D79%26app%3D86%26f%3DJPEG%3Fw%3D1280%26h%3D875",
    ]
},
//获取swiper高度
getHeight:function(e){
    var winWid = wx.getSystemInfoSync().windowWidth - 2*50;//获取当前屏幕的宽度
    var imgh = e.detail.height;//图片高度
    var imgw = e.detail.width;
    var sH = winWid * imgh / imgw + "px"
    this.setData({
        swiperH: sH//设置高度
    })
},
//swiper滑动事件
swiperChange:function(e){
    this.setData({
        nowIdx: e.detail.current
    })
},

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  handlerGobackClick:function(){
    wx.navigateBack()
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})