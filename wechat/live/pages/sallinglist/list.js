//index.js
//获取应用实例
//const app = getApp();
var util = require('../../utils/util.js');
import {
  $startWuxRefresher,
  $stopWuxRefresher,
  $stopWuxLoader
} from '../../dist/index'
var  page=1;
Page({
  data: {
    title: "艺拍",
    loading: false,
    color: '#000',
    background: '#fff',
    show: true,
    animated: false,
    hiddenLoadMore: false,
    pageSize: 10,
    list: []
  },
  onShow: function () {
    var that = this
    page = 1
    that.init()
  },
  onLoad: function () {
    var that = this
    that.init()
  },
  init: function () {
    var that = this
    var param = {
      "page":page,
      "pageSize": that.data.pageSize
    }
    util.request("auctionAction.getAuctionList", param).then(function (res) {
      var li = res.body.list
      console.log(li)
      for (var i = 0; i < li.length; i++) {
        // li.url = JSON.parse( li[i].url);
        console.log(li[i].url)
        // li.image = li.url[0]
      }
      that.setData({
        list: li,
        hiddenLoadMore:true
      })
    })
  },
  search: function () {
    wx.switchTab({
      url: '/pages/demo1/index',
    })
  },
  goToViedoList: function (e) {
    console.log("根据分类获取视频")
    wx.navigateTo({
      url: '/pages/videoList/list?id=' + e.currentTarget.dataset.id,
    })
  },
  goToPlay: function (e) {
    console.log(e.currentTarget.dataset.id)
    wx.navigateTo({
      url: '/pages/video/video?id=' + e.currentTarget.dataset.id,
    })
  },
  goToPeople: function (e) {
    console.log(e.currentTarget.dataset.id)
    console.log("根据人物获取视频")
    wx.navigateTo({
      url: '/pages/people/people?id=' + e.currentTarget.dataset.id,
    })
  },
  //pages/peopleList/peopleList
  goToAllPeople: function (e) {
    console.log(e.currentTarget.dataset.isAll)
    console.log("根据人物获取视频")
    wx.navigateTo({
      url: '/pages/peopleList/peopleList?isALL=' + e.currentTarget.dataset.isAll,
    })
  },
  // 去拍卖
  goBuy: function (e) {
    console.log(e)
    wx.navigateTo({
      url: '/pages/onSelling/salling?id=' + e.currentTarget.dataset.id,
    })
  },
  // 发布拍买的东西
  publish: function () {
    var isEditUderInfo = wx.getStorageSync('has_edit');
   if(isEditUderInfo==1){
    wx.navigateTo({
      url: '/pages/publishSale/publish',
    })
   }else{
    wx.navigateTo({
      url: '/pages/userInfo/index',
    })
   }
  },
  /**
   * 触底上拉加载
   */
  onReachBottom: function () {
    console.log("assas")
  },

  /**
   * 下拉加载
   */
  onPullDownRefresh: function () {
    console.log("assas")
  },
  onPulling: function () {
    console.log('onPulling')
  },
  onRefresh() {
    var that = this
    //   setTimeout(() => {
    //     //加载数据
    //     console.log("刷新数据")

    // }, 3000)
    console.log("下拉刷新1")
    page = 1
    that.init()
    console.log("下拉刷新2")
    $stopWuxRefresher()
  },
  onLoadmore() {
    console.log('onLoadmore')
    setTimeout(() => {}, 3000)
  },
  onReachBottom: function () {
    var that = this;
    console.log("底部刷新")
   page = page+1;
    var param = {
      "page": page,
      "pageSize": that.data.pageSize
    }
    that.setData({
      hiddenLoadMore: false
    })
    util.request("auctionAction.getAuctionList", param).then(function (res) {
      console.log(res)
      var list = res.body.list
      var li = that.data.list
      for (var i = 0; i < list.length; i++) {
        li.push(list[i])
      }
      if (list.length > 0) {
        that.setData({
          list: li,
          hiddenLoadMore: true
        })
      } else {
        // page = page - 1;
        setTimeout(() => {
          that.setData({
            hiddenLoadMore: true
          })
        }, 1000)
      }
    })
  },
});