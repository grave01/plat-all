/*
 ** 省市区 - 收货地址 js
 ** www.meizhuangdaka.com
 ** 省市区数据有误的话，自己改：/utils/citydata.js，开源源码
 */

//获取应用实例
var app = getApp();

// var cityData = require('../../utils/citydata.js');
var util = require('../../utils/util.js');

import {
  $wuxDialog
} from '../../dist/index'
import { $wuxLoading } from '../../dist/index'
const loaing = (content) => {
  $wuxLoading().show({
    text: content,
})
}
const hide = () => {
  $wuxLoading().hide();
}
var app = getApp()
Page({
  data: {
    editSelfInfonotice:"为了更好体验小程序，请您填写真实完整信息！！！为了更好体验小程序，请您填写真实完整信息！！！为了更好体验小程序，请您填写真实完整信息！！！",
    visible1: false,
    visible2: false,
    alert:"",
    provinces: [],
    citys: [],
    areas: [],
    value: [0, 0, 0],
    name: '',
    fieldList: ['素描', '彩绘', '油画', '写真', '摄影'],
    genderList:["男","女","未知性别"],
    index: 0,
    index_1: 0,
    userName:null,
    age:0,
    emaiil:null,
    password:null,
    repassword:null,
    nowAddress:null,
    field:"",
    phone:null,
    value2: [],
    options2: [{
      title: '素描',
      value: '素描',
  },{
      title: '写真',
      value: '写真',
  },{
      title: '色彩',
      value: '色彩',
  },{
    title: '艺术',
    value: '艺术',
},{
  title: '设计',
  value: '设计',
}, {
  title: '速写',
  value: '速写',
}],
  displayValue2: '请选择',
  region: [],//定位获取位置
  array:[],
  userInfo:{},
  sex:0,
  user:{
     name:"",
     sex:null,
     phone:"",
     fields:"",
     email:"",
     address:"",
     wxHeadPhoto:""
  },
  dialogShow:false,
  buttons: [{text: '知道了'}],
  error:""
  },
  tapDialogButton(e) {
    console.log(e)
    this.setData({
        dialogShow: false,
    })
},
  choose:function(e){
    console.log(e)
    var that =this
    console.log("选择照片")
    wx.chooseImage({
      count: 1, // 限制上传数量
      sizeType: ['compressed'], // 必须放在 数组里面才有效； original 原图；compressed 压缩图
      sourceType: ['album', 'camera'], // album 从相册选图; camera 使用相机
      success:function(res){
          loaing("上传中...")
          wx.uploadFile({
          filePath: res.tempFilePaths[0],
          name: 'wehead',
          url: app.globalData.uploadUrl,
          header:{
            "content-type":'multipart/form-data'
           },
          success:function(res){
            console.log(JSON.parse(res.data).body.url[0])
            hide()
            that.setData({
              'user.wxHeadPhoto':JSON.parse(res.data).body.url[0]
            })
          },
          fail:function(err){
            hide()
            console.log(e)
            that.setData({
                dialogShow: true,
                error:"上传没成功，请稍后再试"
            })
          }
        })
      }
    })
  },
    // 页面初始化事件
    onLoad: function () {
      this.getUserInfo();
     
     },
        // 参数点击响应事件
  parameterTap:function(e){//e是获取e.currentTarget.dataset.id所以是必备的，跟前端的data-id获取的方式差不多
    var that=this
    console.log(e)
    var this_checked = e.currentTarget.dataset.id
    that.setData({
      "user.sex":this_checked
    })
  },
  listenerPickerSelected_1:function(e){
    this.setData({
      index_1: e.detail.value
    })
  },
  bindRegionChange: function (e) {
    console.log(e)
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      "user.address": e.detail.value[0]+ e.detail.value[1]+e.detail.value[2]
    })
    // this.getDetailPlace(e.detail.code)
  },
  setValue(values, key) {
    this.setData({
        [`value${key}`]: values.value,
        [`displayValue${key}`]: values.label,
    })
},
  onConfirm(e) {
    const { index } = e.currentTarget.dataset
    this.setValue(e.detail, index)
    console.log(`onConfirm${index}`, e.detail)
},
onValueChange(e) {
    const { index } = e.currentTarget.dataset
    console.log(`onValueChange${index}`, e.detail)
},
  getPhoneNumber:function(e){
   var that = this
   wx.login({
        success:function(res){
          var param ={
            code:res.code,
            iv :e.detail.iv,
             encryptedData:e.detail.encryptedData
           }
           util.request("loginAction.getPhoneNumber",param).then(function(res){
             console.log(res)
             that.setData({
              'user.phone':res.body.object.phoneNumber
             })
           })
        }
   })
 
  },
  onClose1() {
    this.onClose('visible1')
}, 
 onClosed1() {
  console.log('onClosed')
},   
 close1() {
  this.setData({
      visible1: false,
  })
}, 
onClose(key) {
  console.log('onClose')
  this.setData({
      [key]: false,
  })
},
open1(v) {
  this.setData({
      visible1: true,
      alert:v
  })
},
  submit:function(){
    var that= this
    console.log()
    // 'user.name':res.body.userInfo.name,
    // 'user.sex':res.body.userInfo.gender,
    // 'user.phone':res.body.userInfo.phone,
    // 'user.fields':res.body.userInfo.field,
    // 'user.email':res.body.userInfo.email,
    // "user.wxHeadPhoto":res.body.userInfo.wxHeadPhoto
    if(util.isNull(that.data.user.name)){
      that.open1("姓名不能为空")
      return
    }
    if(util.isNull(that.data.user.phone)){
      that.open1("该平台需要绑定手机号")
      return
    }
    if(util.isNull(that.data.user.email)){
      that.open1("邮箱不能为空")
      return
    }
    if(util.isNull(that.data.user.address)){
      that.open1("请选择地址")
      return
    }
    var ads = that.data.provinces[that.data.value[0]]+"·"+that.data.citys[that.data.value[1]]+"·"+that.data.areas[that.data.value[2]]
    var reg = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
		if(!reg.test(that.data.user.email)){
      that.open1("邮箱格式不正确")
      return
    }
    var param = {
      "fields":that.data.user.fields,
      "age":that.data.user.age,
      "userName":that.data.user.name,
      "email":that.data.user.email,
      "password":that.data.user.password,
      "nowAddress":that.data.user.address,
      "phone":that.data.user.phone,
      "name":ads,
      "gender":that.data.user.sex,
      "wxHeadPhoto":that.data.user.wxHeadPhoto
    };
    console.log(param)
    util.request("loginAction.update",param).then(function(res){
      console.log(res)
      if(res.code==500){
        that.setData({
          dialogShow:true,
          error:"输入信息只支持文字，请勿输入其他信息"
        })
      }
     var ur = wx.getStorageSync('backPage');
      console.log(ur)
      if(res.code=="00000"){
        wx.setStorageSync('has_edit', 1)
       wx.navigateBack({
         complete: (res) => {},
       })
      }else{
        that.setData({
          dialogShow:true,
          error:res.msg
        })
      }
    })
  },
  nameClick:function(e){
    var that = this;
    that.setData({
      userName:e.detail.value,
      'user.name':e.detail.value
    })
  },
  ageClick:function(e){
    var that = this;
    that.setData({
      age:e.detail.value,
      'userInfo.age':e.detail.value
    })
  },
  emailClick:function(e){
    var that = this;
    that.setData({
      emaiil:e.detail.value,
      'user.email':e.detail.value
    })
  },
  passClick:function(e){
    var that = this;
    that.setData({
      password:e.detail.value,
      'userInfo.password':e.detail.value
    })
  },
  repassClick:function(e){
    var that = this;
    that.setData({
      repassword:e.detail.value
    })
  },


  getUserInfo:function(){
    var that = this;
    util.request("userAction.getUserInfo").then(function(res){
        console.log(res)
        // name:"",
        // sex:0,
        // phone:"",
        // field:"",
        // email:"",
        // address:"",
        //  that.setData({
        //   userInfo:res.body.userInfo,
        //   sex:res.body.userInfo.gender,
        //   displayValue2:res.body.userInfo.field,
        //   value_2:res.body.userInfo.phone,
        //   value_3:res.body.userInfo.email,
        //   parameter: that.data.parameter,
        //  })
        that.setData({
          'user.name':res.body.userInfo.name,
          'user.sex':res.body.userInfo.gender,
          'user.phone':res.body.userInfo.phone,
          'user.fields':res.body.userInfo.field,
          'user.email':res.body.userInfo.email,
          "user.wxHeadPhoto":res.body.userInfo.wxHeadPhoto,
          "user.address":res.body.userInfo.nowAddress
        })
    })
  },
  handlerGobackClick:function(){
    wx.navigateBack({
      complete: (res) => {},
    })
  }
});