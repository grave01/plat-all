import {
  $wuxDialog
} from '../../dist/index'
var util = require('../../utils/util.js');
import tool from "../../utils/tool.js";
// pages/video/video.js
var videoContext;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    btnVisable:false,
    url: "",
    name: "视频名称...",
    freetime: "",
    isfree: "",
    realurl: "",
    adurl: "",
    video: "",
    videoList: [],
    home:false
  },
  pay: function (e) {
    var that = this
    var id = that.data.video.id
    console.log(id)
    that.setData({
      btnVisable:true
    })
    that.f(id)
  },
  f: tool.throttle(function (msg) {
    var that = this
    var m = msg[0]
    console.log(m)
    var param = {
      id: m
    }
    
    util.request("payAction.CreatePayOrder", param).then(function (res) {
      console.log(res)
      var pay = res.body.payInfo
      wx.requestPayment({
        'timeStamp': pay.timeStamp,
        'nonceStr': pay.nonceStr,
        'package': pay.packageValue,
        'signType': 'MD5',
        'appId': pay.appId,
        'paySign': pay.paySign,
        'success': function (res) {
          console.log('success')
          var v = that.data.video
          v.isFree = 1
          v.showFreeItem = 2
          console.log(v)
          that.setData({
            video:v,
            btnVisable:false
          })
        },
        'fail': function (res) {
          console.log('fail')
        },
        'complete': function (res) {
          console.log('complete')
        }
      })
    })

  }, 1000),
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    console.log("获取");
    console.log(options.id) //开始处理相关请求
    wx.getSystemInfoSync();
    //判断是否带广告
    that.getVideList(options.id)
     var v =  getCurrentPages()
     console.log("路由信息")
     console.log(v)
     this.setData({
       home:true
     })
  },
  onHome:function(e){
    console.log(e)
   wx.reLaunch({
     url: '/pages/index/index',
   })
  },
  getVideList: function (id) {
    var that = this
    var param = {
      "id": id
    }
    util.request("platVideoAction.getVideo", param).then(function (res) {
      console.log(res)
      var video = res.body.object
      // that.videoContext.src = video.url
      that.setData({
        url: video.url,
        freetime: video.freeTime,
        realurl: video.url,
        adurl: video.adUrl,
        isfree: video.isFree,
        video: video,
        videoList: res.body.list
      })
    })
  },
  goToPlay: function (e) {
    console.log(e.currentTarget.dataset.id)
    wx.navigateTo({
      url: '/pages/video/video?id=' + e.currentTarget.dataset.id,
    })
  },
  onReady: function () {
    this.videoContext = wx.createVideoContext("video1212");
  },

  // 视频更新250ms
  bindtimeupdate: function (e) {
    var that = this;
    console.log(e.detail.currentTime)
    var currentTime = e.detail.currentTime;
    var free = that.data.video.isFree;
    var viptime = e.currentTarget.dataset.viptime;
    console.log(free)
    if(free==0){
      if (currentTime > viptime) {
        wx.createVideoContext('video1212').pause()
        that.custom()
      }
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  handlerGohomeClick() {
    wx.navigateBack()
  },
  //对话框
  custom() {
    var that = this
    const alert = (content) => {
      $wuxDialog('#wux-dialog--alert').alert({
        resetOnClose: true,
        title: '提示',
        content: content,
      })
    }
    $wuxDialog().open({
      resetOnClose: true,
      title: that.data.video.videoName,
      content: '支付金额：'+that.data.video.price+'元',
      buttons: [{
          text: '微信支付',
          type: 'primary',
          onTap(e) {
            that.pay()
          },
        },
        {
          text: '取消',
        },
      ],
    })
  },
})