//index.js
//获取应用实例
const app = getApp()
const Pager = require('../common/extpager')
const lib = Pager.lib
var util = require('../../utils/util.js')
var page = 1;
var pageSize = 10;
Pager({
  data: {
    targetConfig: {
      $$id: 'menus-appstore-view',
      listClass: 'demo-list',
      itemClass: 'card demo-list-item',
      // 将listView，转换成scroll-view
      type: {
        'is': 'scroll',
        'scroll-y': true,
        'enable-flex': true,
      },
      data: [],
      methods: {
        onTap(e, param, inst){
          let id = param.id
          console.log("asasas")
         console.log(id)
        }
      }
    },
    hiddenLoadMore:true,
    title:"活动中心"
  },
  onLoad(param) {
    var that = this
    let pageTitle = param.pageTitle
    if (pageTitle) {
      wx.setNavigationBarTitle({
        title: pageTitle
      })
    }
    that.getActivityList()
  },
  getActivityList:function(){
    var that = this;
    var param = {
      page:page,
      pageSize:pageSize
    }
     util.request("platWechatActivityAction.getActivityList",param).then(function(res){
      console.log(res)
      that.setData({
       'targetConfig.data': [
        {
          "@item": {
            id: "nav-1",
            img: {src: "http://www.agzgz.com/imgs/itemlist.jpg", itemStyle: 'width: 100%;', preview: false},
           
            itemClass: 'appstore-item'
          },
          tap: 'onTap?id=2'
        },
      {
       "@item": {
            id: "nav-3",
            title: ['星空故事汇', `仰望星空，那是我们向往的地方`, '科幻故事，探索太空，发现未知星系'],
            img: {src: "http://www.agzgz.com/imgs/glasy.jpg", itemStyle: 'width: 100%;', preview: false},
            itemClass: 'appstore-item cover' 
          },
       tap: 'onTap?id=3'
      },
       {
          "@item": {
            id: "nav-4'",
            title:['探索', `想和你一直这样走下去`, '即将出发的旅程'],
            img: {src: "http://www.agzgz.com/imgs/appstorecover-1.jpg", itemStyle: 'width: 100%;', preview: false},
            itemClass: 'appstore-item cover'
          },
        tap: 'onTap?id=4'
       },
      {
          "@item": {
            id: "nav-5",
            title: ['战神系列故事', `战栗的奥林匹斯众神们`],
            img: {src: 'http://www.agzgz.com/imgs/kts1.jpg', itemStyle: 'width: 100%;', preview: false},
            itemClass: 'appstore-item cover' 
          },
        tap: 'onTap?id=5'
        },
       {
          "@item": {
            id: "nav-6",
            title: ['生活解决方法', '随手翻译手中拿'],
            img: {src: 'http://www.agzgz.com/imgs/itemlist.jpg', itemStyle: 'width: 100%;', preview: false},
            itemClass: 'appstore-item reverse'
          },
        tap: 'onTap?id=6'
        },
       {
          "@item": {
            id: 'nav-8',
            title:  ['小米', `握不住的晶莹剔透 \n小米MIX`, '四面环绕屏，现正推出ALPHA版'],
            img: {src: 'http://www.agzgz.com/imgs/mob.jpg', itemStyle: 'width: 100%;', preview: false},
            itemClass: 'appstore-item cover'
          },
         tap: 'onTap?id=8'
        }
          ]
      })
    })
  },
  onReachBottom: function () {
    var that = this;
    that.setData({
      hiddenLoadMore: false
    })
  },
  handlerGobackClick:function(){
    wx.navigateBack({
      complete: (res) => {},
    })
  }
})
