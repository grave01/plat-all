const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}
/**
 * 用于判断空，Undefined String Array Object Number boolean
 */
function isNull(str) {
  if (Object.prototype.toString.call(str) === '[object Undefined]') { //空
    return true
  } else if (
    Object.prototype.toString.call(str) === '[object String]' ||
    Object.prototype.toString.call(str) === '[object Array]') { //字条串或数组
    return str.length == 0 ? true : false
  } else if (Object.prototype.toString.call(str) === '[object Object]') {
    return JSON.stringify(str) == '{}' ? true : false
  } else if (typeof (str) == 'number') { //Number 型
    if (str) {
      return false
    } else { //数字0 不算空
      if (str == 0) {
        return false
      }
      return true
    }
  } else if (typeof (str) == 'boolean') {
    if (!param) {
      return true;
    } else {
      return false;
    }
  } else {
    return true
  }
}
/**
 * 封封微信的的request
 */

function request(cmd, param = {}, method = "POST", header = "application/json") {
  //var url = "http://f3c4odbmpu.54http.tech/open/api";
  var url = "https://loan.bangnila.com/open/api";
  var data = {
    "head": {
      "cmd": cmd,
      "token": wx.getStorageSync('plat_token'),
      "appid": "appid",
      "service": "user-center",
      "version": "1.0"
    },
    "body": param
  }
  //   wx.showLoading({
  //       title: '加载中...',
  //   });
  return new Promise(function (resolve, reject) {
    wx.request({
      url: url,
      data: data,
      method: method,
      header: {
        'Content-Type': header
      },
      success: function (res) {
        //   wx.hideLoading();
        if (res.statusCode == 200) {
          if (res.data.code == 40001) {
            wx.reLaunch({
              url: '/pages/auth/auth',
            })
          } else {
            resolve(res.data);
          }
        } else {
          reject(res.errMsg);
        }

      },
      fail: function (err) {
        reject(err)
      }
    })
  });
}

function upload(filePath,fileName, method = "POST", header = "multipart/form-data") {
  var url = "http://loan.bangnila.com/open/api";
   return new Promise(function(resolve, reject){
    wx.uploadFile({
      filePath: filePath,
      name: fileName,
      url: url,
      success:function(res){
        resolve(res)
      },
      fail:function(res){
        reject(res)
      }
    })
   })
}
//util.js
function imageUtil(e) {
  var imageSize = {};
  var originalWidth = e.detail.width;//图片原始宽
  var originalHeight = e.detail.height;//图片原始高
  var originalScale = originalHeight/originalWidth;//图片高宽比
  console.log('originalWidth: ' + originalWidth)
  console.log('originalHeight: ' + originalHeight)
  //获取屏幕宽高
  wx.getSystemInfo({
    success: function (res) {
      var windowWidth = res.windowWidth;
      var windowHeight = res.windowHeight;
      var windowscale = windowHeight/windowWidth;//屏幕高宽比
      console.log('windowWidth: ' + windowWidth)
      console.log('windowHeight: ' + windowHeight)
      if(originalScale < windowscale){//图片高宽比小于屏幕高宽比
        //图片缩放后的宽为屏幕宽
         imageSize.imageWidth = windowWidth;
         imageSize.imageHeight = (windowWidth * originalHeight) / originalWidth;
      }else{//图片高宽比大于屏幕高宽比
        //图片缩放后的高为屏幕高
         imageSize.imageHeight = windowHeight;
         imageSize.imageWidth = (windowHeight * originalWidth) / originalHeight;
      }
     
    }
  })
  console.log('缩放后的宽: ' + imageSize.imageWidth)
  console.log('缩放后的高: ' + imageSize.imageHeight)
  return imageSize;
}

module.exports = {
  formatTime: formatTime,
  request: request,
  isNull: isNull,
  upload:upload,
  imageUtil: imageUtil
}