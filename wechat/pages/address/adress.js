var util = require('../../utils/util.js');
import {$wuxDialog} from '../../dist/index'
const alert = (content) => {
  $wuxDialog('#wux-dialog--alert').alert({
    resetOnClose: true,
    title: '提示',
    content: content,
  })
}
import { $wuxLoading } from '../../dist/index'
const loaing = (content) => {
  $wuxLoading().show({
    text: content,
})
}
const hide = () => {
  $wuxLoading().hide();
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    addressList:[],
    hideview:false
  },
  selectAddress:function(e){
    var that = this;
    var isDefault = false;
    for(var i=0;i<that.data.addressList.length;i++){
           if(that.data.addressList[i].isDefault==1){
            isDefault = true
           }
    }
    if(!isDefault){
     alert("请设置一个默认地址");
     return;
    }
    var id = e.currentTarget.dataset.id
    let pages = getCurrentPages()
    let prevPage = pages[ pages.length - 2 ];  
    prevPage.setData({
      addressId:id,
      btnVisable:false
    })
    wx.navigateBack({
      delta:1
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad:function(param) {
    var that =this
    console.log(param)
    console.log("param"+param)
    if(param!=undefined){
      if(param.dataObj==1){
        that.setData({
         hideview:true
        })
      }
    }
    that.getList()
  },
 getList:function(){
   var that =this;
   loaing("加载收货地址中")
   util.request("addressAction.getList",{}).then(function(res){
     hide()
     console.log(res)
     that.setData({
      addressList: res.body
     })
   })
 },
 default:function(e){
  var that =this;
  util.request("addressAction.setDefault",{
    id:e.currentTarget.dataset.id
  }).then(function(res){
     console.log(res)
     that.getList()
  })
 },
 del:function(e){
  var that =this;
  util.request("addressAction.del",{
    id:e.currentTarget.dataset.id
  }).then(function(res){
    console.log(res)
    that.getList()
  })
 },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList()
  },
  addAddress:function(){
    wx.navigateTo({ url: '/pages/address/add/index' });
  },
  /* 删除item */
  delAddress: function (e) {
    this.data.addressList.splice(e.target.id.substring(3), 1);
    // 更新data数据对象  
    if (this.data.addressList.length > 0) {
      this.setData({
        addressList: this.data.addressList
      })
      wx.setStorageSync('addressList', this.data.addressList);
    } else {
      this.setData({
        addressList: this.data.addressList
      })
      wx.setStorageSync('addressList', []);
    }
  },
  handlerGobackClick:function(){
    wx.navigateBack({
      complete: (res) => {},
    })
  }
})