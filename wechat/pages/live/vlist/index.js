var util = require('../../../utils/util.js');
import {
  $wuxDialog
} from '../../../dist/index'
const alert = (content) => {
  $wuxDialog('#wux-dialog--alert').alert({
    resetOnClose: true,
    title: '提示',
    content: content,
  })
}
import { $wuxLoading } from '../../../dist/index'
const loaing = (content) => {
  $wuxLoading().show({
    text: content,
})
}
const hide = () => {
  $wuxLoading().hide();
}
// pages/live/vlist/index.js
import {
  $startWuxRefresher,
  $stopWuxRefresher,
  $stopWuxLoader
} from '../../../dist/index'
var page = 1;
var pageSize = 10;
var onshow = false;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    showAlert:false,
    list: [],
    liveList:[],
    hiddenLoadMore: false,
    bottomAlertMsg: "志合传媒",
    user:"",
    isShow:true,
    canPublish:false
  },
  swiperChange(e) {
    const that = this;
    that.setData({
      swiperIndex: e.detail.current,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that =this

  },
  init() {
    console.log("初始化")
    var that = this
    that.getLiveList()
  },
  getLiveList() {
    var that = this
    var param = {
      page: page,
      pageSize: pageSize
    }
    util.request("platLiveAction.getLiveList", param).then(function (res) {
      console.log(res)
     var   li = JSON.parse(res.body.list);
     var vli = JSON.parse(res.body.liveList);
      if(li.length==0&&vli.length==0){
        that.setData({
          showAlert:true
        })
      }
      that.setData({
        list: li,
        user:res.body.object,
        liveList:vli,
        hiddenLoadMore:true
      })
     
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
    if(that.data.isShow){
      that.init();
    }
    that.getUser()
  },
  getUser:function(){
    var that = this
    util.request("userAction.getUserInfo").then(function(res){
      var canPublish
      if(res.body.userInfo.hasLive==1){
        canPublish = true
      }else{
        canPublish = false
      }
      that.setData({
        canPublish:canPublish
      })
  })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  // 刷新相关
  onPulling: function () {
    console.log('onPulling')
  },
  onRefresh() {
    var that = this
    //   setTimeout(() => {
    //     //加载数据
    //     console.log("刷新数据")

    // }, 3000)
    console.log("下拉刷新1")
    page = 1;
    that.init()
    console.log("下拉刷新2")
    $stopWuxRefresher()
  },
  onLoadmore() {
    console.log('onLoadmore')
    setTimeout(() => {}, 3000)
  },
  handlerGobackClick: function () {
    wx.navigateBack({
      complete: (res) => {},
    })
  },
  goLive:function(){
    wx.navigateToMiniProgram({
      appId: 'wxcbbd86b156ae4441',
    })
  },
  publish: function () {
    console.log("开通直播")
    var isEditUderInfo = wx.getStorageSync('has_edit');
    if (isEditUderInfo == 1) {
      wx.navigateTo({
        url: '/pages/live/publish/index',
      })
    } else {
      wx.navigateTo({
        url: '/pages/userInfo/index',
      })
    }
  },
  goWatch: function (e) {
    var that = this;
    that.setData({
      isShow:false
    })
    //已经订阅了就可以观看
    var item = e.currentTarget.dataset.item;
    console.log(item);
   if(item.isSub==1){
    console.log("去观看");
    console.log(e.currentTarget.dataset.item)
    wx.navigateTo({
      url: 'plugin-private://wx2b03c6e691cd7370/pages/live-player-plugin?room_id=' + e.currentTarget.dataset.item.roomId,
    })
   }else{
     //提示未订阅
    alert("该直播需要订阅")
   }
  },
  subLive:function(e){
    var that =this;
    var param= {
      id:e.currentTarget.dataset.item.id
    }
    that.setData({
      isShow:false
    })
    console.log(e.currentTarget.dataset.item);
    var item = e.currentTarget.dataset.item;
    loaing("正在订阅");
    util.request("platLiveAction.subLive",param).then(function(res){
      hide()
      console.log(res)
      var pay = res.body.pay
      if(util.isNull(pay)){
        that.onLoad();
        var li_1 = that.data.liveList;
        var li_2 = that.data.list;
        for(var i=0;i<li_1.length;i++){
            if(li_1[i].id ==item.id){
              li_1[i].isSub = 1;
              li_1[i].subNum =  li_1[i].subNum+1
            }
        }
        for(var i=0;i<li_2.length;i++){
          if(li_2[i].id ==item.id){
            li_2[i].isSub = 1
            li_2[i].subNum =  li_2[i].subNum+1
          }
        }
        that.setData({
          list:li_2,
          liveList:li_1
        })
      }else{
        wx.requestPayment({
          'timeStamp': pay.timeStamp,
          'nonceStr': pay.nonceStr,
          'package': pay.packageValue,
          'signType': 'MD5',
          'appId': pay.appId,
          'paySign': pay.paySign,
          'success': function (res) {
            console.log(res)
            that.onLoad()
            var li_1 = that.data.liveList;
            var li_2 = that.data.list;
            for(var i=0;i<li_1.length;i++){
                if(li_1[i].id ==item.id){
                  li_1[i].isSub = 1;
                  li_1[i].subNum =  li_1[i].subNum+1
                }
            }
            for(var i=0;i<li_2.length;i++){
              if(li_2[i].id ==item.id){
                li_2[i].isSub = 1;
                li_2[i].subNum =  li_2[i].subNum+1
              }
            }
            that.setData({
              list:li_2,
              liveList:li_1
            })
          },
          'fail': function (res) {
            console.log('fail')
          },
          'complete': function (res) {
            console.log('complete')
          }
         })
      }
    
    })
  },
  onReachBottom: function () {
    console.log("顶部加载更多")
    var that = this
    that.setData({
      hiddenLoadMore: false,
      bottomAlertMsg: "志合传媒"
    })
    page = page + 1;
    var param = {
      page: page,
      pageSize: pageSize
    }
    util.request("platLiveAction.getLiveList", param).then(function (res) {
      console.log(res)
      var li = JSON.parse(res.body.list);
      var arr = new Array();
      var len = that.data.list.length;
      if (li.length > 0) {
        for (var i = 0; i < li.length; i++) {
          arr.push(li[i])
        }
        for (var i = 0; i < len; i++) {
          arr.push(that.data.list[i])
        }
        that.setData({
          list: arr,
          hiddenLoadMore: true
        })
      } else {
        that.setData({
          bottomAlertMsg: "没有内容，请稍后再刷"
        })
        //page = page-1;
        setTimeout(() => {
          that.setData({
            hiddenLoadMore: true,
            user:res.body.object
          })
        }, 1000)
      }
    })
  }
})