//index.js
//获取应用实例
//const app = getApp();
var util = require('../../utils/util.js');
import { $startWuxRefresher, $stopWuxRefresher, $stopWuxLoader } from '../../dist/index'
var page = 1;
var pageSize = 10;
var onshow = false;
Page({
  data: {
    title: "艺圈",
    giveLoveIconColor: "red",
    comment: "red",
    isGetLove: false,
    hidden: true,
    hiddenComment: true,
    hiddenLoadMore: false,
    replyId: "", //回复id
    replyName: "", //回复id
    commentId: "", //评论id,
    content: "", //评论内容
    pics: [],
    x: "",
    y: "",
    screenHeight: "",
    worldList:[],
    isPreview:false,
    canPublish:false
  },
  onShow: function (e) {
    var that = this;
    if(onshow){
      onshow  =false;
      return;
    }else{
      that.init();
    }
    that.getUser()
  },
  onLoad: function (optins) {
    var that = this;
    onshow = true
    that.init();
  },
  getUser:function(){
    var that = this
    util.request("userAction.getUserInfo").then(function(res){
      console.log(res)
      var canPublish
      if(res.body.userInfo.hasMyWorld==1){
        canPublish = true
      }else{
        canPublish = false
      }
      that.setData({
        canPublish:canPublish
      })
  })
  },
  init: function () {
     var that = this;
     that.getList();
  },
   getList:function(){
     var that = this;
    console.log("加载")
    var param = {
      "page": page,
      "pageSize": pageSize
    }
    util.request("myGloableAction.getMyWorldList", param).then(function (res) {
      console.log(res)
      var list = res.body.list;
      for(var i=0;i<list.length;i++){
        list[i].list= JSON.parse(list[i].url)
      }
      console.log(list)
      that.setData({
        worldList:list
      })
    })
   },
  handlerGobackClick() {
    wx.showModal({
      title: '你点击了返回',
      content: '是否确认放回',
      success: e => {
        if (e.confirm) {
          const pages = getCurrentPages();
          if (pages.length >= 2) {
            wx.navigateBack({
              delta: 1
            });
          } else {
            wx.navigateTo({
              url: '/pages/index/index'
            });
          }
        }
      }
    });
  },

  giveLove: function (e) {
    console.log(e.currentTarget.dataset.id);
    var that = this;
    console.log(e.currentTarget.dataset.id);
    that.setData({
      isGetLove: true
    })
    console.log("点赞");
  },
  publish: function () {
   var isEditUderInfo = wx.getStorageSync('has_edit');
   if(isEditUderInfo==1){
    wx.navigateTo({
      url: '/pages/pubMyWorld/publish',
    })
   }else{
    wx.navigateTo({
      url: '/pages/userInfo/index',
    })
   }
  },
  viewComment: function (e) {
    var that = this;
    console.log(e);
    console.log("查看评论")
    that.setData({
      hidden: false,
      x: e.changedTouches[0].clientX,
      y: e.changedTouches[0].clientY
    })
  },
  closeMask: function () {
    var that = this;
    console.log("关闭评论列表")
    that.setData({
      hidden: true
    })
  },
  openCommentMask: function (e) {
    //将id和name往下传
    // var that = this;
    // console.log(e.currentTarget.dataset.replyidname)
    // console.log("打开评论")
    // that.setData({
    //   hiddenComment:false,
    //   replyId:e.currentTarget.dataset.replyid,//回复id
    //   replyName:e.currentTarget.dataset.replyname,//回复id
    //   commentId:e.currentTarget.dataset.commentid,//评论id
    // })
    onshow = true
    wx.navigateTo({
      url: '/pages/commentPage/index?id='+e.currentTarget.dataset.id,
    })
  },
  userCommentInput: function (e) {
    var that = this;
    that.setData({
      content: e.detail.value
    })
  },
  // init: function () {
  //   var that = this;
  //   that.setData({
  //     hiddenComment: true,
  //     replyId: "", //回复id
  //     replyName: "", //回复id
  //     commentId: "", //评论id
  //     content: "", //评论内容
  //     pics: [] //评论图片
  //   })
  // },
  closeCommentMask: function () {
    var that = this;
    console.log("关闭评论")
    that.init();
  },
  startComment: function () {
    var that = this;
    console.log("开始评论");
    console.log("内容的id" + that.data.commentId)
    console.log("回复的id" + that.data.replyId)
    console.log("图片urls" + that.data.pics) //以逗号隔开的url地址
    console.log("评论内容" + that.data.content)

    //拿到评论内容的id(必填)
    //拿到回复的id(有的空)
    //拿到回复的图片(有的空)
    //拿到评论内容(必填想)
    //以上交给后台处理完毕--更新对应commentid下面的list
  },
  //预览图片，放大预览
  preview: function (event) {
    var that = this;
    console.log(event.currentTarget.dataset)
    var list = event.currentTarget.dataset.list
    let currentUrl = event.currentTarget.dataset.image;
    onshow = true;
    wx.previewImage({
      current: currentUrl, // 当前显示图片的http链接
      urls: list // 需要预览的图片http链接列表
    })
  },
  //上传图片开始
  chooseImg: function (e) {
    var that = this,
      pics = this.data.pics;
    console.log(pics);
    if (pics.length < 3) {
      wx.chooseImage({
        count: 3, // 最多可以选择的图片张数，默认9
        sizeType: ['original', 'compressed'], // original 原图，compressed 压缩图，默认二者都有
        sourceType: ['album', 'camera'], // album 从相册选图，camera 使用相机，默认二者都有
        success: function (res) {
          // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
          var tempFilePaths = res.tempFilePaths;
          // wx.showToast({
          //   title: '正在上传...',
          //   icon: 'loading',
          //   mask: true,
          //   duration: 10000
          // });
          for (var i = 0; i < tempFilePaths.length; i++) {
            pics.push(tempFilePaths[i]);
          }
          console.log(pics);
          that.setData({
            pics: pics
          })
        },
      });
    } else {
      wx.showToast({
        title: '最多上传3张图片',
        icon: 'none',
        duration: 3000
      });

    }
  },
  // 删除图片
  deleteImg: function (e) {
    var that = this;
    var pics = this.data.pics;
    var index = e.currentTarget.dataset.index;
    pics.splice(index, 1);
    console.log(pics)
    this.setData({
      pics: pics,
    })
  },
  // 刷新相关
  onPulling: function () {
    console.log('onPulling')
  },
  onRefresh() {
    var that = this
  //   setTimeout(() => {
  //     //加载数据
  //     console.log("刷新数据")
    
  // }, 3000)
  console.log("下拉刷新1")
  page = 1;
  that.init()
  console.log("下拉刷新2")
  $stopWuxRefresher()
  },
  onLoadmore() {
    console.log('onLoadmore')
    setTimeout(() => {}, 3000)
  },
  onReachBottom: function () {
    var that = this;
    console.log("底部刷新")
    page = page+1;
    var param = {
      "page": page,
      "pageSize": pageSize
    }
    that.setData({
      hiddenLoadMore: false
    })
    util.request("myGloableAction.getMyWorldList", param).then(function (res) {
      console.log(res)
      var list = res.body.list;
      for(var i=0;i<list.length;i++){
        list[i].list= JSON.parse(list[i].url)
        that.data.worldList.push(list[i])
      }
      console.log(list)
      if(list.length>0){
        that.setData({
          worldList:that.data.worldList,
          hiddenLoadMore: true
        })
      }else{
        page = page-1;
        setTimeout(() => {
          that.setData({
            hiddenLoadMore: true
          })
        }, 1000)
      }
      
    })
  },
  // 刷新相关
  handlerGohomeClick() {
    wx.navigateTo({
      url: '/pages/index/index'
    });
  },
  toMycenter:function(e){
    onshow =true
    wx.navigateTo({
      url: '/pages/peoplecenter/index?id='+e.currentTarget.dataset.id,
    })
  }
});