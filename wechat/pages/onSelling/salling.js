//index.js
//获取应用实例
//const app = getApp();
var util = require('../../utils/util.js');
import { $wuxCountDown } from '../../dist/index'
Page({
  data: {
    btnVisable:false,
    visible1:false,
    loading: false,
    color: '#000',
    background: '#fff',
    show: true,
    animated: false,
    auction:"",
    surplusTime:"",
    payList:[],
    price:0,
    addNum:1,
    addressId:""
  },
  onClose(key) {
    console.log('onClose')
    this.setData({
        [key]: false,
        addNum:1,
       btnVisable:false
    })
},
  onClose1() {
    this.onClose('visible1')
},
onClosed1() {
  console.log('onClosed')
  this.setData({
    addNum:1,
})
},
close1() {
  this.setData({
      visible1: false,
      addNum:1,
  })
},
onChange(e) {
  var that =this
  console.log(e)
  var num = e.detail.value;
  var range = that.data.auction.rangePrice
   that.setData({
   price:num*range,
   addNum:num
  })
},
  onLoad: function(options) {
    var  that =this
    console.log(options.id)//开始处理相关请求
    that.getAuction(options.id);
    that.getPayList(options.id)
  },
  getAuction:function(id){
    var  that =this
    var param ={
      "id":id
    };
    util.request("auctionAction.getAuctionInfo",param).then(function(res){
      console.log(res)
      that.setData({
        auction:res.body.object
      })
      var d = new Date(res.body.object.surplusTime)
      that.data.surplusTime = new $wuxCountDown({
        //date: 'June 7, 2087 15:03:25',
        date: d,
        render(date) {
            const years = this.leadingZeros(date.years, 4) + ' 年 '
            const days = this.leadingZeros(date.days, 3) + ' 天 '
            const hours = this.leadingZeros(date.hours, 2) + ' 时 '
            const min = this.leadingZeros(date.min, 2) + ' 分 '
            const sec = this.leadingZeros(date.sec, 2) + ' 秒 '
            this.setData({
              surplusTime: days + hours + min + sec,
            })
        },
    })
    })
  },
  search: function() {
   wx.switchTab({
     url: '/pages/demo1/index',
   })
  },goToViedoList:function(e){
   console.log("根据分类获取视频")
   wx.navigateTo({
    url: '/pages/videoList/list?id='+e.currentTarget.dataset.id,
  })
  },
   goToPlay:function(e){
     console.log(e.currentTarget.dataset.id)
     wx.navigateTo({
       url: '/pages/video/video?id='+e.currentTarget.dataset.id,
     })
  },
  goToPeople:function(e){
    console.log(e.currentTarget.dataset.id)
    console.log("根据人物获取视频")
    wx.navigateTo({
      url: '/pages/people/people?id='+e.currentTarget.dataset.id,
    })
  },
  //pages/peopleList/peopleList
  goToAllPeople:function(e){
    console.log(e.currentTarget.dataset.isAll)
    console.log("根据人物获取视频")
    wx.navigateTo({
      url: '/pages/peopleList/peopleList?isALL='+e.currentTarget.dataset.isAll,
    })
  },
  handlerGobackClick:function(){
    wx.navigateBack();
  },
   /**
   * 触底上拉加载
   */
  onReachBottom: function() {
    console.log("assas")
  },
 
  /**
   * 下拉加载
   */
  onPullDownRefresh: function() {
    console.log("assas")
  },
  tapBuy:function(){
    var that =this
    that.setData({
      visible1: true,
      price:that.data.auction.rangePrice,
      addNum:1
  })
  },
  pay:function(){
    var that =this
    var param = {
      id:that.data.auction.id,
      num:that.data.addNum,
      addressId:that.data.addressId
    }
    that.setData({
      btnVisable:true
    })
    util.request("payAction.createRealOrder",param).then(function(res){
        console.log(res)
       if(res.code=="10003"){
        // wx.chooseAddress({
        //   success: function (res) {
        //     console.log(JSON.stringify(res))
        //     var param ={
        //         json:JSON.stringify(res)
        //     }
        //     util.request("platAddressAction.addOrUpadte",param).then(function(res){
        //       console.log(res)
        //       if(res.code=="00000"){
        //         that.setData({
        //           btnVisable: false,
        //           addressId:res.body.object.id
        //         })
        //       }
        //     })
        //   },
        //   fail: function(err){
        //     console.log(JSON.stringify(err))
        //   }
        // })
        wx.navigateTo({
          url: '/pages/address/adress?dataObj='+1, 
        })
        return;
       }
        var pay = res.body.payInfo
        wx.requestPayment({
          'timeStamp': pay.timeStamp,
          'nonceStr': pay.nonceStr,
          'package': pay.packageValue,
          'signType': 'MD5',
          'appId': pay.appId,
          'paySign': pay.paySign,
          'success': function (res) {
            that.setData({
              visible1:false,
              btnVisable:false
            })
            that.getAuction(that.data.auction.id);
            that.getPayList(that.data.auction.id)
          },
          'fail': function (res) {
            that.setData({
              btnVisable:false
            })
            console.log('fail')
          },
          'complete': function (res) {
            console.log('complete')
          }
        })
    })
  },
  getPayList:function(id){
   var that = this
   var param = {
     id:id
   }
   util.request("auctionAction.getAuctionPayList",param).then(function(res){
     console.log(res)
     that.setData({
       payList:res.body.list
     })
   })

  }
});
