// pages/video/video.js
var util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    url: "https://loan.bangnila.com/File/Video/2020-05-13/2e0c97ecf09a4f249cce38b4f4c507c9.mp4",
    name: "视频名称...",
    peopleId: "",
    people: "",
    videoList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    console.log("获取");
    console.log(options.id) //开始处理相关请求
    that.setData({
      peopleId: options.id
    })
    wx.getSystemInfoSync();
    that.init();
  },
  init() {
    var that = this;
    that.getPeople()
  },
  getPeople() {
    var that = this;
    var param = {
      "id": that.data.peopleId,
      "page":1,
      "is_recommend":"1"
    }
    util.request("platPeopleAction.getPeople", param).then(function (res) {
      console.log(res)
      that.setData({
        people: res.body.object,
        videoList: res.body.list
      })
    })
  },
  goToPlay: function (e) {
    console.log(e.currentTarget.dataset.id)
    wx.navigateTo({
      url: '/pages/video/video?id=' + e.currentTarget.dataset.id,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  handlerGohomeClick() {
    wx.navigateBack()
  }
})