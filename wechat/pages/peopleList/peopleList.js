// pages/video/video.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
     url:"https://loan.bangnila.com/File/Video/2020-05-13/2e0c97ecf09a4f249cce38b4f4c507c9.mp4",
    name:"视频名称...",
     videoList:[
      {
          "poster": "http://t7.baidu.com/it/u=3616242789,1098670747&fm=79&app=86&f=JPEG?w=900&h=1350",
          "title":"素描作家",
          "date":"2020年5月16",
           "author":"三爷"
      },
       {
          "poster": "http://t7.baidu.com/it/u=3616242789,1098670747&fm=79&app=86&f=JPEG?w=900&h=1350",
          "title":"素描作家",
          "date":"2020年5月16",
           "author":"三爷"
      }, {
          "poster": "http://t7.baidu.com/it/u=3616242789,1098670747&fm=79&app=86&f=JPEG?w=900&h=1350",
          "title":"素描作家",
          "date":"2020年5月16",
           "author":"三爷"
      }, {
          "poster": "http://t7.baidu.com/it/u=3616242789,1098670747&fm=79&app=86&f=JPEG?w=900&h=1350",
          "title":"素描作家",
          "date":"2020年5月16",
           "author":"三爷"
      }, {
          "poster": "http://t7.baidu.com/it/u=3616242789,1098670747&fm=79&app=86&f=JPEG?w=900&h=1350",
          "title":"素描作家",
          "date":"2020年5月16",
           "author":"三爷"
      }, {
          "poster": "http://t7.baidu.com/it/u=3616242789,1098670747&fm=79&app=86&f=JPEG?w=900&h=1350",
          "title":"素描作家",
          "date":"2020年5月16",
           "author":"三爷"
      }
  ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("获取");
    console.log(options.id)//开始处理相关请求
    wx.getSystemInfoSync();
  },
  goToPeople:function(e){
    console.log(e.currentTarget.dataset.id)
    console.log("根据人物获取视频")
    wx.navigateTo({
      url: '/pages/people/people?id='+e.currentTarget.dataset.id,
    })
  },  
   
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  handlerGohomeClick() {
   wx.navigateBack()
  }
})