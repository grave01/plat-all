var amapFile = require('../../lib/amap-wx.js');
var markersData = {
  latitude: '',//纬度
  longitude: '',//经度
  key: "1c6c7dba50dfdba107644ceaa09a16ae"//申请的高德地图key
};
//index.js
//获取应用实例
//const app = getApp();
var util = require('../../utils/util.js');
import tool from "../../utils/tool.js";
import {
  $startWuxRefresher,
  $stopWuxRefresher,
  $stopWuxLoader
} from '../../dist/index'
import {
  $wuxDialog
} from '../../dist/index'
var page = 1;
var pageSize = 10;
var onshow = false;
const alert = (content) => {
  $wuxDialog('#wux-dialog--alert').alert({
    resetOnClose: true,
    title: '提示',
    content: content,
  })
}
Page({
  data: {
    title: "发布信息",
    pics: [],
    content: "",
    fileList: [],
    swatch:false,
    btnVisable:false,
    gpshidden:true,
    address:"",
    latitude:0,
    longitude:0
  },
  swatch:function(e){
    var that = this
    var flag;
      if(that.data.swatch){
        flag = false;
        that.setData({
          address:""
        })
      }else{
        that.setData({
          gpshidden:false
        })
        flag = true;
        wx.getLocation({
          type: 'wgs84',
          success: function (res) {
            // console.log(res);
            var latitude = res.latitude  
            var longitude = res.longitude
            that.loadCity(latitude,longitude)
            console.log(res)
            that.setData({
              latitude:latitude,
              longitude:longitude
            })
          
          }
        })
      }
      that.setData({
        swatch:flag
      })
  },
  loadCity: function (latitude, longitude){
    var that=this;
    var myAmapFun = new amapFile.AMapWX({ key: markersData.key });
    myAmapFun.getRegeo({
      location: '' + longitude + ',' + latitude + '',//location的格式为'经度,纬度'
      success: function (data) {
        console.log(data);
       
      },
      fail: function (info) { }
    });
    myAmapFun.getWeather({
      success: function (data) {
        console.log(data);
        //成功回调
        that.setData({
          address:data.liveData.province+"·"+data.liveData.city,
          gpshidden:true
        })
      },
      fail: function (info) {
        //失败回调
        console.log(info)
      }
    })
  },
  onLoad: function (optins) {

  },
  handlerGobackClick() {
    wx.navigateBack();
  },
  textareaInput: function (e) {
    this.setData({
      content: e.detail.value
    })
  },

  startComment: tool.debounce(function () {
    console.log("开始评论")
    var that = this;
    var list = that.data.fileList;
    var arr = new Array();
    console.log(that.data.fileList)
    for (var i = 0; i < list.length; i++) {
      arr.push(JSON.parse(list[i].res.data).body.url[0])
    }
    console.log(arr)
    var param = {
      "content": that.data.content,
      "url": arr,
      latitude:that.data.latitude,
      longitude:that.data.longitude,
      address:that.data.address
    }
    if (arr.length == 0&&that.data.content.length == 0) {
      alert("请上传图片或者发布文字");
      return;
    }
    that.setData({
      btnVisable:true
    })
    util.request("myGloableAction.publish", param).then(function (res) {
      console.log(res)
      that.setData({
        btnVisable:false
      })
      if (res.code == "00000") {
        wx.navigateBack();
      }
    })
  }, 1000),
  handlerGohomeClick() {
    wx.navigateTo({
      url: '/pages/index/index'
    });
  },
  onChange(e) {
    console.log('onChange', e)
    const {
      file,
      fileList
    } = e.detail
    if (file.status === 'uploading') {
      this.setData({
        progress: 0,
      })
      wx.showLoading()
    } else if (file.status === 'done') {
      this.setData({
        imageUrl: file.url,
      })
    }

    // Controlled state should set fileList
    this.setData({
      fileList
    })
  },
  onSuccess(e) {
    console.log('onSuccess', e)
  },
  onFail(e) {
    console.log('onFail', e)
  },
  onComplete(e) {
    console.log('onComplete', e)
    wx.hideLoading()
  },
  onProgress(e) {
    console.log('onProgress', e)
    this.setData({
      progress: e.detail.file.progress,
    })
  },
  onPreview(e) {
    console.log('onPreview', e)
    const {
      file,
      fileList
    } = e.detail
    wx.previewImage({
      current: file.url,
      urls: fileList.map((n) => n.url),
    })
  },
  onRemove(e) {
    const {
      file,
      fileList
    } = e.detail
    wx.showModal({
      content: '确定删除？',
      success: (res) => {
        if (res.confirm) {
          this.setData({
            fileList: fileList.filter((n) => n.uid !== file.uid),
          })
        }
      },
    })
  },
});