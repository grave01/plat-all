function onChange(e) {
  console.log('onChange', e)
  const { file, fileList } = e.detail
  if (file.status === 'uploading') {
      this.setData({
          progress: 0,
      })
      wx.showLoading()
  } else if (file.status === 'done') {
      this.setData({
          imageUrl: file.url,
      })
  }

  // Controlled state should set fileList
  this.setData({ fileList })
}
function onSuccess(e) {
  console.log('onSuccess', e)
}
function onFail(e) {
  console.log('onFail', e)
}
function onComplete(e) {
  console.log('onComplete', e)
  wx.hideLoading()
}
function onProgress(e) {
  console.log('onProgress', e)
  this.setData({
      progress: e.detail.file.progress,
  })
}
function onPreview(e) {
  console.log('onPreview', e)
  const { file, fileList } = e.detail
  wx.previewImage({
      current: file.url,
      urls: fileList.map((n) => n.url),
  })
}
function onRemove(e) {
  const { file, fileList } = e.detail
  wx.showModal({
      content: '确定删除？',
      success: (res) => {
          if (res.confirm) {
              this.setData({
                  fileList: fileList.filter((n) => n.uid !== file.uid),
              })
          }
      },
  })
}
module.exports ={
  onChange:onChange,
  onSuccess:onSuccess,
  onFail:onFail,
  onComplete:onComplete,
  onProgress:onProgress,
  onPreview:onPreview,
   onRemove:onRemove
}