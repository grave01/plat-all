// pages/buyedVideo/video.js
var util = require('../../utils/util.js');
import {
  $wuxDialog
} from '../../dist/index'
const alert = (content) => {
  $wuxDialog('#wux-dialog--alert').alert({
    resetOnClose: true,
    title: '提示',
    content: content,
  })
}
import { $wuxLoading } from '../../dist/index'
const loaing = (content) => {
  $wuxLoading().show({
    text: content,
})
}
const hide = () => {
  $wuxLoading().hide();
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
   value:false,
   hideview:false,
   value1: '',
   value2: '',
   displayValue1: '请选择',
   displayValue2: '请选择',
   options1: ['教师', '个人', '企业', '机构'],
   options2: [{
       title: '素描',
       value: '素描',
   }, {
       title: '写真',
       value: '写真',
   }, {
       title: '色彩',
       value: '色彩',
   }, {
    title: '少儿',
    value: '少儿',
}, {
  title: '设计',
  value: '设计',
}, {
  title: '速写',
  value: '速写',
}, {
  title: '艺术',
  value: '艺术',
}],
   front:"/images/front.png",
   back:"/images/back.png",
   lisence:"/images/license.png",
   user:{},
   step:0,
  },
  setValue(values, key) {
    this.setData({
        [`value${key}`]: values.value,
        [`displayValue${key}`]: values.label,
    })
},
onConfirm(e) {
    const { index } = e.currentTarget.dataset
    this.setValue(e.detail, index)
    console.log(`onConfirm${index}`, e.detail)
},
onValueChange(e) {
    const { index } = e.currentTarget.dataset
    console.log(`onValueChange${index}`, e.detail)
},
onVisibleChange(e) {
    this.setData({ visible: e.detail.visible })
},
onClick() {
    this.setData({ visible: true })
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   var that =this
   loaing("加载中")
   that.getUserInfo();
  },
  getUserInfo:function(){
   var that =this;
    util.request("userAction.userInfo",{}).then(function(res){
      hide()
      console.log(res)
      var step = 0;
     if(res.body.object.authStatus==1){
      step = 2
     }
     if(res.body.object.authStatus==3){
      step = 1
     }
     that.setData({
      user:res.body.object,
      step:step,
      hideview:true
     })
    })
  },
  submit:function(){
    var that =this;
    console.log(that.data.value2)//认证领域
    console.log(that.data.value1)//认证机构
    console.log(that.data.front)//身份证正面
    console.log(that.data.back)//身份证反面
    console.log(that.data.lisence)//营业执照
    var that = this
    if(util.isNull(that.data.value1)){
      console.log("认证类型不能为空")
      alert("强选择认证类型");
      return;
    }
    if(util.isNull(that.data.value2)){
      console.log("认证领域不能为空")
      alert("请选择认证领域");
      return;
    }
    if(that.data.value2.length>3){
      alert("最多只能选择三个领域");
      return;
    }
    if("/images/front.png"==that.data.front){
      console.log("身份证正面不能为空")
      alert("请上传身份证正面");
      return;
    }
    if("/images/back.png"==that.data.back){
      console.log("身份证反面不能为空");
      alert("请上传身份证反面");
      return;
    }
    if(that.data.value1=='企业'||that.data.value1=='机构'){
      if("/images/license.png"==that.data.lisence){
        console.log("营业执照不能为空")
        alert("请上传营业执照");
        return;
      }
    }
    var param ={
      front:that.data.front,
      back:that.data.back,
      lisence:that.data.lisence,
      type:that.data.value1,
      field:that.data.value2
    }
    util.request("userAction.apply",param).then(function(res){
      console.log(res)
      if(res.code=='00000'){
        that.onLoad();
        that.setData({
          authStatus:res.body.object.authStatus
        })
      }
    });
    that.setData({
      hideview:true
    })
  },
  handlerGobackClick:function(){
    wx.navigateBack();
  },
 
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  chooseFrontImage: function () {
    var that = this
    wx.chooseImage({
      complete: (res) => {},
      count: 1,
      fail: (res) => {},
      sizeType: ['original', 'compressed'],
      sourceType: ['camera'],
      success: (result) => {
        loaing("上传中")
         wx.uploadFile({
           filePath: result.tempFilePaths[0],
           name: 'name',
           url: "https://loan.bangnila.com/open/api",
           success:function(res){
             console.log(res)
             that.setData({
              front:JSON.parse(res.data).body.url
            })
            hide()
           }
         })
      },
    })
  },
  chooseBackImage: function () {
    var that = this
    wx.chooseImage({
      complete: (res) => {},
      count: 1,
      fail: (res) => {},
      sizeType: ['original', 'compressed'],
      sourceType: ['camera'],
      success: (result) => {
        loaing("上传中")
       
        wx.uploadFile({
          filePath: result.tempFilePaths[0],
          name: 'name',
          url: "https://loan.bangnila.com/open/api",
          success:function(res){
            console.log(res)
            that.setData({
              back:JSON.parse(res.data).body.url
           })
           hide()
          }
        })
      },
    })
  },
  chooseLicenseImage: function () {
    var that = this
    wx.chooseImage({
      complete: (res) => {},
      count: 1,
      fail: (res) => {},
      sizeType: ['original', 'compressed'],
      sourceType: ['camera'],
      success: (result) => {
        loaing("上传中")
        wx.uploadFile({
          filePath: result.tempFilePaths[0],
          name: 'name',
          url: "http://loan.bangnila.com/open/api",
          success:function(res){
            console.log(res)
            that.setData({
              lisence:JSON.parse(res.data).body.url
           })
           hide()
          }
        })
      },
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})