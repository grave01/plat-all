// pages/setting/setting.js
var util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    about:'',
    license:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    util.request("userAction.getUserInfo").then(function(res){
      console.log(res)
      // wx.setStorageSync("has_edit",res.body.userInfo.hasEdit)
      // wx.setStorageSync("has_live",res.body.userInfo.hasLive)
      // wx.setStorageSync("has_ysp",res.body.userInfo.hasYsp)
      // wx.setStorageSync("has_my_world",res.body.userInfo.hasMyWorld)
      // wx.setStorageSync("about",res.body.xcx.about)
      // wx.setStorageSync("license",res.body.xcx.license)
      that.setData({
        about:res.body.xcx.about,
        license:res.body.xcx.license
      })
  })
  },
  handlerGobackClick:function(){
    wx.navigateBack();
  },
  about:function(){
    console.log(this.data.about);
     var about = encodeURIComponent(this.data.about)
     console.log("编码"+about);
    wx.navigateTo({
      url: '/plat/webview/webview?url='+about
    })
  },
  license:function(){
    console.log(this.data.license)
    
    var license = encodeURIComponent(this.data.license)
    console.log("编码"+license);
    wx.navigateTo({
      url: '/plat/webview/webview?url='+license
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})