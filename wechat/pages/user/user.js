//index.js
//获取应用实例
//const app = getApp();
var util = require('../../utils/util.js');

Page({
  data: {
    title:'个人中心',
    pics:[],
    loading: false,
    color: '#000',
    background: '#fff',
    show: true,
    animated: false,
    date:"",
    time:"",
    visible:false,
    fgColor: 'black',
    userInfo:"",
    countInfo:"",
    menu:[
      {
        "image":"https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1951006571,2765556974&fm=26&gp=0.jpg",
        "text":"我的钱包"
      },  {
        "image":"https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1951006571,2765556974&fm=26&gp=0.jpg",
        "text":"高级认证"
      },  {
        "image":"https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1951006571,2765556974&fm=26&gp=0.jpg",
        "text":"收货地址"
      },  {
        "image":"https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1951006571,2765556974&fm=26&gp=0.jpg",
        "text":"在线客服"
      },  {
        "image":"https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1951006571,2765556974&fm=26&gp=0.jpg",
        "text":"我的消息"
      },  {
        "image":"https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1951006571,2765556974&fm=26&gp=0.jpg",
        "text":"设置"
      }
    ]
  },

  onLoad: function() {
    this.getUserInfo()
  },
  onShow:function(){
    this.getUserInfo()
  },
  search: function() {
   wx.switchTab({
     url: '/pages/demo1/index',
   })
  },goToViedoList:function(e){
   console.log("根据分类获取视频")
   wx.navigateTo({
    url: '/pages/videoList/list?id='+e.currentTarget.dataset.id,
  })
  },
   goToPlay:function(e){
     console.log(e.currentTarget.dataset.id)
     wx.navigateTo({
       url: '/pages/video/video?id='+e.currentTarget.dataset.id,
     })
  },
  goToPeople:function(e){
    console.log(e.currentTarget.dataset.id)
    console.log("根据人物获取视频")
    wx.navigateTo({
      url: '/pages/people/people?id='+e.currentTarget.dataset.id,
    })
  },
  //pages/peopleList/peopleList
  goToAllPeople:function(e){
    console.log(e.currentTarget.dataset.isAll)
    console.log("根据人物获取视频")
    wx.navigateTo({
      url: '/pages/peopleList/peopleList?isALL='+e.currentTarget.dataset.isAll,
    })
  },
//上传图片开始
chooseImg: function (e) {
  var that = this, pics = this.data.pics;
  console.log(pics);
  if (pics.length < 9) {
    wx.chooseImage({
      count: 9, // 最多可以选择的图片张数，默认9
      sizeType: ['original', 'compressed'], // original 原图，compressed 压缩图，默认二者都有
      sourceType: ['album', 'camera'], // album 从相册选图，camera 使用相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        var tempFilePaths = res.tempFilePaths;
        // wx.showToast({
        //   title: '正在上传...',
        //   icon: 'loading',
        //   mask: true,
        //   duration: 10000
        // });
        if(pics.length+tempFilePaths.length<=9){
          for (var i = 0; i < tempFilePaths.length;i++){
            pics.push(tempFilePaths[i]);
          }
          console.log(pics);
          that.setData({
            pics: pics
          })
        }else{
          wx.showToast({
            title: '最多上传9张图片',
            icon: 'none',
            duration: 3000
          });
        }
      },
    });
  } else {
    wx.showToast({
      title: '最多上传9张图片',
      icon: 'none',
      duration: 3000
    });

  }
},
// 删除图片
deleteImg: function (e) {
  var that = this;
  var pics = this.data.pics;
  var index = e.currentTarget.dataset.index;
  pics.splice(index, 1);
  console.log(pics)
  this.setData({
    pics: pics,
  })
},
  // 发布拍买的东西
  publish:function(){
   
  },
  bindDateChange:function(e){
    this.setData({
      date: e.detail.value
    })
  },
  bindTimeChange:function(e){
    this.setData({
      time: e.detail.value
    })
  },
  handlerGobackClick:function(){
    wx.navigateBack();
  },
  // 展示二维码
  showQr:function(){
  var that = this;
  that.setData({
    visible:true
  })
  },
    // 关闭二维码
  onClosed:function(){
    var that = this;
    that.setData({
      visible:false
    })
  },
  //授权页面
  goAuth:function(){
    wx.navigateTo({
      url: '/pages/auth/auth',
    })
  },
  //订单页面
  goOrderList:function(e){
    wx.navigateTo({
      url: '/pages/order/order?type='+e.currentTarget.dataset.type,
    })
  },
  //去我购买的视频
  goBuyVideo:function(){
    wx.navigateTo({
      url: '/pages/buyedVideo/video',
    })
  },
  //去编辑收货地址
  goAddress:function(){
    wx.navigateTo({
      url: "/pages/address/adress",
    })
  },
  //去认证vip
  goVip:function(){
    wx.navigateTo({
      url: '/pages/pvip/vip',
    })
  },
  goWallet:function(){
    wx.navigateTo({
      url: '/pages/wallet/wallet',
    })
  },
   /**
   * 触底上拉加载
   */
  onReachBottom: function() {
    console.log("assas")
  },
 
  /**
   * 下拉加载
   */
  onPullDownRefresh: function() {
    console.log("assas")
  },
  goEditUserInfo:function(){
    wx.navigateTo({
      url: '/pages/userInfo/index',
    })
  },
  address:function(){
    wx.chooseAddress({

    })
  },
  getUserInfo:function(){
    var that =this
    util.request("userAction.getUserInfo").then(function(res){
        console.log(res)
        that.setData({
          userInfo:res.body.userInfo,
          countInfo:res.body.countInfo.countInfo
        })
    })
  },
  goSetting:function(){
    wx.navigateTo({
      url: '/pages/setting/setting',
    })
  },
  goSMS:function(){
    wx.navigateTo({
      url: '/pages/sms/sms',
    })
  }
});
