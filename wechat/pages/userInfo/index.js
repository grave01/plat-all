/*
 ** 省市区 - 收货地址 js
 ** www.meizhuangdaka.com
 ** 省市区数据有误的话，自己改：/utils/citydata.js，开源源码
 */

//获取应用实例
var app = getApp();

// var cityData = require('../../utils/citydata.js');
var util = require('../../utils/util.js');
Page({
  data: {
    editSelfInfonotice:"为了更好体验小程序，请您填写真实完整信息！！！为了更好体验小程序，请您填写真实完整信息！！！为了更好体验小程序，请您填写真实完整信息！！！",
    visible1: false,
    visible2: false,
    alert:"",
    provinces: [],
    citys: [],
    areas: [],
    value: [0, 0, 0],
    name: '',
    fieldList: ['素描', '彩绘', '油画', '写真', '摄影'],
    genderList:["男","女","未知性别"],
    index: 0,
    index_1: 0,
    userName:null,
    age:0,
    emaiil:null,
    password:null,
    repassword:null,
    nowAddress:null,
    field:"",
    phone:null,
    value2: [],
    options2: [{
      title: '素描',
      value: '素描',
  },{
      title: '写真',
      value: '写真',
  },{
      title: '色彩',
      value: '色彩',
  },{
    title: '艺术',
    value: '艺术',
},{
  title: '设计',
  value: '设计',
}, {
  title: '速写',
  value: '速写',
}],
  displayValue2: '请选择',
  region: [],//定位获取位置
  array:[],
  },
  bindRegionChange: function (e) {
    console.log(e)
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      region: e.detail.value
    })
    // this.getDetailPlace(e.detail.code)
  },
  setValue(values, key) {
    this.setData({
        [`value${key}`]: values.value,
        [`displayValue${key}`]: values.label,
    })
},
  onConfirm(e) {
    const { index } = e.currentTarget.dataset
    this.setValue(e.detail, index)
    console.log(`onConfirm${index}`, e.detail)
},
onValueChange(e) {
    const { index } = e.currentTarget.dataset
    console.log(`onValueChange${index}`, e.detail)
},
  getPhoneNumber:function(e){
   var that = this
   wx.login({
        success:function(res){
          var param ={
            code:res.code,
            iv :e.detail.iv,
             encryptedData:e.detail.encryptedData
           }
           util.request("loginAction.getPhoneNumber",param).then(function(res){
             console.log(res)
             that.setData({
              phone:res.body.object.phoneNumber
             })
           })
        }
   })
 
  },
  onClose1() {
    this.onClose('visible1')
}, 
 onClosed1() {
  console.log('onClosed')
},   
 close1() {
  this.setData({
      visible1: false,
  })
}, 
onClose(key) {
  console.log('onClose')
  this.setData({
      [key]: false,
  })
},
open1(v) {
  this.setData({
      visible1: true,
      alert:v
  })
},
  submit:function(){
    var that= this
    console.log()
    if(util.isNull(that.data.userName)){
      that.open1("姓名不能为空")
      return
    }
    if(that.data.age==0){
      that.open1("年龄不能为空")
      return
    }
    if(util.isNull(that.data.phone)){
      that.open1("该平台需要绑定手机号")
      return
    }
    if(that.data.value2.length==0){
      that.open1("请选择领域")
      return
    }
    if(that.data.value2.length>3){
      that.open1("领域选择不能超过三个")
      return
    }
    if(util.isNull(that.data.emaiil)){
      that.open1("邮箱不能为空")
      return
    }
    if(that.data.region.length==0){
      that.open1("请选择地址")
      return
    }
    if(util.isNull(that.data.password)){
      that.open1("密码不能为空")
      return
    }
    var ads = that.data.provinces[that.data.value[0]]+"·"+that.data.citys[that.data.value[1]]+"·"+that.data.areas[that.data.value[2]]
    var reg = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
		if(!reg.test(that.data.emaiil)){
      that.open1("邮箱格式不正确")
      return
    }
    if(that.data.password!=that.data.repassword){
      that.open1("秘密不一致")
      return
    }
    if(that.data.password.length<8){
      that.open1("密码必须长度大于8位")
      return
    }
    var param = {
      "field":that.data.displayValue2,
      "age":that.data.age,
      "userName":that.data.userName,
      "email":that.data.emaiil,
      "password":that.data.password,
      "nowAddress":that.data.region[1]+that.data.region[2],
      "phone":that.data.phone,
      "name":ads,
      "gender":that.data.genderList[that.data.index_1]
    };
    util.request("loginAction.update",param).then(function(res){
      console.log(res)
     var ur = wx.getStorageSync('backPage');
      console.log(ur)
      if(res.code=="00000"){
        wx.setStorageSync('has_edit', 1)
       wx.navigateBack({
         complete: (res) => {},
       })
      }
    })
  },
  nameClick:function(e){
    var that = this;
    that.setData({
      userName:e.detail.value
    })
  },
  ageClick:function(e){
    var that = this;
    that.setData({
      age:e.detail.value
    })
  },
  emailClick:function(e){
    var that = this;
    that.setData({
      emaiil:e.detail.value
    })
  },
  passClick:function(e){
    var that = this;
    that.setData({
      password:e.detail.value
    })
  },
  repassClick:function(e){
    var that = this;
    that.setData({
      repassword:e.detail.value
    })
  },

  // 页面初始化事件
  onLoad: function () {
   this.getUserInfo();
  },
  getUserInfo:function(){
    util.request("userAction.getUserInfo").then(function(res){
        console.log(res)
    })
  },
  handlerGobackClick:function(){
    wx.navigateBack({
      complete: (res) => {},
    })
  }
});