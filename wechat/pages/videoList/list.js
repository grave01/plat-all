// pages/video/video.js
var util = require('../../utils/util.js');
var page =1
var pageSize = 10;
Page({

  /**
   * 页面的初始数据
   */
  data: {
     url:"https://loan.bangnila.com/File/Video/2020-05-13/2e0c97ecf09a4f249cce38b4f4c507c9.mp4",
    name:"视频名称...",
  listCategory:[],
  videoList:[],
  categoryId:"",
  hiddenLoadMore:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    console.log("获取");
    console.log(options)//开始处理相关请求
    var categoryId = options.id;
    wx.getSystemInfoSync();
  
    that.setData({
      categoryId:options.id
    })
    that.init()
  },
  init(){
    var that = this;
    page = 1;
    that.getCategory();
    that.getVideList(page);
  },
  async getCategory() {
    var that = this;
    await util.request("platCategoryAction.getCategory", {}).then(function (res) {
      console.log(res);
      that.category = res.body.list;
      that.setData({
        listCategory: res.body.list
      })
    })
  },
  async getVideList(page,categoryId) {
    var that = this
    // "is_recommend":"1"
    console.log("获取的id"+that.data.categoryId)
    var param = {
      "keyword": "",
      "page": page,
      "is_free": "1",
      "create_time": "1",
      "category": that.data.categoryId,
      "pageSize":pageSize
    }
    await util.request("platVideoAction.getVideoList", param).then(function (res) {
      console.log(res)
      that.vList = res.body.list;
      that.setData({
        videoList: res.body.list,
        hiddenLoadMore:true
      })
    })
  },
  goToPlay:function(e){
    console.log(e.currentTarget.dataset.id)
    wx.navigateTo({
      url: '/pages/video/video?id='+e.currentTarget.dataset.id,
    })
 },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this
    // "is_recommend":"1"
    console.log("获取的id"+that.data.categoryId)
    page = page+1;
    var param = {
      "keyword": "",
      "page": page,
      "is_free": "1",
      "create_time": "1",
      "category": that.data.categoryId
    }
    that.setData({
      hiddenLoadMore: false
    })
     util.request("platVideoAction.getVideoList", param).then(function (res) {
      console.log(res)
      var list = res.body.list;
     if(list.length>0){
       for(var i=0;i<list.length;i++){
         that.data.videoList.push(list[i])
       }
       that.setData({
         videoList:that.data.videoList,
         hiddenLoadMore: true
       })
     }else{
      page = page-1;
      setTimeout(() => {
        that.setData({
          hiddenLoadMore: true
        })
      }, 1000)
     }
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  handlerGohomeClick() {
   wx.navigateBack()
  }
})