const app = getApp();
var util = require('../../utils/util.js');
Page({
	data: {
		videos: [],
		videoIndex: 0,
		duration: 500
	},
	onLoad: function() {
		var that = this
		// const videos = this.genVideo(15);
		// setTimeout(() => {
		// 	this.setData({
		// 		videos
		// 	});
		// }, 1000);
		that.init()
	},
	init(){
		var that = this
		that.getVshow()
	},
	doubleTap(e){
		console.log("双击")
		console.log(e)
	},
	singleTap(e){
		console.log("单击暂停或者播放")
		console.log(e)
	},
	onChange(e) {
		console.log('change', e.detail.video);
	},
	onPlay(e) {
		console.log('play', e.detail.video);
	},
	onPanelForward(e) {
		console.log('panel event: forward', e.detail.video);
		wx.showToast({
			title: 'panel事件：' + e.detail.video.title,
			icon: 'none'
		});
	},
	genVideo(count) {
		const length = this.data.videos.length;
		const videos = [];
		for (let i = 0; i < count; i++) {
			let src = '';
			let poster = '';
			if (i % 2 === 0) {
				src = 'http://dev.video.zyzygame.com/short/1584504126348.mp4';
				poster = 'http://dev.img.zyzygame.com/cover/1584504126348.jpg';
			} else {
				src = 'http://dev.video.zyzygame.com/short/1589429023012.mp4';
				poster = 'http://dev.img.zyzygame.com/cover/1589429023012.jpg';
			}
			videos.push({
				title: 'title' + (length + i),
				src,
				poster
			});
		}
		return videos;
	},
	play:function(e){
		console.log("assaas")
		console.log(e)
	},
	getVshow:function(){
		var that = this;
		var param = {
			page:1,
			pageSize:10
		}
		util.request("platShortVideoAction.getShortVideo",param).then(function(res){
			 console.log(res)
			 that.setData({
				 videos:res.body.list
			 })
		})
	}
});
