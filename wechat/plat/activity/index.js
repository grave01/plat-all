//index.js
//获取应用实例
const app = getApp()
const Pager = require('../common/extpager')
const lib = Pager.lib
var util = require('../../utils/util.js')
var page = 1;
var pageSize = 10;
Pager({
  data: {
    targetConfig: {
      $$id: 'menus-appstore-view',
      listClass: 'demo-list',
      itemClass: 'card demo-list-item',
      // 将listView，转换成scroll-view
      type: {
        'is': 'scroll',
        'scroll-y': true,
        'enable-flex': true,
      },
      data: [],
      methods: {
        onTap(e, param, inst){
          let id = param.id
          var pa ={
            id:id
          }
            util.request("platWechatActivityAction.getActivity",pa).then(function(res){
              console.log(res)
              var ob = res.body.object;
              if(ob.isH5==1){
                console.log("跳转h5页面"+ob.url)
                wx.navigateTo({
                  url: '/plat/webview/webview?url='+ob.url,
                })
              }else{
                console.log("跳转小程序页面："+ob.tap)
               
              }
            })
        }
      }
    },
    hiddenLoadMore:true,
    title:"活动中心"
  },
  onLoad(param) {
    var that = this
    let pageTitle = param.pageTitle
    if (pageTitle) {
      wx.setNavigationBarTitle({
        title: pageTitle
      })
    }
    that.getActivityList()
  },
  getActivityList:function(){
    var that = this;
    var param = {
      page:page,
      pageSize:pageSize
    }
     util.request("platWechatActivityAction.getActivityList",param).then(function(res){
      console.log(res)
      var arr = new Array();
      var list = res.body.list;
      for(var i = 0;i<list.length;i++){
        var  a= {
          "@item": {
               id: "nav-3",
               title: [],
               img: {src: list[i].img, itemStyle: 'width: 100%;', preview: false},
               itemClass: 'appstore-item cover' 
             },
            tap: 'onTap?id='+list[i].id
         }
         arr.push(a);
      }

      that.setData({
          'targetConfig.data':arr
      })
    })
  },
  onReachBottom: function () {
    var that = this;
    that.setData({
      hiddenLoadMore: false
    })
  },
  handlerGobackClick:function(){
    wx.navigateBack({
      complete: (res) => {},
    })
  }
})
