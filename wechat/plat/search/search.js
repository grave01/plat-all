//index.js
//获取应用实例
const app = getApp()
const Pager = require('../common/extpager')
const lib = Pager.lib
var util = require('../../utils/util.js')
var page = 1;
var pageSize = 10;
Pager({
  data: {
    hiddenLoadMore:true,
    title:"搜索",
    value:""
  },
  onLoad(param) {
    
  },
  search:function(){
    var that = this
    // "is_recommend":"1"
    var param = {
      "keyword": that.data.value,
      "page": page,
      "pageSize":pageSize
    }
   util.request("platVideoAction.search", param).then(function (res) {
      console.log(res)
      that.vList = res.body.list;
      that.setData({
        videoList: res.body.list
      })
    })
  },
  // onReachBottom: function () {
  //   var that = this;
  //   page = page+1;
  //   that.search()
  //   that.setData({
  //     hiddenLoadMore: false
  //   })
  // },
  handlerGobackClick:function(){
    wx.navigateBack({
      complete: (res) => {},
    })
  },
  goToPlay:function(e){
    console.log(e.currentTarget.dataset.id)
    wx.navigateTo({
      url: '/pages/video/video?id='+e.currentTarget.dataset.id,
    })
 },
  onChange(e) {
    console.log('onChange', e)
    this.setData({
        value: e.detail.value,
    })
},
onFocus(e) {
    console.log('onFocus', e)
},
onBlur(e) {
  var that = this;
    console.log('onBlur', e);
    that.search()
},
onConfirm(e) {
    console.log('onConfirm', e)
},
onClear(e) {
    console.log('onClear', e)
    this.setData({
        value: '',
    })
},
onCancel(e) {
    console.log('onCancel', e)
},
})
